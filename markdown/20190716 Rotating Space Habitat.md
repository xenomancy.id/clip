# Rotating Space Habitat
Date: 20190716

Tangential stress (```sigma_t```, circumferential stress) is the forces acting to the surface of a tube caused by the internal pressure ```P``` and the total tension in the wall (```T```).
If we define ```D``` to be the diameter of the cylinder, and ```t``` be thickness of the wall, and ```r``` is the radius of the cylinder.

    sigma_t = (P*D)/(2*t) = 

Centrifugal force acceleration is:

    F = ( m * v^2 ) / r

If we know the angular velocity (```omega```),

    v = omega * 2 * pi * r

Centripetal Acceleration is expressed as

```
a_c = v^2 / r
    = omega^2 * r
    = (2 * pi * n_rps)^2 * r
    = (2 * pi * n_rpm / 60)^2 * r
    = (pi * n_rpm / 60 )^2 * r
```
Where:

```
a_c   = centripetal acceleration in m/s^2
v     = tangential velocity in m/s
r     = circular radius in m
omega = angular velocity in rad/s
n_rps = revolutions per second
n_rpm = revolutions per minute
```

> source: The Engineering Toolbox, Centripetal and Centrifugal Force-Acceleration.

Centripetal force:

```
F_c = m*a
    = m*v^2/r
    = m * omega^2 * r
    = m * (2*pi* n_rps)^2 * r
    = m * (2*pi* n_rpm/60)^2 * r
    = m * (pi* n_rpm/30)^2 * r
```

Now, *Gamma* has a tube wall of ```8km```, radius of ```4977km```.
Greatest pressure is at ```1e8 Pa```.
Assume CNT at ```1e11 Pa```,

```
sigma_t = (1e11 Pa * 4.977e6) / 8e3
        = 1e11 * 4.977/8 Pa
        = 6.22e10 Pa
```

So,

```
sigma_t = (p*r)/t
        = (F*r)/(A*t)
        = (m*a*r)/(A*t)
```

No, it is ```sigma_t``` that we seek, ```p``` is material strength.

Actually, ```sigma_t``` must not exceeds ```sigma``` of the material, so ```sigma_t <= sigma```, while ```P``` as the internal pressure, is the pressure of which our material must endure from the inside, expressed in Pascals.

As ```sigma_t = (p*r)/t```, we want to know thickness of the material, so ```t=(P*r)/sigma_t```, and we can set ```sigma_t = sigma```, which, for CNT, is in the order of 1e11 Pa, while for our calculation is 1e8 Pa, so we get ```t=(4.98e8*1e6)/1e11 = 4.98e3 m```, ~5 km thick.

Wait, ```P = F/A```, and since we were talking about area density, ```P*A=F```, and ```F=m*a```, it is all about the might of the landscaping, and the weight of the tube, which is dependent to the thickness multiplied  by support density and multiplied by acceleration.

In a way:

```
sigma_t = ( ( P_landscaping + P_support) * r ) / t
```

But then ```P_support``` is:

```
P_support = t * rho_support * a

P_landscaping = h * rho_landscaping * a
```

Then

```
Sigma_P = ( t * rho_support * a ) + ( h * rho_landscaping * a )
        = a * ( t * rho_support + h * rho_landscaping )
```

Now, ```L = h * rho_landscaping``` so,

```Sigma_P = ( t * rho_support + L ) * a```

Therefore,

```
sigma_t = (( t * rho_support + L ) * a * r ) / t
        = a * r * ( rho_support + L * t^-1 )
```

Since ```L``` is essentially the areal mass of the landscaping, we can redefine it as ```L = rho_areal_landscaping = rho_A_landscaping```

Then,

```
sigma_t = a * t * ( rho_support + rho_A_landscaping * t^-1 )
```

We want ```t``` to be out, but how?
Insert ```a*r``` in:

```
sigma_t = rho_support * a * r + rho_A_landscaping * t^-1 * a * r
rho_A_landscaping * t^-1 * a * r = sigma_t - rho_support a * r
t = ( rho_A_landscaping * a * r ) / (sigma_t - rho_support * a * r )
```

Say, let's test it.

```
rho_A_landscape = 1.4e4m * 750 kgm^-3 = 1.05e7 kgm^-2
```

That is the maximum areal density of the habitat surface.
Then consider ```a = 6 ms^-2```, ```r = 4.98e6 m```, ```sigma_t = 1e11 Pa```, ```rho_support = 1300 kgm^-3```

Then, ```t = 5137m```

This is a tube with radius of ```4.98e6m```, and surface thickness of ```5137m```.
Its material density is ```1300kgm^-3```, so areal density of ```6.68e6kgm^-2```, which with an outward acceleration of ```6ms^-2```, is equal to ```4.01e7Nm^-2```.
On the inner surface is landscaping with maximum areal density of ```1.05e7kgm^-2```, or ```6.3e7Nm^-2```.

The combined pressure is ```1.03e8Nm^-2```, then let's find its tangential stress:

```
sigma_t = (1.03e8Pa * 4.98e6m) / 5137m
        = 1.00e11Pa
```

So that (```5137m```) is the minimum thickness.
Let's multiply by 1.2 and we get ```6164m```, and so the structure self-pressure is ```6164m * 1300kgm^-3 * 6 ms^-2 = 4.81e7Nm^-2```.
Then combined pressure is ```1.11e8Pa```, and tangential stress is:

```
sigma_t = (1.11e8Pa * 4.98e6m) / 6164m
        = 9e10Pa
```

I think it is safe to say that a perturbation of 1e10Pa is fatal.
Say, ```t = 10km```, then ```rho_structure = 1e4m * 1300kgm^-3 * 6ms^-2 = 7.8e7Pa```, and ```Sigma_P = 1.4e8Pa```, ```sigma_t = 7.02e10Pa```, I think this is far safer, as at least ```2.98e10Pa``` perturbation is required to break the structure.

Since we mentioned centripetal acceleration to be ```6ms^-2```, we must also calculate that:

```
a_c = v^2 / r = omega^2 * r
```

And ```omega``` is defined as ```omega = v / r``` in ```s^-1```.

As ```v = 2*pi*r / T```, ```omega = 2*pi / T```, ```T = 2*pi*r / v = 2*pi / omega```

Suppose at ```a_c = 6ms^-2```, ```omega = ( a_c / r )^0.5 = 1.1e-3s^-1```, then ```T = 2*pi / omega = 5724.25s; 1h35m24.25s```

We get ```T = 5722.53s; 1h35m22.53s``` for ```r = 4.977e6m```.