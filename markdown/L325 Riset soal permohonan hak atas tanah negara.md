# Riset Soal Permohonan Hak Atas Tanah Negara
<style>body {text-align: justify}</style>

<!-- I found a way to make comments! -->

## Kasus:
LJ punya tanah bersertipikat SHM, luas ~2000M^2 (dua ribu meter persegi).
Tanah tersebut berasal dari tanah negara,
diperoleh dari peralihan tanah garapan,
yang kemudian dimohonkan hak.

## Telusuran Peraturan
Menurut Pasal 73 Peraturan Kepala Badan Pertanahan Nasional Nomor 3 Tahun 1997 tentang Ketentuan Pelaksanaan Peraturan Pemerintah Nomor 24 Tahun 1997 tentang Pendaftaran Tanah (Perkaban 3/1997),
kegiatan pendaftaran tanah secara sporadik dilakukan atas permohonan yang bersangkutan dengan surat sesuai bentuk yang telah ditentukan ([*lampiran 13*](https://peraturan.bkpm.go.id/jdih/userfiles/batang/BPN_9_1999.pdf)).
Permohonan tersebut meliputi permohonan untuk mendaftar hak baru.
Berdasarkan penjelasan Pasal 18 ayat (2) PP 24/1997, yang dimaksud hak baru adalah hak atas tanah yang diberikan atas tanah Negara.
Permohonan mendaftar hak baru didasarkan alat bukti sebagaimana dimaksud dalam Pasal 23 PP 24/1997,
yang menentukan bahwa hak atas tanah baru dibuktikan dengan
penetapan pemberian hak dari Pejabat yang berwenang memberikan hak yang bersangkutan menurut ketentuan yang berlaku
apabila pemberian hak tersebut berasal dari tanah Negara atau tanah hak pengelolaan.

Siapa pejabat yang berwenang?
Berdasarkan Pasal 3 huruf a Perkaban 2/2014 tentang Pelimpahan Kewenangan Pemberian Hak Atas Tanah dan Kegiatan Pendaftaran Tanah, Kepala Kantor Pertanahan memberi keputusan mengenai pemberian Hak Milik untuk orang perseorangan atas tanah pertanian yang luasnya tidak lebih dari 50.000 M^2 (lima puluh ribu meter persegi).

Apa syarat pemberian hak oleh Kepala Kantor Pertanahan?
Untuk itu, dapat dilihat dari Perkaban 9/1999 tentang Tata Cara Pemberian dan Pembatalan Hak Atas Tanah Negara dan Hak Pengelolaan.
Berdasarkan Pasal 9 Perkaban 9/1999, permohonan HM atas TN diajukan secara tertulis.
Permohonan tersebut memuat keterangan mengenai pemohon, yang untuk pemohon perorangan:

- nama
- umur
- kewarganegaraan
- tempat tingga dan pekerjaan
- keterangan mengenai isteri/suami dan anaknya yang masih menjadi tanggungannya.

Selain keterangan mengenai pemohon, dilampirkan juga keterangan mengenai tanahnya, yang meliputi data yuridis dan data fisik tanah:

- Dasar penguasaan atau alas haknya yang dapat berupa:
  - sertipikat
  - girik
  - surat kapling
  - surat-surat bukti pelepasan hak dan pelunasan tanah dan rumah dan atau tanah yang telah dibeli dari pemerintah,
  - putusan pengadilan,
  - akta PPAT,
  - akta pelepasan hak, dan
  - surat-surat bukti perolehan tanah lainnya.
- Letak, batas-batas dan luasnya (jika ada Surat Ukur atau Gambar Situasi sebutkan tanggal dan nomornya).
- Jenis tanah (pertanian/non pertanian).
- Rencana penggunaan tanah.
- Status tanahnya (tanah hak atau tanah negara).

Beserta lain-lain:
- keterangan mengenai jumlah bidang, luas dan status tanah-tanah yang dimiliki oleh pemohon, termasuk bidang tanah yang dimohon;
- keterangan lain yang dianggap perlu.

Berdasarkan Pasal 10 Perkaban 9/1999, permohonan hak milik dilampiri dengan:

1. Mengenai pemohon:
   <ol type="a">
      <li>Jika perorangan: foto copy surat bukti identitas, surat bukti kewarganegaraan Republik Indonesia;</li>
      <li>Jika badan hukum: foto copy akta atau peraturan pendiriannya dan salinan surat keputusan penunjukannya sesuai dengan ketentuan peraturan perundang-undangan yang berlaku.</li>
   </ol>
1. Mengenai tanahnya:
   <ol type="a">
      <li>Data yuridis: sertipikat, girik, surat kapling, surat-surat bukti pelepasan hak dan pelunasan tanah dan rumah dan atau tanah yang telah dibeli dari Pemerintah, PPAT, akta pelepasan hak, putusan pengadilan, dan **surat-surat bukti perolehan tanah lainnya;**</li>
      <li>Data fisik: **surat ukur,** **gambar situasi** dan IMB, apabila ada;</li>
      <li>**Surat lain yang dianggap perlu.**</li>
   </ol>
1. Surat pernyataan pemohon mengenai jumlah bidang, luas dan status tanah-tanah yang telah dimiliki oleh pemohon termasuk bidang tanah yang dimohon, sesuai contoh Lampiran 3.

## Daftar Dokumen Yang Harus Dicari
Jadi, yang harus dicari dari instansi:

- tanda terima berkas permohonan
- Penetapan pemberian hak dari Kepala Kantor Pertanahan

Dari pemohon:
- surat-surat bukti perolehan tanah lainnya
- surat ukur, gambar situasi
- surat-surat lainnya.