# Superconducting Loop Battery

## 20190629

**Fig.0:**
> A torus diagram.

```
b = A / (4*pi^2*a)
a = A / (4*pi^2*b)
```

The design is a torus shell, so the inner volume is described as ```V = (pi*a^2)(2*pi*a)```, where minor radius is ```a```, and major radius is ```b```.
Its surface area is described as ```A = (2*pi*b)(2*pi*a)```.

Pressure in a fluid might be considered to be a measure of energy per unit volume or energy density:

```
P = F/A = F*d / A*d = W/V
```
> source: http://hyperphysics.phy-astr.gsu.edu/hbase/press.html

The magnetic pressure ```P``` is given in SI Units (```P``` in ```Pa```, ```B``` in ```T```, ```mu_o``` in ```H/m```) by:

```
P_B = B^2/2*mu_o
```

In practical units:

```
P_B[Bar] = (B[Bar]/0.501)^2
```
> source: Wikipedia, Magnetic Pressure

Note that ```1 bar = 100kPa```

Solving for ```P_B``` in ```bar```, we got:

```
P_B = (B/0.501)^2 * 1e5 Pa
    = B^2*0.501^-2*1e5
    = B^2*398405
```

So, ```B = (P_B/398405)^0.5```

## 20190702
Longitudinal stress (```sigma_L```) and tangential stress (```sigma_t```, circumferential stress).

**Fig. 1:**
> Schematics for longitudinal and tangential stresses.

> source: https://www.mathalino.com/reviewer/mechanics-and-strength-of-materials/thin-walled-pressure-vessels

The forces acting are the total pressures caused by the internal pressure ```P``` and the total tension in the wall ```T```,

```
sigma_t = (P*D)/(2*t)
        = (P*2*r)/(2*t)
        = (P*r)/t
```

With ```P``` be the differential of internal and external pressure ```Sigma_P = P_i - P_o```, where ```P_i``` is internal pressure, and ```P_`o``` is external pressure.
Meanwhile, longitudinal stress is the total force acting at the rear of the tank.
```F``` must equal to the total longitudinal stress on the wall (```P_T= sigma_L*A_wall```).

**Fig.2:**
> source: Ibid, longitudinal stress schematic.

Area of the wall is close to ```pi*D*t```, as ```t``` will be thin, compared to ```D```.

```
sigma_L = (P*D)/(4*t)
        = (P*2*r)/(4*t)
        = (P*r)/(2*t)
```

Also: ```sigma_t = 2*sigma_L```

I think it means we only need to calculate ```sigma_t```, as if the material can handle ```sigma_t```, it will definitely be able to handle ```sigma_L```.

Spherical tank behaves as ```sigma_t = (P*D)/(4*t) = (P*r)/(2*t)```

So as ```sigma_t = (P*r)/t```, given only ```P``` is known, ```P = (sigma_t*t)/r```

If, we want to know, at which point would the mechanism break, if ```sigma_t``` is limited to ```1e11Pa```, thickness is ```1m``` and radius is ```10m```, we get ```P = 1e10Pa```.
So ```1e10Pa``` is the limit, and therefore its energy density is ```1e10Jm^-3```, or ```158T```.

If we scale it, ```P = (1e11Pa*0.2)/0.5 = 4e10Pa```, it is ```317T```.
Then, ```P = (1e11Pa*0.1m)/1m = 1e10Pa, 158T```.
Apparently I forgot that what's matter is the ratio of the radius and its wall thickness.

Suppose a circle area, ```A=pi*r^2```, we must to have a circle with a hole ```1/3``` of its area, so:

```
   A_1 = 3*A_2
pi*R^2 = 3*(pi*r^2)
 R^2/3 = r^2
     r = (R^2/3)^0.5
     r = R*1/3^0.5
     r = R/3^0.5
```

Suppose we want a storage with ```R=1m```, ```r=3^-0.5 m```, then ```t``` is defined as:

```
R - r = 1-3^-0.5
      = 3^0.5/3^0.5 - 1/3^0.5
      = (3^0.5-1)/3^0.5

t= 1-1/3^0.5m = 0.42m
r= 3^-0.5m = 0.58m

P = (1e11Pa*(1-1/3^0.5)/3^-0.5
  = 7.32e10Pa; ~430T; 7.32e10Jm^-3
```

So, perhaps our superconductors were rated for ```~400T```, with 1/3 of its entire structural volume is to store energy.

**Fig. 3** showed us a design where each superconducting loop cell is a torus with exterior size is defined by its major and its minor radii being equal.

The inner side, which is the vacuum that is where the magnetic field will be pushing against, is defined to have minor radius of ```c```, and major radius equal to ```b```.
```c``` is defined as ```c=a/3^0.5

So internal volume is ```V_in = (pi*c^2)(2*pi*b)```.
Total material volume is ```Sigma_V_s = V_out - V_in```.
While ```V_out=(pi*a^2)(2*pi*b)```.
However, ```a=b```, so:

```
Sigma_V_s = (pi*a^2)(2*pi*a) - (pi*c^2)*(2*pi*a)
          = 2*pi^2*a (a^2-c^2) <----------f(V_s)
          = (4/3)*pi^2*a^3
```
> This equation has been tested and provides equal results compared to the original equation, for a condition where the major radius and the minor radius are equal, and the inner hollow space occupy a third of the entire torus volume.

This constitutes the entire material used for this SCL battery cell.
The rest is vacuum.
So let us see how well do they perform.
From the previous equations, we can get its total support/backend volume, therefore it is only a matter of multiplying it with its density to get its mass, considering that the superconducting film has a negligible mass compared to the support.

First, let's consider its dimension.
The shape is a torus, so it has a major radius ```b``` and a minor radius ```a```.
On the previous sections we discussed the case where ```a=b```.
But they could be different.

Then its inner minor radius, let's call it ```c```, is the radius of vacuum that will store the magnetic fields.
The next thing to consider is its back end maximum strength, in pascals.
Then its back end material density.

After knowing all of those, we first need to discover its external volume.
With that, we subtract the vacuum volume to get total material volume.
Multiplying that material volume with the material density we obtain the back end mass.

We can get the empty volume per external volume to determine its empty space ratio.
Thickness can be obtained by subtracting minor radius of the exterior and minor radius of the cavity.

Its maximum pressure before breaking is calculated with ```P = (sigma*t)/c``` where ```sigma``` is stress, ```t``` is thickness, ```c``` is inner cavity radius.
It also doubles as energy density, as they're dimensionally equivalent.
Magnetic field strength can be obtained by ```B = (P/398405)```, note that ```398405``` is ```(1e5Pa/0.501^2)```.

Averaged torus density can be obtained with ```rho_avg_t = m / V_ext```, where ```m``` is the back end mass, and ```V_ext``` is the external volume.

Maximum energy capacity can be obtained by: ```E_max = rho_energy*V_int```, where ```rho_energy = P```, and ```V_int``` is the volume of the internal cavity.

Specific energy can be obtained with: ```S_E=E_max/m```.

Disregarding all of other parameters but the ratio of internal cavity vs external torus cavity, I got that they'd provide similar ```S_E``` and ```rho_E```, which will make its computations easier.
> However, to date (20200908), I have not formulate an equation that requires only the ratio to calculate both ```S_E``` and ```rho_E```.

At the following ratios, a carbon-nanotube back end with ```rho=2000kgm^-3``` and ```sigma=1e11Pa```, will yields:

| Ratio | Avg.rho_E | S_E | B | c |
|---:|---:|---:|---:|---:|
| ~0.0 | ~100MJ/m^3 | 50kJ/kg^1 | 15,835 T | ~0.0a |
| 0.06 | 18.8GJ/m^3 | 10MJ/kg^1 | 868 T | 0.25a |
| 0.25 | **25.0GJ/m^3** | 16.7MJ/kg^1 | 501 T | **0.50a** |
| 0.56 | 18.8GJ/m^3 | 21.4MJ/kg^1 | 289 T | 0.75a |
| ~1.0 | ~100MJ/m^3 | **25MJ/kg^1** | 16 T | **~1.0a** |
> ratio here is the ratio of its inner cavity volume per its external volume.
> ```c``` is in the fractions of ```a```.

As seen, the thinner the wall is (the ratio of cavity volume and external volume approaches 1), the highest its ```S_E```.
Meanwhile, averaged energy density (energy per external volume) peaked when the ratio is at ```0.25```, or when the wall thickness ```t``` or the cavity minor radius ```c``` is half as thick as the external minor radius ```a```.

## 20190802

**Fig.4:**
> A torus from above, the minor and major radii are demonstrated.

Since they will be packed in cells for a more convenient and smaller cells, we have to stack them together.
I think we should treat them like a pack of long tubes, each torus stacked above another along the axis perpendicular to their major radii (fig.2), so each cell is as tall as its minor radius if ```r``` and ```R``` is equal and the exterior radius is ```2*R```.

**Fig.5:**
> A schematic demonstrating minor radius ```r``` and major radius ```R```

Let the major radius be ```b```, and the minor radius be ```a```, and that ```a=b=x```.
Then, the approximate cylinder has ```r=2*x```, and ```h=2*x```.
```h=2*x``` as ```h``` is equal to the diameter of the minor radius, and ```D=2*r```.

Therefore, the volume of the cylinder is ```V=pi*r^2*h```.
Continuing with the previous definition of ```r``` and ```h```, we get ```V=8*pi*x^3``` per cell.
But they won't be stacked in a very long cylinder.
And the best way to store a bunch of circles in a plane is in a hexagonal grid.
Then, we can know that each torus occupy the space within, no more than the smallest width of the the hexagon (see fig.6), as each sides are the base of an equilateral triangle.
See fig.7 for a clearer representation.

**Fig.6:**
> A hexagon filled with the outline of a torus from inside, with ```a=b```.

**Fig.7:**
> A hexagon is divided into six equilateral triangles.
> Its features such as the angles, and the ratios between its sides and an individual triangle height is highlighted as well.

If a hexagon is split into six equilateral triangles, the largest width of that hexagonal tile would be the sides of its component equilateral triangles times two.
The smallest width of that hexagonal tile must be calculated by knowing the height of each component equilateral triangle.

Suppose, we took a right angle triangle out of one equilateral triangle, with each of its side is 1 unit length.
That right angle triangle would have the base of 0.5 unit length, its hypotenuse is 1 unit length, and its height will be ```h```.

We can just use the Pythagorean theorem where ```a^2+b^2=c^2```, with ```a``` and ```c``` are known, so ```b^2=c^2-a^2```, then we get ```b=(3/4)^0.5=3^0.5/2```.
So ```b``` is the ```h``` we're looking for, and the smallest width of a hexagon is ```2*h```.

Then ```2*h=2*r```, ```h=r=2*x```, and if the sides of a hexagon has a length of ```s```, we get that ```h=r=(3^0.5/2)s=2*x```.
So,

```
(3^0.5/2)s = 2x
         s = 2*2*x*3^-0.5
           = 4*x*3^-0.5

h = (3^0.5/2)*s
s = 2*h*3^-0.5 <-------------------Eq.A
```

**Fig.8:**
> A right angle triangle ```hbs``` with the angle at the ```bs``` corner is ```60 degrees```. ```s``` is the hypotenuse, ```h``` is the height, and ```b``` is the base.

> **Note 20190927**
> Given that the base is ```b```, the side ```s```, the height is ```h```, ```s``` can be discovered by: ```s=(h^2+b^2)^0.5```.
> Now, ```b=0.5s```, but ```s``` is unknown.
> Turning the triangle around, we can discover ```b``` through trigonometry: ```b = h*sin(30 degrees)```

> Since ```sin(30 degrees) = 0.5```, ```s=h```, wait, what?

> Actually, if we consider ```s``` as a vector ```60 degrees``` from the x axis, where ```b``` is perpendicular to, hence ```h``` is perpendicular to the y axis.
> Therefore, knowing y component of the vector is sufficient to find the sum of the vector by: ```h=s*sin(60deg)```, so ```s=h/sin(60deg).

> Since ```sin(60deg)=0.5*3^0.5```, then ```s = 2*h*3^-0.5```, *equivalent to the Eq.A.*
> Apparently my previous dirty solution somehow works just fine.

**Fig.9:**
> A picture of a hexagonal prism, which encapsulated an entire torus.
> There is a caption: ```s = a+b```

So for our torus, ```a=b=x```, which, accordingly, has a height of ```2*x```, which is he diameter of the minor radius.
So the height is also the radius of the perimeter of the torus, say ```R=2x```.
It happens to be the smallest radius of the hexagon, denoted as ```h```, so ```R=h=2x```.

The hexagon, has its smallest radius as ```h```, and its largest radius is ```s```.
Then by the Pythagorean law, its largest radius, which is also the sides of the hexagon, has this following relationship with its smallest radius: ```h=(3^0.5/2)s```, therefore ```s=2*h*3^-0.5 = 4*x*3^-0.5```

It happens that the hexagon has the thickness that is equivalent to the diameter of the minor radius, and therefore equal to ```h```.

So the area of the hexagon ```A```, to its side ```s```, has the following relationship:

```
A = ((3*3^0.5)/2)*s^2
```

Volume is then ```V=h*A```.
Insert our previous definitions, we obtain:
