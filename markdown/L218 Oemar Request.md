# Hak Milik Atas Tanah Sebagai Harta Bersama Perkawinan Internasional Berbeda Kewarganegaraan

## Pendahuluan
Terdapat dua jenis perkawinan yang dilakukan antara seorang warga negara Indonesia (WNI) dan seorang warga negara asing (WNA) yang dikenal dalam sistem hukum Indonesia.
Jenis pertama adalah perkawinan antara seorang WNI dan seorang WNA yang dilangsungkan di dalam wilayah Negara Republik Indonesia, sebagaimana didefinisikan sebagai Perkawinan Campuran pada Pasal 57 Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan sebagaimana telah diubah dengan Undang-Undang Nomor 16 Tahun 2016 tentang Perubahan Atas Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan (selanjutnya masing-masing disingkat sebagai UU 1/1974 dan UU 16/2016, dan secara kolektif disingkat sebagai UU Perkawinan).
Jenis kedua adalah perkawinan antara seorang WNI dan seorang WNA yang dilangsungkan di luar wilayah Negara Republik Indonesia, sebagaimana didefinisikan pada Pasal 56 ayat (1) UU Perkawinan.
Pasal 56 ayat (1) UU Perkawinan juga mengatur mengenai perkawinan antara dua orang WNI yang dilangsungkan di luar wilayah Negara Republik Indonesia.

Isnaeni (Isnaeni, 2016:268-270) mengklasifikasikan perkawinan yang diatur pada Pasal 56 ayat (1) UU Perkawinan dan 57 UU Perkawinan sebagai Perkawinan Internasional, karena keduanya memiliki unsur asing.
Unsur asing pada Pasal 56 ayat (1) UU Perkawinan dapat berasal dari *locus* atau tempat dilangsungkannya perkawinan saja, baik antara dua orang WNI atau antara seorang WNI dan seorang WNA, maupun berasal dari *locus* perkawinan dan kewarganegaraan salah satu pihak dalam perkawinan.
Pada Pasal 57 UU Perkawinan, unsur asing perkawinan tersebut hanya terdapat pada kewarganegaraan salah satu pihak dalam perkawinan.
Sebagaimana telah diuraikan di atas, untuk alasan konsistensi, dalam tulisan ini perkawinan yang diadakan antara seorang WNI dan seorang WNA akan disebut sebagai Perkawinan Internasional Berbeda Kewarganegaraan.

Adanya perkawinan menurut UU Perkawinan mengakibatkan lahirnya harta bersama.
Harta bersama didefinisikan pada Pasal 35 ayat (1) UU Perkawinan sebagai harta benda yang diperoleh selama berlangsungnya perkawinan.
Ketentuan tersebut berlaku pula pada Perkawinan Internasional Berbeda Kewarganegaraan.
Untuk melakukan perbuatan hukum atas harta bersama, berdasarkan Pasal 36 ayat (1) UU Perkawinan, harus dilakukan dengan persetujuan bersama antara suami dan isteri dalam perkawinan tersebut.

Salah satu bentuk harta benda yang dapat dimasukkan dalam harta bersama adalah Hak Milik Atas Tanah.
Hak Milik Atas Tanah diatur pada Pasal 20 Undang-Undang Nomor 5 Tahun 1960 tentang Peraturan Dasar Pokok-Pokok
Agraria (selanjutnya disingkat sebagai UUPA).
Pada Perkawinan Internasional Beda Kewarganegaraan, harta bersama mereka mengandung unsur asing karena kewarganegaraan salah satu pihak dalam perkawinan. (Laurensius Arliman S, 2017:180)
Hak Milik Atas Tanah yang menjadi bagian dari harta bersama Perkawinan Internasional Beda Kewarganegaraan bertabrakan dengan ketentuan Pasal 26 ayat (2) UUPA.
Pasal 26 ayat (2) UUPA mengatur bahwa Hak Milik Atas Tanah hapus demi hukum apabila pengalihannya dilakukan dengan cara-cara yang ditujukan untuk memindahkan hak tersebut kepada seorang WNA. 
Ketentuan tersebut tidak mengatur tentang akibat pengalihan tersebut atas pihak WNI dalam Perkawinan Internasional Beda Kewarganegaraan.

Terdapat satu kasus di mana seorang WNI yang melangsungkan perkawinan dengan seorang WNA, memutuskan untuk membeli suatu satuan rumah susun.
Setelah ia melunasi pembayaran atas satuan rumah susun tersebut, penyerahannya tidak pernah dilakukan oleh pihak pengembang.
Belakangan pihak pengembang memutuskan untuk membatalkan perjanjian jual beli antara pengembang dan WNI tersebut, dengan alasan si WNI tersebut bersuami seorang WNA.
Dasar hukum yang digunakan oleh pengembang tersebut untuk membatalkan perjanjian jual beli tersebut adalah Pasal 35 ayat (1) UU Perkawinan, dan Pasal 36 ayat (1) UUPA.

Atas kasus tersebut diajukan gugatan oleh pihak WNI dalam gugatan dengan nomor register 04/CONS/2014/PN.JKT.TIM.
Majelis Hakim dalam perkara dalam putusannya berpendapat bahwa perjanjian antara WNI tersebut dan pihak pengembang batal demi hukum karena objek perjanjiannya berlawanan dengan Pasal 36 ayat (1) UUPA, yang mengatur bahwa hanya WNI atau badan hukum yang didirikan berdasarkan hukum dan berdomisili di Indonesia yang dapat menjadi subjek hukum Hak Guna Bangunan.

Solusi yang tersedia untuk pihak WNI tersebut adalah dengan membuat Perjanjian Perkawinan yang memisahkan harta antara dirinya dan suaminya yang seorang WNA.
Solusi tersebut tidak dapat dilakukan karena menurut Pasal 29 UU Perkawinan, perjanjian perkawinan hanya dapat dibuat sebelum atau pada saat dilangsungkannya perkawinan.
Akibatnya, pihak WNI tersebut mengajukan gugatan ke Mahkamah Konstitusi.

Putusan Mahkamah Konstitusi Nomor 69/PUU-XII/2015 tentang Uji Materi
terhadap Pasal 29 ayat (1), (3), (4) UU No. 1 Tahun 1974 tentang
Perkawinan terkait Perjanjian Perkawinan memberikan jawaban atas permasalahan pihak WNI tersebut dengan menafsirkan ulang Pasal 29 UU Perkawinan.
Pasca putusan tersebut, perjanjian perkawinan dapat dibuat juga selama berlangsungnya suatu perkawinan, tidak hanya dapat dibuat pada saat atau sebelum perkawinan dilangsungkan.

Perjanjian perkawinan menurut Pasal 29 UU Perkawinan hanya dapat dilakukan berdasarkan kesepakatan antara para pihak dalam perkawinan, yaitu antara suami dan isteri.
Kesepakatan adalah pernyataan kehendak dari suatu pihak, yang terdiri dari dua unsur: penawaran (pernyataan kehendak yang mengandung unsur-unsur esensial dari perjanjian) dan penerimaan (pernyataan kehendak yang menyanggupi syarat dan ketentuan yang terkandung dalam penawaran). (Hernoko, 2014:162-163)
Sehingga, eksistensi perjanjian perkawinan yang mengatur harta bersama suatu pasangan dalam perkawinan yang menyimpangi ketentuan Pasal 35 UU Perkawinan digantungkan pada ada tidaknya kesepakatan atas syarat dan ketentuan yang dikandung dalam suatu perjanjian perkawinan antara para pihak dalam perkawinan.

Hal tersebut menimbulkan isu hukum berupa ketidakmampuan seorang WNI yang kawin dengan seorang WNA untuk memiliki Hak Milik atas Tanah jika mereka tidak dapat mencapai kesepakatan untuk mengadakan perjanjian perkawinan yang memisahkan harta benda pihak WNI dari pihak WNA.
Bagian-bagian berikutnya akan membahas mengenai: Prinsip Nasionalitas, Harta Benda dalam Perkawinan, dan Perjanjian Perkawinan.
Pembahasan tersebut diikuti dengan pembahasan untuk menyusun suatu solusi baru atas isu hukum yang dibahas.

## Metodologi

Tulisan ini menggunakan pendekatan peraturan perundang-undangan dan pendekatan konseptual.
Pendekatan peraturan perundang-undangan dilakukan dengan mengidentifikasi ketentuan-ketentuan peraturan perundang-undangan yang relevan sebagai dasar argumentasi dan pembahasan dalam tulisan ini. (Marzuki, 2005:137)
Pendekatan konseptual dilakukan dengan menelusuri dan mengumpulkan pendapat-pendapat hukum, konsep-konsep hukum, dan doktrin-doktrin ilmu hukum dari para sarjana hukum, yang kemudian digunakan sebagai dasar untuk membangun argumentasi yang relevan dengan isu yang dibahas. (Marzuki, 2005:135-136, 178)

## Prinsip Nasionalitas
Tanah merupakan kebutuhan dasar manusia yang diperlukan untuk berbagai macam kegunaan, sehingga keberadaannya merupakan salah satu faktor yang sangat penting dalam hajat hidup setiap orang. (Hajati, et al., 2017:1)
Berdasarkan Pasal 4 ayat (1) UUPA, tanah merupakan permukaan bumi, dan menurut Pasal 33 ayat (3) Undang-Undang Dasar Negara Republik Indonesia Tahun 1945 (selanjutnya disingkat sebagai UUD 1945), bumi, air, dan segala kekayaan alam yang ada di dalamnya akan digunakan oleh negara untuk sebesar-besarnya kemakmuran rakyat.

Pada Bagian 1 dari Penjelasan Umum UUPA, diuraikan bahwa hukum agraria kolonial sebagai salah satu alat untuk membangun masyarakat yang adil dan makmur, telah gagal mencapai tujuan tersebut.
Alasan-alasan utamanya sebagaimana diuraikan dalam Bagian 1 dari Penjelasan Umum UUPA adalah sebagai berikut:

1. Beberapa bagian dari hukum agraria kolonial disusun semata-mata untuk mendukung tujuan dan kepentingan pemerintahan kolonial, dan bagian lainnya ikut dipengaruhi tujuan dan kepentingan tersebut. Akibatnya, hal tersebut bertabrakan dengan kebutuhan rakyat dan negara dalam menjalankan pembangunan semesta dalam rangka menuntaskan revolusi nasional.
1. Sebagai produk hukum dari politik pemerintahan kolonial, hukum agraria kolonial memiliki sifat dualisme, karena diberlakukannya hukum adat disamping hukum barat. Dualisme tersebut berakibat pada berbagai permasalahan antar kelompok, dan tidak bersesuaian dengan jiwa prinsip persatuan yang dianut oleh Negara Republik Indonesia modern ini.
1. Pemerintah kolonial telah gagal untuk memberikan kepastian hukum untuk kaum pribumi.

Hukum pertanahan dan regulasi yang dianut oleh pemerintahan kolonial berorientasi pada keuntungan dan kepentingan-kepentingan pemerintahan kolonial, dengan mengorbankan kepentingan-kepentingan kaum pribumi. (Santoso, 2012:21)
Sebagai negara merdeka yang tidak lagi terikat dengan pemerintahan kolonial, maka ketentuan-ketentuan hukum agraria kolonial sudah tidak relevan lagi untuk ditegakkan.
Politik hukum pertanahan hukum agraria kolonial tidak sejalan dengan Pancasila sebagai filosofi Negara Republik Indonesia.

UUPA sebagai produk hukum agraria nasional tunduk pada prinsip-prinsip yang berbeda dengan yang dianut oleh hukum agraria kolonial.
Urip Santoso (Santoso, 2012:53-56) berpendapat bahwa terdapat sebelas prinsip-prinsip yang terkandung dalam UUPA.
Beberapa diantaranya adalah:

1. Prinsip Nasionalitas (Pasal 1 UUPA). Tanah bagi masyarakat Indonesia memiliki karakteristik komunal. Semua tanah di wilayah Negara Republik Indonesia adalah tanah bersama bangsa Indonesia, sehingga merupakan hak bagi seluruh bangsa Indonesia, bukan hanya hak dari para pemiliknya. Termasuk di dalamnya adalah tanah yang terletak di daerah dan pulau-pulau, tanah tersebut tidak hanya merupakan hak dari penduduk-penduduk asli di daerah atau pulau tersebut.
1. Prinsip bahwa pada dasarnya hanya WNI yang dapat memiliki tanah. Prinsip ini ditemukan pada Pasal 9 ayat(1) UUPA, dan secara tegas diatur pada Pasal 21 ayat (1) UUPA dan Pasal 26 ayat (2) UUPA. Hanya WNI yang dapat menjadi subjek pemegang Hak Milik Atas Tanah. WNI yang selain kewarganegaraan Indonesia-nya memiliki kewarganegaraan asing tidak dapat menjadi subjek pemegang hak dari Hak Milik Atas Tanah.

Kedua prinsip tersebut menggambarkan pentingnya status kepemilikan hak atas tanah, dalam kaitannya dengan hubungan yang sakral antara bangsa Indonesia dan tanah wilayah Negara Republik Indonesia.
Prinsip ini dapat ditemukan dalam Pasal 1 ayat (3)
