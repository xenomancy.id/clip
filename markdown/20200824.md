# 20200816

## Cheat Sheets

### Kategori Tokopedia
- Dapur
  - Peralatan makan dan minum
    - Gelas dan Mug
    - Tutup gelas dan piring
    - Peralatan minum set
    - Cangkir
  - Penyimpanan Makanan
    - Sealer Makanan
    - Tempat bumbu
    - Plastic Wrap
    - Toples Makanan
  - Peralatan Masak
    - Gelas takar
    - Cetakan es, puding, coklat
    - Wajan
  - Bekal
    - Termos air
    - Botol minum
    - Kotak makan
  - Peralatan dapur
    - Rak Piring
    - Sarung Galon
    - Rak dapur
    - Dispenser air
  - Alat masak khusus
    - Coffee & Tea Maker
    - Ice Cream dan Yogurt Maker
    - Noodle dan Pasta Maker
  - Dapur lainnya
  - Peralatan baking
    - Cetakan kue
  - Aksesoris Dapur
    - Talenan
- Elektronik
  - Elektronik Dapur
    - Blender
    - Slow cooker
    - Juicer
  - Perangkat elektronik lainnya
    - Perangkat elektronik lainnya lainnya.
- Perlengkapan Pesta dan Craft
  - Bungkus dan kemasan
    - Plastik
  - Kebutuhan Pesta
    - Gelas Pesta
    - Piring Pesta
  - Hadiah
    - Hadiah Custom
    - Mug Hadiah
    - Hampers
  - Persiapan Pernikahan
    - Souvenir pernikahan
  - Lainnya
  - Balon
    - Pita Balon
- Rumah Tangga
  - Rumah Tangga lainnya
  - Tempat Penyimpanan
    - Botol
    - Tempat penyimpanan lainnya
    - Tempat obat
    - Kotak Surat
  - Dekorasi
    - Lukisan
    - Wall Sticker
  - Furniture
    - Pengaman Furniture
    - Rak
  - Kebersihan
    - Asbak
  - Ruang tamu & Keluarga
    - Gorden
- Makanan dan minuman
  - Minuman
    - Minuman Kemasan
    - Sirup
    - Energy Drink
    - Jus
  - Teh
    - Teh Kemasan
  - Kopi
    - Kopi Kemasan
  - Makanan Beku
    - Dessert
- Pertukangan
  - Pnemumatic
    - Spray dan Air Gun
  - Cat dan Perlengkapan
    - Cat Semprot
  - Alat Ukur Industri
    - Pengukur Dimensi
  - Mesin Produksi
- Ibu dan bayi
  - Perlengkapan Makan Bayi
    - Alat Makan Bayi
- Mainan dan hobi
  - Mainan dan hobi lainnya
    - Lainnya Lainnya
  - Stress relieve toys
    - Slime
  - Figure
    - Figure Set
- Olahraga
  - Gym dan Fitness
    - Gym dan Fitness Lainnya
  - Hiking dan Camping
    - Alat Masak Camping
  - Sepeda
    - Spare Part Sepeda
- Otomotif
  - Perawatan Kendaraan
    - Obat jamur Mobil
  - Aksesoris Motor
    - Aksesoris Body Motor
  - Spare Part Motor
    - Rantai dan Gir Motor
- Office & Stationery
  - Kertas
    - Kertas print dan Fotokopi
  - Alat Tulis
    - Rautan
- Kesehatan
  - Perlengkapan Medis
    - Alat Laboratorium


### Etalase Kyuuti

## Item untuk entri

### 12: Fastener Kertas Warna Set Isi 5
Ukuran Kemasan: 21 cm x 8 cm

Bahan: Plastik

H004

---

Berat: 100 gr

Harga: 1900

### 13: Mini Kwitansi Tiarna Sakti Indonesia
Ukuran: 21.5 cm x 7.5 cm

Bahan: Kertas

H004

---

Berat: 100 gr

Harga: 1490

### 14: Kwitansi Upah Non Karbon Rangkap 2
Ukuran: 21 cm x 11.5 cm

Bahan: Kertas Non Karbon

H004

---

Berat: 100 gr

Harga: 5900

### 15: Amplop / Envelope Holder 734GA-E Blue KPUE KW A4
Ukuran: 24 cm x 18 cm

Bahan: Plastik

H004

---

Berat: 100 gr

Harga: 3900

### 16: Smart Pocket 209 A5 Daiichi FileX
Ukuran: 23 cm x 18 cm

Bahan: Plastik

H004

---

Berat: 100 gr

Harga: 2900

### 17: Label Multipurpose Fluerescent no 97 (5 lbr)
2 varian warna (orange/hijau), warna yang didapat tergantung stok.

Ukuran: 25 cm x 17.5 cm

Bahan: Kertas fluerescent

H004

---

Berat: 100 gr

Harga: 4900

Stok: 2

### 18: Baju Tidur Kutang Wanita Merah Motif Beruang Dan Angka
Baju tidur kutang wanita motif beruang dan angka.
Bahan lembut dan sejuk.

Ukuran:

- Bahu: 29 cm
- Tinggi: 74 cm
- Lebar: 8 cm

Bahan: Kain

H003

---

Berat: 200 gr

Harga: 12900

### 19: Baju Atasan Rumah Wanita Biru Motif Bunga Dan Buah
Baju atasan rumah wanita warna biru, motif bunga dan buah.
Kain nyaman dipakai, lembut dan sejuk.

Ukuran:

- Ketiak/lebar: 36 cm
- Tinggi: 61 cm
- Tinggi dengan tali kutang: 73 cm

Bahan: Kain

H003

---

Berat: 200 gr

Harga: 12900

### 20: Kotak Makan Multifungsi 2 Rangkap
Ukuran: 17 cm x 10 cm x 8 cm

Bahan: Plastik

H004

---

Berat: 200 gr

Harga: 9900

### 21: Tempat Bumbu Meja Bening
Ukuran:

- Tinggi: 12 cm
- Diameter: 5 cm

Bahan: Plastik

H004

---

Berat: 200 gr

Harga: 19000

Stok: 2

### 22: Pisau Giant Stainless Steel 21 cm
Ukuran (kemasan): 28 cm x 6 cm

Ukuran (pisau): 21 cm

Bahan: Stainless Steel

H004

---

Berat: 200 gr

Harga: 12900

### 23: Spatula Stainless Steel Gagang Plastik Zebra Thailand
Ukuran: 31 cm x 9 cm

Bahan: Logam

H004

---

Berat: 300 gr

Harga: 19900

### 24: Mangkok Bayi Hello Kitty 3.5"
Ukuran: 11 cm x 9 cm x 4 cm

Bahan: Melanin

H004

---

Berat: 200 gr

Harga: 9900

### 25: Tutup Gelas Motif Pluffy
Diameter: 8 cm

Bahan: Melamin

H004

---

Berat: 100 gr

Harga: 4900

### 26: Gelas Ibunda Motif Hello Kitty
Tinggi: 9.5 cm

Diameter: 7 cm

Bahan: Melamin

H004

---

Berat: 200 gr

### 27: Panci Kecil Pegangan Plastik Stainless Steel Bekas
Dimensi Mangkok:

- Diameter: 14 cm
- Tinggi: 7 cm

Panjang Pegangan: 15 cm

Total Panjang: 24 cm

Bahan: Logam + Plastik tahan panas (pegangan)

H004

---

Berat: 500gr
