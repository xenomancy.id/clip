# Keterangan Saksi Tergugat B046

## Refined
1. Bahwa Saksi yang diajukan oleh **Tergugat** atas nama **[XXX]** di bawah sumpah memberikan keterangan sebagai berikut:
   - Keterangan

1. Bahwa berdasarkan pada keterangan saksi atas nama **[XXX]**, dapat disimpulkan:
   - Simpul

## RAW

### Tony
Islam
Karyawan swasta
Kenal dengan bu hermin? Kenal
Ada hub keluarga? Tidak
Kerjaan? Ya, sebagai karyawan Bu Hermin, Digaji sampai sekarang

Usahanya platform, gipsum.

146 HIR. Ini khusus yang pekerjaan yang wajib menyimpan rahasia.

Jadi karyawan bu hermin sejak 2006, tanggal 1 februari.
Kerja di situ karena awalnya saksi rumahnya bu hermin di caruban, jalan pattimura.
Kantornya di jalan sikatan, nomor 68.

Awal mulanya karena kenal sama papa hermin, mau kerja tidak, dan waktu itu pak wihadi datang sama papanya untuk menawarkan kerja.
Yasudah saksi pikir-pikir dulu, karena ada kerjaan di luar kota.
Februari itu baru mulai kerja.

Kerjanya di jalan sikatan, di rumah situ.
Rumahnya jadi 1 tanah dengan rumah tapi beda bangunan.
Platform gipsum Maranatha, kerja di situ sampai sekarang.
Sekarang kurang lebih 14 tahun.

Kantornya depan, rumahnya paling belakang.
Bu Hermin tinggal sejak tidak tahu.
Yang tinggal di situ bu hermin dan keluarganya: vero anak pertama, tirta nomor 2, nomor 3 itu titi vincent.
2006 masih serumah dengan wihadi.
Kalau sekarang tinggal 4 orang: hermin, dan anak-anak.
Pak wihadinya waktu itu dibilang luar kota kerja, yang bilang bu hermin, tapi tidak tahu kerja apa.
Sejak tahun 2006, kerja sekitar 2-3 tahun, pak wihadinya pergi.
Bilang kerja luar kota.
Sering pulang, habis itu tiba-tiba dapat surat dari pengadilan, gugatan cerai. Tidak ingat kapan, 2-3 tahun dari kerja adanya.
Tahu dari admin saksi surat itu datang.

Tahu surat cerai habis jangka waktu 1 minggu 2 minggu.
Bu Hermin sejak terima surat itu murung terus.
Habis itu keluar ke kantor, sama admin, bu hermin bilang suami minta cerai.
Suratnya panggilan sidang.

Sejak itu Pak Wihadi sejak panggilan sidang, sekitar 4 bulan, balik pulang ke rumah.
Yang mengajukan pak wihadi.
Tidak hitung berapa kali sidang.
Tidak pernah ikut sidang.
Hasil putusannya tidak tahu, tahunya pak wihadi kembali lagi ke rumah.
Tidak tahu kenapa kembali.
Kalau pikiran saksi, kerja kembali lagi ke madiun untuk kerja di madiun.
Kurang lebih 2017, 2014.
Tidur di jalan sikatan.

Saksi selama di rumah tidak ada percekcokan.
Keluarga baik semua, tidak pernah lihat perselisihan, sampai sekarang.
Setahu saksi baik-baik saja.

Alasan Pak Wihadi minta cerai tidak pernah.
Sering antar barang sama pak wihadi tapi tidak pernah cerita.
Jadi alasannya tidak tahu apa, tidak pernah tanya juga.

Kerjanya pak wihadi, keluar daerah ke mana saja tidak tahu.
Saksi dengarnya di paidon (?).
Kemudian, ketika ada gugatan perceraian.
Sebelum ada perceraian, kondisinya menurut pandangan saksi, baik sama istri dan anaknya.

Tempat bekerja usaha bersama wihadi dan hermin.
Tidak pernah dengar keributan sebelum gugatan cerai pertama.
Setelah proses persidangan berjalan, pak wihady kembali lagi tinggal di sana.
Sempat keluar lagi, sampai tahun 2017 kembali ke madiun.

Hubungan wihadi dan hermin, dari jeda waktu berhentinya gugatan hingga kembalinya, biasa-biasa saja.
Yang tahu wihadi pulang, habis itu balik lagi.
Renggang 2008-2009 dengan 2017, bantu kakaknya kerja.
Kakaknya kerja di jakarta.

Kembalinya 2017 apa sampai sekarang tetap di sikatan, atau tidak?
Wihadi pergi, tidak diam-diam, sepengetahuan bu hermin.
Itu tahu karena bu hermin cerita, ada kembali semata-mata untuk kerja sepengetahuan istrinya.

Selama kembali ngurus lagi pekerjaan kantor.
Usahanya tetap berjalan.
Bu hermin sekarang tinggal di sikatan, pak wihadi tidak tahu tinggal di mana?
Tinggal di sikatan tidak tiap hari.
Tetap ada pulang.

3 bulan ini tidak kembali ke sikatan.
Tidak ada cerita kenapa pulang atau bagaimana.
Sepengetahuan saksi, atas alasan bekerja.
Kalau masalah tidak tahu.

Bu Hermin ke Pak Wihadi selama ini baik, karena kalau berangkat kerja kan sama saksi pak wihadinya, kadang bu hermin ada antar.
Tidak pernah dengar bu hermin atur-atur jadwal pak wihadi, atau ada jadwal sebelum jam 9 harus balik.
Pak Wihadi dan bu hermin ke kantor berdua, menunggu konsumen.

Urusan jemput anak kadang saksi.
Daftar jadwal pernah lihat, untuk urusan pekerjaan saja, tidak ada urusan rumah tangga.

Pak Wihadi dan bu hermin tidak pernah bicara keras atau kasar antara satu sama lain.
Tidak tahu tanggapan anaknya dengan proses cerai ortunya, sejauh pengetahuan saksi.

Catatan kecil itu yang tulis pak wihadi untuk mengingat pekerjaan, tidak ada urusan pribadi.
Gugatan cerai lagi, setelah yang dulu, yang menggugat itu pak wihadi.
Alasannya tidak tahu.
Tahu ada gugatan cerai tanggal kapan tidak tahu.

Tanggapan dari anak atau bu hermin tidak pernah diceritakan ke pegawainya soal perceraiannya.
Sejak gugatan perceraian yang ini gak ada perubahan sikap, biasa-biasa saja.
Tidak ada saling membentak atau berkata-kata kasar.
Soal gelagat, bersikap ketus cuek dll, tidak ada dilihat sama saksi, komunikasi lancar, normal, tidak cemberutan diam-diaman, biasa-biasa saja.
Sejauh yang saksi tahu tidak ada masalah.

Watak Pak Wihadi baik, sopan santun, menghargai karyawan.
Demikian juga bu hermin.
Gak ada yang terlalu detail-oriented.

Kuasa Tergugat:
Selama pak wihadi kerja di luar, bisnis tetap berjalan, dijalankan sama bu hermin sama saksi, bantu lihat kerjaan.
Tahun 2017 kembali ke madiun, dalam bisnis yang dijalankan sendiri bu hermin, pak wihadi ikut bisnisnya, tidak pernah tahu ada omongan bu hermin bilang gak usah ikut-ikut.
Masih sering pergi berdua untuk urus bisnis.
Bagian keuangan di perusahaan bu hermin.

Dalam urusan bisnis atau rumah tangga, tidak diputus bu hermin sendiri, biasa tanya dulu ke pak wihadi.
Jadi selama di madiun, kalau ada masalah masih konsultasi dan laporan ke pak wihadi.
Kaitan dengan jam kerja saksi di perusahaan maranatha itu jam 07.30 kadang sampai jam 16.00 atau lembur sampai jam 18.00 hingga 19.00.
Kerjanya saksi serabutan, apa saja yang ada dikerja.
Waktu pak wihadi pulang sering saksinya gak mesti sama-sama pak wihadi.
Tahu sendiri karena memang sering sama-sama.
Seringnya bagi tugas WIhadi dan saksi, selama tidak bertabrakan jadwalnya dibarengi.

Selama pak wihadi pulang tidak pernah ada kata-kata yang bersifat negatif, saling mengasari dan sebagainya.
Kalau diam-diaman tiba-tiba lempar barang gak pernah lihat.
Setelah perkara ini berlangsung, sekitar 2-3 bulan terakhir, sepengetahuan saksi, perubahan sikap atau keadaan dari bu hermin: sempat shock, tapi kembali lagi ke kantor urus pekerjaan.
Tidak pernah mengetahui pak wihadi dikuncikan di luar rumah dilarang masuk.
Pagarnya bu hermin selalu dikunci, atau pagarnya selalu dibuka?
Dikunci angka, kalau gak tahu kodenya gak bisa buka.

Kuasa Penggugat:
Usaha itu sejak 2006 kerja, bagian dari usaha keluarga, usaha bapak bu hermin atau dibangun oleh pak wihadi dan bu hermin.
Usaha keluarga.
Pak wihadi pernah mengajukan gugatan perceraian, itu saksi masih bekerja.
Tahun 2009, Pak Wihadi berangkat hanya masih sering pulang ke madiun di jalan sikatan.

Saksi gak hapal kapan saja pulang, tapi pak wihadi sering pulang sehari-dua hari, habis itu balik lagi ke luar kota.
Soal balik lagi ke jakarta gak tahu, tapi kembali kerja setahu saksi.
Itu sekitar 2017, balik lagi mencalonkan diri di madiun.
Selama itu tinggal di rumah bu hermin.

Yang saksi tahu sekitar 2017 gak ke jakarta lagi, tinggal di madiun, urusin pekerjaan.
Sampai caleg, kalah caleg kerja lagi (di luar kota?).
Saksi tahunya karena orderan sepi di madiun jadi pak wihadi ke luar kota untuk kerja.

Tahun 2009, masalah plafon tetap berjalan diurus bu hermin.
Catatan yang dipegang pak wihadi ditulis oleh pak wihadi sendiri, yang ada jadwalnya.
Sering sama-sama ke pak wihadi urus pekerjaan.
Gak pernah lihat schedule dari bu hermin.

Kalau pak wihadi pulang, sebelum gugatan pernah ke sikatan.
Kalau yang ini gugatan tanggal 7 tiba di kantor, wihadi tetap tinggal di sikatan, tidur di sikatan. (?)
Pak Wihadi sebagai pimpinan di perusahaan.

Kantor dengan rumah beda, kantor ke rumah sekitar 10 meteran, itu di jalan sikatan.
Gipsum di jalan sikatan.
Setahu saksi baik-baik saja, karena kalau ke percetakan lewat rumahnya, tidak pernah dengar cekcok dsb.

Tentang keuangan, masalah gaji, bayar orderan dsb, beli bahan, bu hermin semua.
Pak Wihadi tidak tahu urus keuangan atau tidak.

Sehabis gugatan dilayangkan, pak wihadi tidak pulang lagi.
Komunikasi dengan bertha dan timothy pasti kumpul dengan wihadi juga.
Biasa saksi lihat waktu lewat di ruang tamu untuk ke percetakan.

Kuasa Tergugat:
Jadwalnya ada 2, pak wihadi bikin sendiri, dan bikin buat saksi.
Kalau jadwal pak wihadi kebanyakan, dibagi ke saksi sebagian jadwalnya.

Hakim:
Yang meminta hadir memberi kesaksian adalah bu hermin.
Setahu saksi bu hermin mau mempertahankan rumah tangga.

### Veronica +/- 1hr and up
Veronica Magdalena Wihady
21 tahun.
Mahasiswa.
Kenal bu hermin, saksi anak kandungnya
Pak Wihadi

Sebelum ini kira-kira waktu saksi masih SD kelas 4 ada gugatan cerai.
Papa yang mengajukan cerai.
Sekitar 20 tahun lalu nikahnya di Jember.
Belum lahir veronya sudah tinggal di madiun.

Papa yang mengajukan karena di rumah tangga, papa gak setuju karena papa merasa tidak dihargai, direndah-rendahkan, keputusan apa semua, mama yang ambil, di keluarga dan di bisnis keluarga.
Masih tinggal sama bu hermin.
Papa kalau di rumah madiun seringkali pulang ke jalan sikatan, tidur di sana.
Hubungan antara papa dan mama pertengkaran atau selisih pendapat gak pernah ada di hadapan anak-anak, jadi tidak pernah tahu, sepengetahuan saksi damai-damai saja.

2017 papa pulang ke madiun, selama hampir 2 tahun.
Hubungan papa dengan mama sangat harmonis, lebih harmonis daripada sebelumnya, karena sebelumnya papa sering ke luar kota.
Setahu saksi, papa ada hubungan dengan cewek lain.
Papa pernah mau kenalkan dengan cewek lain, hanya saksi tidak pernah mau dikenalkan dengan cewek itu, makanya tidak pernah lihat mereka jalan sama-sama.

2011 saksi kos di malang, papa minta kenalkan dengan cewek lain, nah saksi menolak.
Tapi sebelumnya sudah tahu duluan, karena waktu SMP lihat sendiri notifikasi papa ada chat dari wanita lain.
Isi chatnya gak tahu, gak buka-buka hp orang tua.
Papa kalau di rumah sore kadang tidur, itu hpnya kan dicas, ada telpon masuk dan chat dari cewek lain.

Kalau tidak salah ingat namanya dicantumkan noni atau nama samaran.
Kalau rekan bisnis kan gak mungkin menanyakan hal-hal pribadi, kan notifikasi yang masuk ada keluar pesan apa.
Tapi waktu dilihat aja, gak pernah buka.
Macam tanya "ni lagi apa", dsb, seperti ada hubungan dengan wanita lain.
Itu biasa sore, kalau malam biasa ada telpon tapi tidak tahu itu dari siapa.

Gak ada mama pas kejadian, karena waktu sma kan di malang, mama di madiun.
Secara terang-terangan dilakukan papa.
Kalau jalan berdua gak pernah lihat tapi.
Sekarang papa tinggal di Jakarta, kos.
Dalam rangka bekerja.
Papa sering pulang ke madiun, setahun bisa berapa kali? Durasi dari kelas 4 sd sampai 2017 3 kali pulang pergi ke jalan sikatan.
Dulu gugatan cerai, putusannya isinya gak pernah baca sendiri.
Masih suami isteri sampai sekarang tapinya.

Setahu saksi tidak ada pertengkaran, di hadapan saksi.
Karakter mama selalu mengingatkan saya, meski papa punya hubungan dengan cewek lain, tetap hormati ortu, dan bagaimanapun itu tetap papamu, jadi jangan lost contact.
Saksi pernah cerita soal cewek lain, mama kaget.
Mama bukan tipe orang marah-marah, pasti didiskusikan dulu, atau mencari tahu dulu.
Itu soalnya baru 1 kali kejadian begitu, jadi harus klarifikasi dulu.
Mama pernah sepengetahuan saksi klarifikasi, papa biasa cerita sendiri ke mama.
Di rumah mama gak pernah mau sebut soal wanita itu.

Kalau sudah lewat malam pasti mama tanyakan, sudah malam, pulang jam berapa.
Biasa jam 11.00 bisa pulang paling lama papa.
Di keluarga sendiri tidak ada aturan jam malam, tapi kalau di atas jam 10 pasti ditanyakan, khawatir.
Biasa kalau keluar papa bawa kunci sendiri, hanya sebagai istri mama pasti khawatir.

Papa sering kali kalau makan ada kurang berkenan di hati papa, ditegur sama papa.
Kalau dengan karyawan saksi tidak tahu.
Kalau tipe keluarga saksi, kalau marah tidak pernah dihadapan anak-anak.
Perceraian hanya baca dari gugatan soal mama otoriter, kejam, kata-kata kasar, dsb.
Nyatanya kalau papa ada masalah selalu didiskusikan dengan mama.
Tapi kalau masalah berkelanjutan, didiamkan begitu saja kan tidak ada penyelesaian, jadi mama ambil keputusan, karena papa sudah percayakan ke mama.
Jadi apapun yang disebut di gugatan, senyatanya tidak ada.

Saksi sendiri, setahu saksi mama selalu tekankan untuk dipertahankan, karena kepercayaan saksi, kalau kawin, itu tidak boleh dipisahkan, mengingat perlu memperhatikan masa depan anak-anaknya.
Menurut pandangan saksi, mau melihat ortu tetap mempertahankan, karena di alkitab diajar begitu.
Kalau kedua, saksi ingin ada figur seorang bapak di keluarga.
Saksi melihat masih bisa dipertahankan keluarga ini.

Gak pernah ada bekas kekerasan di mama.

Kuasa Tergugat:
Papa kan baru pulang 2017, tanggapan mama: kapanpun papa pulang selalu kita terima.
Seringkali papa mama masih ajak jalan sama-sama.

Kuasa Perlihatkan dokumen T-8.
Saksi mengkonfirmasi kebenaran foto-foto, termasuk waktunya.

Waktu menjalankan bisnis mama masih tanya-tanya papa.
Waktu urusan keluarga, saksi tanya-tanya ke mama, menyangkut masalah besar keluarga, pasti didiskusikan sama-sama, kecuali hal-hal kecil seperti vero curhat saja, sama mama saja.
Waktu mama tahu ada wanita lain, pasti kecewa, tapi mama tetap memaafkan dan menghargai papa sebagai suami, mengingat ajaran alkitab dan masa depan anak-anak.

Waktu sudah ada gugatan, mama gak pernah jelek-jelekkan papa.
Saksi sendiri tidak sering kontak papa, tapi saksi ingatnya mama selalu ingatkan untuk jaga kontak dengan papa.

Kuasa Penggugat:
Setahu saksi 2 tahun belakangan ini saja ada wanita lain.
Saksi sudah kuliah di surabaya.
Di 2009, sudah ada wanita lain.
Mama dan papa terjadi perselisihan saksi tidak tahu, karena mama selalu kalau di rumah ada masalah ya keluarga saja, tidak disinggung-singgung.

2009-2017 papa hanya 3 kali pulang.
Kembalinya ke rumah di sikatan.
Dalam jangka waktu yang 3 kali, papa tinggalnya berbulan-bulan setiap sesinya.
Tapi 2017-2019 2 tahun stay di madiun.

2017-2018 papa konsisten di rumah, sebelumnya yang 2009 ke 2017, saksi di rumah.
Yang sma saksi di malang.
Pas di madiun papa datang komunikasi dengan mama baik-baik saja, seperti biasa.
Mengurus bisnis, papa yang mengendalikan usaha, ibu yang memegang keuangan.

Papa beberapa kali bilang terserah sama mama.
Seingat saksi, 2017-2019 papa memberi kepercayaan ke papa untuk jalankan bisnis.
Kalau ada masalah diskusi, papa biasa mengutarakan pendapat, tapi kalau papa tidak bisa, otomatis mama yang membantu menyelesaikan, take over.
Biasa papa lepas tangan, terserah, jadi mama yang turun tangan karena kalau tidak masalahnya tidak selesai-selesai.
Biasa tidak begitu, papa jalankan usaha, tapi kalau tidak benar baru mama turun tangan.

Sebelum 2017 pas papa kembali, mama beri kepercayaan untuk jalankan bisnis lagi, kalau power masih di mama, pasti mama tidak beri kepercayaan ke papa.

2017-2018, papa kembali, usahanya sama.

Papa memang sering tersinggung, padahal mama tidak bermaksud begitu.
Saksi pernah dengar mama mengeluarkan suatu umpatan ke Papa.
Menurut saksi itu hanya keceplosan saja, refleks saja, bukan bermaksud menyinggung papa.

"Umbahmu"?

Waktu semester awal saksi pernah terdorong hati untuk menyatukan papa dan mama.
Jadi dulu karena sering di rumah saksi sering cerita dengan mama.
Tapi saksi pengen dengar juga dari sisi papa.
Papa lebih condong ke mama itu selalu menyinggung perasaan papa.
Lalu papa merasa gak punya power di bisnis, padahal menurut saksi mama memberi kepercayaan sepenuhnya ke papa, jadi papa bukannya tidak punya power.

Gugatan yang terakhir itu, gak ada kebersit di pikirannya saksi kalau bakal timbul gugatan itu.
Karena 2 tahun terakhir hubungan papa mama harmonis kembali, seperti orang baru-baru pacaran begitu.
Kalau papa bosan kadang pura-pura cium, itu gak ada yang suruh, maka saksi rasa mereka sudah harmonis.
Makanya tidak ada angin tidak ada hujan tiba-tiba ada surat gugatan.

Saksi sendiri tidak lihat bahwa papa membaik karena alasan politik saja, rasanya kesannya membaik harmonis.
Di Jakarta saksi tidak pernah sempat ke jakarta, kadang di kos, atau di rumah keluarga.
Tapi saksi tidak pernah lihat kos papa seperti apa, biaya rumah tangga tidak pernah beri biaya rumah tangga, jadi mama kerja sendiri demi membesarkan kita.
Papa waktu adik sma baru mulai kirim uang, sebelumnya tidak pernah.

Pernah waktu SMA pernah kirim uang jajan buat saksi, hanya itu saja.
Uang sekolah dan sebagainya itu mama.
Sekolah mitra harapan, papa sebagai ketua yayasan.
Sepulang sekolah, papa tidurnya di kamar, di sikatan, itu tahun 2017-2018 terakhir.

Hakim:
Tanggapan dari keluarga, saudara, ibu, tentang perceraian, dari keluarga besar, beberapa tahu beberapa tidak.
Tanggapannya ya mempertahankan.
Dari keluarga besar tidak pernah bicarakan soal itu bersama-sama mama dan papa.
Gugatan sekarang mama dan papa belum sempat ketemu lagi untuk bicarakan.
Tapi yang gugatan pertama itu pernah.

Dari pihak keluarga besar, tidak mengharapkan perceraian.
Terhadap perceraian ini, dampak perceraian itu pasti ada dampaknya, karena saksi tipenya pemikir, saksi rasa lebih emosional, dan cenderung ada masalah ortu begitu pasti turun prestasi akademisnya, karena tidak ingin kehilangan figur papa.

Saudara-saudaranya saksi, berta merasa sangat terdampak, karena berta paling dekat ke papa.
Berta senang bukan main kalau ada papa di rumah, pasti berta senang main-main sama papa.
Vincent sejak papa meninggal rumah sampai 8-10 tahun, itu dia dulu masih kecil, cenderung pendiam anaknya.
Jadi vincent gak dekat, karena sama adik cowok papa lebih keras.
Vincent sering ditinggal papa, lebih diam, tertutup.
Jadi yang paling berdampak itu adik cewek saksi.
Sering curhat dengan Berta.
Pendapat adik saksi belum pernah saksi ceritakan ke papa.

Saksi kangen dengan keadaan suasana papa dan mama bareng-bareng, karena dulu masih kecil perekonomian masih jaya-jayanya, makanya saksi senang.
Terus pas ditinggal papa, itu berubah total saksinya, jadi pendiam, pikir sendiri, dsb, berdampak dalam hidup saksi.
Lalu perekonomian juga berdampak, karena papa awalnya pimpin usaha, habis itu ditinggal, jadi mama belum diajari sebelumnya tentang bisnis tersebut, makanya turun jauh perekonomiannya.
Sampai biaya spp dsb jadi kesulitan, makanya pengaruh ada tidaknya papa itu.

Upaya mediasi keluarga belum ada, tapi kalau dari agama atau gereja, perkara yang sekarang belum pernah.
Mengenai umpatan itu, konteksnya serius atau bercanda? Menurut saksi, dulu mama guyonan, jawab "uangnya mbahmu", karena uangnya wes gak ada, saksi lupa papa tanya apa.

Menurut saksi itu konteks candaan.
Usaha gipsum itu usaha bersama papa dan mama setelah ada pernikahan atau warisan? Itu rintisan papa mama menurut saksi, bukan dikasih.
Jadi ini usaha ini yang membiayai keluarga.
Jadi keuntungan perusahaan dan sebagainya dipakai untuk kepentingan keluarga.
Selama papa pergi, saksi lupa mama setuju atau tidak, tapi saksi tahunya papa kerja.
Jadi papa tidak pernah kirim uang, semua usaha keuntungan dari mama dipakai untuk keluarga.
Dulu pernah ada bisnis baru, koperasi, untuk support usaha sekarang yang menurun.

Papa bilang terserah apa waktu urusan kerjaan atau urusan rumah tangga juga?
Jawaban terserah itu hanya waktu bisnis saja, semua diserahkan ke mama.
Waktu adik mau SMA ya cenderungnya ya terserah, terserah biarkan pilihan adiknya saja, jadi terserahnya beda.
Papa selalu support keputusannya anaknya.

Terkait pembagian tugas di rumah, dari 2017-2019 bersama papa mama, sepengetahuan saksi tidak ada ketentuan itu secara spesifik.
Kalau mama ada proyek, papa ada di rumah, otomatis papa yang handle rumah.
Jadi tidak ada namanya papa diatur mama jadwalnya, karena fleksibel kok jadwalnya.
Kalau misal mau jemput vincent, mama telpon papa, papa bilangnya tidak bisa, ya kalau bisa mama yang jemput, atau karyawan yang jemput.

Papa setiap pagi bangun tidur ambil kertas lalu catat semua jadwal hari itu mau ngapain saja.
Kalau mama tidak pernah menulis begitu, lebih seperti ingatan saja.
Papa tulis begitu karena takut lupa, makanya ditulis, bukan karena seseorang memberi catatan untuk dilaksanakan.
Papa sudah dari saksi kecil begitu, bahkan beli baju yang ada kantongnya.

Kalau sampai benar-benar terperinci harus pulang jam 10, itu gak ada.
Tapi kalau pulangnya sudah larut malam, pernah saksi lagi ada kompetisi sampai jam 12 malam belum pulang, otomatis mama telpon-telpon.
Jadi kalau sudah larut baru mama telpon, karena tipe orangnya khawatiran.
Papa biasanya cenderung pulang larut, karena papa sering ikut organisasi.
Mama pernah ingatkan agar tidak pulang larut ke papa, spesifiknya kurang tahu, tapi setahu saksi mama tidak pernah bentak-bentak.

Biasanya mama chat wa, kalau gak balas-balas baru telpon. Soalnya mama takut kalau keseringan ditelpon terus jadi mengganggu pekerjaan papa.

Kuasa Penggugat:
Koperasi simpan pinjam, modalnya dari tabungan mama.
Koperasi sudah beroperasi sekitar 1th atau 2th, sekitar tahun 2017-2018, pas saksi awal kuliah.
Awal-awal bertumbuh, lalu akhir-akhir menurun, tapi mama tetap berusaha keras karena anak-anak sudah mulai besar.
Waktu mama buka koperasi papa belum sepenuhnya di madiun.
Kejadian itu saksi di surabaya.
Sekarang koperasi itu sudah ditutup, tidak ada lagi.

> Kuasa Penggugat bilang, kalau koperasi itu yang jadi salah satu alasan perceraian, karena menurut penggugat ini berlawanan dengan prinsip yang dipegang oleh penggugat.
> Hanya di sidang terdahulu belum bisa terungkap, kebetulan ini ada saksi yang mengetahui tentang koperasi itu, makanya ditanyakan.
> Pak Wihadi waktu mau kembali membina keluarga, dia tahu ada koperasi simpan pinjam itu.


