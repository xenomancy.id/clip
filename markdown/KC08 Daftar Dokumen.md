# Daftar Dokumen

## Bab I: Riwayat Perseroan

### Pendirian PT UKP

36.	Copy Sesuai Asli Salinan Akta Pendirian Perseroan Terbatas PT. Usaha Kreatif Pembayaran Nomor: 1601 tanggal 21-11-2019, yang dibuat oleh Notaris Ineu Mauleni, S.H., Notaris di Kota Surabaya. Terdiri dari 19 lembar.

1.	Copy Sesuai Asli Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Nomor AHU-0063497.AH.01.01.Tahun 2019 tentang Pengesahan Pendirian Badan Hukum Perseroan Terbatas PT Usaha Kreatif Pembayaran, ditetapkan tanggal 29 November 2019. Terdiri dari 2 lembar.

### Anggaran Dasar Terakhir PT UKP

37.	Copy Sesuai Asli Salinan Akta Pernyataan Keputusan Rapat Nomor 10 tertanggal 30 Juni 2020 yang dibuat di hadapan Nico Adrian Roekiyanto, Sarjana Teknik, Sarjana Hukum, Magister Kenotariatan, Notaris di Kota Malang. Terdiri dari 28 lembar.

38.	Copy Sesuai Asli Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Nomor AHU-0044311.AH.01.02.Tahun 2020 tentang Persetujuan Perubahan Anggaran Dasar Perseroan Terbatas PT Usaha Kreatif Pembayaran, ditetapkan tanggal 30 Juni 2020. Terdiri dari 2 lembar.
39.	Copy Sesuai Asli Lampiran Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Nomor AHU-0044311.AH.01.02.Tahun 2020 tentang Persetujuan Perubahan Anggaran Dasar Perseroan Terbatas PT Usaha Kreatif Pembayaran, ditetapkan tanggal 30 Juni 2020. Terdiri dari 2 lembar.
40.	Copy Sesuai Asli Surat dari Kementrian Hukum dan Hak Asasi Manusia Republik Indonesia Direktorat Jendral Adminsitrasi Hukum Umum Nomor: AHU-AH.01.03-0268037, Perihal: Penerimaan Pemberitahuan Perubahan Data Perseroan PT Usaha Kreatif Pembayaran, diterbitkan tanggal 30 Juni 2020. Terdiri dari 2 lembar.
41.	Copy Sesuai Asli Surat dari Kementrian Hukum dan Hak Asasi Manusia Republik Indonesia Direktorat Jendral Adminsitrasi Hukum Umum Nomor: AHU-AH.01.03-0268034, Perihal: Penerimaan Pemberitahuan Perubahan Anggaran Dasar PT Usaha Kreatif Pembayaran, diterbitkan tanggal 30 Juni 2020. Terdiri dari 2 lembar.
42.	Copy Sesuai Asli Salinan Akta Pernyataan Keputusan Rapat Perseroan Terbatas PT. Usaha Kreatif Pembayaran, Nomor: 15 tanggal 30 September 2020, dibuat di hadapan Nico Adrian Roekiyanto, S.T., S.H., M.Kn., Notaris di Kota Malang. Terdiri dari 9 lembar.
1.	Copy Sesuai Asli Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Nomor AHU-0067745.AH.01.02.Tahun 2020 tentang Persetujuan Perubahan Anggaran Dasar Perseroan Terbatas PT Usaha Kreatif Pembayaran, ditetapkan tanggal 01 Oktober 2020. Terdiri dari 1 lembar.
1.	Copy Sesuai Asli Lampiran Keputusan Menteri Hukum dan Hak Asasi Manusia Republik Indonesia Nomor AHU-0067745.AH.01.02.Tahun 2020 tentang Persetujuan Perubahan Anggaran Dasar Perseroan Terbatas PT Usaha Kreatif Pembayaran, ditetapkan tanggal 01 Oktober 2020. Terdiri dari 1 lembar.
1.	Copy Sesuai Asli Surat dari Kementrian Hukum dan Hak Asasi Manusia Republik Indonesia Direktorat Jendral Administrasi Hukum Umum Nomor AHU-AH.01.03-0393323, Perihal: Penerimaan Pemberitahuan Perubahan Data Perseroan PT Usaha Kreatif Pembayaran, diterbitkan tanggal 01 Oktober 2020. Terdiri dari 1 lembar.

### Daftar Khusus Pemegang Saham

9.	Copy Sesuai Asli Surat Nomor: 0001/SHM/UKP/X/2020 tertanggal 30 Juni 2020 dengan judul: Daftar Khusus Kepemilikan Saham Usaha Kreatif Pembayaran. Terdiri dari 1 lembar.

10.	Copy Sesuai Asli Surat Nomor: 0002/SHM/UKP/X/2020 tertanggal 30 Juni 2020 dengan judul: Daftar Khusus Kepemilikan Saham Usaha Kreatif Pembayaran. Terdiri dari 1 lembar.
11.	Copy Sesuai Asli Surat Nomor: 0003/SHM/UKP/X/2020 tertanggal 30 Juni 2020 dengan judul: Daftar Khusus Kepemilikan Saham Usaha Kreatif Pembayaran. Terdiri dari 2 lembar.
12.	Copy Sesuai Asli tabel komposisi saham berdasarkan Akta Pendirian tanggal 21-11-2019 No.1601, dibuat di hadapan Ineu Mauleni, S.H., Notaris di Kota Surabaya. Terdiri dari 1 lembar.
13.	Copy Sesuai Asli tabel komposisi saham berdasarkan Akta PKR tanggal 30-06-2020 No. 10, dibuat di hadapan Nico Adrian Roekiyanto, S.T., S.H., M.Kn., Notaris di Kota Malang. Terdiri dari 1 lembar.

### Mengenai Pengurus

> **Catatan:** mengenai surat pernyataan, ada nomor suratnya. Cek.

17.	Copy Sesuai Asli Susunan Direksi dan Lampiran Identitas Direksi. Terdiri dari 1 lembar.

18.	Copy Sesuai Asli Susunan Dewan Komisaris dan Lampiran Identitas Dewan Komisaris. Terdiri dari 1 lembar.
27.	Copy Sesuai Asli Surat Pernyataan tertanggal 27 Oktober 2020 atas nama Charlie Anthony, bahwa selaku Direktur Utama PT Usaha Kreatif Pembayaran, saat mengajukan permohonan tidak sedang dalam pailit, tidak pernah dihukum atas tindak pidana, tidak tercantum dalam daftar kredit macet, dan tidak pernah masuk dalam daftar hitam nasional penarik cek/bilyet giru kosong yang ditatausahakan Bank Indonesia. Terdiri dari 1 lembar.
28.	Copy Sesuai Asli Surat Pernyataan tertanggal 27 Oktober 2020 atas nama Fenny Chiurman, bahwa selaku Direktur PT Usaha Kreatif Pembayaran, saat mengajukan permohonan tidak sedang dalam pailit, tidak pernah dihukum atas tindak pidana, tidak tercantum dalam daftar kredit macet, dan tidak pernah masuk dalam daftar hitam nasional penarik cek/bilyet giru kosong yang ditatausahakan Bank Indonesia. Terdiri dari 1 lembar.
29.	Copy Sesuai Asli Surat Pernyataan tertanggal 27 Oktober 2020 atas nama Susanlim, bahwa selaku Komisaris PT Usaha Kreatif Pembayaran, saat mengajukan permohonan tidak sedang dalam pailit, tidak pernah dihukum atas tindak pidana, tidak tercantum dalam daftar kredit macet, dan tidak pernah masuk dalam daftar hitam nasional penarik cek/bilyet giru kosong yang ditatausahakan Bank Indonesia. Terdiri dari 1 lembar.
30.	Copy Sesuai Asli Surat Pernyataan tertanggal 27 Oktober 2020 atas nama William, bahwa selaku Komisaris Utama PT Usaha Kreatif Pembayaran, saat mengajukan permohonan tidak sedang dalam pailit, tidak pernah dihukum atas tindak pidana, tidak tercantum dalam daftar kredit macet, dan tidak pernah masuk dalam daftar hitam nasional penarik cek/bilyet giru kosong yang ditatausahakan Bank Indonesia. Terdiri dari 1 lembar.

## Bab II: Perizinan

1. NPWP No. 93.584.598.2-607.000 atas nama PT. Usaha Kreatif Pembayaran, yang dikeluarkan oleh Direktorat Jenderal Pajak Kantor Wilayah DJP Jawa Timur KPP Pratama Surabaya Tegalsari.

1. SKT No.S-4794KT/WPJ.11/KP.0503/2019 tanggal 28 Nopember 2019 atas nama PT. Usaha Kreatif Pembayaran.

16.	Copy Sesuai Asli Tanda Daftar Penyelenggara Sistem Elektronik Nomor: 03045/DJAI.PSE/10/2020, atas nama PT. Usaha Kreatif Pembayaran, terbit tanggal 20 Oktober 2020. Terdiri dari 1 lembar.

14.	Copy Sesuai Asli Nomor Induk Berusaha (NIB) No. 9120216122793 atas nama PT Usaha Kreatif Pembayaran, diterbitkan tanggal 29 November 2019, perubahan ke-24 tanggal 2 Oktober 2020. Terdiri dari 1 lembar. Terlampir:

	1.	Copy Sesuai Asli tabel Kode KBLI untuk NIB 9120216122793. Terdiri dari 2 lembar.
15.	Copy Sesuai Asli Izin Usaha Industri, untuk NIB 9120216122793, atas nama PT Usaha Kreatif Pembayaran, tanggal terbit izin usaha proyek pertama: 7 Juli 2020, Perubahan ke-24 tanggal 2 Oktober 2020. Terdiri dari 1 lembar. Terlampir:

	1.	Copy Sesuai Asli Lampiran Izin Usaha Yang Belum Memenuhi Komitmen/Tidak Efektif, untuk NIB 9120216122793. Terdiri dari 2 lembar.

1. SKD No. DM/231/436.9.5.3/XI/2020 tanggal 16 November 2020 yang dikeluarkan oleh Pemerintah Kota Surabaya Kecamatan Tegalsari c.q. Kepala Kelurahan Kedungdoro, mengenai domisili kantor PT. Usaha Kreatif Pembayaran.

1. Keputusan Kepala Dinas Lingkungan Hidup Kota Surabaya Nomor 188.4/992/Kep/436.7.12/2020 tentang izin Lingkungan Kegiatan Komplek Tunjungan Plaza Mall oleh PT Pakuwon Jati TBK di Jalan Basuki Rahmat No. 8-12 Kelurahan Kedungdoro Kecamatan Tegalsari Kota Surabaya.

1. Surat Keterangan No. 073/SK/P3SRS TP-5/XI/20 tanggal 11 November 2020 yang dikeluarkan oleh Badan Pengelola Lingkungan - Perhimpunan Pemilik & Penghuni Satuan Rumah Susun (BPL P3SRS) TP-5 Gedung Pakuwon Center.

52.	Copy Sesuai Asli Surat Izin Kepala Dinas Perumahan Rakyat dan Kawasan Permukiman, Cipta Karya dan Tata Ruang Kota Surabaya Nomor: 188.4/3362-95/438.7.5/2018 tentang Izin Mendirikan Bangunan, atas nama PT. Pakuwon Jati. Dikeluarkan di Surabaya tanggal 22 Juni 2018. Terdiri dari 1 lembar.

19.	Copy Sesuai Asli Surat tertanggal 26 Oktober 2020 dengan judul: Pernyataan Direksi. Surat berisikan pernyataan atas nama Charlie Anthony selaku Direktur Utama PT Usaha Kreatif Pembayaran, bahwa PT Usaha Kreatif Pembayaran telah melaksanakan kewajiban pembayaran Upah seluruh karyawan sesuai dengan Upah Minimum Kota Surabaya Tahun 2020. Terdiri dari 1 lembar.

	> seharusnya ini ada nomor suratnya

21.	Copy Sesuai Asli Wajib Lapor Ketenagakerjaan di Perusahaan, dengan No. Pendaftaran: 6026101 20219 2020 10 1-25-09-2020. Terdiri dari 1 lembar. Terlampir:

	1.	Copy Sesuai Asli Tanda Terima Wajib Lapor Ketenagakerjaan Tahun 2020, didaftarkan tanggal 25 September 2020 untuk No. Pendaftaran: 6026101 20219 2020 10 1-25-09-2020. Terdiri dari 1 lembar.

22.	1 bundel bukti pembayaran BPJSTK Agustus-Oktober tahun 2020. Terdiri dari:

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 11 Agustus 2020, Perusahaan: Usaha Kreatif Pembayaran, Kode Iuran: 200703587838. Terdiri dari 1 lembar.

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 07 September 2020, Perusahaan: Usaha Kreatif Pembayaran, Kode Iuran: 200901191202. Terdiri dari 1 lembar.

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 07 Oktober 2020, Perusahaan: Usaha Kreatif Pembayaran, Kode Iuran: 201001225656. Terdiri dari 1 lembar.

23.	1 bundel bukti pembayaran BPJS Kesehatan Agustus-Oktober 2020. Terdiri dari:

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 11 Agustus 2020, atas nama: Usaha Kreatif Pembayaran, PT. Transaction Reference No. 202008111359487966. Terdiri dari 1 lembar.

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 07 September 2020, atas nama: Usaha Kreatif Pembayaran, PT. Transaction Reference No. 202009070943971822. Terdiri dari 1 lembar.

	1.	Copy Sesuai Asli Bill Payment, dieksekusi tanggal 07 Oktober 2020, atas nama: Usaha Kreatif Pembayaran, PT. Transaction Reference No. 202010071500553272. Terdiri dari 1 lembar.

24.	Copy Sesuai Asli Sertifikat Kepesertaan BPJS Ketenagakerjaan Nomor: 200000000995412, Nama Badan Usaha: PT Usaha Kreatif Pembayaran, Nomor Pendaftaran Perusahaan: 20116015, ditetapkan tanggal 11 Agustus 2020. Nomor Kendali: 2017-274615. Terdiri dari: 1 lembar.

25.	Copy Sesuai Asli Sertifikat keikutsertaan Program Jaminan Kesehatan Nasional – Kartu Indonesia Sehat (JKN-KIS), No. Sertifikat: 309/SER/1301/1020, atas nama Usaha Kreatif Pembayaran, PT, Kode Badan Usaha: 00017331, Periode: 12 Oktober 2020 – 12 Oktober 2021. Terdiri dari 1 lembar.

26.	Copy Sesuai Asli Tanda Terima Berkas Pendaftaran untuk Jenis Izin: Pengesahan Baru Peraturan Perusahaan (PP), tertanggal 26 Oktober 2020, Nomor Pendaftaran: 47951/2020. Terdiri dari 1 lembar.



## Bab III: Pemegang Saham Badan Hukum

54.	Copy Sesuai Asli Salinan Akta Pendirian Perseroan Terbatas PT Usaha Kreatif Indonesia Nomor: 618 Tanggal 08-08-2018 (delapan Agustus dua ribu delapan belas), dibuat di hadapan Ineu Mauleni, Sarjana Hukum, Notaris di Kota Surabaya. Terdiri dari 18 lembar.

1.	Surat Keputusan No. AHU-0039106.AH.01.01.Tahun 2018, tertanggal 20 Agustus 2018 dan telah didaftarkan pada Daftar Perseroan No. AHU-0108399.AH.01.11.Tahun 2018, tanggal 20 Agustus 2018.

1.	Berita Negara Republik Indonesia No. 34717 Tanggal 4 Oktober 2019, TBN No. 80.

53.	Copy Sesuai Asli Salinan Akta Pernyataan Keputusan Rapat Perseroan Terbatas PT Usaha Kreatif Indonesia Nomor: 08 tanggal 26-06-2020 (dua puluh enam Juni tahun dua ribu dua puluh), dibuat di hadapan Nico Adrian Roekiyanto, Sarjana Teknik, Sarjana Hukum, Magister Kenotariatan, Notaris di Kota Malang. Terdiri dari 28 lembar.

1.	Surat Keputusan No. AHU-0043940.AH.01.02.Tahun 2020, tanggal  29 Juni 2020.

1.	Surat Penerimaan Pemberitahuan Perubahan Anggaran Dasar PT. Usaha Kreatif Indonesia No. AHU-AH.01.03-0265602, tanggal 29 Juni 2020.

1.	Surat Penerimaan Pemberitahuan Perubahan Data Perseroan PT. Usaha Kreatif Indonesia No. AHU-AH.01.03-0265606, tanggal 29 Juni 2020.

1. NPWP PT. UKI No. 85.750.142.3-607.000, untuk kegiatan usaha PT. UKI yang berlokasi di Pakuwon Center Tunjungan Plaza 5 LT. 16 Blok OF Nomor 5 Jl. Embong Malang No. 1-3-5 RT. 008 RW. 010 Kedungdoro Tegalsari, diterbitkan oleh Kantor Pelayanan Pajak Pratama Surabaya Tegalsari, terdaftar sejak tanggal 30 Agustus 2018.

1. Surat Keterangan Terdaftar Nomor: S-1324KT/WPJ.11/KP.0503/2018, tanggal 30 Agustus 2018, atas nama PT. Usaha Kreatif Indonesia.

1. Nomor Induk Berusaha (NIB) 8120104992041, atas nama PT. Usaha Kreatif Indonesia, yang diterbitkan tanggal 24 September 2018, perubahan kedua tanggal 24 September 2018, oleh Badan Koordinasi Penanaman Modal melalui sistem OSS.

1. SIUP (Menengah) No. 503/9773.A/436.7.17/2018, tanggal 3 September 2018, atas nama PT. Usaha Kreatif Indonesia yang dikeluarkan oleh Dinas Penanaman Modal Dan Pelayanan Terpadu Satu Pintu Kota Surabaya.

1. SKD No. DM/230/436.9.5.3/XI/2020 tanggal 16 November 2020 yang dikeluarkan oleh Pemerintah Kota Surabaya Kecamatan Tegalsari c.q. Kepala Kelurahan Kedungdoro, mengenai domisili kantor PT. Usaha Kreatif Indonesia.

## Bab IV: Penyertaan Perseroan Pada Perusahaan Lain

1. Surat Pernyataan Direksi Perseroan Nomor 0014/SK-UKP/XI/2020, tertanggal 10 November 2020 pada intinya menyatakan bahwa terhitung sejak berdiri hingga pada tanggal Surat Pernyataan tersebut Perseroan tidak pernah melakukan penyertaan modal dan/atau pelepasan penyertaan modal pada perusahaan lain.

## Bab V: Harta Kekayaan Perseroan

1.	Merek Ultimeal dengan Nomor Permohonan: DID2020003049, dengan status: (TM) Pemeriksaan oleh Kasubdit, diterima tanggal 18 Januari 2020, diumumkan dengan nomor pengumuman: Endah, 4-9-2020, pada tanggal 04 September 2020. Perlindungan dimulai sejak tanggal 18 Januari 2020, dimiliki atas nama PT. Usaha Kreatif Pembayaran, dengan konsultan: Hari Purnomo Chandra B.Sc.
Terdaftar pada Kelas Nice dengan kode kelas 9.

1.	Merek Ultimeal dengan Nomor Permohonan: DID2020003051 dengan status: (TM) Pemeriksaan oleh Kasubdit, diterima tanggal 18 Januari 2020, diumumkan dengan nomor pengumuman: Endah, 4-9-2020, pada tanggal 04 September 2020. Perlindungan dimulai sejak tanggal 18 Januari 2020, dimiliki atas nama PT. Usaha Kreatif Pembayaran, dengan konsultan: Hari Purnomo Chandra B.Sc.
Terdaftar pada Kelas Nice dengan kode kelas 28.

1.	Merek Ultimeal dengan Nomor Permohonan: JID2020003052, dengan status: (TM) Pemeriksaan oleh Kasubdit, diterima tanggal 18 Januari 2020, diumumkan dengan nomor pengumuman: Endah, 4-9-2020, pada tanggal 04 September 2020. Perlindungan dimulai sejak tanggal 18 Januari 2020, dimiliki atas nama PT. Usaha Kreatif Pembayaran, dengan konsultan: Hari Purnomo Chandra B.Sc.
Terdaftar pada Kelas Nice dengan kode kelas 43.

## Bab VI: Perjanjian Kredit Dimana Perseroan Berkedudukan Sebagai Debitur

1. Surat Pernyataan Direktur Utama Perseroan tertanggal 03 November 2020 dinyatakan bahwa Perseroan sedang tidak memperoleh fasilitas pinjaman kredit dari lembaga perbankan atau bentuk perjanjian pinjaman lain dengan lembaga pembiayaan manapun yang serupa dengan hal tersebut.

## Bab VII: Perjanjian Pihak Ketiga

32.	Copy Sesuai Asli Salinan Akta Sewa Menyewa tanggal 28 Februari 2020 Nomor: 138 yang dibuat oleh Notaris Fenty Abidin, S.H., Notaris di Jakarta. Terdiri dari 16 lembar.

1. Persetujuan Tertulis dari PT Kharisma Catur Mandala (“duitku”) berdasarkan surat Nomor : 028/SKPI/DUITKU/KCM/11/2020  tertanggal 6 Nopember 2020 untuk dapat mengungkapkan isi Perjanjian Kerjasama Disbursement No. 151/PKS-KCM/DD/IV/2020 tanggal 22 April 2020 dan Perjanjian Kerjasama Merchant Internet No. 339/PKS-M/DK/IV/2020 tanggal 22 April 2020 untuk keperluan pelaksanaan Uji Tuntas Segi Hukum dalam prosesnya untuk pengajuan perizinan ke Bank Indonesia atas nama Perseroan.

1.	Copy Sesuai Asli Surat No. 151/PKS-KCM/DD/IV/2020 tertanggal 22 April 2020, dengan judul: Perjanjian Kerjasama Merchant Disbursement. Terdiri dari 17 lembar. Terlampir:

	1.	Copy Sesuai Asli Lampiran I Surat No. 151/PKS-KCM/DD/IV/2020, dengan judul: Standar Prosedur Pengoperasian. Terdiri dari 1 lembar.

	1.	Copy Sesuai Asli Lampiran II Surat No. 151/PKS-KCM/DD/IV/2020, dengan judul: Biaya dan Deposit Tunai. Terdiri dari 1 lembar.


	1.	Copy Sesuai Asli Lampiran III Surat No. 151/PKS-KCM/DD/IV/2020, dengan judul: Tujuan Layanan Duitku Kirim. Terdiri dari 1 lembar.

7.	Copy Sesuai Asli Surat Nomor: 339/PKS-M/DK/IV/2020 tertanggal 22 April 2020, dengan judul: Perjanjian Kerjasama Merchant Internet. Terdiri dari 17 lembar.

6.	Copy Sesuai Asli Surat Nomor: 0001/PKS-UKP/IX/2020; Nomor: 030/PK-AS/IX/2020 tertanggal 7 September 2020, dengan judul: Perjanjian Kerjasama Re-Penetration Testing Aplikasi Dompet Elektronik antara PT Usaha Kreatif Pembayaran dengan PT Adikarya Tata Informasi. Terdiri dari 8 lembar.

1. Laporan Re-Penetration Testing Aplikasi Dompet Elektronik Ultimate Pay PT Usaha Kreatif Pembayaran Nomor : 040/LP-AS/X/2020.

2.	Copy Sesuai Asli Surat No. 051/SPK/CON/PMI/VII/2020 tertanggal 14 Juli 2020, dengan judul: Perjanjian Jasa Konsultasi Layanan Pemenuhan Kepatuhan PBI 18/40/PBI/2016 Lingkup Dompet Elektronik Antara PT Proxsis Manajemen Internasional dan PT Usaha Kreatif Pembayaran. Mengacu kepada dokumen proposal sebagaimana yang disebut dalam proposal penawaran (No.049/ITC/PGS-USA/20200626-00, tanggal 26 juni 2020). Terdiri dari 6 lembar.

3.	Copy Sesuai Asli Surat No. 051/SPK/CON/PMI/VII/2020 tertanggal 14 Juli 2020, dengan judul: Perjanjian Jasa Konsultasi Layanan Pemenuhan Kepatuhan PBI 18/40/PBI/2016 Lingkup Uang Elektronik Antara PT Proxsis Manajemen Internasional dan PT Usaha Kreatif Pembayaran. Mengacu kepada dokumen proposal sebagaimana yang disebut dalam proposal penawaran (No.049/ITC/PGS-USA/20200713-00, tanggal 13 juli 2020). Terdiri dari 6 lembar.

4.	Copy Sesuai Asli Surat No. BILLFAZZ: 00028/BILLFAZZ/LEGAL/VII/2019, tanggal berlaku: 29 Juli 2019, dengan judul: Perjanjian Jual Beli Produk antara PT BILLFAZZ Teknologi Nusantara dan PT Usaha Kreatif Indonesia. Ditandatangani oleh Rico Ofna Putra (direktur PT BILLFAZZ Teknologi Nusantara) pada tanggal 29 Juli 2019, dan oleh William (direktur PT Usaha Kreatif Indonesia) pada tanggal 6 Agustus 2019. Terdiri dari 14 lembar.

5.	Copy Sesuai Asli Surat No. UKI: 0002/PKS-UKI/IX/2020, No. UKP: 0002/PKS-UKP/IX/2020, No. BILLFAZZ: 00313/BILLFAZZ/LEGAL/IX/2020, tanggal berlaku: 8 September 2020, dengan judul: Perjanjian Pengalihan Dan Amandemen Kesatu Atas Perjanjian Jual Beli Produk Oleh Dan Antara PT Usaha Kreatif Indonesia dan PT Usaha Kreatif Pembayaran dan PT BILLFAZZ Teknologi Nusantara. Ditandatangani oleh Rico Ofna Putra (direktur PT BILLFAZZ Teknologi Nusantara) pada tanggal 9/8/2020 2019, oleh William (direktur PT Usaha Kreatif Indonesia) pada tanggal 9/9/2020, dan oleh Charlie Anthony (direktur PT Usaha Kreatif Pembayaran) pada tanggal 9/9/2020. Terdiri dari 11 lembar.

1.	Perjanjian Penggunaan Layanan Komputasi Awan antara PT. Sigma Cipta Caraka dengan PT Usaha Kreatif Pembayaran No. TELKOMSIGMA: 496/SCC/UKTI/A/19 tanggal 3 Oktober 2019.

1.	Perjanjian Novasi antara TelkomSigma dengan PT. UKI dan Perseroan No. 564/SCC/UKTI/UKPB/A/2020, No. 0001/PKS-UKI/XI/2020, No. 0001/PKS-UKP/XI/2020, yang dibuat pada tanggal 9 November 2020.

## Bab VIII: Perjanjian Afiliasi

31.	Copy Sesuai Asli Surat Nomor PT UKI: 0001/PKS-UKI/IX/2019, Nomor PT UKP: 0001/PKS-UKP/IX/2019, tertanggal 21-11-2019, dengan judul: Perjanjian Pemakaian Bersama Unit 16 OF 5 Pakuwon Center Antara PT Usaha Kreatif Indonesia Dengan PT Usaha Kreatif Pembayaran. Terdiri dari 7 lembar.

1.	Perjanjian Sewa Menyewa antara PT Usaha Kreatif Indonesia dengan PT Usaha Kreatif Pembayaran Nomor: 0001/PKS-UKI/III/2020; Nomor: 0001/PKS-UKP/III/2020, yang ditandatangani pada tanggal 03-03-2020 (tiga maret dua ribu dua puluh).

## Bab IX: Perkara

1.	Surat Keterangan Bebas Perkara yang dikeluarkan oleh Panitera Pengadilan Negeri Surabaya Nomor: W.14.UI/313/HK/XI/2020 tanggal 10 November 2020.

2.	Surat Keterangan Bebas Perkara yang dikeluarkan oleh Panitera Pengadilan Negeri Surabaya Nomor: W.14.UI/314/HK/XI/2020 tanggal 10 November 2020.

3.	Surat Keterangan Bebas Perkara yang dikeluarkan oleh Panitera Pengadilan Niaga Surabaya Pada Pengadilan Negeri Surabaya Nomor: W.14.U1.Hk.05/319/11/XI/2020/03 tanggal 17 November 2020, dan

4.	Surat Pernyataan Direktur Utama Nomor: 0017/SK-UKP/XI/2020 tertanggal 12 November 2020.


## Raw

8.	Copy Sesuai Asli Surat No: 121/1020/KPS tertanggal 15 Oktober 2020, dengan judul: Penawaran Jasa Audit atas Laporan Keuangan PT Usaha Kreatif Pembayaran Untuk Tahun Yang Berakhir 31 Desember 2019 dan September 2020. Terdiri dari 12 lembar.

20.	***Copy Sesuai Asli Surat tertanggal 26 Oktober 2020 dengan judul: Pernyataan Direksi. Surat berisikan pernyataan atas nama Fenny Chiurman selaku Direktur PT Usaha Kreatif Pembayaran, bahwa PT Usaha Kreatif Pembayaran telah melaksanakan kewajiban pembayaran Upah Pohok seluruh karyawan sesuai dengan Upah Minimum Kota Surabaya. Terdiri dari 1 lembar.***


33.	Copy Sesuai Asli printout Nomor Permohonan Merek: JID2018068552, tanggal penerimaan: 27 Desember 2018, status: (TM) Pemeriksaan Substantif 1 (DISTDOC), tanggal Pengumuman: 25 September 2020, tanggal dimulai perlindungan: 27 Desember 2018, untuk Kelas NICE: 36, Pemilik: PT. Usaha Kreatif Indonesia. Terdiri dari 1 lembar.
34.	Copy Sesuai Asli printout Nomor Permohonan Merek: JID2018068554, tanggal penerimaan: 27 Desember 2018, status: (TM) Pemeriksaan Substantif 1 (DISTDOC), tanggal Pengumuman: 25 September 2020, tanggal dimulai perlindungan: 27 Desember 2018, untuk Kelas NICE: 38, Pemilik: PT. Usaha Kreatif Indonesia. Terdiri dari 1 lembar.
35.	Copy Sesuai Asli Kronologis Perseroan PT Usaha Kreatif Pembayaran, dari tanggal 21 November 2019 hingga tanggal 1 Oktober 2020. Terdiri dari 4 lembar.
43.	Copy Sesuai Asli Surat Keterangan Terdaftar No. S-4794KT/WPJ.11/KP.0503/2019 tanggal 28 November 2019, atas nama PT. Usaha Kreatif Pembayaran, NPWP: 93.584.598.2-607.000. Terdiri dari 1 lembar.
44.	Copy Sesuai Asli **Surat Keterangan Domisili Perusahaan** No. 058/SKDP/PC/XII/19 tanggal 04 Desember 2019, berlaku sampai dengan 31 Desember 2020, atas nama PT Usaha Kreatif Pembayaran. Terdiri dari 1 lembar.
45.	Asli dokumen tertanggal 03 November 2020 dengan judul: Pernyataan Direksi. **Surat berisikan pernyataan atas nama Charlie Anthony selaku Direktur Utama PT Usaha Kreatif Pembayaran, bahwa PT Usaha Kreatif Pembayaran sedang tidak memperoleh fasilitas pinjaman kredit dari lembaga perbankan atau bentuk perjanjian pinjaman lain dengan lembaga pembiayaan manapun yang serupa dengan hal tersebut. Terdiri dari 1 lembar.**
46.	Copy Sesuai Asli Surat Nomor 151/LTR-OPR/TPR-PC/X/2020 tanggal 12 Oktober 2020, Hal: Tanggapan Permohonan Permintaan Data Penunjang. Terdiri dari 1 lembar.
47.	Copy Sesuai Asli Keputusan Kepala Dinas Lingkungan Kota Surabaya Nomor: 188.4/992/Kep/436.7.12/2020 tentang Izin Lingkungan Kegiatan Komplek Tunjungan Plaza Mall Oleh PT Pakuwon Jati Tbk di Jalan Basuki Rahmat No 8 – 12 Kelurahan Kedungdoro Kecamatan Tegalsari Kota Surabaya, ditetapkan tanggal 22 Juli 2020. Terdiri dari 4 lembar.
48.	Copy Sesuai Asli Surat Keterangan Terdaftar No. S-1324KT/WPJ.11/KP.0503/2018 tanggal 30 Agustus 2018, atas nama PT. Usaha Kreatif Indonesia, NPWP: 85.750.142.3-607.000. Terdiri dari 1 lembar.
49.	Copy Sesuai Asli Kartu Nomor Pokok Wajib Pajak No: 85.750.142.3-607.000 atas nama PT. Usaha Kreatif Indonesia, yang dikeluarkan oleh Kantor Pelayanan Pajak Pratama Surabaya Tegalsari. Terdiri dari 1 lembar.
50.	Copy Sesuai Asli Nomor Induk Berusaha (NIB) No. 8120104992041, atas nama PT Usaha Kreatif Indonesia, diterbitkan tanggal 24 September 2018, perubahan ke-2 tanggal 24 September 2018. Terdiri dari 1 lembar. Terlampir:
	1.	Copy Sesuai Asli tabel Kode KBLI untuk NIB 8120104992041. Terdiri dari 1 lembar.
51.	Copy Sesuai Asli Surat Keterangan Domisili Perusahaan No. 032/SKDP/TPR-PC/VIII/18 tanggal 21 Agustus 2018, atas nama PT Usaha Kreatif Indonesia. Terdiri dari 1 lembar.

