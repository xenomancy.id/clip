Bahwa saksi yang diajukan oleh **Penggugat** atas nama **Santoso** di bawah sumpah memberikan keterangan sebagai berikut:

1. Saksi tidak tahu masalahnya karena apa, dan kenapa dirinya bisa berada di perkara ini. Yang Saksi tahu, Saksi dimintai keterangan karena masalah perceraian.

1. Saksi hanya diminta jadi saksi oleh Penggugat, lalu disanggupi Saksi. Sejauh pengetahuan Saksi, Saksi hanya dimintai menerangkan bahwa Saksi tidak pernah bertemu dengan Penggugat.

1. Mengenai gugatan perceraian sebelumnya, Saksi tidak tahu. Setahu Saksi, Penggugat dan Tergugat baik-baik saja.

1. Saksi tidak tahu kapan Penggugat dan Tergugat melangsungkan perkawinan, tapi setahu Saksi mereka suami isteri. Saksi tahu Para Pihak mempunyai dua anak, anak perempuan dan anak laki-laki.

1. Sekitar tahun 2005, kemana-mana Penggugat dan Tergugat selalu jalan berdua. Sering ketemu Saksi di jalan, itu mereka berduaan. Tidak pernah saksi dengar mereka bertengkar.

1. Saksi tidak pernah bersosialisasi dengan Penggugat, hanya kenal sebagai tetangga. Tidak pernah keluar dari pintu, hanya kelihatan di jalan. Kalau lewat biasanya klakson.

1. Saksi dulu tinggal di belakang rumah Penggugat dan Tergugat. Saksi sudah tinggal di sana sejak tahun 1958, lalu pada tahun 2005 pindah rumah sekitar 200 meter dari rumah Penggugat. Beda gang, beda RT dan RW.

1. Saksi tahunya Penggugat dan Tergugat tinggal di Jalan Sikatan Nomor 72.

1. Saksi tidak ingat sejak kapan Penggugat dan Tergugat pindah ke rumah Sikatan. Setahu Saksi, pada saat Saksi mulai tinggal di sana, awalnya tidak ada Penggugat dan Tergugat.

1. Sudah 12 tahun Saksi tidak ketemu Penggugat. Ketemu lagi pas tahun 2018, itu pada saat apel di lapangan. Pada waktu itu Penggugat ada bersama-sama dengan Tergugat. Tau

1. Waktu klakson-klakson pas ketemu di jalan, Penggugat tidak pernah bersama-sama dengan Tergugat, seringkali dengan teman laki-laki Penggugat, atau sendirian.

1. Soal Penggugat pisah dengan Tergugat, Saksi tidak tahu. Saksi tahunya selama 12 tahun itu tidak bertemu. Saksi tidak tahu kenapa bisa tidak bertemu lagi, atau kalau ada pertengkaran di antara Penggugat dan Tergugat. Selama tidak ketemu itu, Saksi tidak tahu Penggugat ada di mana.

1. Saksi tidak pernah bersosialisasi dengan Tergugat. Saksi tahu dari isteri Saksi, karena Tergugat sering ikut arisan dengan isteri Saksi.

1. Soal rumah dijual, setahu Saksi yang dijual bukan rumahnya, hanya gudangnya saja. Rumah itu ada dua, satunya rumah tinggal, yang satunya rumah gudang. Gudang bergabung dengan rumah. Saksi tidak tahu siapa yang menjual dan karena apa, Saksi hanya tahu di atas gudangnya ada ditulis, "Gudang Dijual," jadi bukan rumahnya.

1. Sepengetahuan Saksi, gudang itu dulunya gudang snack-snack, hanya sudah kosong sekarang. Sudah hampir 2 tahunan tidak ada kelihatan pegawai di situ.

1. Saksi tahu Tergugat tinggal di rumah tinggal Jalan Sikatan, sampai sekarang. Setahu saksi, rumah ditinggali Tergugat dan 2 anaknya.

1. Saksi mengetahui bahwa Penggugat masuk partai Demokrat.

Bahwa berdasarkan pada keterangan saksi atas nama **Santoso**, dapat disimpulkan:
1. Bahwa **Penggugat** dan **Tergugat** tidak pernah terlihat tidak baik-baik saja.
1. Bahwa rumah yang dijual