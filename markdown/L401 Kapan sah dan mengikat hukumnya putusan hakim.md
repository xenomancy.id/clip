# Kapan Putusan Pengadilan Sah Dan Berkekuatan Hukum

Pada putusan ada tanggal musyawarah, dan tanggal dibacakan di muka umum.
Jadi kapan putusan itu sah dan mengikat?

Pasal 13 Undang-Undang Nomor 48 Tahun 2009 tentang Kekuasaan Kehakiman menentukan:

1. Semua sidang pemeriksaan pengadilan adalah terbuka untuk umum, kecuali undang-undang menentukan lain.
1. Putusan pengadilan hanya sah dan mempunyai kekuatan hukum apabila diucapkan dalam sidang terbuka untuk umum.
1. Tidak dipenuhinya ketentuan sebagaimana dimaksud pada ayat (1) dan ayat (2) mengakibatkan putusan batal demi hukum.

