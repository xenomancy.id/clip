# B046 Kesimpulan

Summary alur finalnya.

1. Fakta cerai gugatannya telah didaftar pada kapan.
1. Bahwa atas gugatan telah dilakukan mediasi telah terjadi namun gagal.
1. Penyerahan jawaban.
1. Soal eksepsi, pada jawaban, ditegaskan dalam duplik, sehingga mematahkan replik. Karenanya, tidak terpenuhinya syarat formil gugatan, demi kepastian hukum wajar dong tidak diterima.
1. Pengajuan alat bukti dari Penggugat, surat maupun saksi.
1. Pengajuan alat bukti dari Tergugat, surat maupun saksi.
1. Keterangan saksi penggugat. Tidak perlu detil, parafrase hingga intinya saja yang relevan dengan kasus. Fokus ke fakta bukan detil.
1. Kesimpulan keterangan saksi penggugat. Fokus pada kesimpulan yang mendukung argumen kuasa hukum. Mengenai validitas saksi dapat belakangan, *in casu* menekankan saksi memberi keterangan *testimonium de auditu*, sehingga wajar untuk dikesampingkan.
1. Penekanan mengenai saksi-saksi penggugat, pada perkara *aquo*, dari ketiga saksi tidak ada yang mendukung dalil-dalil permohonan gugatan cerai *aquo*, khusunya masalah 2 dari 3 saksinya ternyata saksi *testimonium de auditu*. Akibatnya bertentangan dengan hukum materiil dan formil, berdasarkan 1907BW dan 171 HIR. Selain itu, pokok dalilnya Pasal 19 uruf f PP 9/1975, lalu pada Pasal 22 ayat (2) PP 9/1975, gugatan dapat diterima pabila cukup jelas sebab-sebab perselisihan dan pertengkaran dan setelah mendengar pihak keluarga serta orang-orang yang dekat dengan suami-isteri itu. Agus ternyata kurang mengenal penggugat dan tergugat. Jadi saksi-saksi penggugat tidak relevan dan tidak bernilai.
1. Keterangan saksi tergugat. Tidak perlu detil, parafrase hingga intinya saja yang relevan dengan kasus. Fokus ke fakta bukan detil.
1. Kesimpulan keterangan saksi tergugat. Fokus pada kesimpulan yang mendukung argumen kuasa hukum.
1. Bagian eksepsi addressed here. About the formil requirements of a gugatan. Still insisting about the Permohonan Gugatan Cerai, on the ambiguity of *gugatan voluntair* and *contentiosa*, that causes ambiguity and would violate Asas Kepastian Hukum. That's why it should be rejected.
1. Pokok Perkara. Focusses on:
   - Terjadi perkawinan.
   - Ada 3 anak dalam perkawinan.
   - Tentang dalil terjadi pertengkaran terus menerus, bukti berkata sebaliknya, jadi diuraikan. Soal pergi dari 2009 hingga 2017 ternyata untuk kerja. Saksi Tergugat ada yang bilang sudah harmonis hubungan mereka, lebih harmonis dari sebelumnya.
   - Dalil tidak merasa kepala keluarga tidak dapat dibuktikan oleh Penggugat, justru bukti tergugat berkata sebaliknya.
   - Pembuktian soal keharmonisan keluarga dari alat bukti Tergugat.
   - Ternyata kadang penggugat sendiri yang berinisiatif untuk liburan bersama.
1. Pasal 1865 BW dan Pasal 163 HIR dimention di sini. Baru ditekankan karena tidak berhasil Penggugat membuktikan dalilnya, harus ditolak atau setidaknya tidak dapat diterima.
1. Berdasarkan fakta persidangan ternyata terbukti oleh Tergugat bahwa keluarga harmonis, tidak berselisih dsb, sehingga tepat menurut hukum apabila hakim menolak gugatan yang diajukan untuk seluruhnya.
