# Koleksi Peraturan Perundang-Undangan

## Pertanahan
| Label | Peraturan | Tentang | Keterangan |
| --- | --- | --- | --- |
| U03 | UU 5/1960 | Peraturan Dasar Pokok-Pokok Agraria | UUPA |
| KP01 | PP 10/1961 | Pendaftaran Tanah | Dicabut PP 24/1997 |
| KP02 | PP 24/1997 | Pendaftaran Tanah | Diubah dengan PP 18/2021, mencabut PP 10/1961 tentang Pendaftaran Tanah |
| KP03 | PP 40/1996 | Hak Guna Usaha, Hak Guna Bangunan Dan Hak Pakai Atas Tanah | Dicabut PP 18/2021 |
| KP05 | PP 224/1961 | Pelaksanaan Pembagian Tanah dan Pemberian Ganti Kerugian | Diubah dengan PP 41/1964 |
| KP06 | PP 41/1964 | Perubahan dan Tambahan Peraturan Pemerintah No.224 Tahun 1961 Tentang Pelaksanaan Pembagian Tanah dan Pemberian Ganti Kerugian || KM01 | Perkaban 3/1997 | Ketentuan Pelaksanaan Peraturan Pemerintah Nomor 24 Tahun 1997 tentang Pendaftaran Tanah | Telah diubah dengan Perkaban 8/2012 dan Permen ATR 7/2019 |
| KM02 | Perkaban 8/2012 | Perubahan Atas Peraturan Menteri Negara Agraria/ Kepala Badan Pertanahan Nasional Nomor 3 Tahun 1997 Tentang Ketentuan Pelaksanaan Peraturan Pemerintah Nomor 24 Tahun 1997 tentang Pendaftaran Tanah | Mengubah Perkaban 3/1997 |
| KM03 | Permen ATR 7/2019 | Perubahan Kedua atas Peraturan Menteri Negara Agraria/ Kepala Badan Pertanahan Nasional Nomor 3 Tahun 1997 tentang Ketentuan Pelaksanaan Peraturan Pemerintah Nomor 24 Tahun 1997 Tentang Pendaftaran Tanah | Mengubah Perkaban 3/1997 |
| KM04 | Perkaban 9/1999 | Tata Cara Pemberian dan Pembatalan Hak Atas Tanah Negara dan Hak Pengelolaan | *Masih berlaku?* |
| KM05 | Perkaban 2/2013 | Pelimpahan Kewenangan Pemberian Hak Atas Tanah dan Kegiatan Pendaftaran Tanah | |
| KM06 | Permen ATR 15/2018 | Pertimbangan Teknis Pertanahan | *Ada perubahan?* |