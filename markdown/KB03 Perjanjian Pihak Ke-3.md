# KB03 Perjanjian Pihak Ke-3

## Perjanjian Sewa Menyewa
1. Akta Sewa Menyewa Nomor 138, tanggal 28 Pebruari 2020, dibuat di hadapan Fenty Abidin, Sarjana Hukum, Notaris di Jakarta, antara Zulkarnaen Tanzil (“**ZT**”) selaku pemilik ruangan yang disewakan dengan Perseroan selaku penyewa (“**Perjanjian Sewa Perseroan-ZT**”), yang mengatur hal-hal sebagai berikut:

  **Objek Perjanjian:**

  Objek Perjanjian Sewa Perseroan-ZT adalah sewa menyewa 2 ruangan yang terletak di Gold Coast Office Tower, Tower Eiffel, Lantai 18, Unit K dan Unit L, seluas 133m2 (seratus tiga puluh tiga meter persegi) dan seluas 146m2 (seratus empat puluh enam meter persegi). Yang dimiliki oleh Zulkarnaen Tanzil berdasarkan Perjanjian Satuan Rumah Susun Gold Coast Office Tower tertanggal 20-12-2018 (dua puluh desember dua ribu delapan belas), Nomor GCOT/PERJ/1218/000025.

  **Jangka Waktu Perjanjian:**
 1. Perjanjian Sewa Perseroan-ZT berlaku bagi Perseroan dan ZT untuk jangka waktu 5 (lima) tahun yang dimulai pada tanggal 1 Maret 2020 (satu maret dua ribu dua puluh) dan akan berakhir pada tanggal 01-05-2025 (satu mei dua ribu dua puluh lima).
 1. Perjanjian Sewa Perseroan-ZT dapat diperpanjang dengan pemberitahuan tertulis dalam waktu 3 (tiga) bulan sebelum jangka waktu persewaan berakhir.


1. d

## Perjanjian Jasa Konsultasi
1. Perjanjian Jasa Konsultasi Layanan Pemenuhan Kepatuhan PBI 18/40/PBI/2016 Lingkup Dompet Elektronik Antara PT Proxsis Manajemen Internasional dan PT Usaha Kreatif Pembayaran No. 051/SPK/CON/PMI/VII/2020 tertanggal 14 Juli 2020. Perjanjian antara PT Proxsis Manajemen Internasional sebagai Pihak Pertama dan Persero sebagai Pihak Kedua ini dibuat secara di bawah tangan dan telah ditandatangani serta bermeterai cukup, mengatur hal-hal sebagai berikut:

 **Lingkup Pekerjaan:**
    - Pihak Pertama melaksanakan pekerjaan dengan mengacu kepada dokumen proposal sebagaimana yang disebut dalam proposal penawaran (No.049/ITC/PGS-USA/20200626-00, tanggal 26 juni 2020).
    - Sebagai bukti pelaksanaan pekerjaan, Pihak Pertama wajib menyerahkan dokumen hasil Pekerjaan kepada Pihak Kedu yang dibuktikan dalam Berita Acara Serah Terima (BAST) Pekerjaan sesuai dengan jadwal pelaksanaan Pekerjaan berikut perubahannya yang merupakan bagian yang tidak terpisahkan dari perjanjian ini.

 **Hak dan Kewajiban Pihak Pertama:**
-
   - Pihak Pertama berhak mendapat dukungan dari manajemen dan tim *counterpart* Pihak Kedua untuk ruang lingkup pekerjaan;
   - Berhak memperoleh informasi dan data atau dokumen lain yang dianggap perlu sehubungan dengan pelaksanaan Pekerjaan;
   - Wajib melaksanakan Pekerjaan;
   - Wajib membentuk tim studi yang ahli dan/atau menyediakan konsultan professional yang bertanggung jawab atas pelaksanaan tugas dan kewajiban Pihak Kedua;
   - Wajib melakukan pendampingan terhadap Konsultan dalam pelaksanaan Pekerjaan;
   - Wajib mematuhi petunjuk yang diberikan oleh Pihak Kedua, sepanjang petunjuk tersebut mengenai lingkup Pekerjaan;
   - Wajib menempatkan personlil pendamping yang berkompeten dan mempunyai keahlian di bidang Pekerjaan.
   - Wajib memberikan seluruh dokumen hasil Pekerjaan kepada Pihak Kedua.
   - Wajib menyerahkan laporan kemajuan Pekerjaan pada periode waktu yang disepakati dengan Pihak Kedua;
   - Wajib mempersiapkan Berita Acara Serah Terima (BAST) yang diperlukan.

  **Hak dan Kewajiban Pihak Kedua:**

   - Berhak menerima dokumen pendukung untuk melaksanakan pekerjaan yang diberikan Pihak Pertama;
   - Berhak memberikan petunjuk dan arahan terhadap Pekerjaan yang telah dan/atau akan dilakukan Pihak Pertama, termasuk namun tidak terbatas untuk memberikan penilaian atas kinerja yang telah dilakukan Pihak Pertama;
   - Berhak melakukan pengawasan dan/atau perubahan permintaan atas setiap pelaksanaan Pekerjaan agar terlaksana sesuai dengan jadwal yang ditentukan dalam proposal penawaran dan ketentuan-ketentuan yang ditetapkan oleh Pihak Pertama;
   - Wajib membantu Pihak Pertama untuk melakukan pengumpulan data, melakukan evaluasi, dan konsultasi di Lokasi Pekerjaan;
   - Wajib memilih dan menugaskan karyawan yang ditunjuk sebagai pendamping Pihak Pertama, yang memiliki keahlian sesuai kebutuhan, mengetahui keadaan Lokasi, dan dapat mengatasi masalah non teknis yang mungkin timbul selama dalam pelaksanaan workshop.
   - Wajib mengawasi dan melakukan koordinasi Pekerjaan sesuai ruang lingkup Pekerjaan.

 ** Kerahasiaan Informasi**

1. Perjanjian Jasa Konsultasi Layanan Pemenuhan Kepatuhan PBI 18/40/PBI/2016 Lingkup Uang Elektronik Antara PT Proxis Manajemen Internasional dan PT Usaha Kreatif Pembayaran No. 051/SPK/CON/PMI/VII/2020 tertanggal 14 Juli 2020. Mengacu kepada dokumen proposal sebagaimana yang disebut dalam proposal penawaran (No.049/ITC/PGS-USA/20200713-00, tanggal 13 juli 2020).

## Perjanjian Kerjasama
1. Perjanjian Kerjasama Merchant Disbursment No. 151/PKS-KCM/DD/IV/2020 tertanggal 22 April 2020, antara [_______].

5. Perjanjian Kerjasama Re-Penetration Testing Aplikasi Dompet Elektronik antara PT Usaha Kreatif Pembayaran dengan PT Adikarya Tata Informasi Nomor: 0001/PKS-UKP/IX/2020; Nomor: 030/PK-AS/IX/2020 tertanggal 7 September 2020.

6. Perjanjian Kerjasama Merchant Internet Nomor: 339/PKS-M/DK/IV/2020 tertanggal 22 April 2020, antara [_______].

## Perjanjian Jual Beli
4. Perjanjian Jual Beli Produk antara PT BILLFAZZ Teknologi Nusantara dan PT Usaha Kreatif Indonesia No. BILLFAZZ: 00028/BILLFAZZ/LEGAL/VII/2019, tanggal berlaku: 29 Juli 2019. Ditandatangani oleh Rico Ofna Putra (direktur PT BILLFAZZ Teknologi Nusantara) pada tanggal 29 Juli 2019, dan oleh William (direktur PT Usaha Kreatif Indonesia) pada tanggal 6 Agustus 2019.

5. Perjanjian Pengalihan Dan Amandemen Kesatu Atas Perjanjian Jual Beli Produk Oleh Dan Antara PT Usaha Kreatif Indonesia dan PT Usaha Kreatif Pembayaran dan PT BILLFAZZ Teknologi Nusantara No. UKI: 0002/PKS-UKI/IX/2020, No. UKP: 0002/PKS-UKP/IX/2020, No. BILLFAZZ: 00313/BILLFAZZ/LEGAL/IX/2020, tanggal berlaku: 8 September 2020. Ditandatangani oleh Rico Ofna Putra (direktur PT BILLFAZZ Teknologi Nusantara) pada tanggal 9/8/2020 2019, oleh William (direktur PT Usaha Kreatif Indonesia) pada tanggal 9/9/2020, dan oleh Charlie Anthony (direktur PT Usaha Kreatif Pembayaran) pada tanggal 9/9/2020.
