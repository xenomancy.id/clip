# Perjanjian Dalam Bahasa Asing

## Undang-Undang

1. Undang-Undang Nomor 30 Tahun 2004 tentang Jabatan Notaris, sebagaimana telah diubah dengan Undang-Undang Nomor 2 Tahun 2014 tentang Perubahan atas Undang-Undang Nomor 30 Tahun 2004 tentang Jabatan Notaris (UUJN)

1. Undang-Undang Nomor 24 Tahun 2009 Tentang Bendera, Bahasa, Dan Lambang Negara, serta Lagu Kebangsaan (UU Bahasa)

## Aturan hukum yang relevan
**Pasal 43 UUJN**

1. Akta wajib dibuat dalam Bahasa Indonesia.
   > **Penjelasan:** Bahasa Indonesia yang dimaksud dalam ketentuan ini adalah bahasa Indonesia yang tunduk pada kaidah bahasa Indonesia yang baku.

1. Dalam hal penghadap tidak mengerti bahasa yang digunakan dalam Akta, Notaris wajib menerjemahkan atau menjelaskan isi Akta itu dalam bahasa yang dimengerti oleh penghadap.

1. Jika para pihak menghendaki, Akta dapat dibuat dalam bahasa asing.

1. Dalam hal Akta dibuat sebagaimana dimaksud pada ayat (3), Notaris wajib menerjemahkannya ke dalam bahasa Indonesia.
   > **Penjelasan:** Penerjemah resmi dalam ketentuan ini antara lain penerjemah tersumpah yang bersertifikat dan terdaftar atau menggunakan staf pada kedutaan
besar negara asing jika tidak ada penerjemah tersumpah.

1. Apabila Notaris tidak dapat menerjemahkan atau menjelaskannya, Akta tersebut diterjemahkan atau dijelaskan oleh seorang penerjemah resmi.

1. Dalam hal terdapat perbedaan penafsiran terhadap isi Akta sebagaimana dimaksud pada ayat (2), maka yang digunakan adalah Akta yang dibuat dalam bahasa Indonesia.

**Pasal 27 UU Bahasa:**
Bahasa Indonesia wajib digunakan dalam dokumen resmi negara.
> **Penjelasan:** Yang dimaksud “dokumen resmi negara” adalah antara lain surat keputusan, surat berharga, ijazah, surat keterangan, surat identitas diri, akta jual beli, surat perjanjian, putusan pengadilan. 

**Pasal 31 UU Bahasa**

1. Bahasa Indonesia wajib digunakan dalam nota kesepahaman atau perjanjian yang melibatkan lembaga negara, instansi pemerintah Republik Indonesia, lembaga swasta Indonesia atau perseorangan warga negara Indonesia.
   > **Penjelasan:**

   > Yang dimaksud dengan “perjanjian” adalah termasuk perjanjian internasional, yaitu setiap perjanjian di bidang hukum publik yang diatur oleh hukum internasional, dan dibuat oleh pemerintah dan negara, organisasi internasional, atau subjek hukum internasional lain. Perjanjian internasional ditulis dalam bahasa Indonesia, bahasa negara lain, dan/atau bahasa Inggris.

   > Khusus dalam perjanjian dengan organisasi internasional yang digunakan adalah bahasa-bahasa organisasi internasional. 

1. Nota kesepahaman atau perjanjian sebagaimana dimaksud pada ayat (1) yang melibatkan pihak asing ditulis juga dalam bahasa nasional pihak asing tersebut dan/atau bahasa Inggris. 
   > **Penjelasan:** Dalam perjanjian bilateral, naskah perjanjian ditulis dalam bahasa Indonesia, bahasa nasional negara lain tersebut, dan/atau
bahasa Inggris, dan semua naskah itu sama aslinya. 
