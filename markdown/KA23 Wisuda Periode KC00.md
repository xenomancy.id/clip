# Yudisium Lulusan Semester Gasal 2020/2021 Wisuda Periode Desember 2020

Pendaftaran Yudisium Online, [here](https://bit.ly/Yudisium_MKN), maksimal tanggal 6 November 2020.

Siapkan:

1. Daftar Kumpulan Nilai yang sudah ditandatangani Dosen Wali dan Data Lulusan (diunduh di [sini](https://bit.ly/Blanko_Yudisium_MKn)).
1. Scan print out transkrip akademik dari cybercampus.
1. Scan fotocopy legalisir ijazah S-1 1 lembar.
1. Scan fotocopy legalisir transkrip S-1 1 lembar.
1. Scan fotocopy akte kelahiran.
1. Scan fotocopy legalisir ELPT yang telah mendapat verifikasi dari Pusat Bahasa Unair dengan skor minimal 475 sebanyak 1 lembar.
1. Tanda terima penyerahan tesis dan surat keterangan bebas pinjam buku dari Perpustakaan Unair. Akses penyerahan tesis dan keterangan bebas pinjam buku dari perpus unair di [sini](https://bit.ly/Upl_mandiri).
1. Tanda terima penyerahan tesis elektronik dan keterangan bebas pinjam dari Koleksi Hukum Fakultas Hukum Universitas Airlangga. Penyerahan softcopy tesis ke Koleksi Khusus FH Unair di [sini](https://bit.ly/koleksikhusus).
1. Scan surat keterangan publikasi jurnal ilmiah dan hasil validasi jurnal imiah oleh fakultas. Apabila jurnal sudah terbit maka cover serta halaman pertama jurnal yang menerbitkan publikasi juga dilampirkan.
1. Bukti screenshot publikasi jurnal ilmiah di Cybercampus Unair.
1. Scan bukti asli pembayaran SOP terakhir (Semester Gasal Tahun Akademik 2020/2021).
1. Scan KTM Asli.
1. Butki foto ijazah. Pengambilan foto paling lambat tanggal 16 November 2020 di Direktorat Pendidikan setiap hari kerja pada pukul 900-1430, atau secara online dengan menggunakan HP melalui laman [ini](https://pendidikan.unair.ac.id/webfoto).
1. File pas photo berwarna dengan latar belakang merah, memakai jas almamater (pria memakai dasi) ukuran 3x4 dan 4x6 (bentuk file JPEG).

Semua file, kecuali pas foto, digabung jadi 1 file pdf.
