# Kepemilikan Rumah Susun Komersial
Rumah Susun

Pasal 1 angka 1 UU 20/2011:
> Rumah susun adalah bangunan gedung bertingkat yang dibangun dalam suatu lingkungan yang terbagi dalam bagian-bagian yang distrukturkan secara fungsional, baik dalam arah horizontal maupun vertikal dan merupakan satuan-satuan yang masing-masing dapat dimiliki dan digunakan secara terpisah, terutama untuk tempat hunian yang dilengkapi dengan bagian bersama, benda bersama, dan tanah bersama.

Pasal 1 angka 3 UU 20/2021:
> Satuan rumah susun yang selanjutnya disebut sarusun
adalah unit rumah susun yang tujuan utamanya
digunakan secara terpisah dengan fungsi utama sebagai
tempat hunian dan mempunyai sarana penghubung ke
jalan umum. 

Pasal 1 angka 4 UU 20/2021:
> Tanah bersama adalah sebidang tanah hak atau tanah
sewa untuk bangunan yang digunakan atas dasar hak
bersama secara tidak terpisah yang di atasnya berdiri
rumah susun dan ditetapkan batasnya dalam
persyaratan izin mendirikan bangunan.

Pasal 1 angka 5 UU 20/2021:
> Bagian bersama adalah bagian rumah susun yang
dimiliki secara tidak terpisah untuk pemakaian bersama
dalam kesatuan fungsi dengan satuan-satuan rumah
susun.

Pasal 1 angka 6 UU 20/2021:
> Benda bersama adalah benda yang bukan merupakan
bagian rumah susun melainkan bagian yang dimiliki
bersama secara tidak terpisah untuk pemakaian
bersama.

Pasal 1 angka 10 UU 20/2021:
> Rumah susun komersial adalah rumah susun yang
diselenggarakan untuk mendapatkan keuntungan.

Pasal 1 angka 11 UU 20/2021:
> Sertifikat hak milik sarusun yang selanjutnya disebut
SHM sarusun adalah tanda bukti kepemilikan atas
sarusun di atas tanah hak milik, hak guna bangunan
atau hak pakai di atas tanah negara, serta hak guna
bangunan atau hak pakai di atas tanah hak pengelolaan.

Pasal 1 angka 18 UU 20/2021:
> Pemilik adalah setiap orang yang memiliki sarusun.

Pasal 1 angka 19 UU 20/2021:
> Penghuni adalah orang yang menempati sarusun, baik
sebagai pemilik maupun bukan pemilik.

Pasal 1 angka 20 UU 20/2021:
> Pengelola adalah suatu badan hukum yang bertugas
untuk mengelola rumah susun.

Pasal 1 angka 21 UU 20/2021:
> Perhimpunan pemilik dan penghuni sarusun yang
selanjutnya disebut PPPSRS adalah badan hukum yang
beranggotakan para pemilik atau penghuni sarusun.

Kepemilikan Bersama

Tanah Bersama

Kepemilikan Pribadi

Syarat pemegang SHMSRS

Permen ATR 29/2016

SHPSRS?
