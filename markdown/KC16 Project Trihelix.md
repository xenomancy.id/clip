# Project Trihelix
The premise is a wandering soul of someone that commits suicide given a second chance to redo his life.
In redoing his life, he attempted to find how he could be reincarnated, which in turn causes more problems: Diabolean invasion.
Meeting friends from neighboring worlds, they beat Diabolean invasion.

## Temporary Structure
### Part 1: Zone of Comfort.
Billy died.
He always wanted to.
He finally did.

### Part 2: A desire of something.
Before his eyes, his entire life, his entire regrets, flashed.
He realized there are many things he haven't done.
There are many things he didn't accomplish.
He regrets.

He wished that he could somewhat change it.
He wish he could be given a second chance.
But it was all impossible.
He died.

### Part 3: Enter an unfamiliar situation.
An old man meet him midway in nowhere. They sat together in a campfire, and the old man told stories about their life.
It was wonderful, it was colorful, and it was a good life.
When it comes for his turn, he didn't have much to say.

The old man asked, "do you wan't to change it?"

Billy woke up in his younger self, and discover that he is back in time, to the past.
That or this is a world similar to his past.
Whatever it means, he had a chance to redo everything.

But what for?
For that, he had to meet the old man again.
Knowing nothing on where to look for it, he planned his future.

Things got weirder once his friend, Henry, also gained his past self in a hypnosis session Billy initiated, when Henry was ill.

### Part 4: Adapt to it.
With their wisdom accumulated in their younger selves, the duo planned and executed a plan.
To achieve the goal, that is to meet the old man, they will need to approach it from the esoteric side, and the scientific side.
Henry went for the latter, and Billy went for the former.

Billy studied Theology, Esotericism, Antropology, Metaphysics, Computer Science, History, and Electronic Engineering.
Henry studied and researched into theoretical physics, engineering, material science, and industrial engineering.
Financially, they invest a lot and early on established Gettie Search Engine, knowing that on their previous lives, Google is an established search giant.

In this universe, there's no Google, there's Gettie Search Engine.
And with GSE, they had a new source of funds, to fund their study and research.
They started the Goetia Institute of Technology, to ensure that there is no shortages of researchers, by becoming an academy.
People would enroll and study under GIT, and so all the researches delegated to the undergrads.
To make things easier, they want to automate it, so they program an AI.
No, AIs.
No, tons of AIs.
None worked well.

Until one day, they are able to accidentally create an AI that just works, on training new AIs.
They name the system Digitalized Artificial Elastic Multimodal Orthographically Networked Expert System, DAEMON ES.
With Gettie's DAEMON ES, they started the age of AIs.
AIs created by DAEMON ES are very reliable and scalable, allowing various tasks to be automated and provides better insights for human decision makers.

With the advent of DAEMON ES, they made a better simulation of the universe, a better computer model for quantum system, advancing material science by creating an accurate model of materials, and many more.
And eventually, it helped Henry to develop a formula for a room-temperature superconductor.

They set up a business frontend to produce and market the superconductor: Goetia Industrial Technology, Ltd.
It was a boom, and they reap a lot of profit due to that.
They get distracted, and one day they realized that they've been off-track.
They're too focused on the implications of their research, that they forgot why they did it in the first place.

They started to divert some funds for their clandestine researches.
And to protect their researches without having prying eyes looking at them, they established Goetia Security Solutions, Ltd.
With Goetia Security Solutions, Ltd., they're overwhelmed again by the sheer administrative duties to manage all of their enterprises.

With many enterprises in their hands, they come up with a solution, decentralization.
So they established a parent company, Goetia International Holdings, Inc.

Having plenty of administrative duties and management duties delegated to the children companies, and the GIH merely oversees and direct the researches at high level, they finally have time to refocus.
With all the advancements that weren't available from where they started, they are surprised that they had been able to accomplish it many years ago, if not because of the distractions.

Finally, the Reality Disturptor Arrays is activated somewhere in the middle of an industrial complex located in Miring mountain.
A spherical wormhole opens, suspended in the middle of a hollow spherical near-vacuum region with magnetic suspension.

### Part 5: Get what they wanted.
Exploration to the wormhole revealed a surreal landscape.
Not exactly void, nor is there actually anything.
Sensors provided almost no readings.
But when a man enters, they enter a different world entirely.
It is different for everyone.
Whatever it was, the landscape is always familiar for those that observed it.

Whatever it was, the landscape is littered with many more wormholes, of various sizes.
They enter one, and discover RU.

RU is a world of chaos, there are fights with some sort of demons, they calls Diabolean. (outline this)

Finally they decided to enter the wormhole again, to return to FC.
But they were piqued by another hole that leads to MS. (outline the MS adventure too).

### Part 6: Pay a heavy price.
Returning to FC, they discovered that there are weird beasts, so out of this world, almost lovecraftian.
Termed Therozoids, these beasts doesn't seem to be mindless, they attack in coordinated manners.

Therozoids spread and wreck havoc, and the guys from RU recognized them as having the signature of Diabolean.
Guys from MS provided us help with their Frames, GFOR-108 ARMOR.
However it barely damages the Therozoids.
RU guys and Billy comes with a realization that it is possible to soul forge.
But early attempts were less than optimal, but a promising result could be seen.

With the help of Henry, they discovers that the problem is in the software, the Astrals refused to cooperate becaue it felt wrong, not alive, and cold, to interact with the GAZE System.
So DAEMON ES is tasked to develop a better AI: ACTIVE System.
With ACTIVE System, the Astrals feels comfortable, and they're able to control it freely and naturally.

Battle with the Therozoids are quite tough, mainly due to numbers, and some of them are simply huge and troublesome.

### Part 7: Return to a familiar situation.

### Part 8: Having changed.
