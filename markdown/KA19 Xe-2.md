# Xenomancy, Book 2: The Oneironauts

A couple found each other, but are now running from an unknown group of people that chased them.
They're captured, coerced, and was forced to work for the group, prevented to meet their lover.
Until one day, they offered a chance to escape this world and be together somewhere else.
Would the couple take the chance?

A list appeared out of an anomalous case Hendrik is investigating as a WTF officer.
Upon inspection, the names appear to be random, but one of the name was his name, with a wrong master title, followed with the name of his sister, also with a wrong title.
Who wrote that list and what does it mean?

## The Red Thread Of Fate (姻緣紅線)
Elbert Hardiman (a Vishnu soul) and Febrian Gautama (a Sperry soul) are tied by fate.
In this cycle, they are fighting for a better life, so that they could finally find their peace together.
Meanwhile, there are three more Apex souls that are involved in their struggle: a Brahma soul, a Shiva soul, and a Fermion soul.
This part would explore the interaction of those five Apex souls.

**Apex souls participating in this part.**

1. A **Vishnu** soul.
The most recent incarnation is **Elbert Hardiman** (nick: **Bert**, m).
Previously was **Lee Ah Ping** (m), and before that was **Liong In Chuang** (m).
A Vishnu soul is bestowed with the ability of preservation, that is, the ability to protect their immediate vicinity from reality bending.
1. A **Sperry** soul.
The most recent incarnation is **Febrian Gautama** (nick: **Ben**, m).
Previously was **Chen Yun Fei** (f), and before that was **Zhang Kim Fa** (m).
A Sperry soul is bestowed with the ability of altering someone's neurochemical makeups.
Therefore they can alter the state of consciousness of someone at neurochemical level, making them sleep, to be agitated, to be depressed, to be intoxicated, or any ways they see fit.
1. A **Shiva** soul.
The most recent incarnation is **Dominic Muerte** (m).
Previously was **Juan Muerte** (m), was an agent of the *Butterfly Initiative*, and he is a grandfather of his latest incarnation.
A Shiva soul possesses the ability of destruction.
With some mental effort, they can produce a field that would immediately annihilate anything within it into nothingness.
1. A **Fermion** soul.
The current incarnation is **Zarah Walidah** (f).
She is an agent of the *Butterfly Initiative*.
A Fermion soul possesses the ability of manipulating all fermionic particles, provided that they are focussed on what they're controlling, and know what transformation is done to those particles.
In effect, for all practicality, their power manifests in an indistinguishable manner from telekinesis.
1. A **Brahma** soul.
The most recent incarnation is **Hendrik Lie** (Integra version, m).
Previously was **Michael Carmichael** (m), a member of the *MTF unit "Samsara"*.
A Brahma soul possesses the power of creation, provided that they understand what they're doing.
Practically the most powerful form of reality bending could be performed, but the ability is very taxing for the user.
For small-scale reality bending on the scale of several centimeters, it is almost effortless.
However for large scale reality bending, encompassing a large area or volume of effect, it would take enormous focus on the user's side.

**Mobile Task Force Alpha-47 "Samsara"**

A task force of the **Department of Normalcy, Global Foundation of Esotericism.** Specialized in dealing with the Apex Souls.
In 1993, the MTF was led by Sergeant Chandra Watthuprasongkh.
It was a Section or a Squad composed of three Fireteams.

*1st Fireteam (**The Iron Fists**).*

- **Sgt. Chandra Watthuprasongkh** (Nickname: **Chan**, Thai script: "จัน" จันทรา วัตถุประสงค์) (m, 26). He is very close with Michael (calling him Nong Mich), as they went into training at a similar time, and spent years together. Highly determined to do his jobs, very friendly to his squad, and knows every single one of them very well. However, he's very emotionally attached to his squad, that losing a squad member is almost always devastating for him.
- **Pvt. 吳脚踏 (Go Jiaota)** (f, 23), usually called **Ms. Go.** A calm, quiet, and stern person. Dislikes unorganized actions, and always executes everything with precision and determination. Wouldn't twitch for every soul killed. Technically, she is also a medic.
- **Pvt. 王实地 (Wang Shidi)** (m, 24), usually called **Wang**. A highly reliable wingman when needed. However, he's quite messy when it comes to his own belongings. Does not hesitate to jump and save his colleagues, even when it may cost his life.
- **Pvt. Djunita** (f, 24), usually called **Djun**. A bright and cheerful lady, always trying to liven up her squad by throwing jokes (often bad or fail, but, well). Never wish to hurt anyone, and always try to get close with everyone.

*2nd Fireteam (**The Wizards**)*

- **Pvt. Michael "Mich" Carmichael** (m, 24). Is Integra Hendrik's previous incarnation, and is a Brahma soul. An agile reality bender, that can perform small-scale minor reality bending relatively quickly. A hard and wide-scale reality bending requires more time of focusing.
- **Barong Ket**, usually called **Ket**. An extrauniversal agent from *Paramundus Jagadpadang.* His human form is a lean but strong south asian male martial artist. His beast form is a male lion. Specializes in aural attack, projected energy, shielding techniques, and strategic planning. Can teleport, but only toward a spot in his direct line of sight. If he can't see it, he can't teleport there.
- **Barong Bangkal**, usually called **Bangkal**. An extrauniversal agent from *Paramundus Jagadpadang.* His human form is a heavily built aryan male. His beast form is a babirusa. Specializes in strength, and brute force.
- **Barong Macan**, usually called **Macan**. An extrauniversal agent from *Paramundus Jagadpadang.* His human form is a strong and fit javanese male. His beast form is a Bengal Tiger. Specializes in sensory acuity, clairvoyance, camouflage, and navigation. Can teleport in rapid succession, and is very precise at doing so.

*3rd Fireteam (**The Harem**).*

- **Pvt. Kang Haein** (m, 20), usually called **Haein**. A bright, cheerful, and playful person. Barely shows his emotions, always mask it with smiles and positive vibes. Very perceptive to other individuals, and highly sensitive to changes in mood, and emotional distress.
- **Pvt. Angelica Gears** (f, 22), usually called **Gears**. She is highly perceptive, a perfectionist, and likes to tidy her belongings and appearances. Very adaptive and resourceful in battle.
- **Pvt. Ninawati Berlina** (f, 21), usually called **Watt**. A strong woman, and a very precise grenade thrower.
- **Pvt. Emma Bright** (f, 20), usually called **Emma**. A hypersex and playful woman. At the same time being very emotionally matured, high self control (except for sex), and very intuitive. Also specifically trained to be a paramedic.

### Part 1: Contact
It begins with a normal day for Febrian (nick: Ben), preparing to enter his class.
It also begins as the first day for Elbert (nick: Bert) to be the assistant lecturer of a class.
And they attend the same class, one as a class attendee, and another as an assistant lecturer.

Have you ever felt like you've been waiting for someone you don't even know, but you know are there somewhere?
That you would know that someone when you see them, even though you have no idea who they are.
That at the moment you meet them, your heart rings, "That is the one."

At the same time, someone's wrist watch alarm is turned on.
The owner, at the corner of the class, was slouching, now fully alert, and gazed around.
He made a phone call, and his words were: "A Vishnu is detected."

### Part 2: Memories
That night, Ben and Bert were having a dream.
It was not just a singular dream, they both dreamed the same dream, of different perspectives.
In their dream, Ben was Chen Yun Fei, and Bert was Lee Ah Ping.

Lee Ah Ping and Chen Yun Fei managed to escape from a building.
Men with tuxedos chased them, and they stole a car.
A number of cars were chasing them, and gunshots could be heard.
Some bikes were able to go near, and Chen Yun Fei managed to touch some of them.
Those that are touched lost their balance and fell, they're hallucinating.

The chase continued, until someone came in front of them, clasping his hands hard.
The car Chen Yun Fei and Lee Ah Ping rode disappeared, and they fell to the ground.
The man approached them, but a giant babirusa came and threw the man away.

The babirusa looked at Chen Yun Fei and Lee Ah Ping, and it approached.
Chen Yun Fei and Lee Ah Ping ran away, and turned someone that was about to enter a car into somnambulism, and took his car.
Then a barrage of shots were chasing them, and high above the sky was a young woman, flying, with a bunch of giant stones floating around.
A stone came into her focus, and it rapidly disassembled into high velocity projectiles, shooting toward a running car.

Lee Ah Ping, not currently driving, aimed his palm at the woman, and she fell to the ground.
Lee Ah Ping returned to the car, zooming away to the interstates.
The woman managed to recover her power near the ground, and a great crack formed to the ground beneath her, as she pushed everything away to slow her fall.

In the car, Lee Ah Ping and Chen Yun Fei held hands, and Chen Yun Fei told him that she arranged a meeting with an argon gas supplier.
Chen Yun Fei explained that their previous suicide method often cause enormous pain, and she wanted her death at this cycle to be painless, as they suffered a lot already.
In their previous cycle, Chen Yun Fei was Zhang Kim Fa, and Lee Ah Ping was Liong In Chuang.
Cornered, with no foreseeable future, Zhang Kim Fa believed that they would fight together until the end, but Liong In Chuang betrayed him, by killing himself.
Chen Yun Fei told Lee Ah Ping, as Zhang Kim Fa, suddenly the world was torn apart, and he felt emptiness.
Imagine, suddenly your loved one was drop dead, no matter how many times he called, his love wouldn't answer.
That ultimate horror, took away his sanity, and he killed himself too with the same gun.
That is why, in this incarnation, Chen Yun Fei decided to use a pain free suicide method, and they'd be in it together.
Lee Ah Ping agreed, apologized as an incarnation of Liong In Chuang, and they headed to the industrial zone.

After taking the argon gas tank, they head to the countryside.
To avoid pursuit, they stopped in an interstate mart.
Chen Yun Fei induced somnambulism on a customer, and brought him in.
Then she turned the tender into a hallucination session, and they moved the gas tank to the customer's car.

They left the interstate, only to be chased by an army led by Juan Muerte, the guy that can cause things to disappear.
As a wave of disappearance approached their car, Lee Ah Ping concentrated and his radius of normalcy expanded beyond the car, causing an abrupt return to normalcy.

As Juan continued to shoot them with more disappearance field lines, Lee Ah Ping pulled his bow and with a stunt, shot three arrows lined with special metals toward Juan.
It wasn't aimed for Juan, but they were lodged in the cars.
Juan tries to launch another disappearance field attack, but apparently it doesn't work, as Lee Ah Ping's blood in his arrows produces a short-ranged normalcy field.

Then there was a bunch of cars from behind Juan's army, shooting them.
Some cars fled off road, and some explosion could be heard, as military cars were destroying Juan's army one by one.
Some military copters came by and attacked the Juan's army.
Then some military cars were flown away by unknown forces, and some of the military copters fled and crash landed ahead of Lee Ah Ping and Chen Yun Fei's car.

A woman landed far ahead, and destroyed the road by a movement.
Lee Ah Ping concentrated again, and refocused his normalcy field ahead, and the roads returned to normalcy.
The woman step aside before she's crushed by the car.
Then an arrow was shot by Lee Ah Ping, it pierced through the woman's leg, and she lost her power.
She can only gaze at the widening gap between her and the car.

After some distance away, Lee Ah Ping and Chen Yun Fei stopped in the middle of a tunnel, where a service boy happened to just left the tunnel's maintenance room.
Chen Yun Fei came out and induced somnambulism on him, and led him to open the door again for them.
They entered, and descended far down, to a small confined room within.
She put the maintenance boy to sleep, and Lee Ah Ping dropped the tank nearby.

They took their time, gazing at one another, and tears started to form.
They knew they'd be found eventually, they're just not lucky in this lifetime.
They'd be doing what they've been doing in many lifetimes already, that they couldn't remember how it all starts.
They've been chased away for many lifetimes, only to be taken and be forced to do things they dislike, and they couldn't meet each other.

Their dreams are simple, to live a peaceful life, together.
They couldn't fulfill that in this lifetime, so they need to find better luck in the next cycle.
Lee Ah Ping opened the valve of the tank, and they kissed, cried, hugged, as the argon gas filled the room.
The service boy fell unconscious first, as he's sitting by the corner.
It took some time after, and Lee Ah Ping and Chen Yun Fei fell unconscious as well, with their last memories being together, loving one another, in each other's grasp.

Ben woke up with sweats, and the feeling of emptiness dawned again.
It was him, that man in his dream, Lee Ah Ping, that he had been waiting for.
It was the most vivid, intense dream he had, and he knew he just met his lost love, that he is sure never been in his life before.
Not in this life anyway.

Bert woke up and tried to recollect what he saw in his dream, someone that he loved.
He could not make up the face of that person, he could only recollect the feelings he felt around that person.
The one that he felt just this morning, in the class that he taught.

### Part 3: The Butterflies and The Esoteric Group
"Vishnu and Sperry ran away," said Captain Chandra, the head of the Mobile Task Force Alpha-47 "Samsara", a part of the Department of Normalcy, Global Foundation of Esotericism, or GaFE.

"So, we will pretend to chase them?" asked Wang.

"Not just that, there are Butterfly Initiative forces interfering in this cycle," said Michael.

"So, what would we do?" said Haein.

"We will fight the butterflies first," said Sgt. Chandra.

A car disappeared, and a couple, who were passengers of the car, fell to the road, and Juan approached them.
Michael ordered Bangkal to intercept, which then proceeded to transform into a giant tank-sized babirusa.
He charged toward Juan, throwing him into the air, while Chen Yun Fei and Lee Ah Ping ran away from him.

Zarah can be seen floating with a lot of stones around her, and she rapidly disassembled one of the stones into high velocity projectiles.
Before Michael could do anything about it, she fell along with her stones, but managed to slow down her fall.
Michael knew it means Lee Ah Ping, a Vishnu soul, uses his ability to counter Zarah, a Fermion soul.

Michael turned the consistency of the paving stone Zarah stood on into that of a quicksand, and as her feet submerged on it, he returned it to concrete, trapping her.
She destroyed the paving stone, and gazed around to find the source of that witchcraft.
Stationed atop of a nearby campus building was Michael, Wang, and Sgt. Chandra, and they carefully observed Zarah.

Michael came up with an idea to enhance nine pairs of glasses to increase the wavelength of infrared to visible light, and turn them into makeshift lightweight night vision glasses, then distributed it to all human members.
That means they can see in the night as if it was a broad daylight.
Wang shot his grenade launcher at Zarah, and Zarah quickly deflected it toward Bangkal, whose explosion splattered him into a fleshy mess.

Zarah, agitated, looked at the origin point of the grenade, and formed a gun gesture with her palm, shooting an imaginary disruptor wave that rapidly disintegrated anything in its path, at the time aimed toward Chandra.
Looking at the approaching destruction wave, Wang pushed Chandra away and took the blow.
Disintegrated into dusts in front of Chandra's eyes, he screamed.
Chandra tried to collect Wang's dust, as if he could reconstitute it back to Wang.

Michael stepped in, and put Chandra's head down, so as not to be seen by Zarah.
Chandra sobs, hard, as Michael calmed him down, patting his back, and hugged him, while dragging him away from the roof.
Chandra still sobs, but Michael knew Zarah would fly toward them, and get them, so he gathered everyone in that building, that are Ms. Go, Djun, Ket, and Macan.
Michael asked everyone to physically touch Macan, then ordered Macan to initiate a teleportation jump to their designated safe location.

They could see the first building at a safe distance, and it collapsed into ashes, as Zarah crumbled it away.
Chandra still sobs, and Michael hugged him, until he calmed down, where Michael said that the loss is inevitable, and he must remain calm and collected.
They need to go to the next phase, and Chandra asks the status of their 3rd Fireteam, The Harem.
Private Haein reported as the team leader, that they're still following Chen Yun Fei and Lee Ah Ping toward a manufacturing plant, where they seemingly collect a tank of gas.
Michael, as the team leader of the 2nd Fireteam, The Wizards, asked Ket, how long would it take for Bangkal to return to this physical plane, and he said it would be another half an hour or so.

Go, that was in charge of observation, reported that Zarah and Juan are on the move, supposedly toward the target (Chen Yun Fei and Lee Ah Ping).
Chandra thought for a moment, and ordered The Wizards to catch up with the enemy units, currently chasing the target, because they're the only one capable of dealing with the Apex Souls (considering Michael is too, an Apex soul).
Chandra and the remaining team Iron Fists: Go and Djun, would catch up with their rover.
Djun expressed her dissatisfaction for not seeing the handsome men for the next move, and Michael said thank you, but she replied that he is not included.

Michael complied, and asked Ket for a ride, while Macan is in charge of leading them with the nonlocal jump points, as he's the one with clairvoyance.
Michael jumped on Ket's back, and both Ket and Macan shifted into their beast forms.
The Wizards initiated the first nonlocal jump, and The Iron Fists descended to the parking lot.

Michael, Ket, and Macan arrived right at the time a contact between the enemy units and the target occured.
Macan was told to stand by, while Ket charged toward Juan to take him down, but instead got shot with one of Lee Ah Ping's normalcy arrows.
Ket abruptly reverted to his human form, with an arrow penetrating his right shoulder from behind.
Ket runs away from the battlefield in pain, as the BIs are shooting him.

Michael noticed he couldn't use his powers near Ket, and proceeded to give him first aids.
As Macan came near them, he reverted back to human form unwillingly, and noticed that he could revert back to his beast form when he is at least three meters away from Ket.
Michael realized that the arrow is the cause, as it is made with the same metal as the core circuitry of GaFE's Sperry-Vishnu Reality Anchor System.

Bangkal manifested near Macan, stating that he couldn't manifest near Ket.
With a cue from Michael, Bangkal immediately transforms into a giant babirusa, charging toward one of the cars of the Butterfly Initiatives.
Some cars fled off road, and some explosions occurred as Bangkal smashed their engines.

Macan screamed to Michael and Ket, that The Harem, that was following their target, had their car crushed by Zarah, and he immediately jumped there to save the team members.
The full team of The Harem and The Wizards gathered, while Bangkal wrecked havoc on BI's fleet.
Emma came by and inspected Ket's wound, and "checked" other signs.

Haein and Watt advance to open fire for the BIs, distracting them, and allowing Bangkal to keep wrecking havoc to their fleet.
Gears and Macan observed and scouted the battlefield to determine the best tactic.
Bangkal told them that he was nearing his limits, and he requested Ket to come into battle.

Realizing that to kill Ket means he'd be unavailable for the next half an hour or so, Michael asked Emma to pull the arrow through, and just band up the wound, so Ket could quickly resume battle.
Ket objects, but Emma kissed Ket immediately, and Michael pulled the arrow through, followed by Ket's scream.
Banded tight, and the wounds cleaned, Ket transformed into his beast form, that, albeit substantially weaker and unable to properly use his right front feet, able to jump in and provide shielding against the open fire aimed toward Bangkal.

Gears reported that The Iron Fists are coming with a copter, and Zarah flew toward the copter.
Macan quickly teleported to the cockpit of the copter and took The Iron Fists to reunite with Michael.
The Iron Fists, with Michael, had to take a civilian car, under the pretense of emergency and the war ahead, and advanced to catch up with Haein and Watt, already far ahead with a sport bike.
Gears was jumping from points to points with Macan to provide reconnaissance for the entire MTF.
Ket and Bangkal duet keeps chasing and trashing BI fleets.

Up ahead, the roads were destroyed and shattered, with the rubbles floated about.
However, Lee Ah Ping shot the arrow to her leg, and she was unable to use her power, and the road was left with a narrow passage of normal path, while the rest were devastated.
The Samsara MTF was stopped by the cars of BIs stopping as well due to the road blockages, and crossfire occured.

With Juan's power intact (as long as he's away from the arrows), he delivered an attack to Ket, and Ket's right arm was decapitated, as his right elbow is shrunk into nothing.
In pain, Ket reverted to his human form, and Watt shot Juan to prevent his pursuit.
Juan was shot, but managed to suck out Watt's upper body out of existence.
Then, at last Emma threw the arrow that was in Ket's body to Gears, then stabbed it to Juan's thigh, which resulted in his loss of power again.

Seeing that the powers of the opponents Apex souls were disabled, Michael and Sgt. Chandra realized it is their chance to advance.
They took two cars, one car for Sgt. Chandra, Ms. Go, and Djun, while the other are for Haein, Gears, Emma, and Ket.
Bangkal and Macan would be in their beast form, clearing their path ahead, while Michael turned those two cars with his remaining capacity to their standard issue Operational Armored Electric Vehicles (OAEVs), and then rested in Sgt. Chandra's car.

Michael ordered Macan to chase and tail the target in stealth, so they could keep an eye to the target.
Michael told Sgt. Chandra that he's nearing his limit, that he could no longer perform wide scale or complex reality bending due to his mental fatigue, resulting in an intense headache.
Sgt. Chandra said it would be okay, and they're hoping it wouldn't be necessary for the remaining of the day.
Ms. Go is the one driving.

Ket said that his arm is so severely damaged, that he wouldn't grow it back unless he is to die and return to this plane.
Haein said perhaps it should be done after they're back to the base, as they couldn't afford the wait time until he returned to this plane, and they might need Ket's ability anytime soon.
Haein then discussed with Gears about their inventories, while Ket and Emma decided to have fun in the back seat.

Macan tailed the target until they entered a tunnel.
On which Macan realized that Lee Ah Ping threw his arrows around the tunnel and along the tunnel, as he was forced to revert back to his human form due to their presence.
Carefully he observed that they entered a maintenance shaft, and couldn't do much as they locked the entrance to the shaft.

Macan left the effective zone of the arrows, and jumped back to Sgt. Chandra's car.
His sudden appearance to the car, naked, distracted Djun and Ms. Go for a split second that they almost went out of the road.
Macan reported his findings to Michael, which then ordered him to return there and monitor, if anything weird happens, and try to identify and clear away the arrows for when they finally arrived there.
Michael handed him spare clothing so he could operate there without attracting attention.
Macan promptly obeyed it and jumped off the car.

Macan had just finished cleaning up the arrows, when two OAEVs and a giant babirusa came by the tunnel.
Michael ordered Ket to guard the entrance of the tunnel, and Bangkal on the other end.
Haein took his team with Ket, and Sgt. Chandra joined Bangkal, as Michael and Macan are to engage with the two Apex souls.

Michael transmuted the maintenance hatch into mercury, and then returned the composition as they melted away.
Macan proceeded to enter first, and Michael followed.
As they went through the maintenance tunnel, Macan told Michael to stop, and return to the surface, as he detected Argon gas, and that he's the only one able to pass through the gas unharmed, while Michael would suffocate.

Macan found three dead bodies in a confined room, full of argon gas.
One is a maintenance officer, then Chen Yun Fei, and Lee Ah Ping.
He teleported it back to the surface, on which they loaded the bodies to Haein's car.

Zarah appeared in the entrance, and was held back with Ket and Haein's team.
Haein realized they're overwhelmed, and retreated back to their car, while Ket held them up with the shield.
They informed Michael, who quickly entered Sgt. Chandra's car, and turn it on.

Macan went ahead to Sgt. Chandra to warn them, only to find that Juan Muerte is already facing Bangkal and Sgt. Chandra's team.
Bangkal's body was annihilated, sucked into nothingness, followed by Ms. Go, and Djun.
Sgt. Chandra was almost annihilated as well, but Macan came just in time and teleported him back to his car, where Michael is driving.

Macan took over the steering wheel in his human form, as Michael came to the back and calm Sgt. Chandra down, that was emotionally agitated when two more of his team members died in front of his eyes.
Michael hugged him up, shook him up, and shouted to get him back to his senses.
Michael reminded him, to keep calm now, mourn later.
Loss is normal in a battle like this, and mourning is necessary for them to move on, but he must not mourn in the battlefield.

Macan told Michael that they lost Ket on the other end, so behind them, the tunnel collapsed due to Zarah's power, while at the other end the tunnel was disappearing into open space, due to Juan Muerte's power.
Michael realized what it means, and he fixed his gaze to Sgt. Chandra.

"*Khobkhun mak nah Phi*, thank you very much," said Michael.

"No, you can't do this," said Sgt. Chandra.

Sgt. Chandra's sight blurred with his forming tears.
Michael's arms wrapped around Sgt. Chandra, whose arms wrapped around Michael in response.
Michael's deep breath could be felt through their chest,

"P'Chan, I'm honored to work in your Section," he paused, "and let me do the honor to protect this Section."

"No, No, don't do this, Nong Mich. Please don't."

"*Phoom khothut*, I am sorry, P'Chan. I must do this."

Tears started to form in Michael's eyes, but he didn't waver, and immediately turned to Macan.

"Macan, keep P'Chan safe."

"Affirmative."

Before Sgt. Chandra could react, Michael jumped over the hatch, and with all of his might, jumped toward Juan Muerte.

Juan Muerte extruded all of his strength to cause Michael to disappear, but with all of his remaining might, against all of his headache intensity, fought back with his power of creation.
The power of Shiva the destroyer, and the power of Brahma the creator, tasted each other at the moment.
The ground around them quivers.

One to create, another to destroy.

With no Vishnu's power to preserve, it was pure destruction and creation clashing with each other.

Macan and Haein navigated their cars away from Michael and Juan Muerte.
Their body stood still, each with their own stance.
Each and every vein of their body screamed, and boiled.
Lights could be seen to extrude from their skin, which peeled off violently.
That night, two screams could be heard.

Sgt. Chandra opened the hatch and looked back to his Nong Mich.
He could see the lights from the two still bodies, he believed he could see the two bodies quiver.
The next thing he saw was a rapidly growing pitch black orb, devouring the hill, and the tunnel.
Then, the pitch black orb cracked and a second sun emerged from its inside.

Sgt. Chandra squinted his eyes so much, he didn't see any of those directly.
But that night, everybody around the site, despite their orientation from the orb, disregarding the closure of their eyelids, saw a very sudden, very brief, high intensity blinding blue light, overwhelming their sights.

### Part 4: A New Cycle
Note: in this part, Sgt. Chandra, is now 60 years old, and has been promoted into a Colonel.
Currently the head of the Department of Normalcy, Global Foundation of Esotericism.
Pvt. Haein, is now 54 years old, and has been promoted into a Lt. Colonel.
Haein is Chandra's second in command.

Col. Chandra was all wet when he found himself on the couch of his ready room.
Not only was his body wet with sweats, his face was also wet with tears and mucus.
He found himself panting, and an intense pressure could be felt on his chest.
The next thing he heard was his own cry, and the noise made by Lt. Colonel Kang Haein as he barged in.

"*Hyeong!*" shouted Haein.

Col. Chandra can only hear himself coughing hard, and his palm grasped and pressurized his chest.

"*Gwaenchanh ni, hyeongje!* Are you okay?" Haein said.

Haein's handkerchief wiped Col. Chandra's tears, while Haein's arm supported and repositioned Col. Chandra's body to the couch.
Col. Chandra held Haein's other arm that was wiping his face, and tried to recollect himself.
Their gaze meets.

"*Hyeong,* are you okay?"

"*Khobkhun nah.* Thank you, Nong Haein. I am okay."

Col. Chandra gazed around the room, he must be sleeping slightly deeper than a short nap he originally planned.
Haein offered him a glass of water, that he gulped away in seconds.
He started to talk as Haein took the glass and put it back to the desk.

"I dreamed of Nong Mich, again."

There was silence.

There was a bang, as Sgt. Pride Loudgulf barged in.

"Colonel, a Vishnu is detected!"

There was another silence.

Sgt. Pride's gaze trailed to Haein.

"Colonel Haein, I'm sorry, I didn't realize that you're in a meeting."

"That's okay, Sergeant. Carry on," said Col. Chandra.

Sgt. Pride affixed his gaze to Col. Chandra again.

"A Vishnu is detected in the University of Surabaya, Sir. Allow me to hand you this report," said Sgt. Pride.

Col. Chandra gestured to put the report to his desk.

"You're dismissed," said Col. Chandra.

Sgt. Pride left the room after giving them a salute.

The gaze of Col. Chandra and Lt. Colonel Haein caught each other again.

"It is starting again," said Haein.

Col. Chandra nodded.

### Part 5: A Dream Journal
Hendrik (In) jerked hard in his sleep, that he was wide awake a moment later.
He was confused at first, but he remembered to peer back into the sensations of his dreams.
He must remember the dreams first before he could write it down in his dream journal.

Balancing in the border of the land of the dreams while maintaining his awareness, he reconstructed his dreams again.
Scenes flashes by: a chase, military outfit and equipment, a death of a colleague in front of the team leader, and a weird paranormal phenomenon.
He reached for his phone and opened a note.
He started to type.

"There was this guy named Michael, and he was one of the Apex souls."

He paused again, and attempted to get a clearer picture.

"He was chasing two Apex souls, and was chased by two other Apex souls, and he was an Apex soul himself."

An hour passed when the note was done.
He sat on his bed and contemplated his dreams through his notes again.
It was very vivid, very clear as a daytime observation.

"That is it, that is the missing piece. This is what happens in Paramundus Integra!"

Hendrik jumped off his bed and fired up his laptop, and he started to type.

> There are nine Apex souls in Integra, and through many cycles of reincarnation, they gained powers.
> One group wanted to help the Apex souls, while the others were trying to fight it back, to preserve normalcy.
> Michael, was a part of the latter.

He took another moment, and contemplated again.

> Here's the basic summary:
> Two cursed souls entangled over each other in the reincarnation cycles.
> For every death, they're reborn and fated to meet each other. Once they meet each other, the organization captures them for their own use.
> Eventually they'd run away together, made a vow, and performed suicide again. And the cycle repeats.

Dominic Muerte woke up very late that day, in Bogota, Colombia.
He had been sleeping late that night, and was having a bad case of headache.
He checked his phone and there was a message from Hendrik.

He read the message about Hendrik's idea of two cursed souls, and how they're chased by two groups of people.
What surprised Dominic the most was that Hendrik mentioned his grandpa's full name.
The second most surprising thing he realized was, Dominic never gave Hendrik his surname.

His headache intensified, and now he saw the scenes he read in Hendrik's message.
He saw how it is important for the Butterfly Initiative to capture or to kill other Apex souls not aligned to the initiative, so that they have a chance to capture them in the next cycle, and bring them to align with the Butterfly Initiative.
He saw that the Butterfly Initiative was there to achieve ascension.
He knew, just at that time, that to achieve it, all nine souls must gather in a place.

Finally, he saw that he was Juan Muerte, his grandfather.
He went down, and asked his father, how grandpa died.
Father explained that he didn't know how grandpa died, grandpa was just missing.

There was silence, and Dominic said that, "*Padre soy tu padre,*" he was his father's father.
Father asked what he meant.
Dominic touched the couch, and it was sucked out of existence.

Dominic knew what he must do.
That he must find the incarnation of Michael, Chen Yun Fei, and Lee Ah Ping.
Then they must gather the rest of the Apex souls.

But first, he must replace the couch he just annihilated.

### Part 6: Finding The Other End
Ben and Bert found each other again.

### Part 7: Project Antarabhava
Ben and Bert were given a chance to start over in a parallel world.

## The List
### Part 8: Divine Problems Require Divine Solutions
Ashton introduced Alt to Manov, Anthony, David, and Michelle, as his brother from 15k years ago.
Ashton explained that they found a way to defeat the Divine Council of Earth, that is via a pair of divine weapons, Yagrush and Aymur, that they can obtain from Kothar, that according to Alt, is in hell.
Manov and Anthony decided to find a way to learn more about Kothar and a possible bargaining position against him.

According to Alt, to get access to hell, they must first find El, as he is the key, according to Alt.
Alt claimed that El is currently somewhere in Integra, that he had been observing and collecting information about the Divine Council within the last 15k years.
They concluded, that they must first find El, and then extract Kothar from hell, and bargain him to get Yagrush and Aymur.

### Part 9: Bargaining Position
The Antichrist and The Beast clusters suffers after they went through all of it.
The Christ Cluster, in the other hand, is intact, and healthy.
They come with a solution, that is to directly attack the Divine Council.
For that, they visited The Antichrist Cluster, and they said it was a crazy plan, and is impossible.
Tweaking around, they need to have an insider among Powers, and Ashton suggested that it could be Kothar, which is currently in Hell.
They could help him escape hell and have him return the favor for them.
Heinrich objected, saying that they don't really have a bargaining position once Kothar is out of hell.
After some convincing, Ashton said they first need to know why Kothar is in hell in the first place, which, could be their key to convince Kothar to help them.
Manov volunteered to help, on which Ein objected.
Ashton concluded that it means they can know Kothar's weakness, and afterwards, all they need to do is to find El.
Ashton's source said that El had the key to the Hell, and so, they need to find him first.

### Part 10: Kothar's Guilt
Manov embarks on a journey to find Kothar's guilt, and he asked Anthony for help, considering that he had two favors to return to Manov anyway.
They embarked on a journey, and discovers Kothar's guilt.
During their trip to hell, Heinrich came to their body, and tried to kill Anthony, which is when Manov split and fought Heinrich at the same time.
After some arguments, Manov said that Ein had to trust him in this one, and that he won't be hurt now.
Ein finally softens, and gave Manov a hug.
He said that if he ever caught Anthony hurting Manov, Anthony is going to be finished by his very own hands.
Manov said he knew.
So they learned that Kothar was in hell because of his guilt on betraying Yam.
And when they return to the real world, Anthony was surprised to find the environment around them are devastated, which Manov casually said as: we got a visit by Ein.
Before they part ways, Manov accidentally mentioned that he is curious about who is El, and Anthony laughs.
Anthony said that El was never a part of his faction's goals.
And it is fine for him to talk about it.
And that El is the father of all Divines on Earth.

### Part 11: The List of Names
At the same time, a list of familiar names was found in front of the Kukermall, where there are two dead bodies.
Hendrik was assigned by the WTF to investigate it.
Curiosity appears as his name was in that list, but with wrong titles on it.
Hendrik was supposed to be Hendrik Lie, S.H., M.H., but this one had an M.Kn.
This is either a mistake, or a clue.
The first three names had been crossed out.

> **The List**

> 1. ~~Prawito Ardianto~~
> 7. ~~Francis Silvester~~
> 1. ~~Joseph Chandra, S.M.~~
> 3. Elbert Hardiman, S.TK.
> 4. Febrian Gautama, S.TI.
> 2. Abigail Devi Putriani, S.Ak.
> 6. Muzdalifah Rahmadani Putri, S.H.
> 8. Hendrik Lie, S.H., M.Kn.
> 9. Bright Spears, S.H., M.Kn.
> 1. Nurhayati Maulidia, S.H., M.Kn.
> 1. Tere Luxury, S.TI.
> 1. drh. Ezekiel Tanputra

Turns out, there had been killings reported, with those three names that had been crossed out in that list, died, in sequences.
And the dead bodies he was investigating in Kukermall building, had been identified to be the next two names.
He tried to find the next spot of the killing, and discovered that the next names are currently visiting at the Kukermall.

He looked through control room, trying to identify the attacker.
Seeing the direness of the situation, and that the people on that list might be killed anytime soon, he ran to the middle of the mall.
He casted protective fields toward the targets, and tried to locate the attacker.

He did find it, and it turns out, the Tiamat-invested woman was the culprit.
She jumped, as all of the water from a nearby fresh seafood restaurant was drained and used as her weapons.
With a very precise highly pressurized water jet, she managed to cross out the remaining two visitors whose name listed before Hendrik's name.
More and more WTF officers landed on the mall, and a full-blown WTF task forces attempted to capture the woman.
After some fights, they managed to capture the lady and had him brought to the WTF Containment Chamber.

Meanwhile, Manov came afterwards, to have some chat with Hendrik.
Manov told Hendrik, that he is an active member of MTF "Samsara" in Integra, and he found out that they have a doppelganger in Integra.
Hendrik was piqued, and after some inquisition, Manov revealed that the only difference is that Integra's Hendrik took the Master of Notary (M.Kn) instead of the Master of Law (M.H.).

### Part 12: Parents in Hell
Around this moment, David meets with Anthony and Michelle at their house.
It appears the family brighten, as they've been reunited, and David is a healthy and decent speciment for Michelle's mate, despite them not dating at the moment.
Besides, David is the son of Martha's boss, she is happy to have him here.
They were supposedly hanging out.
Anthony happens to explain a story about his familiars, and his profession as a necromancer.
Michelle asked him what about her father, that is also his father, and he said that he has no idea who his father was, so he couldn't track him.
Michelle went out of her room for a while, and returned with a photograph, an old one, and weathered significantly.
In it, was shown the picture of Anthony as a baby, Martha, and their father.
Anthony focussed on it, and he glimsed to hell.
He found his father there, and he saw the scene, sequences similar to his dreams: his father wanted to leave him in the middle of the woods, memories of his father when attacked by Aurelia when he hit Martha and Anthony in their old bathroom.
it appears, that he dreamed of that because he accidentally connected to his father in hell before.
And it triggers his actual memory of that day.
It also confirms that his father is in hell.
But then, Anthony saw yet another face he recognize, David's mom. She was in hell as well.
David asked what about her, and Anthony saw a man in her torture chamber.
No, he said, there were two men, one is Steven, the other is, Fernando.
Apparently, Anthony said, David's mom had an unrequited, unresolved feelings toward Fernando, while still having a relationship with his father.

### Part 13: Quest of El
Reunited, Hendrik, Heinrich, Manov, Ashton, Steven, Daniel, Fernando, Henokh, and Derictor, gathered around and discussed this matter.
It is clear that they had to find El on Integra, as well as protecting the other guys whose name is in the list, from being killed.
Their killings must have some reasons behind it, that they wouldn't know if they let them be killed.

So the party decided that they must went there, secure El, contain the malice, and protect the witnesses.
Other than that, Steven put forward the intel he had from David, that Helena is in hell.
This makes Steven, Daniel, and Fernando so adamant in finding El.
Because of the time constraint, they must go immediately.
With the help of Adran and Manov (which already is a volant), Steven, Daniel, Fernando, Henokh, and Derictor were uploaded and beamed to the Integra Paramundus Dataspace.

They took some time to orient themselves in Integra, they arrived at a local organization set up by WTF to facilitate transfer and policing on Integra, since Integra turned out to be a world where baseline to normalcy is calculated.
The organization was called the Global Foundation of Esotericism (GaFE).
They protect Integra from unauthorized accesses, sometimes anomalous to the locals.
Afterwards, they were equipped with necessary tools to help them, including normal guns, resources, etc.

They were briefed with Col. Chandra and Lt. Colonel Haein.
As recently MTF "Samsara" had been in a terrible loss of members, Sgt. Pride Loudgulf suggested that they're assembled into members of the MTF.
Barong Ket step up and said that Romanov Dexter is a honorary member of his band, Band Airlangga, and requested him to be in his fireteam.

Col. Chandra hesitated, but Manov convinced them, that they also have their own battle experiences from where they come from.
Henokh and Derictor are an active member of a clandestine organization where they come from, Fernando was an active Indonesian Marine, while Steven and Daniel had some experiences warding off aliens.
Barong Ket said, he had been visiting their world as well, and he can confirm what they said.
Col. Chandra, after discussing with Lt. Colonel Haein (that this case is related with the threat that Apex souls might face), finally agreed.

Barong Ket also offered Barong Asoo, that with his clairvoyance ability, and the ability to use aural attack of his barks to cast out unwanted weak enemies, helps Sgt. Pride's team.
Col. Chandra approved the temporary reassignment of Barong Asoo to the 1st team.
Sgt. Pride objects, as he's supposedly the squad leader, not Ket.
However when asked who would he like to be transferred to his team, he said Asoo.

They're split into:

#### The Iron Fists

Their objective: locate El.

| Name | Description | Powers |
| --- | --- | --- |
| Sgt. Pride Loudgulf | Squad Leader. He is playful, has no regards of personal space, like to tease others physically, but he never intend to hurt his friends. His human form is a medium built, hairless pale albino man, with bluish gray irises. His animal form is a higher porpoise. | In his beast form, moving in any medium as if they're the ocean. In his human form, can liquify and solidify objects at will, as long as he touched them. In all forms, he could see through objects as if they're all translucent. |
| Steven Pontirijaris | An energetic person with proficiency in accuracy and force redirection as his fighting style. Something like airbenders. | His tablet allowed him to access all tools available for him in Xenomancy, along with several features inherent to the projector nature of his device. |
| Daniel Lusien | Calm, sometimes random, and like to act weak, while having a very strong rooting stance in a fight, very good in defense and firm. | Can alters the consciousness of his victim, provided that he initiated eye contacts with his victim, and performed a handshake with them. |
| Fernando Suryantara | Effervescent and sometimes goofy. Actually very kind and dislike violence, but could pack quite a punch if required. Has a set of strong and rapid offensive capabilities. | Can push or pull objects to his center of mass, provided that those objects had been seen by him. |
| Asoo | His human form is a short and bright skinned human boy with black eyes and hair. Playful, loyal, and inquisitive. He's easily distracted, but he wouldn't think twice to do anything to protect those he cares about, especially his team mates. Doesn't matter if he knew them or not, if the team leader said they're a member of the team, he'd protect them. | Can launch sonic attacks, and shockwaves from his barks that can selectively cast out unwanted weak enemies. His sniff is so strong he could practically see smells with stunning accuracy and range. |


#### Band Airlangga

Their objective: secure and protect those in The List, then investigate why they're being chased.

| Name | Description | Powers |
| --- | --- | --- |
| Barong Ket | Team Leader. His human form is a lean but strong southasian male martial artist. His beast form is a male lion. He's very keen of his surroundings, and is very good at strategic planning. | Specializes in aural attack, projected energy, shielding techniques. Can translocate, but only toward a spot in his direct line of sight. If he couldn't see it, he couldn't translocate there. |
| Romanov Dexter | A free soul, that sometimes appear to be careless, but very detail-oriented. He appeared to be dismissive and indifferent, but he cares about those around him. | His powers from Xenomancy is transferable to Integra, with more control and flexibility due to the virtual nature of Integra. |
| Barong Gajah | Emotional, clingy, but is actually huge and very firm in stance. If he decided that he wouldn't move, nobody could move him. He is also very strong and can easily break concretes. | He has seismic senses, and he could transfer forces through and from the Earth. |
| Henokh Lisander | Timid, turbulent, and persistent. At the same he is charismatic, so much that he could convince others to do things they originally didn't want to. | Can force others to do his bidding, provided that they are within hearing distance from him, and they're in a sustained eye contact when he is giving them orders. |
| Derictor Wijaya | Calm, observant, and determined. He can sustain a prolonged focus, in a battle, while also quite playful when they're not in a fight. | Can remotely manipulate knives, provided that he touched them beforehand. He is equipped with a special vest by GaFE that could produce countless copies of blades of various forms and shapes, as long as they exist in Integra. Also, as long as he's still focused on the knives, even if those knives are broken in pieces, he could still control the pieces. |

### Part 14: Getting to know Integra

They left the GaFE base, that is currently disguised as an old DVD rental store, with two OAEVs, one for every fireteam.
Each team leader briefed their team about the situations in Integra, including their possible doppelgängers.
Henokh has no counterpart in this world, and a quick search to his family, discovered that his mother is not married, and his father marry another woman, and had a son named Sylvester Lisander.
Likewise, Derictor has no counterpart as well. Daniel had a counterpart, but the family is poor.
Steven, being half etoan, is not even exist here.
Fernando had a counterpart, and his house is nearby.
Manov, curiously enough, has a counterpart as well.
Even though he is not technically Manov's counterpart, but Hendrik's, Manov's template origin.

So they decided to go to the house of Fernando's counterpart, that appears to be not present, and Team 1 took his car (as OAEV is too conspicuous in a city setting).
It turns out the house possess a working internet connection, so they take the time to browse and familiarize themselves to the world.
Daniel looked for El, but instead found a version of Hendrik in this world, that is a writer, that writes about their adventure, in the real world!
Meanwhile, Manov, Henokh, and Derictor managed to pinpoint several names that are present in the list.
They consulted on how would they do this.

Ket came up with a plan.
First of all, for Steven and Co., as a part of the Iron Fists, finding El is their utmost objective, while for Henokh and Co., as a part of Band Airlangga, finding informations on why people listed on the list are killed, is their utmost objective.
Besides, it is very weird for Manov to try and locate Hendrik, since he shares the same face as Hendrik.
So it is decided, they are going to be split into two teams: Team 1, The Iron Fists, to find Hendrik and a way to use Hendrik to find El; Team 2, Band Airlangga, is to find and secure people listed on The List.

### Part 15: Hendrik of Integra

Team 1 tried to get the address of this Hendrik guy, but they had a hard time, since this guy left only a trace of online presence, other than his blog.
Since his account is not private, albeit rarely updated, they use it to track his friends, and happen to find a recent story stream in the last 7 hours, that mentions his whereabouts.
It is tagged with a map location, so they searched the direction to that location.

With Fernando's previous experiences on the military, they decided to impersonate officials, and asked in a manner as if they're investigators.
They started to question the whereabouts of Hendrik to the store owner.
It turned out, it wasn't in their shift, so they requested addresses of those that work in that shift.
It turned out, one of them knew the friend Hendrik was with, it was a worker in that bar too.
They locate the worker, and uses the picture they had with Hendrik in real world to convince her that they were his friends, and that they planned to make a surprise party.
The friend asked, "what surprise party?" They remembered that Hendrik's birthday is yet to come, so they decided to say that it is the anniversary of his oath as a lawyer, and they're the colleagues of Hendrik from his university.

As absurd as it sounds, the friend of him actually believes in it.
She also noted how different Hendrik is in that picture compared to his usual expressions when they took pictures together.
They wished to exchange pictures, but unfortunately the Kaos of Kukerphones and the Android Operating System won't cooperate.
Nor can Kaos uses internet connection in this world, due to vastly different protocols used.
Apparently technology on prime Earth and Integra Earth, despite being superficially similar, is vastly different in the background.

Afterwards, they managed to reach Hendrik's apartment, only to discover that they'd need access card to get anywhere in it.
They tried to ask the security guy, saying that they lost their key and if the lobby has a spare key.
It turned out, the security guy don't have the spare key, they can only provide help with the access card.
After some thinking, they decided to wait around, since they believe Hendrik will come home from the office, and then they can follow him to his room.
They waited in the lobby.

### Part 16: Shiva's Advance

Team 2 arrived at Sylvester's residence, and apparently he's wasted, probably having a hangover since last night.
So Team 2 took his phone, his wallet, and his car keys (again, as OAEVs appeared too conspicuous, that they started to wonder what's the merit of bringing those OAEV in the first place).
They tried to find the four names listed in The List that happen to exist in this world, and happens to be in the city they're in.
The first name that come in mind is Bright Spears, which happens to be in the Prosecutor Office.

Trying to meet him in the office is quite hard, they have to pretend to be someone important, Manov conjured badges of Badan Intelijen Nasional (BIN), and tried to convince the prosecutors that they need to find Bright.
As it doesn't work (Manov realized that despite being one of Hendriks, he is not the lawyer Hendrik is, not enough to convince them).
So they tried to sneak in, and capture Bright.

At the same time, Dominic and his men arrived at the scene, and wreck havoc on lower levels.
Objects and people disappeared into thin air as Dominic casted his Shiva power, and his men uses gun laced with his blood, that provided almost the same effect to everything they shot.
People are screaming.

Manov said: something that is hard to explain is happening downstairs, you need to follow us.
Ket transformed into a lion and Bright was stunned due to the sight.
Gajah was about to transform, but Ket reminded him that they're in an enclosed space, so Gajah prepared a stance instead.
Henokh readied his gun, and Derictor pulled some knives off his vest.
Manov drew Bright's attention with a snap, and said: we need to go now, be surprised later.

They finally traced through the corridor to find an alternative route to get out of the building, with the help of Ket and his clairvoyance.
Ket felt something, and ordered them to step aside, while a bolt of something inscrutable screamed toward them.
Ket's mane were straightened as he roared, forming a protective shield that bounced the bolt to the side, and the wall disappeared.

Ket: It's Juan's power, but he's dead long time ago.

Dominic: Yes he did die, and now he's back as Dominic.

Ket: Great, nice to meet you again (Ket waved his paws, and if a lion could smile, that's what Ket's doing.)

Dominic was confused with Ket's response, and Manov had been using the little time they had to prepare a spell.
Manov told everyone to jump through the hole of the wall.
Not having any other options, they followed, and then Ket followed.
Dominic was enraged and dashed toward them.

Manov's spell caused the exterior wall of the building to be as level as the ground, and they use it to run to the streets.
Bright had been screaming but Gajah carried him effortlessly and on their run.
Bright told Gajah that he could run by himself, so Gajah dropped him, and he ran with them.
On the junction between the wall and the ground, they step on the ground and their weight normal displaced from the normal surface of the wall and the normal surface of the ground.
Bright, Henokh, and Derictor fell face first to the ground.

Gajah punched the wall, and it cracked, then concrete spikes emerged from the walls, hindering Dominic's men.
Dominic could be seen trying to vanish some of the spikes, but it was a lot and he is yet to regain Juan's proficiency of massive disappearance wave.
Dominic had to settle with only destroying an object at a time.

Manov ran the car toward them, and Ket, now in his human form, naked, grabbed them up and they boarded their car before Dominic's men followed them.
To the car they go and left, but then the car disappeared and six of them fell to the ground.
They hide behind a parked car, which also disappeared.
They ran to the nearby building, returning the shots with their normal guns and Derictor's knives.

### Part 17: Meta-narrative

Team 1 waited for too long, and Asoo said he's hungry.
They decided to eat around the apartment in turns, and take some meals, etc.
Apparently the night dawned, and they haven't discovered Hendrik yet.

Daniel noticed, that the lobby could be entered from the parking lot as well, which means Hendrik could've been in his room already, but they didn't know.
They asked the security guy again, saying that their friend that lives on the 31st floor replied, and they'd like to take the lift now.
The security guy scanned his access card on the lift and they went to the 31st floor.
There, they knocked, and after some pause, some rustles, etc, they found Hendrik opening the door in his small apartment room.
They barged in.

Hendrik, baffled, asked them who they are.
However his face told them that he recognized their face.

Steven asked: how did you know about my past, our past?

Hendrik, seemed cautious, asked: what do you mean?

Steven: you know about our school days. That Fernando proposed me, I was scared, and ran away from him. Then I almost killed Daniel who was confronting me at the wrong moment. Then I finally forgive each other, and Daniel is now with Fernando (he pointed at them, and looked at Hendrik again)

Hendrik just gave an audible "ah," and he thought for some more.

Hendrik: So, Integra is here?

Steven, Fernando, and Daniel, looked at each other, and then to Hendrik, Steven said: yes.

Hendrik: So, Xenomancy is real? (there's a brief moment of excitement in his eyes)

Daniel: what is Xenomancy?

Hendrik: The name of my novel, that is still in progress. It is named as such because foreign divinities are interfering with mundane matter, and the characters are trying to understand those divinities. So, to divine the intent of the divinities, hence the name, Xenomancy.

Fernando: I'm sorry, but we have other matters in our concern. We need to find El.

Hendrik: And I have no idea. I was stuck in writing because of that as well.

Some audible screams could be heard from the outside, far as in some hundred meters away.
They decided to look at the balcony, and discovered that the screaming come from the ground level, there are guys thrown away to the air, some shouts from the securities, and people at the pool on the first level gazed down.
Some guys jumped to the pool, and then more peasants thrown into the air.

Hendrik then commented: and there comes, um, **Zarah Walidah,** the Fermion Soul with the ability to control fermionic particles. Basically, all normal matter that comprises our world.

### Part 18 Tere Lux

Team 2 had their next target tracked, a public figure known as Tere Lux.
She was having a photo session for her new project, as seen in her instagram updates.
Manov asked Ket, that now wore a different, spare clothing, to translocate them there, as Ket can see the place.
Ket said that he can only do translocation to places he could see in his direct line of sight.
Looking through the live stream or story updates from a smartphone is not considered a direct line of sight.

Bright said he knew the place, it was near his usual hiding place, whenever he felt like he want to be alone at his office, and it was a break time.
Derictor asked if it is near, so they can go there as soon as possible, and he said that it was quite remote.
Henokh protested that he made it sound like he could reach it within minutes or so, as he often visited it in his break time.
Bright said yes, he could reach it within seconds, in fact.

Manov had his attention piqued: How?

Bright: I kept it as a secret all of this time, but now it seems like we need to act quick, and you're all weird, like me. So there's no point in hiding it.

Ket: Hiding what?

Henokh: we're not weird, we're just not from this world.

Manov: elaborate.

Bright: it will sound crazy, and I don't know how to describe it as well, so I'd just show you guys. Come, close to me.

They move close to him, and he closed his eyes, then opened it again.
His irises turned white, and his sclera turned pitch black.
The scenery around them pixelated.
Some of the pixels started to turn white, until they're in a space that could be described to be a space, but the color is inverted.
The sky is bright white, and the stars are black dots.

Bright: Don't be alarmed, I usually call this place as the White Room. This is a buffer space before I would make us reappear somewhere else.

Gajah: The White Room? This is...,

Ket: Antarabhava

Manov: The space between paramundus?

Bright: Oh, so this place has a name.

Manov: Wait, how could you access this place?

Bright: I just can. I found it out when I was little. I often find myself wandering here, in the white expanse. Until I learn that I can control where I reappear. Anywhere in the world.

Gajah: He's an apex soul.

Bright: Pardon, what?

Ket: an apex soul is the soul with special abilities, that are being chased by powers of the world for their own advantages. You are a Tachyon Soul, the soul with a power of non-locality.

Bright: So I'm right, I'm peculiar, just like you guys.

Manov: No, don't get us wrong. We got powers because we're not from this world, we're, um, *extranatural* in nature. Your power, is something inherent to this world.

Henokh: Need I remind you that we need to get this Tere Lux lady soon?

Bright nodded, and pixels started to form around them, replacing the scenery with granulated shape of a forest side villa, as more pixels made the scenery crispier.
The next thing they know, they're there, shocking the entire production team that was shooting there.
Bright closed his eyes and opened it again, and his eyes back to normal: black irises with white sclera.

Henokh: We need to bring Tere Lux with us.

The team looked at the gun Henokh is carrying.
It was sufficient to cause panic, and Manov said don't panic.
He said that they need Tere Lux to be with them, so that she could be brought to safety.
There are group of people that wanted her to die.

Bright, with his official prosecutor uniform, said that it is an official business, and he showed his badges, saying that he is a prosecutor.
It was their luck that the production team didn't understand what a prosecutor do, and believed in their authority.
They allowed Tere Lux to join them, who looked confused.

Manov, with his black cassocks and a white cleric collar, who looked more like a priest to her, appeared familiar.
Bright and Tere Lux had a brief eye contact, and she thought to herself, she trusted this man, and that's why she decided to follow them.

Then some of the production team disappears, along with some large props, and a group barged into the site, making people screams.
Derictor throws some of his knives, some of the enemy members fell, and adds more screams to the civilians.
And it would be another set of clothes Ket had to destroy as he transform into a lion.
Since they're outdoor, Gajah also transforms into an asian Elephant.

Ket translocate from one opponent to the other, mauling them one by one.
Some of the opponents launch some shots to the group, but Gajah covered them, letting himself to be shot, and he charged toward those shooters.
Until Dominic came face to face with Gajah, and launched a bolt of indecipherable shape toward Gajah's face.
Ket translocate near Gajah at that instant and quickly translocate away from Dominic, but it was too late, Gajah's head is gone.
It was a mess, but Ket didn't seem to be sad, while Bright almost fainted due to the sight.

Ket: He will be back. All of you, go first, I'd deal with them.

Henokh proclaimed that he never actually liked face-to-face fight with psychics before, on which Manov corrected that he is a reflexior, not a psychic, but paused to think awhile, concluding that reflexiors are just a thing on their real world, in this world it could be anything.
Tere Lux asked, "real world? Isn't this the real world?"
Manov took some moment to think, and only said, "long story, tell you once we're safe."
Manov instructed Henokh to go with Tere and Bright to a safe place, then they can regroup.
Henokh, Tere, and Bright disappeared to Antarabhava.

Meanwhile Manov and Derictor assisted Ket to slaughter the opponent.

### Part 19 Escape Room
Team 1 decided to go through the emergency exit, as, according to Hendrik, he believes that the gang members might not be well aware about the emergency exit.
Steven readied his phone to the offensive mode, and Hendrik asked if he could see his exoself, and Steven said it wasn't the best time.
Nodded, they barged through the emergency exit that leads to the parking lot.
Soon after, they're only midway, Hendrik said that he forgot that they'd need some good minutes to actually finish the stairs, and the trio mentioned that it was quite hot here.
There are no receptions here as well.
They exited at a blocked exit.
In the other side of the door were boxes, so they tried to push through slowly, and peeked.

The gang members are barging toward the entrance to lobby.
They made the door disappears, while normally one would need access card to access it.
Hendrik said he is not sure if his bike is a proper method to get out of here.
Steven said they had a car.

So they tried to push through the boxes, only to find out that there is some box that prohibit the opening wide enough for a person.
That is when Pride stepped forth and touched the door.
The door melt, and so does the boxes, and he let them walk past it.
The melt appear solid when they step on it, and after they're done, Pride let go his touch and the doors and the boxes returned to their normal shape.

Steven: "You can do that all of these time?"

Pride nodded.

Steven: Why don't you tell us before? We could just go to Hendrik's room since the beginning, we don't have to wait.

Pride: We must maintain normalcy, that's why we don't do that in public. There's a danger if a lot of Integra inhabitants realized that their world isn't normal.

Hendrik: But, um, there's a camera right in front of the emergency exit.

Pride: Oh shit.

Daniel: Let's get to the safety first.

They managed to sneak toward their car, when it was obliterated by the gang member that noticed them.
Steven aimed his phone and flashed a blinding light to temporarily blind him.
They ran, and now have no idea how to escape, with no car available.
It would take some time for the OAEV to arrive there, even with autopilot.
Hendrik said that he is currently taking care of his uncle's car, His uncle and his son were in a vacation now, and they decided to take the car.

Fernando drove, and they left the building, breaking the security bars, and full speed toward the exit gate.
The gang members ran to chase them, but they turned north as soon as they reached an intersection, blocking the gang's view to the car.
Hendrik suggested to go to a nearby empty field next to yet another, calmer apartment building, north to his apartment.
They decided to contact Team 2, but Team 2 seemed to be busy.
Soon after, they received a prompt to meet at the house of Fernando's counterpart.

In their trip, Hendrik took a moment to ask, "so, when I lost one of my phones and my wallet today, is it because of what I think it is?" Steven said that most probably it is.

### Part 20 Nurhayati

Nurhayati is a tall woman, and is currently working happily at a notary office.
She had been worried because Bright hadn't replied anything to her messages.
She was upset already, when her friends told her that Bright is here looking for her.

She was surprised and ran to the front gate, and Bright pulled her.
She resisted, but then she discovered that he's not alone.
There are four chinese-indonesian with him, one with a tattoo in his left face.

She recognized the one with tattoo, asking, "Hendrik?"
Manov said, "No, I am Romanov, I just happen to share the same face with the guy you recognize. Now get in, we're going to meet Hendrik at our place."
Her boss angrily bursted out of the office and said they can't leave, and Bright said it is urgent.
They left.

### Part 21 Fernando

Team 1 was puzzled, Fernando's counterpart stood in front of the opened gate beside his motorbike, and the opened front door.
He was surprised to find an OAEV parked at his house instead of his car.
Then at another random car that stood right next to him, that belong to Team 1.
The night was calm in the complex, and Daniel stepped down and approached him.
Fernando's counterpart recognized Daniel immediately.

Fernando (In): Daniel?

Daniel (Xe): Listen, I and my friends can explain everything. All I ask is, for you to stay calm, whatever you're going to see, okay?

Fernando (In): Just explain everything already.

Fernando and Steven stepped off the car and approached Fernando's counterpart.
Gasped, he asked them who they are, while backing down, and Daniel reminded him to stay calm.
He explained that they're from an alternative Earth, and is currently here for a mission.
And they happen to have Fernando's counterpart here.
To ease things out, they used Fernando's facility to help their mission.
After all, having the car driven by the owner-lookalike with a proper ID is less suspicious than having a stolen car around.
They asked him to stay calm, and Fernando talked with his counterpart, while Hendrik drove the car in.

Apparently his counterpart prefers to be called Edo, and Fernando said he'd like to be addressed as Nando.
So Nando told him again that they're from an alternative Earth.
And they are currently in a mission to save their friend from hell.
However, to help their friend, they need to find the Biblical El that hides here, in this version of Earth.
Hendrik added that he is from this world that Edo inhabits.

Edo objected, stating that their excuse is just extravagantly ridiculous, how could the key to go to hell to save a friend requires the Biblical El, and that the Biblical El is in this Earth, let alone the part about multiverse shit.

Fernando (Xe): Okay, what about this, Asoo, show him your beast form.

Fernando (In): his beast what? (he gazed at Asoo that is stripping off) Hey, keep your clothes on!

Completely naked, Asoo transformed into a giant, long and slender black dog, about a meter tall.
Edo couldn't say a word.

Fernando (Xe): believe me now?

Then they noted that another car came in to Edo's house.
Edo went outside with the group to greet the second group.
Team 2 arrived, and Hendrik was stunned to see Manov.
Bright and Nurhayati were surprised to find Hendrik, and that Manov wasn't Hendrik.
And Edo inquired, why are they on Sylvester's car, until Henokh came out, which, he recognizes, as resembling Sylvester, but not exactly Sylvester.
Nando explained that Henokh doesn't have a counterpart here, because his father married another woman, and his mother is an old virgin in this version of Earth.
Therefore, he was never born, but he had a biological half-brother known as Sylvester.

Until Ket exited the car, and then Edo was stunned for a moment, before calling, "Khemachat?"

Ket too, was frozen for a moment, "Edo?"

Then from inside, Daniel yelled that he found his pictures on Edo's yearbook.
Tere shouted, that he finally remembers, when seeing Hendrik, that they meet in a training on an investment agency! Hendrik also recognizes Tere.

### Part 22 Recollection

Edo usually enjoys silence in his house, but today it was the exact opposite of what he enjoys.
The house is full of people.
Most are strangers, one an exact copy of him, one is a half brother of his best friend that never is a half brother, his ex, and a bunch of people he just met.

Daniel here turned out to be the doppelganger of his high school best friend, Daniel.
He claimed that in his version of Earth, they went to the medical school together, but Edo said that in this version of Earth, Daniel never went to the same medical school as him, he is not even sure if Daniel here took the medical school.
Manov explained that, Hendrik, Bright, Tere, and Nurhayati are in danger because a group of bad guys are chasing them, and it appears the gang are chasing the names in a list he hold.

Hendrik said he believed that the one behind this all was Tiamat, at least that's the way it is in his story.
Manov gazed at Hendrik, then he stated that he can't track the last name that he must protect, Ezekiel, so they must make do with what they have now.
And Steven added that they haven't able to locate El yet.
Manov inquired, who is this Tiamat, and Hendrik said he had no idea.
Hendrik said that Tiamat somehow led a group that seeks to find El, so El could be resurrected, and in doing so, she deceived Hadad to help her.
Steven asked once more, "what about El?" and Hendrik said he had no idea yet.

Nurhayati asked them, why are their name on the list, and why are they chased?
Manov said he had no idea.
At first he thought one thing that all of them had in common is because they all know Hendrik, but it makes no sense, since in his version of Earth, where nobody knew about people from this version of Earth, he is certain that all the victims had no connection to Hendrik of this world.

While they contemplated about why the list exist in the first place, Edo withdrew from them, and approached this one guy that's trying to flirt Tere.

"Ket,"

Ket's attention was withdrawn completely from Tere, that discovered it was a perfect time for her to leave as far away from Ket and to the rest of the crows.

"Yes Edo? Long time no see."

There's this brightness in Ket's smile.
That's the kind of smile he missed for many years already.
Fifteen years to be exact.
Yet this one face barely ages, looking exactly the same as the last time Edo looked at it, fifteen years ago.

"I missed you."

"I know," Ket answered.

Edo was stunned for a moment, "did you miss me?"
But Edo didn't say that.

Ket's gaze appeared as if he understand what Edo wanted to say.
"What is important is that we're here again, together," he let out another smile.

It was the smile Edo found to be irresistible.
All it did was to draw Edo closer to Ket.
Edo gave Ket a hug.

"Why did you leave me?"

"I'm not from this world. I had to take care other business beyond this world."

Edo and Ket gazed at each other for some moment.
It was a moment, Edo could feel it, and they kissed, and hugged, and kissed again.
Edo didn't know what to say, but he should just enjoy the moment.
Instead, he let out a random topic that pop out of his head.

"Where's Macan and Bangkal?"

There's a stark difference in Ket's expression the moment he let out that question.
His eyes emanate deep pain, that Edo regretted ever letting that question out.
Manov came to them, "You don't mention about *Tako* and *Thang* to *Singto*."

"Who's Tako, Thang, and Singto?"

"*cayen* (ใจเย็น ๆ), calm down Ai'Singto," Manov said, hugging Ket that were crying like a baby.

Manov looked at Edo, "Tako and Thang, which literally means a tiger and a tank, are nicknames P'Kao," Manov paused, as he realized Kao is Chandra's nick in Xenomancy, not in Integra, "uh, I mean, those are nicknames P'Chan gave for Macan and Bangkal. Singto, which literally means a lion, is the nick P'Chan gave for Ket."

"And, what happened to them?"

"Tako and Thang died in 2016, when Tengus from another world invaded their world. The fight was quite intense, as it reached my world as well."

"I'm feeling fine now, thank you Manov," said Ket.

Ket hugged Manov, which Edo recognized as the kind of hug Ket used to provide for Edo.
Edo realized that he's not the only one in Ket's heart.
That was when Edo decided to return to the rest of the group.
He couldn't stand it.

### Part 23 El

Hendrik started to talk, that in his design, Tiamat was on her quest to track down the last key required to open the gate of Hel.
Actually, either that, or she just wanted to find her father, El.
Hendrik couldn't decide yet.

And the protagonists are looking for a way to find the key required to open the gate of hell, the thing they need to save their friends.
Then to do it, they have to find the Mark of El, that Hendrik himself still have no idea where.
He just know it must be here in Integra.

So far, his lead was a vision he had, the biblical figure of Cain.
He bore the mark that was given by El, now is known as the Mark of Cain.
The problem is that, in his research to build up the character Cain, he did not find any mention that Cain could still be alive today.
Biblical sources didn't provide any definitive clue that he would be an immortal, so most likely he is not.

"Whatever it is," Hendrik said, "perhaps our best chance is to track down Cain, if there's any here."

"How?" inquired Henokh.

"I have no idea."

Henokh then asked, what about these guys, why are they been attacked?
Hendrik said that they're related to El in one way or another.
Manov had his attention drawn.

"What kind of connection?"

Hendrik said he had no idea.

"But perhaps, it is that they had been interacting with El during their entire life."

"Have you?"

"I don't know,"

"but your name is in the list."

"Does it mean, I must've been interacting with him without my knowledge?"

Manov thought hard on this.

"Why are Tiamat chasing those that had been interacting with El?"

"Perhaps because we carry clues on how to find El, or his mark?"

Henokh stepped up and said that it is apparent that they have no other choice but to protect those whose names are on the list.
Then they need to interrogate them and try to find who is this El they've been interacting with.
Manov said, then they must secure Ezekiel first.

### Part 24 Surabaya Run

The next morning, the group of two OAEVs left Edo's house.
They have to use OAEVs because of their group size, and splitting into 4 cars are quite risky, as they need to face Apex Souls and possible encounter with gods.
The GaFE issued OAEV still had some remaining normalcy field generating capacities, those that remained in this world after Sperry and Vishnu were killed again in their real world exile.

Hendrik recognized the guy they're after, a cousin of one of his friend.
Ezekiel are currently a professor at a veterinary school to the west of Surabaya.
Team 1 is like yesterday: Steven, Daniel, Nando, Edo, Hendrik, Sgt. Pride, Asoo.

Team 2 is: Manov, Henokh, Derictor, Ket, Gajah, Nurhayati, Bright, Tere.
They dispatched with the goal of securing Ezekiel.
And afterwards, they could worry on how to extract the information that leads to El.
Manov still wonders on why would Tiamat provided the list to Hendrik (Xe) in the real world, and why are they chasing them.

Steven, Fernando, and Daniel thought really hard about how to find El, as Helena is still in hell.
There are two possibilities on Ezekiel's whereabouts.
The first one is that he is attending his class on a veterinary school to the west of Surabaya, or that he is visiting his relative on the southern Surabaya.

So Team 1 decided to go to the western Surabaya, and Team 2 to southern Surabaya.

### Part 25 Recap

Here is the situation recap so far.
Fernando, Hendrik, Tere, Bright, and Nurhayati from Integra had their life changed forever.
They can no longer return to their normal life, not with the rampage of Apex Souls under Tiamat's rule are still on the loose.

Romanov, Henokh, Derictor, Fernando (Xe), Daniel (Xe), and Steven are yet to accomplish their missions.
Romanov, Henokh, and Derictor are yet to secure all names in the list they found, and they still have to figure out why are they targeted.
Steven, Fernando (Xe) and Daniel (Xe) are yet to find El, they need to find a way to track El down in this world.

### Part 26 Crossover

Team 1 was on their way.
Daniel sat in between Nando and Edo, and he fell asleep due to lack of sleep.
Out of curiosity, Edo asked Nando, are Daniel and Nando great friends?
Nando said that it is more, he and Daniel are a couple.

Hendrik from the driver's seat asked, "wait, what?"

Steven, right beside Hendrik stated, "they had sex together."

Hendrik said: too much details,

Steven: you asked it.

Edo just asked, "so Daniel is gay?"

Nando: "No, more like bisexual. He was into Steven's wife, Helena. But as Helena chooses Steven, he moved on, and happen to be fond of me."

Steven responded:
"We made quite a drama back then.
I love Helena, and so did Daniel.
We fought for Helena, which, as I lately learned, Helena had feelings for Fernando, but Fernando confessed his love to me back then.
It was two thousand and three I believe.
I freaked out, and because of that I almost got raped by a man-eater, and then I almost had Daniel killed because he confronted me at the wrong time.
In the end, I stays with Helena and had four great kids, and Fernando-Daniel are together ever since."

Edo took some moment in silence.

Nando asked, "so what about you?"

Edo:
"Your life is colorful.
I didn't know that Daniel was bi.
If I knew, I'd dated him back then, on high school."

Nando: "why didn't you?"

Edo:
"Is your Earth more accepting to LGBT?
Here it was harsh.
I decided that I would rather be alone, and die alone, than to pretend and live my life with a woman that, despite being a great woman, I can't love."

Nando took some time to let Edo's words sink in him,
"No, my Earth is just as harsh.
But I accepted that as a part of reality, and just embrace who I am.
I can't live forever in a lie I built myself.
And I happen to discover Daniel, and he is great."

Edo looked at Daniel, "yes, he was great."

A flashback of Ket consoling Manov came to Edo's thought.

Edo: Actually, I found someone else that I love. But,

Nando: And what is the problem with it?

Asoo: He saw the one he loves being taken care of by Manov.

Nando: Manov?
Manov is gay?
How?

Asoo: Manov is a pansexual.
I mean he had sex with Purpose, Pride's friend, a fellow higher porpoise.

Edo: Wouldn't it be a bestiality?

Steven: Would it be a bestiality if the other party is a thinking being?
I always thought bestiality is a sexual relationship with feral animals.

Edo: But a porpoise is an animal,

Asoo: Then you're saying Pride is an animal?
The one driving our car is actually a porpoise, but in this world he choose to represent himself as a human for ease of interaction with us.

Pride: While you're all having a little gossip in the back, we arrived at our destination already.

They parked at the campus, full with college students going by around.
After some time walking around, they stumbled upon an administrative building.
And at the time, they saw Helena.

Steven: Len!

Steven ran hard toward her, hugging her, and kissing her, and got a slap from her.
On the floor, Steven confused, looking at this Helena.
Different hairstyles, so energetic, and strong.

Slightly confused, she looked at Daniel (Xe).

Helena (In): who is he? And, uh, what did you do to your hair.

Daniel (Xe): "Uh, I just got a haircut, and this is my friend, Steven,"

Helena, confused: "You could grow your hair at the barbershop?"

Before Daniel (Xe) could even answer, Helena slapped another statement,

Helena (In): "Doesn't it bother you when a friend of yours kissed your wife in front of you?"

Daniel (Xe): "A wife?"

Even more confused, she looked at Nando and Edo,

Helena (In): "..., Edo? And, y, your twin?"

Hendrik: perhaps it wasn't the best idea to bring Edo and Nando together, (while trying to get Steven up)

Daniel (In): "What the hell?"

That was the voice of Daniel, but not coming from Daniel's (Xe) mouth, it was from behind them, where another Daniel (In) came by, with a freshly trimmed hard top.
They could see Helena's (In) face is blanking, full of confusion.
Hendrik backed off slightly, "Guys, I don't know anything about what is going to happen next. This part is yet to be written,"

### Part 27 Tiamat

Team 2 arrived at a dormitory next to a large campuss in Southern Surabaya.
There is one dormitory painted in red, that exactly looks like what Hendrik described.
But when they came by, people are running away from the dormitory.
People wounded from the fall from higher up, some had broken bones, some had plenty of blood around.
The never ending scream accompanied what looked like Zarah, among with her men, from the fourth level, the topmost level.
People are flung away, and they tried to reach a locked room.

Romanov looked at the opening in between walkways.
The dormitory's floor-plan is made by three rows of bedrooms set in parallel.
The first row left to the entrance is facing right, and in front of it is the second row, facing the first row.
The third row is right behind the second row, facing away to an alley in the back.
The next levels are similar in floor plans, but there are open fenced gap where the first floor could be seen.
Atop of it, at the uppermost ceilings, are translucent glasses, that allows passage of natural lights during daylight.

A lot of open spaces, and they could see most of the rooms effortlessly.
It was a perfect battlefield for Ket's ability.

Romanov: Ket, can you see the room they are approaching on?

Ket: Right. I can see it clearly.

Ket grabbed the hands of all of the group members and put it on his body, he transformed into a lion and they translocate upstairs.
They arrived right before Zarah and her men arrived there.
Zarah looked at them, confused, but she recognized Ket.

Ket: Long time no see. You're an old lady now.

Zarah was enraged and she raised an arm.
Ket let out a roar, and the shockwave threw her and her men, and they were thrown down the stairs.
Manov knocked the door, room D-12.

Manov: Ezekiel, we're here to help you.

Ezekiel: Ko Hendrik?

Manov was going to say "Manov here, Hendrik is not here," but he just said, "Just get out now. We have to go."

Some of the men rushed back to the 4th floor.
Derictor readied his knives, and Henokh started to spar with some of the bad guys, then he ordered, "Why don't you fight for me instead?"
One by one, men that sparred with him and heard that phrase, ended up fighting the other men.

Zarah was furious, and the concrete around her start to crack. Metal beams that comprised the stair's handles were twisted and corked, and thrusted toward the group.
Derictor threw many knives in quick succession and each knives faced the metal beams head-on, pushing them back.
Some knives were thrown toward Zarah, and Zarah disintegrated them into dusts.
Gajah readied his stance and a thump of his foot to the concrete caused the stair's staircases to flatten that the entire stairs be a slide instead.
She slipped and tripped.

Nurhayati hide behind Bright, whose eyes turned to white irises with black sclera, and caused some of the men to be turned into pixels and disappear.
Only for some of them could be found fell from the sky to the translucent roofs.
Some break the translucent roofs and a mixture of flesh, bones, blood, and glass chunks rained the entire corridor.

Henokh: What was that?

Bright: I just want them to be gone, didn't specify where, and they just appear on the sky instead. I'm sorry, I didn't mean to,

Derictor: Great, keep it going.

Manov could feel that Ezekiel hesitated to come out, with all of the noise from outside.
He kicked the door that it broke open, and pulled Ezekiel, wearing only undergarments.

"If you wanna live, follow us."

Tere barged in and pulled some clothes from his closet, and gave it to him.

Tere: bring it with you, wear it later. Now run.

Ezekiel: Where?

Manov, now right at the terrace: Here. Quick.

The front terrace was protected with bars of aluminum fences.
Manov waved his hands, and they twisted to provide an opening for them.
He casted a spell, and jumped.
The outer surface of the building is now the normal surface again.

As if looking down at a trench, he who was walking by the side of the building called them,

Manov: Come here guys! Be scared later!

Gajah raised a new stance, and pushed his feet to the ground, and the floor of the fourth story started to crumble behind them, as they ran toward the terrace.
Zarah threw some floating stones, but Gajah quickly redirect them with his fists.
Ket turned into a human, naked, and pulled Derictor and Henokh to run toward the terrace.

It was just some ten meters walk to the ground, but then another lady was standing at the side of the building, as if she was standing above the ground.
They were in fact, stood perpendicular to the horizon, and supposedly it was just them that can do that.
But this woman can pull the same trick.

The group stopped, Manov was confused.
She was half naked, with only some pieces of clothes to cover her cleavages and her groin.
Her body was full with inscriptions, they glowed light ocean blue.
Her hair spread, almost giving the appearance of living snakes.
Instead of eyes, she had stripes, extending from her eye sockets to the sides, flailing as if they were softly blown by the wind.

Manov: Tiamat?

Ket: She's gorgeous as fuck.

Manov: and she's more than capable to actually kill us.

Ket: Oh fuck.

Tiamat: You've collected those that are in my killing list. How convenient.

Zarah, flying by the side of the building: Do you think you can run from me now?

Ket: Fuck

Manov: Stop cursing will you, I'm thinking.

### Part 28 Tiamat and Hadad

Daniel (In): So this lookalike of mine, with unkempt hair, is me from another world?

Daniel (Xe): It is not an unkempt hairstyle, I just didn't get the chance to had a haircut, yet!

Helena (In): And that guy that kissed me earlier, is my husband there?

Steven: Yes, you're my wife in my world.

Daniel (In): He is the husband of your double, your husband is me.

Helena replied him with a smile.
Her gaze landed at Edo once, then at Nando.
Daniel (Xe) rest his head at Nando's shoulder.
She tried to contain her giggles.

Edo: And we are together, Daniel and I, in their world.

Helena's attention were snapped.
She wasn't sure what to feel.
On which one hurts more, that someone secretly loves her husband, or that the one she used to love, or probably she still loves, loves her husband.

Daniel: We (pointing at himself and Nando) are together, not you.

Nando signed Daniel (Xe) to stop.

Daniel (In): So, um, you like me?

Asoo: He likes Ket now, Khemachat Thunyakorn, not you.

Helena wasn't sure what to feel.
That the man that used to love her husband, now loves someone else.
Or that the man that she used to love, or maybe she still loves, moved on from her already.

Edo didn't expect that it would be this hurtful.
Ket is with someone else now.
Daniel, is with Helena now.
Even though his versions from the other Earth are together with the one he loves, he's all alone here.

Pride: Shouldn't we focus on a more important?

Steven: Oh, finding El, we must find El to save Helena.

Pride: Not that, (he pointed at a group of people approaching), those.

It doesn't seem weird at first, until they realized that there are two group of people.
A group of men, and a group of women.
All men wore tuxedos and they're all identical.

All women almost wear nothing but some piece of clothing to cover their groin and their breasts.
The women's body had bluish white markings, and their eyes were light stripes.
And they were all identical.

The weirdness didn't stop there.
As the leading man instructed the group to split, some of the women and the men literally split, cloning themselves into more.
They spread to all directions, ready to cover the university premises.

Steven: Can we do that too?

Daniel (Xe): Is it even the correct question for a time like this?

Daniel (In): Who are they?

Hendrik: I think, they're the representation of Hadad and Tiamat.

Pride: And how would you know that?

Hendrik: I designed them, or so I thought.
Besides, look at the markings on her body, it glowed bluish hues, and her eye stripes where her eyes should be, those are properties of Yam.
Tiamat is a part of Yam, that was taken by Hadad some eons ago, on the last Baal Cycle.
I think Henokh told the story that his grandpa told him, when he was having a gathering with Derictor and Aditya.
The day before Yam came for a visit...

Pride: Save the rest of the story for later.
What can we do to defeat them?

Hendrik: They're here in Integra, and Integra is by nature a prison of some sort.
When they came here, they have to have a human form, especially gods like them.
Like you, Barongs from Paramundus Jagadlangit, you have to have human forms, though you can retain your true form because you're not gods.

Asoo: So, they're as weak as humans?

Hendrik: They're just like you, and you (to Pride), and you three (to Steven, Daniel (Xe), and Nando) from Xenomancy.
Visitors.

Steven: That means, they may also have special powers to a limited degree like what we can?

Hendrik nodded: plus, apparently they can split into many, as their personalities can't be contained into a single body like ours.

Steven, Daniel, and Fernando (Xe) thought for some time, looking at Hendrik, Daniel, Fernando, and Helena (In).

Steven: I think you should go with us.
We can explain later.

Bright eyes that emanated youthfulness, full of energy.
He was not smiling, but Helena was convinced that if he smiled, any woman would fall for his smile.
No wonder her double want to be his wife, but she knew deep inside, her heart is not with him.
He was just too youthful and energetic.
Almost childlike, but his body is an adult.

Pride: Great, (sarcastically) more people to be taken care of.

Asoo: The more the merrier!

They started to move, away from the incoming men and women spreading through the campus complex.
However, some of the men noticed them, and the chase started.
Steven activated his parser suite from his phone, and he started to shot bolts of fire.
Some of the men returned more bolts of fire, and with a move, Nando's repellent field deflected the bolts.

Some of the men and women ran toward them, and some managed to get through, despite all the efforts of Nando, Steven, Asoo, and Pride.
Daniel (Xe), having his power to be neurological in nature and requires physical contact, really couldn't do remote attacks, so he clustered with Hendrik, Daniel (In), Helena (In), and Edo.
His defense, however, is very firm and rock-like.
Every one approached them, is blocked by him, and it gave him a chance to use his powers.
After blocking their attacks, he caused every one in contact with him physically and had their eyes met his, to collapse, unconscious.

Daniel (Xe): Apparently, despite being gods, they are subject to the rule of my power.

Pride: at least it confirms Hendrik's hypothesis.

Pride swam on the concrete, and pulled some bodies of Tiamat and Hadad to sink under the concrete.

Pride: So whatever can kill humans, can also kill their bodies here.

Asoo barked hard to some of the approaching Tiamat and Hadad, and they were thrown back hard.
Some lifted pieces of earths and threw it to Asoo.
Nando came and jump in front of Asoo, and repelled the pieces.

Steven did a rotating jump, and as his kick reached the ground, a piece of rock emerged up and flew in front of Nando.
Nando maintained his stance, and jolted a fist, that the rock repelled it hard toward the approaching bodies of Hadad and Tiamat.
Daniel (Xe), despite being the weakest, managed to dismantle and disable those bodies of Hadad and Tiamat that managed to approach the group.

Daniel (Xe): We're simply outnumbered! There must be a quick way out of here!

Daniel (In): We can try to get to the back door, but I'm not sure if the gate is open.

Hendrik: with their powers, I don't think it mattered whether the gate is open or not.

Daniel (Xe): Then, we'd go there. Daniel, lead the way!

It was just two steps ahead, a Tiamat came and hugged Daniel (In) hard, she tried to bite his neck, but Daniel (Xe) locked her arms from her behind.

Tiamat: So you like it through the backdoor, eh (she licked her lips, while jerking, trying to get off the rock grip of Daniel (Xe), and her hairs started to form snake-like properties, they're about to peck on Daniel (Xe)'s eyes.

Daniel (Xe): (releasing the grid, and pushed her away to the ground) I prefer to face you directly, (he pulled an arm of her to make her face him, and she fell unconscious immediately).

Hendrik: Wow, you neutralize a medusa with your gaze, how ironic.

Daniel (In): (patting Daniel Xe's shoulder) I know we can count on you

Daniel (Xe): Thank you

Daniel (In): No problem, I know I can always count on myself anyway (he laughed, Daniel Xe just raised an eyebrow)

A Hadad came to lock Daniel (Xe) from behind, but with a move, he caused the Hadad to fell in front of him instead, while still holding a hand in a lock, the Hadad fell unconscious afterward.
He turned to face ahead, but a Hadad came in front of him, and with his palm, an immense pressure could be felt that push him backwards.
He firmed his stance, and jolted a palm, trying to penetrate the Hadad's defense.
His clothes torn from his penetrating arm, and to his shoulder, and his entire top wear is torn into pieces.
When he managed to hold the Hadad's neck and maintained eye contacts, the Hadad fell unconscious.

Bright skin, smooth and calming gaze, firm and well-defined body features.
Those qualities of Daniel (Xe) was more than sufficient to make any woman fall for him.
Especially when the top wear was removed.
Helena (In) and Edo were no exception in this case.
Given this magnificent sight of a healthy male specimen like her husband's double, she and Edo couldn't help but to have their jaw dropped open.
Daniel (In) held Helena's (In) jaw and closed it again.

Daniel (In): Contain yourself, will you? Your husband is right beside you.

Edo had to hold his jaw for himself.

Helena (In): I'm sorry. (she smiled at Daniel (In), her stomach fluttered and her heart raced)

Helena could never see her husband the same way ever again, she thought to herself.

A group of three Hadads approached Fernando, their fists thrust from all directions.
He blocked the first one with a palm, that before the fist contacted his skin, got repelled back.
The second one was deflected with a wave of his elbow, again without contact, got pushed back really hard.
The third one, almost hit his neck, when Daniel (Xe) pulled that Hadad's feet back with his rock-hard grip.
After wrestling with that particular Hadad, Daniel (Xe) managed to disable him, while Nando rounded up the other two.

Tall, dark, and handsome, was all she could describe about Nando.
Nando exchange smiles with Daniel (Xe), and it was more than sufficient to boil her fujoshi soul.
If not because of their current situation, she'd shriek as hard as she could.

Nando: You know, your power might not be a ranged one, but it complimented your rock-hard body.

A Hadad and a Tiamat approached the group, very close that they almost touched Hendrik.
Edo came to protect her, Daniel (In), and Hendrik, but got slapped far from Tiamat.
Steven came in between them and Helena-Daniel-Hendrik (In), and in a move too fast for their eyes to follow, the attackers fell to the sides.
Nando helped Edo to get up, while uprooting paving stones and threw them to the incoming Tiamat and Hadad.

Daniel (Xe) came and rounded up the fallen Hadad.
It was quite a spar, but Daniel's (Xe) defense was impenetrable.
After he managed to both hold the Hadad's body, and locked his gaze at Hadad's gaze, that Hadad fell unconscious.

Daniel (In): Where did you learn to fight like that anyway?

Daniel (Xe): Well, you know that my boyfriend is a soldier.
What do you think we do at our free time?

Helena (In): Making out?

Daniel (Xe): Yes! No, I mean, it's not completely wrong,

Nando: And we spar, when we weren't making out.

Hendrik: Can we save the details for later?

The Tiamat had her hair extended to grab Hendrik, but with his fist thrusted toward her, a bolt of fire burned her hair.
Asoo jumped to them, and with a bark she was thrown back far.
Charging toward her, Asoo bite her neck at such force that it snapped.

Hendrik: This is so wrong, why are they after us?

Pride: Not just after us, but some of us.
They don't really care about Asoo, I, Steven, Daniel (Xe), and Nando, we're more like a hindrance for them.

Another Tiamat run toward them.
With Asoo's barks, she fell toward the ground, then Pride swam under her, and they both sank under the concrete.
Pride came out after, alone.

Steven: If they're after those that are from this world, I guess they have everyone else but us to chase, but they're only targeting our group,

A Hadad bursted a large ball of fire, so Steven kicked the ground to erect a giant boulder.
Nando came forth and with his power thrusted the boulder toward the Hadad.
Steven jumped to the ceiling, stayed there upside down, and he thrusted his fists, launched his kicks, where firebolts came from his fists and kicks toward the approaching Hadads and Tiamats.

Nando: .., that aren't visitors like us. (continued Nando)

Edo: They were not chasing us before,

Daniel (In): Definitely our life was a normal one before you guys meet us.

Helena (In): Guys, that means, if we exclude my husband and I, the only ones that they chase would be either Edo and, um, this young man here.

Hendrik: I'm Hendrik (he sniffed, as she totally forgot about him). And for the records, they didn't chase me or Edo yesterday either.

Edo: But, Hendrik is the only one among us that is in the list that Manov held.

Helena (In): What list?

Steven: The list! I totally forgot about that.

Pride was swimming on the ground toward the incoming Hadads, but a Hadad penetrated the ground with his palm, and pulled Pride off the ground.
Pride struggled, but before he could do anything about it, the Hadad snapped his neck.
Asoo barked toward them, but the bark beams of Asoo was deflected by a group of three Hadads, with a single move caused a stone wall to be erected from the ground.

Asoo, surprised, didn't expect that two Tiamats are approaching him from the side.
The two Tiamats had their snake-like hairs to launch attacks on his bodies.
He bleeds, wailed, and with their bare arms, they tore Asoo into pieces, then they devoured him in his entirety, pieces by pieces.

Edo: Did they just,

Daniel (In): defeated our guardians?

A Hadad jumped to the front of them, and approached those that aren't visitors on their group, but Daniel (Xe) pulled his feet with his rock-like grip.
He was about to establish eye contact with Hadad, but Hadad shut his eyes, and fought back.
Without access to his eyes, Daniel couldn't turn him unconscious, so all he could do was to wrestle with him.

The group gathered close to one another, losing two of their teams really limits their defense capability.
Steven and Nando wait patiently for an opening to separate that Hadad from Daniel, and Daniel managed to prevent him from closing toward the group with his impenetrable defense.

But more and more Hadads and Tiamats are approaching them.

Steven: I think they adapted with our tactics already.

Daniel (Xe): That's it. (He jumped and with a move around that Hadad's body, he pulled him down, and slammed him to the ground.)

He ran toward the group,

Daniel (Xe): Now Run!

They ran toward the gate.
Steven and Fernando, the only ones capable of ranged attacks, were behind them to launch defense attacks.
Daniel (Xe) and Daniel (In) lead them to the back gate.
Daniel (Xe) dialed his phone, and the autopilot of their OAEV caused it to drive itself from the parking spot to the other side of the gate, waiting for them.

The gate was locked.
With little effort, Daniel (Xe) jumped off the one and a half meter tall fence, and told them to follow.

Daniel (In): That's a freaking tall gate!

Steven prepared a stance and pulled the gate down, it sunk to the ground.
They ran to the other side of the gate, and Steven pulled the gate back up.

Nando: I don't think it is sufficient to stop them.

Steven: I know, I am still thinking.

Daniel (Xe) ran toward the OAEV and let the rest of the group to enter it.

Steven stomped on the ground and raised his stance, and columns of earth erected up.
Nando pushed the columns toward the gate, stacking them to form a wall.
He moved close to the wall and with his power, pulled them that they compressed into a compact, solid wall, while Steven rooted his stance and pushed the stone away from Nando's body.

Steven: That should be more than enough.

Nando and Steven boarded the OAEV and Daniel drove it away from the campus.

Daniel (Xe): So where are we going?

Steven: To Henokh's team.

### Part 29 (Part 16 of the Integra Crisis)

The dormitory was next to a campus, it was flattened to the ground with Zarah's power.
The group translocated to the campus, that Ket recognized to be the site where he fought Zarah in 1993, the day MTF Samsara lost a significant fraction of their force.
Ket looked around, when Zarah and Tiamat landed near them.

Zarah: It appears, we're here again, Ket.

Ket: But this time, we will win, not you.



---

Team 2 got cornered by a Tiamat manifestation.
Romanov involved in a close quarter fight with her, and she was impressed by Romanov's realization of the relativity of this reality.
After she's bored, she's going to kick them off this reality with a hand grip.
The grip is almost completed when Steven ran toward them and raised his phone, that obliterated immediately and sends a shockwave that obliterates all instances of Hadad and Tiamat in the entire campus.
It was Adran's work, etoan tech, in expense of Adran's instance in this world.
Only three were present, that acts as the projector of the (Xe) visitors, also the only ones strong enough to cancel other projections.
As one is down, two are left on Fernando and Daniel.

They decided to return to the gate and initiate a jump out of Integra.
Romanov realized that as they also need projectors in this world to exist as guests, the Men of Tiamat must also have their own projectors, a conduit that translates their existence to Integra environment.
If they managed to find their projectors and destroy it with the expense of one of the protagonists' remaining projectors, they'd be gone from Integra for some time before they could resend their manifestations back.

### Part 30 (Part 17 of the Integra Crisis)

### Part 31 (Part 18 of the Integra Crisis)

### Part 32 (Part 19 of the Integra Crisis)

### Part 33 (Part 20 of the Integra Crisis)

### Part 34 (Part 21 of the Integra Crisis)

### Part 35 (Part 22 of the Integra Crisis)
