# Millennials Rising : Ground Zero to Finding Success

| Label | Value |
| - | - |
| Host & Speaker | Leonard Tanjung S (Entrepreneur) |
| MC | Indra & Huang (Business Partners Entrepreneurs) |
| Zoom Link | https://us02web.zoom.us/j/8498852764?pwd=VFh5dWpaUU5tN2R5d1FWNEZ4anpSUT09 |
| Meeting ID | 849 885 2764 |
| Password | 781266 |


## Sesi dimulai 19.00

Indra di sini adalah Indra SMP.

Dimulai dengan doa, "sebelum memulai sesuatu yang baik, ada baiknya kita mulai dengan doa."

Lauren Katolik, pimpin doa.

Leonard Tanjung masuk.

Welcome ke acara grup ini.
Untuk kebaikan kita bersama.
Teman-teman boleh ambil positif dan buang negatif, semoga bisa jadi berkat dalam hidup teman-teman.

Teman-teman dengari aja dulu sampai tuntas, bakal dibahas cepat.
Jika mau tanya nanti di akhir.

Ortu dulu pegawai biasa, terbiasa hidup sederhana.
Ia tahu kehidupannya Tuhan pasti atur yang terbaik.
Tapi di posisi itu dia berkeinginan menjadi sesuatu yang bermakna.
Dia pengen jadi orang kaya sejak kecil.
Tapi dulu tidak tahu dia mau bagaimana.
Akhirnya tetap berjalan bagai manusia normal.
SD gak banyak teman saya, bisa dibilang cuman dihitung jari.

Dulu waktu SD kan tipe anak yang tertutup, jadi teman SD itu dekat banget dengan saya.
Saking dekatnya, cuman mau temenan sama dia.
Akhirnya pas dia tidak masuk, bangku sebelah saya kosong.
Akhirnya dia pulang waktu bel, karena degaga teman dia mau langsung pulang.
Alasannya sakit gigi lah, kepala, demam, padahal tidak.
Tapi karena terikat dengan temannya, maka perlu dia berubah.

Pas SMP baru masuk dunia bisnis.
Mamanya bikin catering, dia tawarkan ke teman-teman sekolah.
Ternyata banyak yang minat.
Setiap sekolah dia bawa tangannya full kiri kanan dengan makanan teman-temannya.
1 makanan 8-9rb, akhirnya bisa pegang 100-150rb per hari.
Dia sudah rasa banyak, tapi bukan uangnya.
Akhirnya dia pikirkan, ternyata pegang duit itu enak, sampai posisi dia SMA.

SMA dia sekolah normal, tapi ada sesuatu yang buat dia berubah.
Dia kan cowok, makanya harus pandangannya lebih hebat dari orang-orang lain.
Dia harus bisa lihat peluang.
Saat itu dia berubah hidup, tidak enak memang, tapi dipaksa untuk berubah.

Apa sih?
Ternyata dia ditinggal papa untuk selamanya.
Kalau anak kelas 2 SMA ditinggal papanya, apa sih dipikir?
Di posisi itu mama gak pingin anaknya tidak sekolah, hidup susah.
Jadi mama harus menggantikan peran papanya, menghidupi ketiga anaknya.

Kerja keras, dedikasi mamanya, demi kebaikan anak.
Jadi dia sadar kalau dia harus berubah.
Dia tidak mengerti cara cari duit, jadi dia belajar jadi anak baik-baik, tidak macam-macam.

Tapi kalau hanya jadi anak baik, itu tidak cukup.
Kuliah, dihadapkan dengan 3 pilihan:

- hukum, bertentangan dengan hati nurani.
Mamanya tidak setuju.
Tahunya, kalau mama sudah tidak setuju, tidak ada berkat.
- Akuntansi, (saran dari omnya) akhirnya dicek, ternyata hitungan.
Padahal dulu dia tidak pintar hitungan, tidak suka hitung.
Ujian tidak mau belajar, apalagi kalau hitungan.
1.5jam bobok, .5jam kerja di ujian.
- Manajemen business, di WM.
Ternyata ada hitungan juga.
Akhirnya dia belajar sama teman-teman dia.
Tapi dia nyontek sama mereka.

Akhirnya dia pikir, kalau begitu terus mana bisa berkembang.
Ibarat jari kelingking, tidak pernah bisa panjang dari jari tengah, dia merasa soal hitungan itu seperti jari kelingking dia.
Dulu teman-temannya ajak-ajak main-main.

Tapi semester dua ke semester tiga, adiknya kanker.
Akhirnya tidak ada uang.
Mereka perlu hadapi sebagai keluarga.
Akhirnya mereka harus beradaptasi.
Sebagai koko, dia harus support adiknya.
Tapi jadi korban mamanya, akhirnya tidak bisa kerja karena harus rawat mama.

Suatu hari mamanya panggil koko 2 orang.
Mamanya minta maaf, kenapa?
Sepertinya tidak bisa dibayar kuliahnya lagi.

Di posisi itu hancur dia.
Ketika dengar kata-kata itu, serasa ditampar hidupnya.
Dia tahu kalau tidak ada S1, masa depannya di belakang teman-teman yang ada ijazah.
Dia bertanya-tanya ke Tuhan.
Hanya bisa pasrah dia, merasa pecundang, karena anak cowok paling besar tapi tidak bisa apa-apa, sementara di posisi itu mama dia butuh duit.

Dia takut kehilangan masa depannya.
Akhirnya ketemu dia bisnis ini.
Perubahannya signifikan 2 tahun terakhir.

Sebelum bahas bisnis itu, dia mau sharing prestasinya di bisnis ini.
Pada awalnya hanya naik motor, tetap kuliah.
Semester 3 belum terbayar, jadi dia masih kuliah karena semester 2 sudah dibayar.
Ketika tabungannya gak ada, dari 7 pagi, sudah harus jalan dari sidoarjo sejak jam 6, biar gak ngegas kencang-kencang agar gak boros.
Kedua, kan tabungan minus, jadi di kampus, hanya boleh makan 10rb.
Kalau masih lapar, harus tahan sampai malam nantinya.

Lebih lucunya, ketika teman-teman ajakin main (nonton, ngafe, dll), dia tidak ikut.
Akhirnya karena temennya kasihan, diajak main, dibayarkan temannya.
Tapi akhirnya karena bisnis itu, akhirnya dapat mobil dia.
Under 2 years.
Mobil itu diambil pas COVID-19 mulai masuk, maret 2020.

Sebelum bisnis itu, dia tidak pernah naik pesawat.
Pasca bisnis, bisa terbang-terbang ke mana-mana.
Di bandara dia tidak mengerti ngapa-ngapain, takut hilang keringatan.
Bersyukurnya, di surabaya, momentumnya dia grow.

## Masuk materi, Indra siapkan PPT.
Diambil kesimpulan, bisnisnya bisnis anak milenial, yang range usia 18-30 tahun.
Ingin dicetak milyuner keren.
Dunia sekarang, milenial menguasai bisnis, seperti gojek, tokopedia.
2018 menjalankan startup, fakta apa?

Milenial harus bangun bisnis, karena dia dan papanya beda generasi.
Kalau mau kerja keras, papa mama mungkin kerja keras, tapi soal skill kurang.
Dia peka cek peluang.
Kenapa?
Dia pengen keren.

Pertama pengen rumah keren.
Pilih rumah benar-benar rumah, atau rumah dalam gang?
Ini benar-benar rumah yang bisa dinikmati.
Pengen mobil keren, apalagi anak cowok.
Ketika naik mobil keren, meski tampangnya tidak keren, tetap keren.
Lalu pengen nikah keren, ini kan momen sekalo seumur hidup.
Sakral katanya, jangan sampai tidak bisa beri yang terbaik.
Jalan-jalan keren, ya keliling eropa dari timur ke barat, 1 bulan tanpa pikir bisnis.
Soal duit tidak dipikir, sudah ready.

Pokoknya keren itu kaya, dan kaya itu keren.
Keren itu punya duit banyak gak usah lihat harga.
Enak hidup demikian, tapi kesimpulannya, orang kaya bebas memilih.

Coba ngitung keren.

| Objek | Budget |
| - | -: |
| Rumah Keren | 2M |
| Mobil Keren | 1M |
| Nikah Keren | .5M |
| Jalan-jalan Keren | .5M |
| Punya Tabungan (untuk likuiditas) | 1M |
| **Total** | **5M** |

Ketika lihat 5M itu, masih mikir pengen keren kah?
Ketika punya 5M itu, levelnya lebih baik dari teman-teman level lain.
Jawabannya ya kerja dong, kan masih muda.
Gimana kalau gaji 10jt per month.
Kalau digaji demikian, fresh graduate cukup besar, atau besar?
Tentu besar.

Sisihkan 50%, dan 5M tercapai dalam 80 tahun.
Pertanyaannya, umur bagaimana?
Jangan bangka dengan gaji 10jt dong.

Faktanya, 17% saja milenial yang bisa beli rumah di Jakarta.
Hanya 17% punya gaji di atas 7.5jt.
5 tahun lagi tidak bisa beli rumah, karena inflasi properti meningkat lebih tinggi.

Kalau startup business bagaimana?
The world's top 10 startups, butuh modal berapa?
Uber USD 5.6b.
Xiaomi USD 4.6b.

Fakta, 85% bisnis tutup di 5 tahun pertama, ini risiko bisnis.
Kesimpulannya, kalau kerja dengan gaji 10jt, gak mungkin.
Kalau startup bisnis, harus tahu modal, market, dan konsumer.

Modalnya lalu 400jt, untuk hidup saja.
Makanya butuh bisnis keren.
"If you want to be a great company, find social problems to be solved." Jack Ma.

Bisnis, itu simpel.
**Jika ada masalah, bisa diberikan solusi, akan ada profit.**
Ibaratnya kalau sakit tidak bisa coba-coba dong.
Tidak mungkin dia langsung sembuh.
Pertama-tama konsultasi sama dokter, dokter beri obat, akhirnya sembuh.
Demikian juga harga masker.

Ada 2 macam masalah.
Bisnis ada:

1. Short Term.
Ini seasonal, gak bertahan lama. Kalau permintaan turun, omzet turun, demikian juga income.
Tentu kita tidak mau bisnis begitu.
2. Long term.
Year around panjangnya.
Kalau orang lapar, pasti makan, pasti cari nasi.
Makan nasi doang bisa hidup.
Sama, mau ada duit atau tidak, orang pasti lapar, jadi problemnya seumur hidup.

Beberapa nasib dunia usaha di masa korona.

1. Tertekan. Penerbangan, dll.
1. Meningkat. Teknologi, makanan, kesehatan.
1. Bertahan. Gas, peternakan, dsb.

Fokus bisnisnya adalah masalah jangka panjang, yaitu masalah kesehatan.
Survei WHO, 100% manusia meningga.
80% menderita kondisi kritis sebelum meninggal dunia.
Orang rajin olahraga belum tentu tidak sakit.
Anak kecil bisa sakit, orang pintar belum tentu bisa hidup sehat.
Mengobati penyakit kritis itu mahal.

Penyakit kritis ibarat gunung es.
Yang dilihat orang, hanya permukaannya: pengobatan di RS, IDR 500-600jt.
Tapi yang di bawah yang bahaya:

- akomodasi dan transpor ke luar negeri
- Bisnis/pekerjaan.
- Income/gaji.
- Kepercayaan pelanggan dan pemasok.
- Masa depan karir.
- Stress.
- Cicilan (rumah, mobil, kredit)
- Biaya hidup sehari-hari.

Total kerugian 5 kali lipat biaya pengobatan.
Itu semua tidak ada asuransi yang bisa cover.
Apa yang terjadi kalau belum siap dananya?
85% pasien kanker pasti bangkrut.
Penyakit kritis ibarat bom waktu.
Kalau meledak, tidak hanya melukai anda, tapi orang-orang di dekat anda.

Gak zaman lagi nabung di rekening sendiri.
Asuransi dan BPJS, kena kanker, struk, gagal ginjal.
Asuransi ada model:
- inner limit. Hanya sebagian kecil dibayar.
- sesuai tagihan. Ini popular, hanya sebagian kecil ditolak.

Tapi dipikirkan, kalau sakit tidak bisa kerja.
Orang sakit biaya hidupnya lebih mahal.
Kalau sudah begitu, bagaimana fulfil?
Tabungan, habis dong.

Makanya dibuat FInancial Stability Program.
Sudah masukkan, 3jt perbulan.
Kemudian ketika bulan keempat sakit parah, dikirimkan langsung 1M.
Sejak saat sakit, bulan kelima dikirimkan, dijamin sampai 65 tahun.
Ada lagi 1M life protection, dan 1M accident.
Premiere Card: rawat inap, on BILL, kamar VIP, di negara-negara asia.
Tentu FSP yang bisa tolong dong.

Ini program orang-orang terpilih, karena bukan program sosial.
Rentang pasarnya dari orang kecil hingga tua.
Indonesia punya pasar terbesar di dunia, dengan total perdagangan MEA sebesar USD 847m.
Indonesia baru 4% saja yang punya produk ini, sementara singapore ada 250%, Malay 48%, Thai 30%, Japan 600%.

Program ini tentu membantu.
Struktur bisnisnya simpel.
Semua orang bisnis pasti jualan, kita harus belajar lihat market.

| Level | Sumber | Tipe | Y1 | Y2 | Y3-5 |
| ----- | ------ | ---- | -- | -- | ---- |
| BE |  | Comission | 30% | (?) | (?) |
| Business Partner | BP > BE | Overriding | 16.5% | 15% |  |
| Business Partner | BP > BP | Royalty | 5.1% | 5.1% | 5G |

Cara jadi BP,
1. Omzet 300jt ALP (25jt).
1. Pribadi 200jt ALP (17jt). Rekrut 2 BE @50jt ALP.

Tahapnya simpel.

| | | Personal Selling | People Development | Monthly Income |
| - | - | - | - | - |
|1 |BE | 10 keluarga @3jt | 3BE > 3BP | 20jt |
|2 |BP |  | 3BE > 3BP | 35jt |
|3 |BP |  | 3BE > 3BP | 92jt |
|4 |BP |  | 3BE > 3BP | 322jt |
|5 |BP |  | 3BE > 3BP | 1.2M |
| | | 10 keluarga | 15 Calon Leader |  |

Bisnis ini SPEED: Simple, Practical, Easy, Effective, Duplicable.
Misi mulia, hanya cari keluarga yang worth it to be helped.
10 keluarga saja.

Bisnis bukan soal passion, tapi soal peluang.
Perusahaannya apa?
Distributor buka cabang, harus ada brandnya apa, servicenya apa, segmentnya apa (marketnya di segmen masyarakat apa saja), dan kekuatan perusahaan.

Apa kesamaan Bayern Munich, Juventus, sama Ecopark yang di Ancol?
Semuanya punya Allianz.
Allianz itu asuransi, tapi gak ada yang salah dengan produk dan sistemnya?
Gak mungkin dong orang Allianz sendiri yang datang ke sini untuk tawarkan.

Secara company, kita kuat di dunia.
Partneran sama banyak orang.
Sponsor di dunia olahraga.
Di dunia, 70 negara, 88jt nasabah, 33 kuadrilion dana kelolaan.
Total dikelola USD2.4T.
Top 10 largest insurance companies by total assets: Allianz.
Menurut Forbes, top 22 di Dunia sebagai usaha finance and insurance.
Secara brand, tidak ragu kita.
Di Indonesia, sudah bayar 9.5T per tahun; 26M per hari.

Ada legacy compensation.
Bisnis bisa dilanjutkan oleh anaknya tanpa harus mengulang.
Jadi anak tidak mulai dari 0.

Kenapa harus join?
Satu-satunya divis yang setiap tahun bertumbuh 3-5 kali lipat setiap tahun.
Tunggu apa lagi?
Gak perlu ada pengalaman.
Tidak usah takut sendiri, ada yang mentorin.

## Maxi dipanggil masuk.
Baru kenal Leo semester 1.
Pertama kenalan, dia nunjukkan rekaman mobil nyetir 150 kmh.
Jadi pas dari awal kesannya Leo orangnya keren.
Akhirnya dari situ saling kenal pribadi satu sama lain.
Jadi mau ajak Leo itu sungkan, karena dia gak selalu bisa.
Semester 3-4, sempat mau putus kuliah, cerita sambil nangis-nagis.
Setelah kejadian itu, dia ikut bisnis, tapi belum bisa ditawarin bisnis.
Dikeep dulu, kalau jalan, baru dia ngajak Maxi.
Setahun, dia jalankan, berhasil, baru dia cerita ke Maxi, tunjukin income perbulan.
Dia ikut tidak lihat sistem, tapi lihat orang yang ngajak, si Leo ini.

## Leo take over.
Dia senang karena tidak ada yang leave.
Info hari senin 19.00 akan ditunjukkan kantor di Pakuwon Center.

## Post Meeting, Indra contacted me.
    Persyaratannya ini sj

    - KTP
    - Buku Tabungan dpn
    - Foto 3x4
    - Biaya AAJI 225rb TF ke Allianz

    Biaya AAJI itu untuk biaya ujian agar dapat lisensinya saja.

## Raw

    Host & Speaker : Leonard Tanjung S (Entrepreneur)
    MC : Indra & Huang (Business Partners Entrepreneurs)

    https://us02web.zoom.us/j/8498852764?pwd=VFh5dWpaUU5tN2R5d1FWNEZ4anpSUT09

    Meeting ID : 849 885 2764
    Password : 781266
