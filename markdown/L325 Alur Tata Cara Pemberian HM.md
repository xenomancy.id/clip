# Alur Tata Cara Pemberian Hak Milik Perkaban 9/1999 untuk Tanah Belum Terdaftar

<style>body {text-align:justify}</style>

1. Pasal 11, Permohonan HM diajukan kepada Menteri melalui Kepala Kantor Pertanahan yang daerah kerjanya meliputi letak tanah yang bersangkutan.
1. Pasal 12, setelah berkas permohonan diterima, Kepala Kantor Pertanahan:
   1. Memeriksa dan meneliti kelengkapan data yuridis dan data fisik.
   1. Mencatat dalam formulir isian sesuai contoh Lampiran 4.
   1. Memberikan **tanda terima berkas permohonan** sesuai formulir isian contoh Lampiran 5.
   1. Memberitahukan kepada pemohon untuk membayar biaya yang diperlukan untuk menyelesaikan permohonan tersebut dengan rinciannya sesuai dengan ketentuan peraturan perundang-undangan yang berlaku, sesuai contoh Lampiran 6.
1. Pasal 13, Kepala Kantor Pertanahan meneliti kelengkapan dan kebenaran data yuridis dan data fisik permohonan HM atas tanah, dan memeriksa kelayakan permohonan tersebut (dapat diterima atau tidak.
   1. Jika tanah belum ada surat ukur, Kepala Kantor Pertanahan memerintahkan Kepala Seksi Pengukuran dan Pendaftaran Tanah untuk melakukan pengukuran.
   1. Selanjutnya Kepala Kantor Pertanahan memerintahkan:
      - Untuk tanah yang sudah terdaftar dan tanah yang data yuridis dan data fisiknya telah cukup untuk mengambil keputusan, Kepala Seksi Hak Atas Tanah atau petugas yang ditunjuk menuangkan keputusannya dalam Risalah Pemeriksaan Tanah (konstatering Rapport) sesuai contoh Lampiran 7.
      - Untuk **tanah yang belum terdaftar**, Tim Penelitian Tanah memeriksa permohonan yang dituangkan dalam berita acara, sesuai contoh **Lampiran 8**.
      - Untuk permohonan hak selain yang diperiksa sebagaimana dimaksud pada huruf a dan huruf b, Panitia Pemeriksa Tanah A menuangkan hasil pemeriksaan permohonan dalam Risalah Pemeriksaan Tanah sesuai contoh Lampiran 9.
   1. Jika data yuridis dan data fisik belum lengkap, Kepala Kantor Pertanahan memberitahukan kepada pemohon untuk melengkapinya.
   1. Jika telah dipertimbangkan, Kepala Kantor Pertanahan menerbitkan:
      - **Keputusan Pemberian Hak Milik** atas tanah yang dimohonkan; atau
      - Keputusan Penolakan yang disertai dengan alasan penolakannya.

Terlampir:

1. Lampiran 4 model buku Register Permohonan Hak
1. Lampiran 5 model Tandah Terima Berkas Permohonan Hak Atas Tanah
1. Lampiran 8 model Berita Acara Pemeriksaan Tim Peneliti Tanah
