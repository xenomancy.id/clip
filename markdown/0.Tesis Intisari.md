# Bab I: Pendahuluan

## Latar Belakang

## Rumusan Masalah

## Metodologi

# Bab II: HAK MILIK ATAS TANAH SEBAGAI BAGIAN DARI HARTA BERSAMA DALAM PERKAWINAN CAMPURAN

## Konsep Harta Bersama Dalam Perkawinan Campuran
Pasal 35 UU Perkawinan:

1. Harta benda yang diperoleh selama perkawinan menjadi harta bersama.
1. Harta bawaan dari masing-masing suami dan isteri dan harta benda yang diperoleh masing-masing sebagai hadiah atau warisan, adalah di bawah penguasaan masing-masing sepanjang para pihak tidak menentukan lain.

### Konsep Perkawinan menurut UU Perkawinan

### Harta Benda dalam Perkawinan

### Perkawinan Campuran

## Hak Milik atas Tanah dalam Harta Bersama Perkawinan Campuran

### Asas Nasionalitas dalam Hukum Pertanahan

### Kepemilikan Hak Atas Tanah

### Harta Bersama Perkawinan Campuran


# Bab III: KESEPAKATAN PADA PERJANJIAN PERKAWINAN MENGENAI HARTA BERSAMA DALAM PERKAWINAN INTERNASIONAL BERKEWARGANEGARAAN BEDA TERHADAP HAK MILIK ATAS TANAH

## Akibat Hukum Tidak Adanya Kesepakatan untuk Membuat Perjanjian Perkawinan

### Perjanjian Perkawinan

### Makna Kata “Sepakat”

### Kata Sepakat dalam Perjanjian Perkawinan

## Analisis Putusan Mahkamah Konstitusi Terkait Perjanjian Perkawinan

### Perjanjian Perkawinan Sebelum Putusan Mahkamah Konstitusi Nomor 69/PUU-XIII/2015

### Perjanjian Perkawinan Sesudah Putusan Mahkamah Konstitusi Nomor 69/PUU-XIII/2015

### Pisah Harta Benda dalam Perkawinan Campuran