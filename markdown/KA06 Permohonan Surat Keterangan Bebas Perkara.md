# Alur Permohonan Surat Keterangan Bebas Dari Perkara Perdata, Pidana, dan Kepailitan

1. Syarat-syarat berkas untuk mengajukan permohonan:
   - **Surat Permohonan.**
Untuk permohonan surat keterangan bebas dari perkara perdata, pidana, dan kepailitan, masing-masing dibuat satu permohonan.
Pada Surat Permohonan disebut untuk keperluan apa dan surat keterangan apa yang diminta (perdata, pidana, atau kepailitan).
Untuk tiap permohonan dibuat 2 rangkap: 1 asli dan 1 copy untuk tanda terima.
Dibuat oleh pemohon, atau oleh kuasanya.
   - **NPWP Perusahaan.**
   - **KTP Direktur.** Umumnya hanya direktur utama saja, tidak perlu semua direktur.
   - **Akta Pendirian Perusahaan.** Beserta perubahan-perubahannya, dan pengesahan dari kemenkumham.
   - **Tanda Daftar Perusahaan.**
   - **SIUP Perusahaan.**
1. Berkas diserahkan ke ***Loket 1***.
Pemrosesan dimulai setelah turun dispensasi (2-3 hari selesai).
Dari permohonan ke diterbitkannya surat keterangan, umumnya selesai dalam waktu 1 minggu:
1. Pengambilan Surat Keterangan dengan penunjukan tanda terima surat permohonan.
   - Untuk surat keterangan bebas dari perkara **kepailitan**, diambil di ***Loket 2*** (Niaga).
   - Untuk surat keterangan bebas dari perkara **pidana**, dan surat keterangan bebas dari perkara **perdata**, keduanya diambil dari ***Loket 3*** (Kepaninteraan Hukum).
1. Tidak perlu SKCK untuk subjek hukum berupa badan hukum.