<style>body {text-align: justify}</style>

Bahwa pendapat dan alasan Pemohon Kasasi ini mengacu pada ketentuan Pasal 2 ayat (2) dan (2) UU No. 5 tahun 1960 Tentang Peraturan Dasar Pokok-Pokok Agraria, yang menyebutkan:

1. Atas dasar ketentuan dalam pasal 33 ayat (3) Undang-Undang Dasar dan hal-hal sebagai yang dimaksud dalam pasal 1, bumi air dan ruang angkasa, termasuk kekayaan alam yang terkandung didalamnya itu pada tingkatan tertinggi dikuasai oleh Negara, sebagai organisasi kekuasaan seluruh rakyat.
1. Hak menguasai dari Negara termaksud dalam ayat (1) pasal ini memberi wewenang untuk:
   <ol type="a">
      <li>mengatur dan menyelenggarakan peruntukan, penggunaan, persediaan dan pemeliharaan bumi, air dan ruang angkasa tersebut;</li>
      <li>menentukan dan mengatur hubungan-ubungan hukum antara orang-orang dengan bumi, air dan ruang angkasa;</li>
      <li>menentukan dan mengatur hubungan-hubungan hukum antara orang-orang dan perbuatan-perbuatan hukum yang mengenai bumi, air dan ruang angkasa.</li>
   </ol>

Bahwa kemudian dalam Pasal 4 ayat (1) UU No. 5 Tahun 1960 menyebutkan:

> Atas dasar hak menguasai dari Negara sebagaimana yang dimaksud dalam pasal 2 ditentukan adanya macam-macam hak ats permukaan bumi, yang disebut tanah, yang dapat diberikan kepada dan dipunyai oleh orang-orang baik sendiri maupun bersama-sama dengan orang lain serta badan hukum.

Bahwa dalam penjelasan Pasal 4 UU NO. 5 Tahun 1960 terkait macam-macam hak atas tanah menunjuk penjuelasan umum UU No. 5 Tahun 1960 angka romawi II angka 1, disana menyebutkan macam-macam hak atas tanah, yaitu hak milik, hak guna usaha, hak guna bangunan, hak pakai, hak sewa dan hak-hak lainnya.

Bahwa dalam ketentuan Pasal 22 UU No. 5 tahun 1960 menyebutkan:

1. Terjadinya hak milik menurut hukum adat diatur dengan Peraturan Pemerintah.
1. Selain menurut cara sebagai yang dimaksud dalam ayat (1) pasal ini hak milik terjadi karena:
   <ol type="a">
      <li>penetapan Pemerintah, menurut cara dan syarat-syarat yang ditetapkan dengan Peraturan Pemerintah;</li>
      <li>ketentuan Undang-undang.</li>
   </ol>

Dalam penjelasan Pasal 22 UU No. 5 tahun 1960 disebut sebagai misal dari cara terjadinya hak milik menurut hukum adat ialah pembukaan tanah. Cara-cara itu akan diatur supaya tidak terjadi hal-hal yang merugikan kepentingan umum dan Negara.

Bahwa sebagaimana ketentuan Pasal 22 ayat (1) UU No. 5 tahun 1960 bahwa terjadinya hak milik menurut hukum adat diatur dengan Peraturan Pemerintah.
Perlu diketahui hingga sekarang Peraturan Pemerintah yang dimaksudkan Pasal 22 ayat (1) UU No. 5 tahun 1960 belum ada.
Oleh karena itu ketika bidang Agraria/Pertanahan berada di bawah Departemen Dalam Negeri pada tahun 1967, Menteri Dalam Negeri mengeluarkan Peraturan Menteri Dalam Negeri Nomor: 1 Tahun 1967 tentang Pembagian Tugas Dan Wewenang Agraria.
Dalam Pasal 4 ayat (1) Permendagri No. 1 tahun 1967 menegaskan: Kepala Daerah dilarang memberikan tanah negara dengan sesuatu hak apapun, dan ayat (2) menegaskan: Kepala Daerah dilarang memberi izin mempergunakan atau menguasai tanah Negara ataupun memberikan dengan hak lain dengan nama apapun.

Bahwapada tahun 1972 Menteri Dalam Negeri mengeluarkan Peraturan Menteri Dalam Negeri No. 6 tahun 1972 tentang Pelimpahan Wewenang Pemberian Hak Atas Tanah.
Peraturan Menteri Dalam Negeri No. 6 tahun 1972 mencabut Peraturan Menteri Dalam Negeri Nomor: 1 Tahun 1967.
Dalam ketentuan Pasal 10 dan 11 Permendagri No. 6 tahun 1972, jo Konsiderans Menimbang huruf a Permendagri No. 6 tahun 1972 tersebut menggariskan bahwa diberikan kewenangan kepada para Bupati/Walikota dan pada Camat/Kepala Kecamatan untuk memberi keputusan mengenai permohonan izin membuka tanah dalam kedudukan dan fungsinya sebagai wakil Pemerintah.

Bahwa pda tahun 1973 Menteri Dalam Negeri yang saat itu masih membawahi bidang Agraria/Pertanahan mengeluarkan Peraturan Menteri Dalam Negeri No. 5 Tahun 1973 tentang Ketentuan-Ketentuan Mengenai Tata Cara Pemberian Hak Atas Tanah.
Dalam Pasal 4 ayat (1) Permendagri No. 5 tahun 1973 pada pokoknya menyebutkan:
Permohonan untuk mendapat tanah negara Hak Milik diajukan oleh pemohon kepada Pejabat yang berwenang dengan perantaraan Bupati Wali Kota Kepala Daerah c.q. Kepala Sub Direktorat Agraria Kabupaten/Kotamadya yang bersangkutan.
Dalam Pasal 7 ayat (1) Permendagri No. 5 tahun 1973 pada pokoknya menyebutkan: Apabila semua keterangan yang diperlukan telah lengkap, maka Kepala Direktorat Agraria Propinsi atas nama Gubernur Kepala Daerah yang bersangkutan segera mengeluarkan surat keputusan pemberian Hak Milik atas tanah yang dimohon itu.

Bahwa pada tahun 1999 Menteri Negara Agraria/Kepala Badan Pertanahan Nasional  mengeluarkan Peraturan Menteri Negara Agraria/Kepala Badan Pertanahan Nasional No. 9 tahun 1999 tentang Tata Cara Pemberian Dan Pembatalan Hak Atas Tanah Negara Dan Hak Pengelolaan.
Dalam Pasal 9 ayat (1) Permen Agraria No. 9 tahun 1999 menyatakan: Permohonan Hak Milik atas Tanah Negara diajukan secara tertulis.
Dalam Pasal 3 ayat (1) dan (2) Permen Agraria No. 9 tahun 1999 pada intinya menyatakan pemberian hak milik atas tanah dilakukan oleh Menteri dan Menteri dapat melimpahkan kewenangannya kepada Kepala Kantor Wilayah Pertanahan dan Kepala Kantor Pertanahan.
Dalam Pasal 13 ayat (5) dan Pasal 14 ayat (3) Permendagri No. 9 tahun 1999 pada intinya menyatakan: Apabila pemberian Hak Milik dilimpahkan oleh Menteri kepada Kepala Kantor Pertanahan, maka Kepala Kantor Pertanahan menerbitkan keputusan pemberian hak milik atas tanah yang dimohon.
Sedangkan apabila pemberian Hak Milik dilimpahkan oleh Menteri kepada Kepala Kantor Wilayah Pertanahan, maka Kepala Kantor Wilayah Pertanahan menerbitkan keputusan pemberian hak milik atas tanah yang dimohon.

Bahwa ketentuan-ketentuan tersebut di atas merupakan *derviat* dari ketentuan Pasal 2 UU No. 5 tahun 1960 yang pada pokoknya menyatakan bahwa bumi, air, dan ruang angkasa termasuk kekayaan alam yang terkandung di dalamnya dikuasai oleh Negara sebagai organisasi kekuasaan seluruh rakyat.
Hak menguasai dari Negara memberi wewenang pada Negara untuk mengatur dan menyelenggarakan peruntukan, penggunaan, menentukan dan mengatur hubungan-hubungan hukum antara orang-orang dan perbuatan-perbuatan hukum mengenai bumi, air dan ruang angkasa.
Dan ketentuan Pasal 2 UU No. 5 tahun 1960 tersebut merupakan *derivat* dari Konstitusi Pasal 33 ayat (3) yang pada pokoknya menyatakan bumi, air dan kekayaan alam yang terkandung di dalamnya dikuasai oleh Negara.

Bahwa wewenang dari negara untuk mengatur dan menyelenggarakan peruntukan, penggunaan, menentukan dan mengatur hubungan-hubungan hukum antara orang-orang dan perbuatan-perbuatan hukum mengenai bumi *in casu* tanah negara diwujudkan dan dilaksanakan oleh pemerintah sebagai personifikasi dari Negara dengan mengeluarkan kebijakan hukum, yaitu UU No. 5 tahun 1960, Permendagri No. 1 tahun 1967, Permendagri No. 6 tahun 1972, Permendagri No. 5 tahun 1973, Permen Agraria No. 3 tahun 1993, Permen Agraria No. 9 tahun 1999 dan lainnya.

Bahwa dari ketentuan-ketentuan peraturan perundang-undangan tersebut diperoleh petunjuk bahwa seseorang utnuk dapat membuka/membabat tanah Negara harus ada izin dari Bupati atau Camat sesuai dengan kewenangannya masing-masing.
Sedangkan seseorang untuk dapat memperoleh hak milik atas tanah Negara harus ada keputusan pemberian hak milik dari Menteri atau Gubernur ata Kepala Kantor Wilayah Pertanahan/Agraria atau Kepala Kantor Pertanahan Kabupaten sesuai dengan kewenangannya masing-masing.

Bahwa dalam persidangan perkara *a quo* Penggugat/Termohon Kasasi ternyata tidak dapat menunjukkan bukti surat terkait adanya izin dari Bupati Lombok Tengah atau Camat atas tanah sengketa yang merupakan tanah negara yang dikalim dibuka oleh saksi Rede alias Amaq Rede lebih dari 46 tahun yang lalu, yaitu pada tahun 1972 atau sebelum tahun 1972.
Demikian pula Penggugat/Termohon Kasasi tidak mampu membuktikan tanah sengketa sebagai hak milik Rede alias Amaq Rede berdasarkan Surat Keputusan Pemberian Hak Milik atas tanah sengketa dari Menteri atau Gubernur Nusa Tenggara Barat atau Kepala Wilayah Kantor Pertanahan Nusa Tenggara Barat atau Kepala Kantor Pertanahan Kabupaten Lombok Tengah.

Bahwa dalam Putusan *Judex Facti* Pengadilan Tinggi Mataram dalam perkara *a quo* sama sekali tidak menilai dan tidak mempertimbangkan keharusan adanya izin dari Bupati Lombok Tengah atau camat *in casu* camat Pujut terhadap saksi Rede alias Amaq Rede yang mengklaim membuka tanah sengketa yang merupakan tanah negara sebagaimana diamanatkan ketentuan Pasal 10 dan Pasal 11 Permendagri No. 6 tahun 1972, jo Konsideran Menimbang huruf a Permendagri No. 6 tahun 1972.
Demikian pula dalam Putusan *Judex Facti* Pengadilan Tinggi Mataram sama sekali tidak menilai dan tidak mempertimbangkan keharusan adanya surat keputusan pemberian hak milik atas tanah sengketa kepada Rede alias Amaq Rede dari Gubernur NTB atau Kepala Wilayah Pertanahan NTB atau Kepala Kantor Pertanahan Kabupaten Lombok Tengah.

Bahwa sebaliknya Pemohon Kasasi telah memperoleh surat keputusan pemberian hak atas tanah sengketa dari pejabat yang berwenang, yaitu dari Kepala Kantor Wilayah Badan Pertanahan Nasional NTB setelah membeli tanah sengketa dari Lalu Sukrin sehingga terbit Bukti T-2 berupa Sertifikat Hak Milik Nomor: 1761 Desa Kuta Kecamatan Pujut Lombok Tengah.
Oleh karena itu Putusan *Judex Facti* Pengadilan Tinggi Mataram dalam perkara *a quo* adalah putusan yang salah menerapkan hukum atau tidak menerapkan hukum sebagaimana mestinya.

Bahwa oleh karena Penggugat/Termohon Kasasi dalam persidangan perkara *a quo* tidak mampu menunjukkan dan membuktikan adanya surat keputusan izin kepada Rede alias Amaq Rede (yang diklaim sebagai penjual tanah sengketa kepada Termohon Kasasi) untuk membuka tanah sengketa (yang merupakan tanah negara) dari Bupati atau Camat Pujut, juga Penggugat/Termohon Kasasi dalam persidangan perkara *a quo* tidak mampu menunjukkan dan membuktikan adanya surat keputusan pemberian hak milik atas tanah sengketa kepada Rede alias Amaq Rede dari Gubernur NTB atau Kepala Wilayah Kantor Pertanahan NTB atau Kepala Kantor Pertanahan Kabupaten Lombok Tengah, maka menurut hukum keterangan saksi Rede alias AMaq Rede yang mengklaim tanah sengketa dibuka lebih dari 56 tahun yang lalu, yaitu pada tahun 1972 atau sebelum tahun 1972 adalah tidak dapat dipercaya kebenarannya dan *mutatis mutandis* tanah sengketa sesungguhnya tidak pernah dikuasai dan dimiliki oleh Rede alias Amaq Rede.
Oleh karena itu dalil jual beli tanah sengketa antara saksi Rede alias Amaq Rede dengan Penggugat/Termohon Kasasi patut dipertanyakan kebenarannya, meskipun Termohon Kasasi dalam persidangan perkara *a quo* mengajukan bukti dbawah tangan yaitu bukti P-1 berupa Surat Pernyataan Jual Beli tanggal 25 Agustus 2008 antara Dusuki Satriya sebagai pihak pembeli dengan Rede alias Amaq Rede sebagai pihak penjual yang diklaim disaksikan oleh Kiyai Selimah dan Aq Nur dan diketahui oleh Kepala desa Kuta.

Bahwa akan tetapi nama-nama orang yang tercantum pada bukti P-1, yaitu Kiyai Selimah dan Aq Nur maupun Kepala Desa Kuta tidak pernah dihadirkan oleh Termohon Kasasi dalam persidangan perkara *a quo*.
Padahal orang-orang yang dicatut namanya pada bukti P-1 yaitu Kiayi Selimah dan Aq Nur serta Kepala Desa Kuta sangat urgen untuk dihadirkan didepan persidangan perkara *a quo* untuk membuktikan apakah benar atau tidak benar pernah terjadi hubungan hukum jual beli atas tanah sengketa antara Rede alias Amaq Rede dengan Termohon Kasasi.
