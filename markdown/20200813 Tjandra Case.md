# Kasus 20200813

Kerja di A, lalu pinjam uang.
Tidak bisa bayar, pindah ke B.
Kerja di B, lalu pinjam uang.
Tidak bisa bayar, pindah ke C.
Repeat for C, D, dsb.

Terakhir kerja di D misalnya, si D ini diminta bayarin hutangnya.
Si D bayar, lalu akhirnya tidak dibayar sama pekerja i

Pasal apa yang dapat dikenakan?

## Perdata
Yang telah terbukti:

- SMS mengakui utang.
- Pengakuan pinjam uang 150jt ditandatangani Tjandra.

Yang perlu dibuktikan:

- Nomor tersebut adalah nomor milik Tjandra.

Mengenai perjanjian:

- Terbukti ada utang.
- Utang sebesar 150jt.
- Jatuh tempo tidak ditentukan.

| Pasal | Catatan |
| ----- | ------- |
| 1760 BW | Kalau tidak ditentukan jatuh tempo, dan pemberi pinjaman menuntut kembali, hakim yang berkuasa menentukan. |
| 1761 BW | Kalau telah diperjanjikan kalau peminjam akan mengembalikan semampunya, hakim menentukan waktu pengembaliannya. |
