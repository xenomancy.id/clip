# KA28 Periksa

1. PBI No. 18/40/PBI/2016 Lingkup Dompet Elektronik.
1. PBI No. 20/6/PBI/2018 tentang Uang Elektronik tanggal 3 Mei 2018.
2. SE BI No. 16/11/DKSP tanggal 22 Juli 2014 perihal Penyelenggaraan Uang Elektronik (*Electronic Money*).
3. SE BI No. 18/21/DKSP tanggal 27 September 2016 perihal Perubahan atas Surat Edaran Bank Indonesia Nomor 16/11/DKSP tanggal 22 Juli 2014 perihal Penyelenggaraan Uang Elektronik (*Electronic Money*).

Ditandatangani secara elektronik melalui sistem elektronik, termasuk Privy atau DocuSign.

