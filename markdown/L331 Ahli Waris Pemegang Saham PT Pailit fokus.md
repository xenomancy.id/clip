<style>body {text-align: justify}</style>

Pertanyaan:

Dasar Hukum

1. **Pasal 24 ayat (1) UU 30/2004:** Debitor demi hukum kehilangan haknya untuk menguasai dan mengurus kekayaannya yang termasuk dalam harta pailit, sejak tanggal putusan pernyataan pailit diucapkan.
1. **Pasal 25 UU 37/2004:** Semua perikatan Debitor yang terbit sesudah putusan pernyataan pailit tidak lagi dapat dibayar dari harta pailit, kecuali perikatan tersebut menguntungkan harta pailit.
1. **Pasal 833 BW:** Sekalian Ahli waris dengan sendirinya karena hukum memperoleh hak milik atas segala barang, segala hak dan segala piutang si yang meninggal.<br>
Jika timbul suatu perselisihan sekitar soal siapakah ahli warisnya dan siapakan yang berhak memperoleh hak milik seperti di atas, maka Hakim memerintahkan, agar segala harta peninggalan si yang meninggal ditaruh terlebih dahulu dalam penyimpanan.
Untuk menduduki hak milik seperti di atas, Negara harus minta keputusan Hakim terlebih dahulu, dan atas ancaman hukuman mengganti segala biaya, rugi dan bunga, berwajib pula menyelenggarakan penyegelan dan pendaftaran akan barang-barang harta peninggalan dalam bentuk yang sama seperti ditentukan terhadap cara menerima warisan dengan hak istimewa akan pendaftaran barang.
1. **Pasal 52 ayat (5) UUPT:** Dalam hal 1 (satu) saham dimiliki oleh lebih dari 1 (satu) orang, hak yang timbul dari saham tersebut digunakan dengan cara menunjuk 1 (satu) orang sebagai wakil bersama.
1. **Pasal 56 ayat (1) UUPT:** Pemindahan hak atas saham dilakukan dengan akta
pemindahan hak.
1. **Pasal 56 ayat (2) huruf c UUPT:** Akta pemindahan hak sebagaimana dimaksud pada ayat (1) atau salinannya disampaikan secara tertulis kepada Perseroan.
1. **Pasal 56 ayat (3) UUPT:** ***Direksi wajib mencatat pemindahan hak atas saham, tanggal, dan hari pemindahan hak tersebut dalam daftar pemegang saham atau daftar khusus sebagaimana dimaksud dalam Pasal 50 ayat (1) dan ayat (2) dan memberitahukan perubahan susunan pemegang saham kepada Menteri untuk dicatat dalam daftar Perseroan paling lambat 30 (tiga puluh) hari terhitung sejak tanggal pencatatan pemindahan hak.***
1. **Pasal 56 ayat (4) UUPT:** ***Dalam hal pemberitahuan sebagaimana dimaksud pada ayat (3) belum dilakukan, Menteri menolak permohonan persetujuan atau pemberitahuan yang dilaksanakan berdasarkan susunan dan nama pemegang saham yang belum diberitahukan tersebut.***
1. **Pasal 57 ayat (1) huruf c UUPT:** Dalam  anggaran dasar dapat diatur persyaratan mengenai pemindahan hak atas saham, yaitu: ***c. keharusan mendapatkan persetujuan terlebih dahulu dari instansi yang berwenang sesuai dengan ketentuan peraturan perundang-undangan.***
1. **Pasal 57 ayat (2) UUPT:** Persyaratan sebagaimana dimaksud pada ayat (1) tidak berlaku dalam hal pemindahan hak atas saham disebabkan peralihan hak karena hukum, kecuali kehanrsan sebagaimana dimaksud pada ayat (1) huruf c berkenaan dengan kewarisan.

Permenkumham 4/2014, diubah dengan 1/2016, 14/2016, tentang TATA CARA PENGAJUAN PERMOHONAN PENGESAHAN BADAN HUKUM DAN PERSETUJUAN PERUBAHAN ANGGARAN DASAR SERTA PENYAMPAIAN PEMBERITAHUAN PERUBAHAN ANGGARAN DASAR DAN PERUBAHAN DATA PERSEROAN TERBATAS

1. Pasal 27:
   1. Perubahan data Perseroan cukup diberitahukan oleh Pemohon kepada Menteri.
   1. Perubahan data Perseroan sebagaimana dimaksud pada ayat (1) meliputi: ... a. perubahan susunan pemegang saham karena pengalihan saham dan/atau perubahan jumlah kepemilikan saham yang dimilikinya;
