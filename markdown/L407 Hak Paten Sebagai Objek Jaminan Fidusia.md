<style> body {text-align:justify}</style>
# Inggrid Request L407

Pertanyaan:

1. syarat hak kebendaan agar bisa dijadikan jaminan fidusia dielaborasi sedikit
2. apakah paten memenuhi hak kebendaan
3. contoh di negara lain paten  termasuk hak kebendaan

Jawaban:

## Syarat Hak Kebendaan Sebagai Jaminan Fidusia

1. Pasal 1 angka 2 Undang-Undang Nomor 42 Tahun 1999 tentang Jaminan Fidusia (UUJF):

   > Jaminan Fidusia adalah hak jaminan atas benda bergerak baik yang berwujud maupun yang tidak berwujud dan benda tidak bergerak khususnya bangunan yang tidak dapat dibebani hak tanggungan sebagaimana dimaksud dalam Undang-undang Nomor 4 Tahun 1996 tentang Hak Tanggungan yang tetap berada dalam penguasaan Pemberi Fidusia, sebagai agunan bagi pelunasan utang tertentu, yang memberikan kedudukan yang diutamakan kepada Penerima Fidusia terhadap kreditor lainnya.
