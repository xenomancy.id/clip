Pendahuluan Proposal Penawaran Jasa Hukum OXE

# Pendahuluan
Revolusi industri keempat merubah fokus perkembangan perekonomian dari sektor industri dan manufaktur tradisional ke perekonomian berbasis penerapan teknologi informasi dan komunikasi yang menekankan perkembangan komunikasi dan konektivitas.
Salah satu manifestasi dari perubahan tersebut adalah lahirnya sistem transaksi nontunai, yang dewasa ini telah menjadi kebutuhan masyarakat.
Akibat dari timbulnya kebutuhan tersebut, model bisnis penyelenggaraan uang elektronik juga semakin berkembang.
Negara Republik Indonesia sebagai negara hukum dalam mengayomi kebutuhan masyarakat atas penyelenggaraan uang elektronik telah mengeluarkan instrumen hukum yang memayungi bisnis penyelenggaraan uang elektronik, yaitu Peraturan Bank Indonesia Nomor 20/6/PBI/2018 tentang Uang Elektronik (PBI 20/6/PBI/2018).

Berdasarkan Pasal 4 PBI 20/6/PBI/2018, penyelenggaraan uang elektronik memerlukan izin dari Bank Indonesia, dan pihak yang mengajukan izin tersebut harus memenuhi persyaratan umum dan aspek kelayakan.
Pada Pasal 13 PBI 20/6/PBI/2018 diatur mengenai persyaratan aspek kelayakan pihak yang memohon izin, sementara pada Pasal 14 PBI 20/6/PBI/2018, terdapat kewajiban bagi pihak yang memohon izin untuk menyertakan pernyataan dan jaminan (*representation and warranties*) secara tertulis kepada Bank Indonesia.
Pernyataan dan jaminan tersebut diikuti dengan pernyataan dari konsultan hukum yang independen dan profesional berdasarkan hasil uji tuntas dari segi hukum (*Legal Due Diligence*, atau LDD).
LDD sangat diperlukan sebagai salah satu syarat dalam penerbitan izin dari Bank Indonesia, agar pihak penyelenggara uang elektronik dapat menjalankan usahanya.

Pada penjelasan Pasal 14 ayat (3) PBI 20/6/PBI/2018, ditentukan bahwa konsultan hukum tersebut adalah pihak yang secara khusus menyediakan jasa konsultasi hukum.
Di sinilah diperlukan peran Advokat yang menyediakan jasa konsultasi hukum, yang keberadaannya dipayungi oleh Undang-Undang Nomor 18 Tahun 2003 tentang Advokat.
Advokat sebagai profesi mulia (*officium nobile*) yang hadir karena adanya kebutuhan masyarakat untuk mendapatkan pelayanan jasa hukum.
Dengan ini, kami Kantor Advokat dan Konsultan Hukum **MST & Associates** yang beralamat di Ruko Klampis Megah Nomor C-20, Surabaya, menawarkan Jasa Hukum sebagai berikut:

1. **Pada penjelasan Pasal 14 ayat (3) PBI 20/6/PBI/2018, ditentukan bahwa konsultan hukum tersebut adalah pihak yang secara khusus menyediakan jasa konsultasi hukum.**
1. **Memberikan Pendapat Hukum (*Legal Opinion*) berdasarkan hasil Pemeriksaan dari Segi Hukum;**
1. **Melakukan pendampingan kepada UKP pada saat melakukan Registrasi di Bank Indonesia.**

Adapun mengenai detil dari Jasa Hukum yang kami tawarkan, ruang lingkup, jangka waktu pemberian jasa berikut honorarium akan diuraikan pada bagian-bagian berikutnya dari proposal ini.
