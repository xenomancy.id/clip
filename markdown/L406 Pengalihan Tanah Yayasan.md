<style>body {text-align: justify}</style>
# Pengalihan Tanah Yayasan

Pasal 37 ayat (1) Undang-Undang Nomor 16 Tahun 2001 *jo.* Undang-Undang Nomor 28 Tahun 2004 tentang Yayasan (UU Yayasan):

> Pengurus tidak berwenang:<ol type="a"><li>Mengikat Yayasan sebagai penjamin utang;</li><li>Mengalihkan kekayaan Yayasan kecuali dengan persetujuan pembina; dan</li><li>Membebani kekayaan untuk kepentingan pihak lain.</li></ol>

Pasal 26 ayat (2) UU Yayasan:

> <ol type="1"><li>Kekayaan Yayasan berasal dari sejumlah kekayaan yang dipisahkan dalam bentuk uang dan barang.</li><li>Selain kekayaan sebagaimana dimaksud dalam ayat (1), kekayaan Yayasan dapat diperoleh dari:<ol type="a"><li>Sumbangan atau bantuan yang tidak mengikat;</li><li>Wakaf;</li><li>Hibah;</li><li>Hibah wasiat; dan</li><li>Perolehan lain yang tidak bertentangan dengan Anggaran Dasar Yayasan dan/atau peraturan perundang-undangan yang berlaku.</li></ol></li><li>Dalam hal kekayaan Yayasan sebagaimana dimaksud berasal dari wakaf, maka berlaku ketentuan hukum perwakafan.</li><li>Kekayaan sebagaimana dimaksud dalam ayat (1) dan ayat (2) dipergunakan untuk mencapai maksud dan tujuan Yayasan.</li></ol>

Pasal 40 Undang-Undang Nomor 41 Tahun 2004 tentang Wakaf (UU Wakaf):

> Harta benda wakaf yang sudah diwakafkan dilarang:
<ol type="a"><li>dijadikan jaminan;</li><li>disita;</li><li>dihibahkan;</li><li>dijual;</li><li>diwariskan;</li><li>ditukar; atau</li><li>dialihkan dalam bentuk pengalihan lainnya.</li></ol>

Pasal 41 UU Wakaf:

> 1. Ketentuan sebagaimana dimaksud dalam Pasal 40 huruf f dikecualikan apabila harta benda wakaf yang telah diwakafkan digunakan untuk kepentingan umum sesuai dengan rencana umum tata ruang (RUTR) berdasarkan ketentuan peraturan perundang-undangan yang berlaku dan tidak bertentangan dengan syariah.
1. Pelaksanaan ketentuan sebagaimana dimaksud pada ayat (1) hanya dapat dilakukan setelah memperoleh izin tertulis dari Menteri atas persetujuan Badan Wakaf Indonesia.
1. Harta benda wakaf yang sudah diubah statusnya karena ketentuan pengecualian sebagaimana dimaksud pada ayat (1) wajib ditukar dengan harta benda yang manfaat dan nilai tukar sekurang-kurangnya sama dengan harta benda wakaf semula.
1. Ketentuan mengenai perubahan status harta benda wakaf sebagaimana dimaksud pada ayat (1), ayat (2), dan ayat (3) diatur lebih lanjut dengan Peraturan Pemerintah.

Pasal 67 ayat (1) UU Wakaf:

> Setiap orang yang dengan sengaja menjaminkan, menghibahkan, menjual, mewariskan, mengalihkan dalam bentuk pengalihan hak lainnya harta benda wakaf yang telah diwakafkan sebagaimana dimaksud dalam Pasal 40 atau tanpa izin menukar harta benda wakaf yang telah diwakafkan sebagaimana dimaksud dalam Pasal 41, dipidana dengan pidana penjara paling lama 5 (lima) tahun dan/atau pidana denda paling banyak Rp 500.000.000,00 (lima ratus juta rupiah).

Pasal 49 Peraturan Pemerintah Nomor 42 Tahun 2006 tentang Pelaksanaan Undang-Undang Nomor 41 Tahun 2004 tentang Wakaf, sebagaimana telah diubah dengan Peraturan Pemerintah Nomor 25 Tahun 2018:

> 1. Perubahan status harta benda Wakaf dalam bentuk penukaran dilarang kecuali dengan izin tertulis dari Menteri berdasarkan persetujuan BWI.
1. Izin tertulis dari Menteri sebagaimana dimaksud pada ayat (1) hanya dapat diberikan dengan pertimbangan sebagai berikut: <ol type="a"><li>perubahan harta benda Wakaf tersebut digunakan untuk kepentingan umum sesuai dengan rencana umum tata ruang berdasarkan ketentuan peraturan perundang-undangan dan tida kbertentangan dengan prinsip Syariah.</li><li>harta benda Wakaf tidak dapat dipergunakan sesuai dengan ikrar Wakaf; atau</li><li>pertukaran dilakukan untuk keperluan keagamaan secara langsung dan mendesak.</li></ol>
1. Dalam hal penukaran harta benda Wakaf sebagaimana dimaksud pada ayat (2) huruf a dilakukan terhadap harta benda Wakaf yang memiliki luas sampai dengan 5.000 m2 (lima ribu meter persegi), Menteri memberi mandat kepada Kepala Kantor Wilayah untuk menerbitkan izin tertulis.
1. Menteri menerbitkan izin tertulis penukaran harta benda Wakaf dengan pengecualian sebagaimana dimaksud pada ayat (1) berdasarkan:<ol type="a"><li>harta benda penukar memiliki sertifikat atau bukti kepemilikan sah sesuai dengan ketentuan peraturan perundang-undangan; dan</li><li>nilai dan manfaat harta benda penukar paling kurang sama dengan harta benda Wakaf semula.</li></ol>
1. Kepala Kantor Wilayah menerbitkan izin tertulis sebagaimana dimaksud pada ayat (3) berdasarkan:<ol type="a">persetujuan dari BWI provinsi;</li><li>harta benda penukar memiliki sertifikat atau bukti kepemilikan sah sesuai dengan ketentuan peraturan perundang-undangan; dan</li><li>nilai dan manfaat harta benda penukar paling sedikit sama dengan harta benda Wakaf semula.</ol>