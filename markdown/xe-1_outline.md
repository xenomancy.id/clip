## Short Summary
This is a story that explores the insurgency attack of Aditya and its effects.

A clandestine operation is conducted to stop an upcoming insurgency.
However the insurgents managed to steal a divine artifact.
What do they plan with the artifact?

Steven and Daniel got an emergency visit by Anderson, and he told them about the upcoming invasion of the beasts.
Meanwhile Ashton came to inform them about the Second Coming.
What on Earth is happening?

## Notes
Alternative titles are:
- Xenomancy: The Second Coming
- Xenomancy: The Three Ways War
- Xenomancy: The Antichrist, The Beast, and The Christ
- Xenomancy: The Sea Serpent
- Xenomancy: Dawn

# Planned Chapter Structures

## Table
### Parts:
| Part | Chapters | Part Name | Description |
| ---: | -------: | :-------- | :---------- |
| 1 | 6 | The First Trigger | How the separation of Aditya and Henokh started a chain reaction to reveal the hidden world of esotericism and alien involvements. |
| 2 | 3 | The Divine Matters | How the Powers of Earth and their influences affect the life of mortals. |
| 3 | 3 | The Commotion | The fall of the Anderson's Cluster and the start of the Ashton's Cluster. |
| 4 | 4 | Like A Thief | The activation sequence of Ashton's Cluster: The Four Horsemen. |
| 5 | 6 | 2022: The Battle of Kendari | Ashton's Cluster mitigating the denefasan invasion. |
| 6 | 3 | Nightmares | Michelle, David and Anthony on their quest of understanding friendship and relationship. |
| 7 | 6 | The Battle of Medan | Major battles between Anderson's Cluster, Ashton's Cluster, and Aditya's Cluster. |
| 8 | 3 | Out of Hands | Aditya's Cluster gaining an upper hand. |

### Part 1: The First Trigger
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 1 | [**Polar Opposites**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Polar%20Opposites.md) | [Aditya](https://gitlab.com/heno72/xenomancy/-/blob/master/Characters/Aditya%20Wijaya.md) and [Henokh](https://gitlab.com/heno72/xenomancy/-/blob/master/Characters/Henokh%20Lisander.md) were best friends, until they separate into their own ways. |
| 2 | [**The Boy from The Woods**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Boy%20from%20The%20Woods.md) | Aditya found an esoteric [lost boy in the woods](https://gitlab.com/heno72/xenomancy/-/blob/master/Characters/Anthony%20Matthias.md), that led his life to an esoteric organization. |
| 3 | [**Incursion**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Incursion.md) | The founding of an Intelligence Agency, and Henokh's quest to find Aditya again. |
| 4 | [**The Silent Concert**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Silent%20Concert.md) | The assassination  of Dominic Muerte and its consequence to the Paramundus Jagadpadang. |
| 5 | [**The ORCA-strated Event**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20ORCA-strated%20Event.md) | The battle of the Barongs and the Tengus at the real world and its aftermath. |
| 6 | [**The Esoteric Business**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Esoteric%20Business.md) | The founding of the fifth division of the Intelligence Agency. |

### Part 2: The Divine Matters
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 7 | [**Overtime**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Overtime.md) | Encounter of Cal and Anthony. |
| 8 | [**Facing The Forking Path**](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Facing%20The%20Forking%20Path.md) | Cal stayed for too long by Anthony's side that things went against his wishes |
| 9 | [The Lost Son](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Lost%20Son.md) | Cal decided to end his life for a new life as a normal human. |
| 10 | [Messianic Arrival](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Messianic%20Arrival.md) | Os came to Earth, but found that Charles Lee was not available for merger. |
| 11 | [An Offer in a Vacation]() | Charles Lee found his peace, but Ostaupixtrilis Pontirijaris gave him an offer: a mission to humanize humans. |
| 12 | [The Angel's job](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Angel's%20Job.md) | Two angels, two different orders. |

### Part 3: The Commotion
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 13 | 2021: [The Artifact Hunt](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Artifact%20Hunt.md) | What the EPL was planning to do? |
| 14 | [The Breach]() | The quest to protect the Black's Staff. |
| 15 | [The Mass]() | Investigation on Edward Kevlar resulted in knowledge about the mass. |
| 16 | The Umbra Cahaya | An attempt to contact Anthony through Ernie revealed the connection of Anthony and Manov, while Anderson discovered the interference of the Powers on Earth. |
| 17 | [The Birth of Daniel Ashton](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Birth%20of%20Daniel%20Ashton.md) | Charles's meeting with Fernando and his conversation with Tee, triggered his decision to accept Os's offer. |
| 18 | 2021: The Dawn Strike | Anderson played out his plan to end the Godfluenza vectors at once, but was disturbed by Aditya's cluster. |

### Part 4: Like A Thief
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 19 | [Anderson's Failure](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Anderson's%20Failure.md) | Anderson visited Steven and Daniel, to warn Ashton's Cluster about his failure. |
| 20 | [Epiphaneia](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Epiphaneia.md) | The rest of the Nine Tailed Fox task force came to pick Anderson, followed with a visitation by Ashton. |
| 21 | [Grandpa is my classmate](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Grandpa%20Is%20My%20Classmate.md) | Ashton's quest on being a part of his great grandson's life. |
| 22 | [The Invisible Hero](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Invisible%20Hero.md) | David's love story on his encounter with a peculiar woman. |

### Part 5: 2022: The Battle of Kendari
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 23 | [The Archangels](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Archangels.md) | Steven and Daniel on facing the Denefasan invasion. |
| 24 | [The Four Horsemen](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Four%20Horsemen.md) | Ashton's primary team on facing the Denefasan invasion. |
| 25 | [The Bet](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Bet.md) | Steven and Daniel on deciding which one to attack first: a suspicious submarine on the bay or a fleshy floating island-sized whale on the open sea? |
| 26 | [Victory is sweet, reunion is sweeter](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Victory%20Is%20Sweet.md) | The Intelligence Agency finally responded, while Ashton's cluster take a rest. |
| 27 | [Consolidation of the Intelligence Agency](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20Consolidation%20of%20the%20Intelligence%20Agency.md) | Aftermath of the invasion from the perspective of the Intelligence Agency |
| 28 | [The Weakest Link of a Chain](https://gitlab.com/heno72/xenomancy/-/blob/master/Chapters/Xe-1%20The%20Weakest%20Link%20of%20a%20Chain.md) | Aditya's scheme to hurt Steven, by making him choose between his best friend or his first son, followed by a choice to let his son be killed by Aditya, or by his own hands. |

### Part 6: Nightmares
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 29 | Problems of Our Parents | Recovery of Steven, David, and Anthony after the events of the previous chapter. |
| 30 | Dehumanizing a lady is never good | Michelle's attempt to heal both David and Anthony, that ends with a fight to determine which is best for Michelle. |
| 31 | My Friend is The Son of My Father's Enemy | Michelle was mad because she was objectified, that results in the development of a friendship between Anthony and David. |

### Part 7: The Battle of Medan
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 32 | 2025: The Fallen Angel | To fulfill Tiamat's request of global terror, Aditya had to make a decision on whether or not to sacrifice his cousin that was in a flight intended to be shot down. |
| 33 | The 2027 April Fool Insurgency: The Vengeance | Terror tyres spread terrors to the entire Medan, followed by EPL's take over, and Aditya's plan to avenge Steven. |
| 34 | The 2027 April Fool Insurgency: The Four Horsemen's Rescue Mission | The Four Horsemen's quest to help David's brothers from the EPL. |
| 35 | The 2027 April Fool Insurgency: The Esoteric Counter-Response | G28 Forces was helpless against the enemy's esoteric forces, until the Intelligence Agency's Fifth Division came to help. |
| 36 | The 2027 April Fool Insurgency: The Handover | The struggling Archangels got a help from the Four Horsemen, that led to the defeat of Aditya's Cluster, and Steven's decision on whether or not to avenge Aditya. |
| 37 | The Caged Beast | An apparent defeat of Aditya's cluster, and Aditya spilled Anderson's secret assassination plan. |

### Part 8: Out of Hands
| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 38 | The Dragon Assisted Escape | Anthony's quest to save Aditya that ends with Hendrik getting terribly hurt. |
| 39 | Yam's Vacation | Yam was having a vacation, that interfered with Anderson's cluster attempt to halt Aditya's cluster. |
| 40 | The Goddess of the Sea | Anthony's ritual to summon Tiamat went awry. |
