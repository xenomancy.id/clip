# Local Snippets for Xe-1 TBTW

The school Arwin mentioned was set up like any normal school.
The only difference would be the presence of certain extracurricular classes.
Disguised as spiritual and martial art classes, they appeared to be just ordinary extracurricular activities.
Arwin gave Aditya a tour while waiting for Anthony's class to finish.

"The ability to master the environment, is what we are cultivating here.
That ability is classified into what we now call the Magic Spectrum.
Three of the most common would be Type Blue, Type Green, and Type Yellow," said Arwin.

Aditya looked into one of the classrooms.
A gymnastic class where the students were practicing.
Nothing weird, except to the fact that they practiced without toucing anything.
Objects there were free-floating, while the students concentrated on those objects.

"Type Blue consisted of those with the ability to interact with tangible objects remotely, or colloquially called psychokinetic powers, sometimes telekinesis," said Arwin.

At the other room, Aditya observed a bunch of students flipping through papers supposedly containing the problems for them to solve, and they wrote answers at their worksheets.
However Aditya noticed something, the papers they're flipping through were blank sheets.

"Type Yellow consisted of those with extrasensory perceptions, usually passive skills.
The catch-all name of Type Yellow would be those with the sixth sense.
In this batch," Arwin pointed at the classroom Aditya was inspecting, "were those with the ability to observe the properties of objects not normally seen to the naked eyes.
We projected pictures to those papers before handing those papers to them, and those students were instructed to look at the history of interactions those papers had that contain specific images.
Then they have to wrote it down for scoring."

"They could do that?"

"About zero point one per mille of all masses on the surface of Earth were the mass of reflexium particles, a part of the pervasive and ubiquituous network that enabled magic-like activities to occur.
Type Yellows, can access sensory informations that the reflexium particles managed to store, as long as the information were still there."

"So," Aditya looked around, "what about Anthony?"

Arwin smiled.
He brought Aditya to a meditation class, where Aditya could observe Anthony sitting in a chair, conversing with Aurelia.
Anthony was the only one in that class other than the instructor.

"A rarity they are," said Arwin, "we don't have many Type Greens.
Most new mages manifest their powers to be Type Yellow or Type Blue."

"What is so special about them?"

"Type Green is a mixture of Type Yellow and Type Blue, but distinctive from the other two.
They are able to interact with intangible objects.
The ability to converse and interact with spirits is one of the manifestation.
Others can alter your perception of reality by interacting with your essence, your soul.
Some can form pacts with powers," explained Arwin.

Aditya found himself led to yet another room, where all students had the same walkie-talkie like device pinned to their shoulders, just like what Arwin had.
One of the instructor ordered one of them to lift a vase positioned at a table in the middle of the room.
One stood up, and they unpinned the device, turning it on and waved the antenna toward the vase.
The vase lifted off.

"Some of us, that could not perform any of those ability, were provided with training wheels," Arwin unpinned the device at his shoulder, "this, is a Computerized Organotronic with Neodymium-based Calculation Core, or a Conductor for short."

"That is not a proper acronym...," said Aditya.

"We're still looking for a better name, so bear with it for now," Arwin's lips were tightened, "the point is, this device allow us to communicate with the reflexium particles like any natural mages.
It helped us to conduct the environment the way we see fit, though limited to what this Conductor can compute."

"So I will also be converted into a mage here, with or without a training wheel?"

"If you like to, I can procure a Conductor for you."

"Meh, being a mage is not my thing.
So what else can I do here, in this," Aditya raised two of his fingers of each hands, "...,community?"

"Be a part of our organization.
We have non-mage branches here as well.
Mastering magic is never our main objective, you know," said Arwin.

Arwin brought Aditya to a building.
It was a featureless, unremarkable building by the edge of the campus complex.
The reception clerks at the lobby appeared to be quite ordinary, not even very attractive.

Past the lobby and the ground floor, however, the building was bustling with activities.
People roamed about, documents literally flew from one section to another.
Arwin greeted and be greeted by a lot of them.

"Welcome to the control center of the Extrasensory perception and Psychokinesis Laboratory, Kendari City Branch," said Arwin, "where we change the world step by step, under the guidance of our patron god, Lord Yam-Nahar."

Arwin continued to speak about the aim of this organization.
An organization that work in the shadow with an aim to destroy the current governments of the world to establish a new world order.
A world of equal chances, free from poverty, free from scarcity of basic needs, no rulers needed.

Anarchism without competitions, with the aim for the benefits of all.
Personal freedom would be highly upheld, with welfare system that ensures all basic needs of its citizen to be fulfilled.
A liberal-left dream, with an actual mean to solve their problems: equal access to magic, not only restricted by those with the gift to naturally perform it.

"When magic was available for anyone to access, labor would no longer be required.
People would no longer need to focus on how to procure materials, on how to process them, on how to deliver them to where it would be needed.
People would then focus on their personal growth, of limitless path of self-exploration and self-expression.
True freedom, where they would not be bound by struggles just to stay alive," said Arwin.

Aditya however, couldn't focus on any of those sayings.
His mind stopped working when Arwin mentioned Lord Yam-Nahar.
*Lord Yam-Nahar is the answer,* thought Aditya to himself.
"I'm in," said Aditya.

Arwin's eyebrows were raised, then squinted.
His gaze took some moment to observe Aditya,
"that's it? You're in? I haven't even finished my explanation."

"Do you want me in or not?"

"I like your spirit," Arwin smiled, "of course I'd let you in."

\---

It was year two thousand and thirteen, Anthony was enrolled to a junior high school.
He was assigned to a normal school, to enable him to blend in into normal social circles.
It was when he learned an actual power struggle, the survival of the strongest.

It was the day a boy was harassed by a group of junior high senior at the toilet.

"You can't run now, Kiel," said the head bully, shoving the fragile thin boy to the corner near an urinal, "now where's my money?"

Kiel didn't respond at all, his gaze was cold but stern, and it was aimed at the head bully.

"Do you think you're strong?" Said one of the accomplices, he gave Kiel a kick to his ribs.

Anthony raised his hand to one of the bullies from their back.
He was about to speak the name of Aurelia, when a warm but firm grasp lowered his arm.
The owner of that grasp was a high school student with a lean body and white hair -it wasn't gray, but white, as white as a plain paper, with a silvery quality.

"Hi, I am Michael," greeted the boy with white hair to the bullies.

The bullies, despite being about two years older than Anthony, had larger bodies compared to the boy with white hair.
They looked at Michael, scanning every inch of his body, and quickly dismissed him, proceeding to kick Kiel.

"You're Ezekiel Tanputra, a second-grader junior high student, right?"
Continued Michael.

The bullies stopped, they weren't sure what was happening.
"Hey kid, don't you see we're having a business with this sucker here?"
Said the head bully, pulling Kiel's necktie,
"go away and mind your own business."
The entire gang laughed.

Michael's hand, still grasping Anthony's wrist, didn't shake.
Anthony never thought anyone would be this calm, facing three boys bigger tham him.
Grades didn't matter when it comes to a fight, a high school student would not be any stronger than a bunch of giant junior high school bullies.
Anthony would expect that Michael's eyes would at least waver at the threat.

That was when Anthony realized that Michael's irises were white.
That was when Anthony realized that there were no trace of reflexium particles in Michael's body.
That was when Anthony realized that Michael was not nobody, definitely wasn't anything he had ever encountered in his entire life.

"You're not doing business here.
You're bullying, and that isn't nice" said Michael again.

The sternness of Michael's tone somewhat cause the gaze of the bullies to quiver.
"G-go mind your own business!" said an accomplice with a kick thrusted toward Michael.

Effortlessly, Michael slided away, and with his vacant feet, slided the thrusting feet farther away, stretching the accomplice's feet as far away from each other.
The accomplice screamed in pain due to excessive strain on his adductor muscles.
*He would have a hard time trying to stand up,* thought Anthony.

He was in fact unable to move.
His abductor muscles were so in pain he couldn't move them without causing more pain.
He cried, and cried, while the head bully cursed him for not being helpful.

"You're not being nice," said Michael.
His gaze didn't move away from the head bully.

The remaining bullies backed away as Michael approached them.
There was something about being extremely calm under threats, that the ones threatening found it frightening.
Michael didn't need to be brawny or act to scare out the bullies, he just need to be extremely calm, indifferent, and firm in his approach.

Normal people didn't do that.
Michael didn't show fear, and bullies feed off the fear of those they threatened.
With no fear to feed on, the fear emerged in the bullies themselves.

Disregarding the two frozen bullies, and one sobbing bully in a split position at one corner, Michael helped Ezekiel up.
Ezekiel, however, prostrate in front of Michael, "let me be your accomplice!"

Michael smiled, "I'd teach you, so you don't have to be anyone's accomplice."

"Then, let me be your student!" said Ezekiel again.

"Very well."

"L-let me be your student too," said Anthony.

Michael looked back to Anthony.
His gaze was an old soul, full of assurance and wisdom, that Anthony backed away unconsciously.

"Anthony Matthias, we're not supposed to meet yet," said Michael, "let us be in our own ways until the day we meet again."

Michael and Ezekiel left the bathroom.
The two bullies helped their sobbing friend to get up.
They too, left the toilet, all three of them sobbed.
Anthony was left in the toilet, petrified, not sure what was happening.

Weeks after, it appeared that the older brothers of the bullies confronted Michael for hurting their younger brothers.
Rumor had it that they became Michael's students afterwards.
More and more students joined Michael as his students, and they started to acquire members from different schools as well.

Whenever Anthony tried to look for the group, it was like trying to catch eels in a mudpool.
They were slippery and agile, always slipped away whenever they were in one's grasp.
It wasn't long until the new group gained a name, somewhat along the line of a killer whale, as they hunted in pack.
It was an Orca group, or so Anthony heard.

Anthony's attempt to get into the group bear no fruits.
Until one day, about four thousand defectors of the group came to the EPL.
They revealed, that Michael's teaching allows non-mages to acquire mage abilities without any aid.
Aditya, then was a newly appointed chief of the EPL, knew that they must acquire that teaching.
EPL must acquire the so called Orca group.

"What is it like to be in the group?" Asked Anthony to one of the defector.

"It was more like joining a *dojo*, we studied a form of martial art," said the defector.

"Martial art? How does it relate to the acquisition of esoteric abilities?"

"Control over one's meta, it is divided into tiers.
Control over one's body comes first, then followed with metapresence, the ability to project yourself outside your physical body.
After that, would be metaforming, the ability to transform your meta projection to the environment."

"And that metaforming, is how you perform magic?"

The defector scoffed, "we don't use the term magic.
It was an inherent part of the nature, that obeyed one's command only when one is faithful enough to command the nature.
Have you read the Bible?
At Matthew chapter seventeen verse twenty."

Anthony shaked his head.

The defector smiled, "it says that, if you have faith like a grain of mustard seed, you can order a mountain to move, and it will move."

"Just that?"

"The problem is in the faith.
Michael's teaching involves body and mind practices that would help in shaping our faith, in understanding the nature of ourselves, of our meta."

"I don't need to do that to perform magic, I just do it," bragged Anthony.

The defector scrubbed Anthony's hair in delight, "of course, you're naturally gifted, but not everyone are.
To you, faith comes effortlessly."

Aditya joined to the table Anthony and the defector were sitting on.

"Dominic," said Aditya to the defector, "come and join me, we have to discuss something."

Anthony came to learn that night, that despite being very good at the so called *metaforming*, Dominic was very bad at teaching it.
Among the four thousand defectors that joined, at various proficiency over the technique known as M.G. Style, none was as good as Dominic in mastery.
The only way to study the teaching, would be to force Michael himself to teach them.

Skirmishes of EPL officers and ex-Orca members were launched toward the group.
It resulted with a series of bombing to various facilities associated with the target group.
One of them was a bombing at Anthony's school, that was also the headquarter of the Orca group.

"Is it really necessary, father?" Asked Anthony, he sat on their bed.

"Yes," said Aditya, slowly positioned himself beside Anthony, "we need the technique, so more people could use magic.
Only when magic is widely spread, could our goals be achieved."

Anthony nodded, but his gaze turned to his fingers, playing with each other.
He remembered Michael, the kind guy behind the entire Orca movement.
"Why couldn't we just ask them nicely?"

"We couldn't," Aditya sighed, "another mage organization was behind that group, which was why Dominic and his mates left the group in the first place."

"What group?"

"Dominic said, it was the *WTF* guys.
He just couldn't remember what it stood for."
