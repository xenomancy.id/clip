# KB02 Daftar RUPS UKP

K630, RUPSLB PT UKP. Akta Pernyataan Keputusan Rapat Perseroan Terbatas PT. Usaha Kreatif Pembayaran No. 10 tanggal 30-06-2020 (tiga puluh juni tahun dua ribu dua puluh), dibuat di hadapan Nico Adrian Roekiyanto, Sarjana Teknik, Sarjana Hukum, Magister Kenotariatan, Notaris di Kota Malang. Keputusan:

I. Menetapkan untuk merubah Tempat Kedudukan Perseroan Terbatas PT. Usaha Kreatif Pembayaran, berkedudukan di Kota Surabaya, yaitu dengan merubah Pasal 1 ayat 1 dan ayat 2 Anggaran Dasar Perseroan.

II. Menetapkan untuk merubah maksud dan tujuan serta kegiatan usaha Perseroan Terbatas untuk disesuaikan dengan Klasifikasi Baku Lapangan Usaha Indonesia (KBLI), yaitu dengan merubah Pasal 3 ayat 1 dan ayat 2 Anggaran Dasar Perseroan.

III. Menetapkan untuk meningkatkan Modal Dasar yang semula Rp. 6.000.000.000,- (enam miliar rupiah) yang terbagi atas 6.000 (enam ribu) lembar saham dengan nilai nominal Rp.1.000.000,- (satu juta rupiah) per lembar sahamnya, akan ditambah sebanyak 4.000 (empat ribu) lembar saham atau Rp.4.000.000.000,- (empat miliar rupiah), sehingga Modal Dasar seluruhnya menjadi 10.000 (sepuluh ribu) lembar saham atau sebesar Rp.10.000.000.000,- (sepuluh miliar rupiah), sehingga dengan demikian merubah ketentuan Pasal 4 ayat 1 Anggaran Dasar perseroan.

IV. Menetapkan untuk merubah pengaturan pemindahan hak atas saham dan tugas serta wewenang direksi perseroan, yaitu dengan merubah Pasal 7 Anggaran Dasar Perseroan tentang Pemindahan Hak atas Saham dan Pasal 12 Anggaran Dasar Perseroan tentang Tugas dan Wewenang Direksi.

V. Menetapkan menyetujui pengalihan saham sebagai berikut:

| Pemegang Saham Lama | Pemegang Saham Baru | Jumlah Saham | Nominal | Persen (%) |
| --- | --- | ---: | ---: | ---: |
| Tuan HEPPY | Tuan WILLIAM | 30 | Rp.30.000.000,- | 1% |
| PT. USAHA KREATIF INDONESIA | Tuan WILLIAM | 300 | Rp.300.000.000,- | 10% |
| PT. USAHA KREATIF INDONESIA | Tuan CHARLIE ANTHONY | 60 | Rp. 60.000.000,- | 2% |

VI. Menetapkan untuk merubah kepemilikan saham-saham, sehingga untuk selanjutnya susunan para pemegang saham Perseroan adalah sebagai berikut:

| Pemegang Saham | Jumlah Saham | Jumlah Nominal | Persen (%) |
| --- | ---: | ---: | ---: |
| Tuan CHARLIE ANTHONY | 90 | Rp.90.000.000,- | 3% |
| Tuan WILLIAM | 360 | Rp.360.000.000,- | 12% |
| PT. USAHA KREATIF INDONESIA | 2.550 | Rp.2.550.000.000,- | 85% |
| **Total** | **3.000** | **Rp.3.000.000.000,-** | **100%** |

VII. Menetapkan memberikan persetujuan atas pengunduran diri Tuan HEPPY, sebagai Direktur Utama Perseroan dengan memberikan pembebasan pelunasan serta pemberesan tanggungjawab sepenuhnya (*acquit et de charge*) mengenai segala tindakan pengurusan yang telah dilakukan selama masa jabatan mereka.

VIII. Menetapkan untuk merubah susunan pengurus perseroan terbatas tersebut, sehingga dengan demikian susunan pengurus baru Perseroan menjadi sebagai berikut:

- Direktur: Tuan CHARLIE ANTHONY
- Komisaris: Tuan WILLIAM

IX. Menetapkan memberikan persetujuan untuk merubah status Perseroan Terbatas dari yang semula PMDN (Penanaman Modal Dalam Negeri) menjadi PMA (Penanaman Modal Asing), sehubungan dengan adanya kepemilikan saham perseroan.

X. Menetapkan menyetujui untuk merubah seluruh Anggaran Dasar Perseroan untuk disesuaikan dengan ketentuan perundang-undangan dalam rangka Penanaman Modal Asing.

