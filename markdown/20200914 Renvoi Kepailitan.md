# Renvoi Prosedur di Kepailitan
Pasal 127 UU 37/2004

1. Dalam hal ada bantahan sedangkan Hakim Pengawas tidak dapat mendamaikan kedua belah pihak, sekalipun perselisihan tersebut telah diajukan ke pengadilan, Hakim Pengawas memerintahkan kepada kedua belah pihak untuk menyelesaikan perselisihan tersebut di pengadilan.
1. Advokat yang mewakili para pihak harus advokat sebagaimana dimaksud dalam Pasal 7.
1. Perkara sebagaimana dimaksud pada ayat (1) diperiksa secara sederhana.
1. Dalam hal Kreditor yang meminta pencocokan piutangnya tidak menghadap pada sidang yang telah ditentukan maka yang bersangkutan dianggap telah menarik kembali permintaannya dan dalam hal pihak yang melakukan bantahan tidak datang menghadap maka yang bersangkutan dianggap telah melepaskan bantahannya, dan hakim harus mengakui piutang yang bersangkutan.
1. Kreditor yang pada rapat pencocokan piutang tidak mengajukan bantahan, tidak diperbolehkan menggabungkan diri atau melakukan intervensi dalam perkara yang bersangkutan. 

Bantahan yang dimaksud dalam Pasal 127 ayat (1) UU 37/2004 merujuk pada bantahan atas piutang sebagaimana dimaksud dalam ketentuan Pasal 117 UU 37/2004.
Pengadilan yang dimaksud diuraikan pada penjelasan ayat tersebut sebagai berikut: "Yang dimaksud dengan "pengadilan" dalam ayat ini adalah pengadilan negeri, pengadilan tinggi, atau Mahkamah Agung."
Pada praktiknya, bantahan ini dikenal dengan nama *renvoi* prosedur.
Pasal 68 ayat (2) UU 37/2004 menentukan bahwa atas penetapan pada Pasal 127 ayat (1) UU 37/2004 tidak dapat diajukan banding.

Perubahan nama bantahan sesuai dengan norma Pasal 127 ayat (1) UU 37/2004 menjadi *renvoi* prosedur diatur dalam Buku Pedoman Teknis Administrasi dan Teknis Peradilan Perdata Umum dan Perdata Khusus Buku II Edisi 2007, pada bagian B (Teknis Peradilan), angka 5 (Hal-hal lain yang berkaitan dengan Kepailitan), pada huruf b (*Renvoi* Prosedur) halaman 132.
Kemudian istilah *renvoi* prosedur digunakan dalam setiap perkara yang berkaitan dengan prosedur permohonan pencocokan piutang atas bantahan tagihan piutang oleh kurator di pengadilan niaga.

Renvoi dilakukan dengan perintah Hakim Pengawas untuk menyelesaikan perbedaan pendapat akan bantahan tagihan-tagihan tersebut di pengadilan manakala:

- terdapat bantahan atas tagihan kreditur oleh Balai Harta Peninggalan/Kurator dan seorang atau lebih kreditur dalam rapat verifikasi, dan
- Hakim Pengawas tidak berhasil menyelesaikan perbedaan pendapat tersebut.

## Prosedur
Untuk prosedur *renvoi* berlaku ketentuan dan dilakukan sebagai berikut: (disadur dari: [essayhukum.com](https://essayhukum.com/2019/11/06/verifikasiinsolvensi-prosedur-renvoi-dan-pemberesan-harta-pailit/))

1. Hakim pengawas memerintahkan para pihak yang berbantahan, jika tidak bisa didamaikan, untuk menyelesaikan sengketanya kepada pengadilan niaga (*vide* Pasal 127 ayat (1) UU 37/2004) tanpa perlu lagi suatu panggilan dari pengadilan niaga. Persidangan kembali oleh pengadilan niaga ini sering disebut dengan istilah *renvoi*.
1. Para pihak harus diwakili oleh advokat;
1. Para pihak yang berbantahan dapat minta debitor untuk hadir di mana debitor tersebut boleh diwakili oleh advokat;
1. Apabila debitor tidak mau hadir, perkara dilanjutkan berdasarkan berkas yang ada;
1. Yang hadir dalam sidang hanya para pihak yang bersengketa dan kreditor lain yang tidak bersengketa tidak boleh hadir (*vide* Pasal 127 ayat (5) UU 37/2004);
1. Debitor pailit pun dapat mengajukan perlawanan, baik untuk seluruh utang atau untuk sebagian maupun hanya bantahan tentang adanya hak untuk diistimewakan;
1. Jika kreditor yang meminta pencocokan piutang tidak hadir dalam sidang, harus dianggap permintaannya ditarik kembali (*vide* Pasal 127 ayat (4) UU 37/2004);
1. Jika yang mengajukan bantahan terhadap piutang tidak hadir dalam sidang, bantahan dianggap ditarik kembali sehingga hakim mengakui piutang tersebut (*vide* Pasal 127 ayat (4) UU 37/2004);
1. Perkara dilangsungkan secara singkat.
Mengenai perkara yang dilangsungkan secara singkat, menurut Dwi Sugiarto sebagaimana disadur oleh Pupung Faisal
