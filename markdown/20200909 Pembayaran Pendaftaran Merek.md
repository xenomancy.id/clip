# Pembayaran Pendaftaran Merek

## Dasar Hukum
> Sumber: https://dgip.go.id/peraturan-perundang-undangan-terkait-merek

* UU 20/2016 tentang Merek
* PP 22/2018 Tentang Pendaftaran Merek Internasional Berdasarkan Protokol Terkait Dengan Persetujuan Madrid Mengenai Pendaftaran Merek Secara Internasional
* PP 90/2019 tentang Komisi Banding Merek ditetapkan Tanggal 29 Agustus 1995 Tentang Tata Cara Permohonan, Pemeriksaan, dan Penyelesaiaan Banding pada Komisi Banding Merek
* PP 24/1993 tentang Kelas Barang atau Jasa Bagi Pendaftaran Merek ditetapkan Tanggal 31 Maret 1993
* PP 28/2019 tentang Jenis Dan Tarif Atas Penerimaan Negara Bukan Pajak Yang Berlaku Pada Kementerian Hukum dan Hak Asasi Manusia
* Permenkumham 67/2016 Tentang Pendaftaran Merek
* Keputusan Direktur Jenderal Kekayaan Intelektual Nomor HKI-02.KI.06.01 Tahun 2017 tentang Penetapan Formulir Permohonan Merek

## Alur
> Sumber: https://dgip.go.id/prosedur-diagram-alir-permohonan-merek

1. Registrasi akun di merek.dgip.go.id
1. Klik **tambah** untuk membuat permohonan baru
1. Pesan **kode biling** dengan mengisi tipe, jenis dan pilihan kelas
1. Lakukan pembayaran sesuai tagihan pada aplikasi **SIMPAKI**
1. Isi **seluruh formulir** yang tersedia

   > (sumber: https://dgip.go.id/formulir-terkait-permohonan-merek)

1. Unggah **data dukung** yang dibutuhkan
   - Label Merek
   - Tanda Tangan Pemohon
   - Surat Keterangan UMK (jika pemohon merupakan usaha mikro atau usaha kecil).
1. Jika dirasa semua sudah diisi dengan benar, selanjutnya **klik selesai**
1. Permohonan kamu sudah kami terima

## Soal PNBP
Pada lampiran PP 28/2019, h.53, dirumuskan: Permohonan pendaftaran merek yang dilakukan oleh umum secara eletronik (*online*), dikenakan tarif Rp. 1.800.000,- per kelas.
Artinya, untuk setiap kelas yang dimohonkan, bayar Rp. 1.800.000,-.
Jadi kalau permohonannya pendaftarannya ditolak, uang tidak dikembalikan, karena bukan dihitung per pendaftaran, tapi per kelas *permohonan*.