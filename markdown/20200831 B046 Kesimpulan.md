No. [ ]/MST/[ ]/2020

Madiun, [ ] September 2020

Kepada Yth,

**Ketua Pengadilan Negeri Madiun**

*cq.* Majelis Hakim dalam Perkara

Nomor: 32/Pdt.G/2020/PN.Mad.

Pengadilan Negeri Madiun

Jl. Raden Ajeng Kartini No. 7

Madiun

**Perihal: Kesimpulan Atas Gugatan Cerai No. 32/Pdt.G/2020/PN.Mad.**

---

->**Antara:**<-

**Wihady, S.E.**, selanjutnya disebut sebagai **Penggugat**;

->***Melawan:***<-

**Hermin Winarsih, S.E.**, selanjutnya disebut sebagai **Tergugat.**

---

Dengan Hormat,

Untuk dan atas nama **Tergugat**, dengan ini menyampaikan Kesimpulan sebagai berikut:

1. Bahwa, pada tanggal 6 Juli 2020 **Penggugat** telah mengajukan **Permohonan Gugatan Perceraian**, yang didaftarkan pada Pengadilan Negeri Madiun, pada tanggal 6 Juli 2020, dengan nomor registrasi perkara: 32/Pdt.G/2020/PN.Mad.;

1. Bahwa, atas gugatan tersebut telah dilakukan proses mediasi oleh mediator yang ditunjuk oleh Pengadilan Negeri Madiun, dan Mediasi tersebut dinyatakan gagal pada tanggal 15 Juli 2020;

1. Bahwa, Jawaban dari **Tergugat** telah diserahkan melalui fasilitas *e-Court* pada tanggal 22 Juli 2020;

1. Bahwa, berdasarkan pada gugatan **Penggugat**, **Tergugat** telah mengajukan Eksepsi yang dimuat dalam Jawaban tertanggal 22 Juli 2020 dan *Duplik* tertanggal 5 Agustus 2020, yang terhadapnya juga telah mematahkan tangkisan terhadap eksepsi **Penggugat** yang dimuat dalam *replik*nya tertanggal 29 Juli 2020, sehingga dengan tidak dipenuhinya syarat-syarat formil suatu gugatan oleh **Penggugat** dan demi tercapainya "Azas Kepastian Hukum" adalah wajar jika gugatan **Penggugat** tidak dapat diterima;

1. Bahwa, dalam persidangan, **Penggugat** mengajukan alat bukti surat yang bertanda **P-I** sampai dengan **P-V** pada persidangan tertanggal 12 Agustus 2020, alat bukti surat bertanda **P-VI** dan **P-VII**, serta 3 (tiga) orang saksi atas nama **Santoso**, **Agus Karyono**, dan **Charles Jefferson** pada persidangan tertanggal 26 Agustus 2020;

1. Bahwa, dalam persidangan, **Tergugat** mengajukan alat bukti surat yang bertanda **T-1** sampai dengan **T-5**, dan **T-7** pada persidangan tertanggal 12 Agustus 2020, alat bukti surat bertanda **T-6**, **T-8A**, **T-8B**, dan **T-8C** pada persidangan tertanggal 26 Agustus 2020, serta 2 (dua) orang saksi atas nama **Tony** dan **Veronica** pada persidangan tertanggal 2 September 2020;

1. Bahwa saksi yang diajukan oleh **Penggugat** atas nama **Charles Jefferson** di bawah sumpah memberikan keterangan sebagai berikut:

   - Saksi bekerja sebagai *supplier* material untuk **Penggugat** dan **Tergugat**.

   - Saksi tidak tahu banyak soal masalah antara Wihadi dan Hermin, hanya diberitahu **Penggugat** bahwa ada persidangan sekitar 4-5 hari sebelum saksi memberi kesaksian.

   - Saksi tidak mengetahui kapan **Penggugat** dan **Tergugat** melakukan perkawinan, dan dimana perkawinan tersebut itu dilangsungkan.
Saksi hanya mengetahui bahwa **Penggugat** dan **Tergugat** aktif di gereja.

   - Saksi mengetahui adanya gugatan cerai antara **Penggugat** dan **Tergugat** sebelum gugatan ini, hanya saksi tidak ingat persisnya.
Setelah itu rujuk ketika mediasi dengan seorang pendeta.
Pasca rujuk, Saksi tidak mengetahui kehidupan perkawinan **Penggugat** dan **Tergugat** seperti apa.

   - Saat akan dilakukan pencalonan **Penggugat** sebagai Caleg, **Penggugat** dan **Tergugat** mendatangi Saksi untuk diajak makan.
Pada waktu itu, **Tergugat** tidak banyak bicara, Saksi dan **Penggugat** hanya berbicara soal urusan pencalonan, tidak soal masalah pribadi.
Menurut Saksi, pada waktu itu sekilas tampak **Penggugat** dan **Tergugat** akur-akur saja.

   - Menurut Saksi, sekitar 1 minggu sebelum sidang, **Penggugat** mencalonkan diri di partai jadi Caleg.
Sebelum hari pendaftaran pencalonan, **Penggugat** di Jakarta.
Pada waktu pendaftaran, **Penggugat** dan **Tergugat** datang ke tempat Saksi, tampaknya baik-baik saja.
Pada kegiatan tersebut, **Tergugat** mengatur konsumsi untuk kegiatan partai tersebut.

   - Saksi mengetahui bahwa **Penggugat** dan **Tergugat** memiliki rumah di Jalan Sikatan, karena pertama kali Saksi jemput **Penggugat**, itu di rumah jalan Sikatan.
Penggugat memberitahu saksi bahwa dulu rumah tersebut dibeli bersama-sama oleh Penggugat dan Tergugat.

   - Saksi memperoleh informasi bahwa **Penggugat** melakukan pengurusan di lokasi, sementara keuangan diurus oleh **Tergugat**.
Secara pribadi, Saksi tidak mengetahui pasti apa benar keuangan dikendalikan oleh **Tergugat**.

   - Saksi mengetahui bahwa **Penggugat** memiliki jadwal kegiatan yang harus diikuti **Penggugat**, namun Saksi tidak mengetahui pasti apakah jadwal tersebut dibuat oleh **Tergugat** atau **Penggugat**, atau apakah jadwal tersebut merupakan tulisan tangan siapa.

   - Saksi menerangkan bahwa sebelum gugatan cerai yang pertama, Saksi dan **Penggugat** pernah pergi ke Pare, Kabupaten Kediri.
Pada waktu itu **Penggugat** berkali-kali dihubungi **Tergugat** melalui telpon.
Saksi mengetahui itu adalah **Tergugat**, karena waktu itu Saksi yang bersama-sama dengan **Penggugat** mendengar lewat telpon suara **Tergugat**, yang mengatakan jika **Penggugat** kalau tidak segera pulang, pintu rumah akan dikunci oleh **Tergugat**.

   - Setelah ditelpon oleh **Tergugat**, Saksi mengantarkan **Penggugat** hingga ke gerbang depan rumah, dan setelah itu langsung saksi tinggalkan.
Saksi tidak mengetahui apakah **Penggugat** dibolehkan masuk rumah oleh **Tergugat** atau tidak.
Saksi mendengar informasi bahwa pintu telah dikuncikan oleh **Tergugat**, sehingga **Penggugat** terpaksa tidur di depan rumah.
Ketika Saksi menanyakan, **Penggugat** hanya menyampaikan bahwa pintunya telah dikuncikan.

   - Saksi kurang mengetahui **Penggugat** dan **Tergugat** tinggal di mana saat ini.
Terakhir kali Saksi berbicara dengan **Penggugat**, **Penggugat** sedang berada di Jakarta.
Mengenai kediaman **Tergugat**, Saksi hanya mendengar informasi dari teman-teman saksi bahwa **Tergugat** saat ini tinggal di Carukan, dan menitipkan rumah di Sikatan untuk dijual.

   - Saksi hanya mengetahui bahwa **Penggugat** saat ini berada di Jakarta untuk urusan pekerjaan. **Penggugat** tidak menceritakan soal adanya pertengkaran, pisah ranjang, dan sebagainya.

   - Terakhir ketemu **Penggugat** sebelum sidang.
Saat berbincang-bincang, ternyata **Penggugat** masih menjadi pengurus di Sekolah Tiga Bahasa.
Saksi bertanya kenapa masih jadi pengurus, sementara sekarang tinggalnya di Jakarta, dan **Penggugat** menjawab bahwa ia masih memiliki tanggung jawab di sana.
Saksi bertanya lagi apakah **Penggugat** siap untuk sidang, dan **Penggugat** menjawab siap, karena memang niatnya pisah.

   - Selama berbincang-bincang, Saksi tahu bahwa **Penggugat** ada pegang uang untuk anak-anaknya.
Menurut Saksi, **Penggugat** ada mengirimkan uang ke rekening Willy, anak pertama.
Ada polis asuransi anak-anaknya juga dibayarkan oleh **Penggugat**.

   - Mengenai alasan dari perceraian para pihak, Saksi kurang mengetahui secara langsung.
Saksi mendengar cerita bahwa **Tergugat** bercerita dengan isteri saksi, bahwa **Penggugat** sudah tidak mau tahu lagi soal **Tergugat**,
Soal gugatan perceraian, berdasarkan yang diceritakan isteri Saksi kepada Saksi, **Tergugat** telah pasrah.
Mengenai makna pasrah sendiri, Saksi tidak tahu pasti.

1. Bahwa berdasarkan pada keterangan saksi atas nama **Charles Jefferson**, dapat disimpulkan:

   - Bahwa dari keterangan saksi di atas mengenai hal-hal berikut ini adalah keterangan yang bersifat *testimonium de auditu*, yaitu kesaksian karena mendengar dari orang lain:

     - Keuangan **Penggugat** dikendalikan oleh **Tergugat**, karena Saksi hanya mendengar dari teman-teman saksi saja.

     - Pada saat perjalanan pulang dari Pare, Kabupaten Kediri, **Penggugat** tidak dibukakan kunci rumah sehingga terpaksa tidur di luar rumah.

     - Para pihak tidak tinggal bersama-sama lagi, karena Saksi hanya mendengar dari **Penggugat** bahwa beliau tinggal di Jakarta, sementara **Tergugat** tinggal di Carukan diketahui Saksi berdasarkan cerita dari isteri Saksi.

     - Bagi **Tergugat**, **Penggugat** sudah tidak mau tahu lagi soal **Tergugat**.

     - Setelah mengetahui **Penggugat** menggugat cerai, **Tergugat** merasa pasrah akan gugatan tersebut.

     Sehingga berdasarkan penjelasan Pasal 171 HIR, keterangan saksi harus merupakan keterangan atas apa yang dia lihat, dengar, dan alami sendiri, bukan merupakan hal-hal yang ia ketahui dari keterangan-keterangan dari orang lain. (Soesilo, R. (1995). RIB / HIR Dengan Penjelasan. Politeia, h. 125)
Pada Pasal 1907 Kitab Undang-Undang Hukum Perdata (KUH Perdata) menentukan bahwa setiap kesaksian harus disertai dengan alasan-alasan bagaimana diketahuinya hal-hal yang diterangkan oleh saksi tersebut, dan tidak termasuk pendapat-pendapat maupun perkiraan-perkiraan khusus yang diperoleh dengan jalan pikiran saksi tersebut.

     Berdasarkan ketentuan-ketentuan tersebut, adalah wajar bahwa keterangan-keterangan yang bersifat *testimonium de auditu* dikesampingkan dari pertimbangan ***Majelis Hakim Yang Mulia***.

   - Bahwa berdasarkan keterangan Saksi, yang diketahui sendiri oleh Saksi adalah hubungan Para Pihak tampak baik-baik saja, tidak ada tanda-tanda pertengkaran, dan tidak ada masalah.

   - Bahwa pada saat kegiatan pencalonan **Penggugat** sebagai caleg, **Tergugat** bersama-sama dengan **Penggugat**, bahkan membantu menyiapkan konsumsi untuk acara tersebut. Hal ini membuktikan **Tergugat** masih memberikan dukungan atas urusan-urusan **Penggugat**.

   - Bahwa berdasarkan keterangan Saksi, ternyata **Penggugat** *masih memegang uang* yang ditransferkannya ke anak pertama Para Pihak. Hal ini ***bertentangan*** dengan keterangan yang diberikan Saksi sendiri bahwa keuangan diatur oleh **Tergugat**.

1. Bahwa saksi yang diajukan oleh **Penggugat** atas nama **Agus Karyono** di bawah sumpah memberikan keterangan sebagai berikut:

   - Saksi menerangkan bahwa Saksi mengenal baik **Penggugat**, karena dulu pernah bekerja di perusahaan **Penggugat** dan **Tergugat** selama 3 (tiga) tahun di tahun 2000.
Saksi keluar karena dirinya kurang menguasai soal gipsum.

   - Saksi hanya mengenal **Tergugat** secara sekilas saja karena jarang ngobrol dengan **Tergugat**.
Menurut Saksi **Tergugat** tidak ada masalah dengan pegawainya.

   - Saksi menerangkan bahwa pada tahun 2009 Saksi sering bertemu **Penggugat** pada tahun 2009 sebagai partner kerja **Penggugat**, setelah itu bertemu sekali lagi pada tahun 2019.

   - Saksi menerangkan bahwa ia tidak tahu kapan Para Pihak melangsungkan perkawinan, tapi pada tahun 2009 Wihadi Hermin, masih suami isteri.

   - Saksi menerangkan bahwa Para Pihak sudah tidak serumah lagi.
Saksi mengetahui hal tersebut karena sewaktu menjadi pegawai, Saksi sering datang di rumah Jalan Sikatan, Kecamatan Madiun.
Saksi tidak melihat **Penggugat**, sementara **Tergugat** masih tinggal di rumah itu hingga tahun 2009.
Sepanjang tahun 2009 bertemu dengan para pihak, setahu Saksi mereka tidak serumah lagi, sementara pada tahun 2019 hanya bertemu sebentar saja.

   - Saksi tidak mengetahui **Penggugat** tinggal di mana, tapi **Penggugat** tidak tinggal di Jalan Sikatan.
Saksi pernah dengar bahwa **Penggugat** pergi ke Malang dan Jakarta, sisanya tidak tahu.

   - Saksi tidak pernah melihat atau mengetahui bahwa Para Pihak terlibat dalam pertengkaran fisik.
Yang Saksi tahu, **Tergugat** berlaku otoriter terhadap **Penggugat**.
Saksi berkesimpulan kalau **Tergugat** keluar, jam 9 malam sudah harus pulang, karena **Tergugat** sering menelpon **Penggugat**, itu sekitar tahun 2000.
Saksi tidak tahu bertengkar karena apa, karena marahannya melalui telpon.

   - Saksi mengaku bahwa pada tahun 2019, Para Pihak sudah tidak akur lagi, sudah cuek acuh, itu di rumah mereka di Jalan Sikatan. Saksi *menyimpulkan* hal itu karena **Tergugat** tidak bergabung diskusi, bukan karena diacuhkan. Saat ditanyai sesuatu sama **Penggugat**, **Tergugat** hanya menjawab ketus sambil terus jalan. Hanya sejauh itu saja yang Saksi tahu, tidak pernah lihat Para Pihak bertengkar selama pertemuan.

   - Menurut Saksi, **Tergugat** sangat mengontrol ketat kehidupan **Penggugat** karena **Penggugat** dilarang ketemu orang atau keluar kota di atas jam 9 malam. Tidak peduli perginya sama siapa, biar sama teman sekalipun, tidak dipedulikan oleh **Tergugat**. Jika ada tamu di rumah, pada jam 9 malam sudah disuruh pulang oleh **Tergugat**.

   - Mengenai apakah **Tergugat** berbuat atau berkata kasar kepada **Penggugat** di hadapan Saksi, Saksi tidak tahu. Yang Saksi tahu, semua tagihan harus melalui Bu Hermin, karena setahu Saksi, Bu Hermin merupakan bagian keuangan di perusahaan bersama Para Pihak.

   - Mengenai mengapa Para Pihak tidak tinggal serumah lagi, Saksi kurang tahu, Saksi hanya tahu diberitahu oleh temannya bahwa Para Pihak sudah tidak akur lagi, makanya tidak serumah.

   - Saksi mengaku kurang paham mengapa perkara ini bisa timbul. **Penggugat** hanya minta tolong kepada Saksi untuk memberikan keterangan di persidangan. Saksi tahu bahwa dirinya diminta menjadi Saksi untuk perkara pisahnya Para Pihak.

   - Sewaktu **Penggugat** meminta tolong Saksi memberikan keterangan dalam persidangan, **Penggugat** tidak menjelaskan apa masalahnya dan karena apa. Berdasarkan cerita dari teman-teman Saksi, itu karena Para Pihak sudah tidak akur lagi.

1. Bahwa berdasarkan pada keterangan saksi atas nama **Agus Karyono**, dapat disimpulkan:

   - Bahwa dari keterangan Saksi di atas mengenai hal-hal berikut ini adalah keterangan yang bersifat *testimonium de auditu**, yaitu kesaksian karena mendengar dari orang lain, atau karena persangkaan-persangkaan dari Saksi sendiri:

     - **Penggugat** tinggal di Malang atau di Jakarta.

     - Para Pihak sudah tidak akur lagi, sehingga tidak tinggal bersama-sama lagi.

     - **Tergugat** bersifat otoriter karena mengatur jam pulang dari **Penggugat**, dan meminta tamu **Penggugat** untuk pulang pada jam 9 malam ketika mereka berkunjung ke rumah **Tergugat**.

     - Para Pihak sudah tidak akur lagi karena pada saat bertemu di rumah Jalan Sikatan pada tahun 2019, **Tergugat** tidak ikut diskusi, dan ketika ditanyai oleh **Penggugat**, hanya dijawab oleh **Tergugat** sambil terus berjalan.

     - Para Pihak tidak tinggal bersama lagi karena Saksi tidak melihat **Tergugat** di rumah Jalan Sikatan saat Saksi datang berkunjung. Saksi hanya melihat **Tergugat** tinggal di rumah tersebut.

     Berdasarkan ketentuan Pasal 171 HIR dan Pasal 1907 KUH Perdata, keterangan yang diberikan oleh Saksi haruslah disertai dengan alasan-alasan bagaimana diketahuinya hal-hal yang diterangkan oleh saksi tersebut, tidak termasuk pendapat-pendapat maupun perkiraan-perkiraan khusus yang diperoleh dengan jalan pikiran saksi.
Keterangan tersebut harus merupakan hal-hal yang dia lihat, dengar, dan alami sendiri, bukan atas hal-hal yang didengar Saksi dari pihak lain.
Atas dasar tersebut, adalah wajar apabila keterangan-keterangan yang diberikan Saksi atas dasar mendengar cerita dari orang lain maupun atas hal-hal yang Saksi simpulkan sendiri berdasarkan keadaan yang Saksi amati dikesampingkan sebagai pertimbangan hukum dalam perkara ini.

   - Berdasarkan penjelasan Saksi, terkuak bahwa **Tergugat** merupakan bagian keuangan dari usaha bersama milik Para Pihak, sehingga wajar apabila segala tagihan dilakukan melalui **Tergugat**.

   - Terlepas dari persangkaan-persangkaan pada keterangan Saksi dan hal-hal yang didengar Saksi dari teman-teman Saksi, Saksi sendiri *tidak pernah melihat atau mengamati langsung* Para Pihak berada dalam pertengkaran.

1. Bahwa saksi yang diajukan oleh **Penggugat** atas nama **Santoso** di bawah sumpah memberikan keterangan sebagai berikut:

   - Saksi tidak tahu masalahnya karena apa, dan kenapa dirinya bisa berada di perkara ini. Yang Saksi tahu, Saksi dimintai keterangan karena masalah perceraian.

   - Saksi hanya diminta jadi saksi oleh **Penggugat**, lalu disanggupi Saksi. Sejauh pengetahuan Saksi, Saksi hanya dimintai menerangkan bahwa Saksi tidak pernah bertemu dengan **Penggugat**.

   - Mengenai gugatan perceraian sebelumnya, Saksi tidak tahu. Setahu Saksi, **Penggugat** dan **Tergugat** baik-baik saja.

   - Saksi tidak tahu kapan **Penggugat** dan **Tergugat** melangsungkan perkawinan, tapi setahu Saksi mereka suami isteri. Saksi tahu Para Pihak mempunyai dua anak, anak perempuan dan anak laki-laki.

   - Sekitar tahun 2005, kemana-mana **Penggugat** dan **Tergugat** selalu jalan berdua. Sering ketemu Saksi di jalan, itu mereka berduaan. Tidak pernah saksi dengar mereka bertengkar.

   - Saksi tidak pernah bersosialisasi dengan **Penggugat**, hanya kenal sebagai tetangga. Tidak pernah keluar dari pintu, hanya kelihatan di jalan. Kalau lewat biasanya klakson.

   - Saksi dulu tinggal di belakang rumah **Penggugat** dan **Tergugat**. Saksi sudah tinggal di sana sejak tahun 1958, lalu pada tahun 2005 pindah rumah sekitar 200 meter dari rumah **Penggugat**. Beda gang, beda RT dan RW.

   - Saksi tahunya **Penggugat** dan **Tergugat** tinggal di Jalan Sikatan Nomor 72.

   - Saksi tidak ingat sejak kapan **Penggugat** dan **Tergugat** pindah ke rumah Sikatan. Setahu Saksi, pada saat Saksi mulai tinggal di sana, awalnya tidak ada **Penggugat** dan Tergugat.

   - Sudah 12 tahun Saksi tidak ketemu **Penggugat**. Ketemu lagi pas tahun 2018, itu pada saat apel di lapangan. Pada waktu itu **Penggugat** ada bersama-sama dengan **Tergugat**.

   - Waktu klakson-klakson pas ketemu di jalan, **Penggugat** tidak pernah bersama-sama dengan **Tergugat**, seringkali dengan teman laki-laki **Penggugat**, atau sendirian.

   - Soal **Penggugat** pisah dengan **Tergugat**, Saksi tidak tahu. Saksi tahunya selama 12 tahun itu tidak bertemu. Saksi tidak tahu kenapa bisa tidak bertemu lagi, atau kalau ada pertengkaran di antara **Penggugat** dan **Tergugat**. Selama tidak ketemu itu, Saksi tidak tahu **Penggugat** ada di mana.

   - Saksi tidak pernah bersosialisasi dengan **Tergugat**. Saksi tahu dari isteri Saksi, karena **Tergugat** sering ikut arisan dengan isteri Saksi.

   - Soal rumah di Jalan Sikatan dijual, setahu Saksi yang dijual bukan rumahnya, hanya gudangnya saja. Rumah itu ada dua, satunya rumah tinggal, yang satunya rumah gudang. Gudang bergabung dengan rumah. Saksi tidak tahu siapa yang menjual dan karena apa, Saksi hanya tahu di atas gudangnya ada ditulis, "Gudang Dijual," jadi bukan rumahnya.

   - Sepengetahuan Saksi, gudang itu dulunya gudang snack-snack, hanya sudah kosong sekarang. Sudah hampir 2 tahunan tidak ada kelihatan pegawai di situ.

   - Saksi tahu **Tergugat** tinggal di rumah tinggal Jalan Sikatan, sampai sekarang. Setahu saksi, rumah ditinggali **Tergugat** dan 2 anaknya.

   - Saksi mengetahui bahwa **Penggugat** masuk partai Demokrat.

1. Bahwa berdasarkan pada keterangan saksi atas nama **Santoso**, dapat disimpulkan:

   - Bahwa rumah tangga **Penggugat** dan **Tergugat** terlihat baik-baik saja, tidak pernah terlihat ada masalah.

   - Bahwa rumah di jalan Sikatan yang ditawarkan untuk dijual adalah rumah gudang, yang terletak di samping rumah tinggal, bukan rumah tinggalnya. Rumah tinggal tersebut ditinggali oleh **Tergugat** dan dua orang anaknya.

   - Bahwa ada jangka waktu selama 12 tahun sebelum tahun 2018 di mana Saksi tidak pernah bertemu dengan **Penggugat.**

   - Bahwa pada saat Saksi bertemu lagi dengan **Penggugat** di lapangan untuk apel pagi pada tahun 2018, **Penggugat** sedang bersama-sama dengan **Tergugat**.

1. Bahwa Saksi yang diajukan oleh **Tergugat** atas nama **[XXX]** di bawah sumpah memberikan keterangan sebagai berikut:
   - Keterangan

1. Bahwa berdasarkan pada keterangan saksi atas nama **[XXX]**, dapat disimpulkan:
   - Simpul

1. Bahwa, mengenai bagian dalam eksepsi yang diajukan oleh **Tergugat**, setelah memperhatikan fakta-fakta hukum secara keseluruhan alat bukti yang terkuak selama persidangan, maka dapat ditarik kesimpulan sebagai berikut:

   **Exceptie Obscuur Libel**:

   - Pada Jawaban, **Tergugat** menguraikan: bentuk gugatan tidak jelas antara gugatan *voluntair* atau gugatan *contentiosa*.
   Gugatan *voluntair* tidak berasal dari sengketa antara para pihak, sifatnya berdasarkan kepentingan sepihak saja, tidak ada pihak lain yang diarik sebagai lawan. (Harahap, 2009:29)
   Gugatan *contentiosa* berasal dari sengketa antara dua pihak atau lebih, sehingga gugatannya bersifat partai (*party*), yang terdiri dari pihak penggugat (*plaintiff*, pihak yang mengajukan gugatan hukum atau klaim) dan pihak tergugat (*defendant*, pihak yang digugat). (Harahap, 2009:47-48)
   Akibat perbedaan mendasar dari karakteristik gugatan *voluntair* dan gugatan *contentiosa*, kedua bentuk gugatan tersebut tidak dapat dipersamakan.

     Berdasarkan fakta yang dapat ditemukan dari perihal gugatan yang diajukan oleh **Penggugat**, redaksional kalimat yang digunakan "**Permohonan Gugatan Perceraian**."
     Hal tersebut terjadi pula pada petitum gugatan, yang menggunakan terminologi seperti "**permohonan perceraian**" dan "**permohonan gugatan cerai**."
     Ketidakjelasan yang dimaksudkan dalam eksepsi ini adalah mengenai bentuk gugatan apa yang dimaksudkan oleh **Penggugat**, karena terkait dengan karakter hukum acara perdata mana yang akan digunakan: hukum acara perdata untuk gugatan **voluntair** atau hukum acara perdata untuk gugatan **contentiosa**?
     Ketidakjelasan dan kerancuan bentuk gugatan yang disusun oleh Penggugat sebagaimana telah diuraikan di atas berakibat pada kekaburan (*obscuur*) yang nyata, sehingga gugatan tersebut tidak memenuhi syarat formil suatu gugatan yang harus terang, jelas, dan tegas (*duidelijk*).
     Demi kepentingan beracara (*process doelmatigheid*), adalah wajar apabila gugatan **Penggugat** ditolak, atau setidaknya tidak dapat diterima.

   - Pada Duplik, **Tergugat** menguraikan bahwa replik yang disusun **Penggugat** adalah dalil-dalil yang bukan merupakan argumentasi hukum yang benar, yaitu dalil yang disusun berdasarkan penalaran hukum.

     Penalaran hukum adalah sistem logika silogistik yang menggunakan premis mayor berupa aturan hukum, diikuti dengan premis minor berupa fakta hukum, yang kemudian berdasarkan premis mayor dan premis minor ditarik suatu konklusi. (Marzuki, 2005: 89-90)
     Suatu gugatan disebut sebagai *Obscuur Libel* manakala formulasi gugatannya tidak jelas, yaitu gugatan yang tidak terang atau isinya gelap (*onduidelijk*). (Harahap, 2009:448)
     Dalil-dalil yang dikandung dalam replik **Penggugat** disusun tanpa adanya dasar hukum, sehingga *replik* tersebut harus ditolak atau setidak-tidaknya tidak dapat diterima.

   Berdasarkan uraian tersebut, Permohonan Gugatan Perceraian oleh **Penggugat** akan menimbulkan suatu hal yang tidak pasti, sehingga "Azas Kepastian Hukum" tidak dapat tercapai dan bertentangan dengan ketentuan hukum (*Rule of Law*) yang berlaku di Indonesia.
  Karenanya adalah wajar dan berdasarkan hukum manakala **Permohonan Gugatan Perceraian Penggugat** tidak dapat diterima.

1. Bahwa, selanjutnya dalam pokok perkara sebagaimana fakta-fakta hukum serta keseluruhan alat bukti yang terkuak selama persidangan dapat ditarik kesimpulan sebagai berikut:

   - Bahwa diantara **Penggugat** dan **Tergugat** telah terjadi perkawinan yang sah berdasarkan Pasal 2 Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan, yaitu perkawinan yang telah sah berdasarkan hukum agama dan kepercayaan masing-masing mereka, dan telah dicatatkan di catatan sipil, sebagaimana terbukti dengan alat bukti surat bertanda T-1, T-2, dan P-1.
Hal ini diikuti dengan keterangan saksi Tergugat atas nama Charles Jefferson, Agus Karyono, dan Santoso, yang mengetahui kalau **Penggugat** dan **Tergugat** adalah suami isteri, dan saat ini sedang melakukan proses gugatan perceraian.

   - Bahwa dalam perkawinan tersebut telah lahir 3 (tiga) orang anak sah, yaitu Veronica Magdalena Wihady, Bertha Magdalena Wihady, dan Timothy Vincent Wihady, sebagaimana terbukti dari alat bukti surat bertanda T-3, T-4, T-5, P-II, P-III, dan P-IV.
Hal ini dikuatkan dengan keterangan saksi-saksi **Penggugat** bahwa mereka mengetahui **Penggugat** dan **Tergugat** telah memiliki setidaknya 2 (dua) orang anak dalam perkawinan mereka.

   - Bahwa untuk memperkuat fakta tentang perkawinan **Penggugat** dan **Tergugat**, beserta dengan telah lahirnya 3 (tiga) orang anak dalam perkawinan mereka, Alat Bukti Surat bertanda T-6 dan P-V menunjukkan bahwa keluarga **Penggugat** dan **Tergugat** terdiri dari 5 (lima) orang, yaitu **Penggugat**, **Tergugat**, Veronica Magdalena Wihady, Bertha Magdalena Wihady, dan Timothy Vincent Wihady.

   - Bahwa berdasarkan Pasal 39 ayat (2) Undang-Undang Nomor 1 Tahun 1974, untuk melakukan perceraian harus ada cukup alasan, bahwa antara suami dan isteri itu tidak dapat hidup rukun sebagai suami isteri.
Bahwa pada penjelasan ayat tersebut, dan sebagaimana dipertegas lagi pada Pasal 19 Peraturan Pemerintah Nomor 9 Tahun 1975 tentang Pelaksanaan Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan, perceraian hanya dapat terjadi karena alasan-alasan berikut ini:
     - Salah satu pihak berbuat zina atau menjadi pemabok, pemadat, penjudi, dan lain sebagainya yang sukar disembuhkan;
     - Salah satu pihak meninggalkan pihak lain selama 2 (dua) tahun berturut-turut tanpa izin pihak lain dan tanpa alasan yang sah atau karena hal lain diluar kemampuannya;
     - Salah satu pihak mendapat hukuman penjara 5 (lima) tahun atau hukuman yang lebih berat setelah perkawinan berlangsung;
     - Salah satu pihak melakukan kekejaman atau penganiayaan berat yang membahayakan pihak yang lain;
     - Salah satu pihak mendapat cacat badan atau penyakit dengan akibat tidak dapat menjalankan kewajibannya sebagai suami/isteri;
     - Antara suami dan isteri terus-menerus terjadi perselisihan dan pertengkaran dan tidak ada harapan akan hidup rukun lagi dalam rumah tangga.

   - Dalil **Penggugat** dalam Permohonan Gugatan Cerai tertanggal 6 Juli 2020 bahwa dalam perkawinan **Penggugat** dan **Tergugat** telah tidak tinggal serumah sejak tahun 2009 (*posita* ke-4 dan *posita* ke-5 dalam gugatan), dan di dalam perkawinan mereka, terdapat perselisihan antara **Penggugat** dan **Tergugat** (*posita* ke-9), sehingga tidak mungkin lagi mereka dapat hidup akur sebagai suami isteri.
Perselisihan tersebut menurut **Penggugat** dimanifestasikan dengan tidak dihargainya atau direndahkannya kedudukan **Penggugat** sebagai suami oleh **Tergugat** (*posita* ke-8 dan *posita* ke-10 dalam gugatan).

   - Berdasarkan **Pasal 1865 Kitab Undang-Undang Hukum Perdata**, setiap orang yang mendalilkan bahwa ia mempunyai sesuatu hak, atau guna meneguhkan haknya sendiri maupun membantah suatu hak orang lain, menunjuk pada suatu peristiwa, diwajibkan membuktikan adanya hak atau peristiwa tersebut.
   Rumusan yang sama ditegaskan pada **Pasal 163 HIR**, bahwa barangsiapa yang mengatakan ia mempunyai hak, atau ia menyebutkan suatu perbuatan untuk menguatkan hak-nya itu, atau untuk membantah hak orang lain, maka orang itu harus membuktikan adanya hak itu atau adanya kejadian itu.
   Mengenai pasal tersebut R. Soesilo menguraikan bahwa yang harus dibuktikan hanyalah perbuatan atau kejadian yang disengketakan oleh kedua belah pihak yang berperkara, yaitu **hal-hal yang tidak mendapat persetujuan kedua belah pihak.** (R. Soesilo, 1995:119)
   Berdasarkan ketentuan-ketentuan tersebut, adalah wajar apabila Penggugat dapat membuktikan bahwa benar dirinya telah tidak tinggal serumah sejak tahun 2009, dan terdapat perselisihan antara **Penggugat** dan **Tergugat** yang berakibat tidak mungkinnya mereka hidup rukun sebagai suami isteri.

   - Bahwa dari keterangan para saksi **Penggugat** atas nama **Charles Jefferson**, **Agus Karyono**, dan **Santoso** yang terungkap di persidangan, didapatkan fakta bahwa berdasarkan apa yang dilihat, didengar, dan dialami sendiri oleh para saksi tersebut, tidak ada masalah ataupun pertengkaran antara **Penggugat** dan **Tergugat**.
Bahwa berdasarkan keterangan saksi atas nama **Charles Jefferson**, pada saat **Penggugat** melakukan pendaftaran menjadi calon legislatif, yaitu setidaknya sekitar 1 (satu) minggu sebelum tanggal 26 Agustus 2020, **Tergugat** ada bersama-sama dengan **Penggugat**, bahkan menyiapkan konsumsi untuk acara tersebut.
Berdasarkan keterangan saksi atas nama **Santoso**, pada saat saksi bertemu dengan **Penggugat** di apel pagi pada tahun 2018, saksi melihat **Penggugat** sedang bersama-sama dengan **Tergugat.**
Keterangan-keterangan saksi tersebut apabila dikaitkan dengan alat bukti bertanda T-8A, T-8B, dan T-8C, menunjukkan bahwa kehidupan rumah tangga **Penggugat** dan **Tergugat** tidak sedang mengalami pertengkaran yang terus menerus.

   - Bahwa berdasarkan keterangan saksi atas nama **Santoso**, pada tahun 2018 **Penggugat** dan **Tergugat** hadir bersama-sama pada saat apel pagi.
Jika dikaitkan dengan keterangan saksi atas nama **Charles Jefferson**, pada saat Penggugat melakukan pendaftaran menjadi calon legislatif sekitar 1 (satu) minggu sebelum tanggal 26 Agustus 2020, **Penggugat** dan **Tergugat** menghadirinya bersama-sama, bahkan **Tergugat** membantu menyiapkan konsumsi.
Hal tersebut diperkuat dengan alat bukti surat tertanda T-8A, T-8B, dan T-8C, bahwa dari rentang waktu sejak 15 Juni 2018 sampai dengan 10 Februari 2019, **Penggugat** dan **Tergugat** telah beberapa kali hadir bersama dalam beberapa acara.
Berdasarkan alat-alat bukti tersebut, terbukti bahwa setidak-tidaknya dalam 2 (dua) tahun terakhir, **Penggugat** tidak secara terus menerus meninggalkan **Tergugat** sebagaimana diatur pada Pasal 19 huruf b Peraturan Pemerintah Nomor 9 Tahun 1975 tentang Pelaksanaan Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan.

   - Pada alat bukti bertanda T-7 dan P-VII, ditemukan fakta hukum sebagaimana pertimbangan hakim *in casu*, halaman 15 putusan Pengadilan Negeri Madiun dalam perkara nomor 28/Pdt.G/2009/PN.KD.MN, bahwa hubungan rumah tangga P dan T bahagia, harmonis, dan saling menyayangi, tidak ada pertengkaran terus menerus.
Putusan pengadilan dalam rangka hukum pembuktian merupakan suatu akta otentik, sehingga ia memiliki segala kekuatan pembuktian yang ada pada suatu akta otentik. (fn: **R. Subekti, *Hukum Pembuktian*, Balai Pustaka, Jakarta Timur, 2015, h.69**)
Pertimbangan hakim pada putusan Pengadilan Negeri Madiun dalam perkara nomor 28/Pdt.G/2009/PN.KD.MN berlaku sebagai positif dan benar, karena apa yang telah diputus oleh hakim harus dianggap benar. (fn: **Sudikno Mertokusumo, *Hukum Acara Perdata Indonesia*, Yogyakarta, Liberty, 2009, h.217**)

1.  Bahwa berdasarkan **Pasal 1865 Kitab Undang-Undang Hukum Perdata** dan **Pasal 163 HIR**, sebagai pihak yang mendalilkan adanya suatu kejadian tersebut harus membuktikan dalilnya.

1. Bahwa apa yang didalilkan oleh **Penggugat** dalam gugatannya ternyata tidak lagi dapat dipertahankan karena tidak dapat dibuktikan kebenarannya, karena **Penggugat** dalam persidangan **telah gagal membuktikan adanya perselisihan dan pertengkaran yang terus menerus** sehingga tidak ada harapan akan hidup rukun lagi dalam rumah tangga sebagaimana dimaksud pada Pasal 19 huruf f Peraturan Pemerintah Nomor 9 Tahun 1975 tentang Pelaksanaan Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan, maupun membuktikan bahwa salah satu pihak meninggalkan pihak lain selama 2 (dua) tahun berturut-turut tanpa izin pihak lain dan tanpa alasan yang sah atau karena hal lain diluar kemampuannya sebagaimana dimaksud pada Pasal 19 huruf b Peraturan Pemerintah Nomor 9 Tahun 1975 tentang Pelaksanaan Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan.
Maka dalam hal ini menjadi jelas bahwa, gugatan **Penggugat** harus dinyatakan ditolak untuk seluruhnya atau setidak-tidaknya gugatan **Penggugat** tidak dapat diterima.

1. Bahwa berdasarkan pada keseluruhan fakta persidangan, **Tergugat** telah berhasil membuktikan bahwa **Penggugat** dan **Tergugat** tidak dalam keadaan sedang berselisih atau bertengkar secara terus menerus sehingga tidak ada harapan akan hidup rukun lagi dalam rumah tangga sebagaimana dimaksud pada Pasal 19 huruf f Peraturan Pemerintah Nomor 9 Tahun 1975 tentang Pelaksanaan Undang-Undang Nomor 1 Tahun 1974 tentang Perkawinan.
Maka, adalah tepat menurut hukum apabila Yang Mulia Majelis Hakim yang memeriksa dan memutus perkara ini berkenan untuk menolak Permohonan Gugatan Perceraian yang diajukan oleh **Penggugat** untuk seluruhnya.

Berdasarkan hal-hal yang telah terurai di atas, serta dengan didasari pada alat-alat bukti yang lengkap dengan fakta-fakta persidangan, **Tergugat** tetap pada Jawaban semula.

->Hormat kami,<-

->**Kuasa Hukum Tergugat:**<-

| ________________________ | ________________________ |
|:-|:-|
| **Michael S. Talatas, S.H., M.H.** | **Richard Subroto, S.H., M.Kn.** |
| *Advokat* | *Advokat* |