# KB22 Webinar, Minggu
Attendant: MST

UU PT Pasca UU Cipta Kerja

Pasal 36-Pasal 56 KUHD > Pasal yang berkaitan dengan PT.

UU Cipta Kerja:

- UU yang dicabut: 2 UU
- UU yang diubah: 75 UU

## UUPT
- Badan hukum perseorangan > badan usaha mikro & kecil
- Pasal 1 angka 15 > hari adalah hari kalender
- Kalau dulu status badan hukum diperoleh setelah pengesahan menteri, sekarang status badan hukum diperoleh setelah didaftarkan kepada menteri. Pasal 7 ayat (4) > lebih lanjut diberikan sertifikat pendaftaran.
- Pasal 7 ayat (7)
- Pasal 7 ayat (8)
- Perseroan perorangan dapat minta fasilitas perbankan (Ps 93 UMKM) yang dijadikan jaminan fasilitas kredit adalah kegiatan usahanya (proyeknya).
- Modal usaha:

  | Jenis | Modal Dasar |
  | :-- | --: |
  | Mikro | 200jt |
  | Kecil | 200jt - 1Mi |
  | Menengah | 1Mi - 15Mi |
  | - | - |
  | - | - |

- Modal dasar minimum > dulu minimal 50jt, sekarang terserah keputusan pendiri perseroan.
- Modal disetor minimal 25% dari modal dasar dan harus dibuktikan dengan bukti setoran yang sah dan disubmit secara elektronik kepada kementrian.
- Perseroan perorangan harus dimiliki oleh WNI, usia minimal 17 tahun.
