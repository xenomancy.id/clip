# KC08 Webinar KPPU: Sosialisasi tentang Pedoman Penilaian terhadap Penggabungan, Peleburan, dan Pengambilalihan

- Moderator, Abdul Halim Pasaribu (?)
- Komisioner KPPU (Bu Dinni Melanie, S.H., M.E.)
- Direktur Merger Akuisisi (Pak Daniel Agustino)

Peraturan terkait merger, dan akuisisi

- UU 5/1999 pasal 28 dan Pasal 29
- PP 57/2010
- PerKPPU 4/12
- PerKPPU 3/2019
- Pedoman Penilaian Terhadap Penggabungan, peleburan, atau pengambilalihan kppu KB06
- perkppu 3/2020

Syarat notifikasi M&A,

- batasan nilai (threshold), nilai aset gabungan lebih dari 2.5T, dan/atau nilai penjualan gabungan sebesar 5T, khusus antara pelaku usaha perbankan, batas nilai aset gabungan adalah 20T. Aset/penjualan gabungan, sum aset/penjualan para pihak yang transaksi M&A + seluruh Badan Usaha yang mengendalikan atau dikendailkan oleh para pihak tersebut, langsung atau tidak.
- tidak terafiliasi,
- perubahan pengendalian

Pengambilalihan yang wajib notifikasi: (bab II Pedoman Penilaian terhadap penggabungan, peleburan, atau pengambilalihan)

- penggabungan
- peleburan
- pengambilalihan
	- pengambilalihan saham:
		- secara langsung dari Pemegang Saham,
		- Pengambilan saham melalui pasar modal
		- pengambilalihan saham melalui penambahan modal
	- pengambilalihan yang dipersahamakn degnan saham, yaitu:
		- perpindahan aset,
		- pengambilalihan *participating interest*.

Pengambilalihan, tidak terbatas hanya pada perbuatan hukum untuk mengambil alih saham, tetapi juga untuk *instrumen lain yang memiliki karakteristik yang sama dengan saham*, yang memiliki nilai bagi pemiliknya untuk mengendalikan dan menerima manfaat dari kepemilikan tersebut. Sehingga dengan pengambilalihan dapat menimbulkan perubahan pengendalian Badan Usaha yang diambil sehingga berdampak pada persaingan.

Materi bisa didownload di website KPPU. Di [sini](https://kppu.go.id/aktivitas/) kah?
## Perpindahan aset
Perpindahan Aset dipersamakan dengan pengambilalihan saham badan usaha, dalam hal perpindahan aset tersebut:

1. mengakibatkan beralihnya pengendalian dan/atau penguasaan aset, dan/atau
1. meningkatkan kemampuan penguasaan atas suatu pasar tertentu oleh badan usaha yang mengambilalih.

## Participating Interest
Participating interest dapat didefinisikan sebagai proporsi b- eksplorasi dan produksi yang akan ditanggung, dan prop- produksi yang akan diterima masing-masing pihak, sebagaim- tercantum dalam kontrak kerja sama.
Layaknya saham, PI dapat dialihkan -0- keseluruhan maupun sebagian. Perlaihan PI berarti mengalihkan hak dan kewajiban yang melekat p- pi tersebut berdasarkan suatu kontrak kerja sa- yang memiliki nilai ekonomis.

tidak wajib notifikasi: Bab 3.2.1.1

- nilai transaksi perpindahanb aset bagi pelaku usaha non perbankan < 250M
- nilai transaksi perpindahan aset bagi pelaku usaha perbankan < 2.5T
- perpindahan aset dalam rangka transaksi rutin:
	- produk akhir yang dibeli oleh pelaku usaha ritel duntuk dijual kembali kepada konsumen
	- barang persediaan yang akan digunakan paling lama 3 bulan dalam proses produksi
- perpindahan aset untuk industri properti
	- aset beruap gedung yang dibeli diperuntukkan sebagai kantor
	- aset yang diperuntukkan sebagai fasos/fasum
- perpindahan aset yang tidak ada kaitannya dengan kegiatan usaha pengambilalihan seperti lahan untuk keperluan csr, nirlaba, atau kegiatan melaksanakan UU.

Joint venture

- yang tidak melalui proses penggabungan, peleburan, atau pengambilalihan tidak wajib notifikasi kepada kppu.
- pada saat JV telah terbentuk kemudian melakukan penggabungan, peleburan, atau pengambilalihan, wajib notifikasi ke KPPU.
- dalam hal penggabungan, peleburan, atau pengambilalihan dilakukan oleh JV, maka identitas BUIT dari JV adalah JV itu sendiri, penghitungan nilai aset dan nilai penjualan adalah berdasarkan laporan keuangan JV tersebut.

Transaksi M&A di LN (Pasal 23 PerKPPU 3/2019), yang memenuhi batasan nilai notifikasi dan terjadi di luar Indonesia wajib melakukan notifikasi ke KPPU, jika salah satu atau seluruh pihak yang melakukan M&A melakukan kegiatan usaha atau penjualan di Indonesia, usaha mana secara langsung atau tidak langsung berdampak pada persaingan usaha di indo.

penggabungan, peleburan, atau pengambilalihan asing (bab 3.4)

Notifikasi atas penggabungan, peleburan, atau pengambilalihan (PPP) asing dilakukan terhadap penggabungan, peleburan, atau pengambilalihan yang dilakukan di luar wilayah NRI, yang memenuhi keselruuan syarat:

1. seluruh pihak atau salah satu pihak yang melakukan PPP melakukan kegiatan usaha atau penjualan di wnri
1. pihak  merupakan bagian dari entitas ekonomi tunggal sesuai dengan doktrin entitas ekonomi tunggal, yang dapat berupa bagian dari
	1. kelompok usaha dari badan usaha yang menerima penggabungan dan kelompok usaha dari badan usaha yang melakukan penggabungan
(...)

Penggabungan, peleburan, atau pengambilalihan asing (bab 3.4)

PPP berdampak kepada pasar domestik indonesia: makna berdampak termasuk:

1. apabila terdapat 1 pihak yang melakukan PPP melakukan kegiatan di Indonesia
1 . pihak yang melakukan ppp tidak melakukan kegiatan usaha di indonesia namun memiliki sister company yang memiliki kegiatan usaha yang memiliki kegiatan usaha dan/atau penjualan di indonesia.

Transaksi terafiliasi dan perubahan pengendalian, Pasal 6 PerKPPU 3/2019:

1. kewajiban notifikasi tidak berlaku bagi M&A antar perusahaan yang terafiliasi
1. Hub. afiliasi:
	1. hubngan antara perusahaan baik langsung ataupun tidak langsung, mengendalikan atau dikendalikan oleh perusahaan tersebut
	1. hub antar 2 perusahaan yang dilendalikan, direct or indirect oleh pihak yang sama
	1. hub perusahaan dan pemegang saham utama
1. hub afiliasi merupakan hubungan pengendalian yang terjadi akibat kepemilikan saham (...)

konsultasi tertulis antara rencana M&A Pasal 20 PerKPPU 3 Tahun 2019

1. pelaku usaha dapat melakukan konsultasi tertulis ke KPPU sebelum melakukan M&A dengan mengisi formulir dan melengkapi dokumen pendukung.
1. permohonan konsultasi tertulis wajib melampirkan rencana merger dan akuisisi.
1. Hasil konsultasi tertulis dapat digunakan dalam proses penilaian pada saat notifikasi sepanjang tidak ada perubahan data maks 2 tahun.

notifikasi dengan penilaian sederhana (bab VII)

penilaian umum: notifikasi M&A ? penilaian dalam 90 hari kerja > pendapat
Penilaian cepat ... 14 hari ...

prosedur notifikasi penilaian sederhana:

1. dalam rangka meningkatkan efektivitas dan efisiensi notifikasi M&A,
1. prosedur notifikasi dengan penilaian sederhana
1. penilaian dilakukan berdasarkan analisis pasar bersangkutan dan konsentrasi pasar
	- berdasarkan pertimbangan KPPU
	- kelengkapan dokumen pemohon (?)
1. dengan melihat apakah terdapat isu persaingan atau tidak terdapat potensi persaingan signifikan (?)

Kriteria notifikasi

1. tidak memiliki kegiatan usaha yang sama
1. tidak memiliki kegiatan usaha yang terintegrasi vertikal
1. memiliki kegiatan usaha yang sama dengan pangsa pasar gabungan memenuhi kriteria:
	1. spektrum I dengan nilai HHI kurang dari 1.500
	1. spektrum II dengan nilai HHI 1500<=HHI<=2500, dan perubahan data hhi kurang dari/sama dengan 250
	1. spektrum III dengan nilai HHI>2500, dan perubahan data hhi <= 150
1. memiliki kegiatan usaha yang terintegrasi secara vertikal dengna nilai HHI dari masing-masing kegiatan usaha tersebut memenuhi kriteria spektrum I
1. tidak berpotensi dapat melakukan tying danaatau bundling, atau perilaku yuang menimbulkan dampak eskternalitas jaringan (network effect)
1. notifikasi disampaikan paling lama 30 hari sejak tanggal berlaku efektif secara yuridis, dan/atau
1. Pengambilalihan yang menghasilkan BU dengan:
	1. pengendalian tunggal (sole control) oleh salah satu pengendali yang ...
	1. d
	1. D

Contoh perpindahan aset yang wajib dinotifikasi ke kppu: perusahan properti yang memilik gedung perkantoran dari perusahaan properti lain (sesama pesaing) di atas 2T, yang tidak wajib 250M. Karena pengaruh ke perubahan persaingan pangsa pasar.

contoh perpindahan aset yang tidak wajib dinotifikasi ke kppu: transaksi di bawah 250M, tidak melampaui *threshold* tidak wajib notifikasi.

pihak yang wajib menyampaikan notifikasi ke KPPU (Pasal 3 PerKPPU 3/2019

1. pelaku usaha yang menerima penggabungan
1. pelaku usaha hasil peleburan. Kalau konsultasi pra peleburan, perusahaan yang akan peleburan yang konsultasi.
1. pelaku usaha yang melakukan pengambilalihan

Perbedaan konsultasi dan notifikasi:

- konsultasi, sifatnya volunteer, menyampaikan pemberitahuan ke kppu rencana PPP oleh perusahaan, secara tertulis. Closing date, tanggal M&A berlaku efektif secara yuridis, mandatory.

Tanggal efektif yuridis untuk PT (Pasal 8 PerKPPU 3/2019)

1. penggabungan: tanggal persetujuan mentri (kumham) atas perubahan anggaran dasar
1. peleburan: tanggal pengesahan menteri atas akta pendirian perseroan
1. pengambilalihan: tanggal pemberitahuan diterima menteri atas perubahan AD.

tanggal efektif yuridis lainnya:

1. merger dan akusisi yang dilakukan oleh perusahaan terbuka: mengacu pada tanggal surat keterbukaan informasi atas pelaksanaan transaksi disampaikan kepada OJK atau tanggal terakhir pembayaran saham dan/atau efek bersifat ekuitas lainnya dalam pelaksanaan hak memesan efek terlebih dahulu.
1. m&a bagi BU non PT: tanggal ditandatanganinya perjanjian penggabungan atau peleburan para pihak
1. m&a yang terjadi extraRI: paling lambat 30 hari sejak tanggal ditandatanganinya dan/atau diselsesaikannya perjanjian dan atau persetujuan pemerintah (bagi yang perlu persetujuan pemerintah) para pihak yang melakukan PPP saham dan atau aset perusahaan oleh para pihak. Mana yang terakhir ada, itu yang dipakai untuk hitung.
1. m&a di sektor migas: khusus pengambilalihan saham PI yang mengakibatkan perubahan pengendalian secara tidak langsung, kontraktor wajib lapor tertulis pada menteri melalui Kepala SKK Migas, berdasarkan surat persetujuan menteri esdm (?)
1. tanggal berlaku efektif secara yuridis bagi BAU yang menerima atau mengambilalih aset adalah tanggal perjanjian jual beli aset

pasal 5 PerKPPU 3/2019 (denda keterlambatan notifikasi M&A), dikenakan sanksi adminstratif 1M untuk setiap hari keterlambatan, paling tinggi 25M. Pengenaan denda ini mengacu pada Pasal 47 UU 5/1999 terkait pengenaan sanksi denda administrasi, min 1M max 25M. Pasca UU Cipta Kerja, hanya minimal denda, tidak ada maksimal. UU Cipta Kerja tidak disinggung Pasal 29 PP 57/2010

Mitigasi resiko persaingan usaha dalam kondisi pandemi covid19 dan relaksasi penegagan hukum.

Pasal 7 perkppu 3/2020, penambahan waktu penghitungan kewajiban notifikasi menjadi 60 hari pasca ppp saham dan/atau aset perusahaan berlaku efektif secara yuridis.

KPPU merilis peraturan komisi 1/2020, terkait penanganan perkara secara elektronik. tetap menerapkan peratiran notifikasi M&A sesuai ketentuan uu 5/1999 dan pp 57/2010. notifikasi yang bias berlangsung tatap muka, selama pandemi diefektifkan dan dimaksimalkan dengan penggunaan media elektronik.

Notifikasi secara elektronik:

1. surel ke notifikasi.merger@kppu.go.id
1. sertakan kelengkapan dokumen dalam format pdf maks 5mb. Kalau belum lengkap, tidak diberikan nomor register notifikasi. pasca perkppu 3/2019, ada dokumen minimum, kalau sudah lengkap baru tercatat telah dilakukan notifikasi.
1. ukuran dokumen lebih dari 5mb, kirim lelalui surel berbeda atau melalui tautan unduhan dokumen
1. notifikasi akan segera ditidnaklanjuti sekretariat kppu (Up. Direktorat M&A), untuk menilai kelengkapan dokumen
1. dokumen hardcopy kemudian dapat disampaikan ekapda kantor kppu setelah masa wfh kppu selesai.

Transaksi asing, syaratnya:

1. kegiatan usaha yang memiliki penjualan di Indonesia < ini utama. Kalau tidak, tidak wajib.
1. Treshold afiliasi, pengendalian

Khusus untuk transaksi aset, tidak diperlukan identitas badan usaha tertinggi dari badan usaha pemilik aset, cukup identitas pemilik aset yang diambil alih saja. Karena tidak ada dihitung untuk perhitungan threshold-nya. Peralihan aset beda dengan pengalihan saham. Pengendalian terhadap aset yang diambilalih yang berpindah, bukan pengendalian, jadi tidak perlu sampai ke paling atas. Kalau saham, semua dihitung, karena pengaruh ke pengendalian.