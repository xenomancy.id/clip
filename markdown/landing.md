---
title: Landing Page
date: 2021-04-09
author: Hendrik Lie
---
This sub domain is not meant for public consumption. Feel free to browse around, but please don't use anything in this sub domain without my prior permission.

Contact: ```info``` at ```xenomancy.id```

## Contents
| Date | Title | Description |
| ---- | ----- | ----------- |
| April 8, 2021 | [Daniel Ashton Profile](/clips/ashton-profile.html) | Ashton’s profile and datapanel. Along with some arts of him. |
| April 8, 2021 | [Xe-1 Outline](/clips/xe-1_outline.html) | Outline of Xenomancy Book 1. |
| April 8, 2021 | [Hak Paten Sebagai Jaminan Fidusia](https://clip.xenomancy.id/clips/L407-paten.html) | Currently it’s just a draft. |
| March 28, 2021 | [Daniel Ashton - Timeline](https://clip.xenomancy.id/clips/ashton-timeline.html) | This page outlined past lives of Daniel Ashton, a character in my novel project: Xenomancy. Yeah, you can guess it now why did I use this domain name.
| March 28, 2021 | [Daniel Ashton - FTF](https://clip.xenomancy.id/clips/ashton-ftf.html) | FTP is short for Fibonacci Task Force, and this table explained how they get the name of their group. I'm afraid you would have to wait until the novel is completed before you can learn more about this.
| March 21, 2021 | [PoC Perjanjian Sewa menyewa](https://clip.xenomancy.id/clips/L321-perjanjian.html) | PoC formatting for a contract. |
| March 10, 2021 | [Essay Tentang Perjanjian](https://clip.xenomancy.id/clips/L310-essay-perjanjian.html) | An essay I made by my boss’s request. Full disclaimer, I am not very prudent on including citations within this essay. |
