# Gugatan Perdata Sederhana

## Dasar Hukum
1. Peraturan Mahkamah Agung Republik Indonesia Nomor 2 Tahun 2015 tentang Tata Cara Penyelesaian Gugatan Sederhana.
2. Peraturan Mahkamah Agung Republik Indonesia Nomor 4 Tahun 2019 tentang Perubahan atas Peraturan Mahkamah Agung Republik Indonesia Nomor 2 Tahun 2015 tentang Tata Cara Penyelesaian Gugatan Sederhana.

## Syarat (Pasal 3-Pasal 4)
1. Perkara yang digugat merupakan  perkara cidera janji dan/atau perbuatan melawan hukum
1. Dengna nilai gugatan materiil paling banyak Rp 500.000.000,-
1. Bukan merupakan perkara yang penyelesaian sengketanya dilakukan melalui pengadilan khusus sebagaimana diatur di dalam peraturan perundang-undangan; atau
1. Bukan merupakan sengketa hak atas tanah.
1. Terdiri dari penggugat dan tergugat yang masing-masing tidak boleh lebih dari satu, kecuali memiliki kepentingan hukum yang sama.
1. Gugatan dilakukan di domisili tergugat.
1. Penggugat yang domisilnya di luar domisili tergugat, dalam mengajukan gugatannya menunjuk kuasa, kuasa insidentil, atau wakil yang beralamat di wilayah hukum atau domisili tergugat dengan surat tugas dari institusi penggugat.
1. Persidangan wajib dihadiri langsung oleh Penggugat dan Tergugat dengan atau tanpa didampingi oleh kuasa, kuasa insidentil, atau wakil dengan surat tugas dari institusi penggugat.

## Alur
1. **Pendaftaran.**
Penggugat mendaftarkan gugatan di kepaniteraan pengadilan dengan mengisi blanko gugatan, disertai dengan bukti surat yang telah dilegalisasi.
1. **Pemeriksaan Kelengkapan Gugatan Sederhana.**
Syarat pendaftaran gugatan sederhana diperiksa oleh panitera, yang kemudian dicatat dalam register khusus gugatan sederhana apabila telah memenuhi syarat.
Ketua Pengadilan menentukan panjar biaya perkara yang wajib dibayar penggugat.
Jika Penggugat tidak mampu, dapat diajukan permohonan beracara secara prodeo.
1. **Penetapan Hakim dan Penunjukan Panitera Pengganti.**
Ketua PN menunjuk Hakim untuk periksa gugatan sederhana.
Panitera Pengganti ditunjuk oleh Panitera untuk membantu Hakim melakukan pemeriksaan gugatan sederhana.
Dari pendaftaran hingga penunjukan hakim dan panitera pengganti dilaksanakan paling lambat 2 hari.
1. **Pemeriksaan Pendahuluan.**
Hakim memeriksa materi gugatan sederhana dan menentukan apakah pembuktiannya sederhana atau tidak.
Jika diputus pemeriksaannya tidak sederhana, dikeluarkan penetapan bahwa gugatan bukan gugatan sederhana, dan gugatan dicoret dari register gugatan sedrehana.
Tidak ada upaya hukum atas penetapan tersebut.
1. **Penetapan hari sidang.**
Jika Hakim berpendapat bahwa gugatan yang diajukan adalah gugatan sederhana, maka Hakim menentukan hari sidang pertama.
1. **Pemanggilan dan Kehadiran Para Pihak.**
1. **Pemeriksaan Sidang dan Perdamaian.**
Pemeriksaannya mengecualikan ketentuan yang diatur dalam ketentuan Mahkamah Agung mengenai prosedur mediasi.
Jika perdamaian tercapai, Hakim mebuat Putusan Akta Perdamaian.
Tidak ada upaya hukum atas Putusan Akta Perdamaian.
Perdamaian yang dilakukan di luar persidangan dan tidak dilaporkan ke hakim, tidak mengikat hakim.
Jika perdamaian tidak tercapai pada hari sidang pertama, sidang dilanjutkan dengan pembacaan surat gugatan dan jawaban tergugat.
Tuntutan provisi, eksepsi, rekonvensi, intervensi, replik, duplik, atau kesimpulan, tidak dapat diajukan selama proses pemeriksaan gugatan sederhana.
Hakim dapat memerintahkan peletakan sita jaminan atas benda milik tergugat atau benda milik penggugat yang berada dalam penguasaan tergugat.
Hanya dalil-dalil yang dibantah yang dilakukan pembuktian.
1. **Putusan.**
1. **Upaya Hukum.**
Terhadap putusan dapat dilakukan upaya hukum berupa pengajuan keberatan.
Keberatan diajukan dengan menandatangani akta pernyataan keberatan di hadapan panitera beserta alasan-alasannya.
1. **Permohonan Keberatan.**
Permohonan keberatan diajukan paling lambat 7 hari setelah putusan diucapkan atau setelah pemberitahuan putusan, dengan mengisi blanko permohonan keberatan, disertai dengan memori keberatan.
Pemberitahuan keberatan beserta memori keberatan disampaikan ke pihak termohon keberatan dalam waktu 3 hari setelah permohonan diterima di Pengadilan.
Kontra memori keberatan dapat diajukan dengan mengisi blanko yang disediakan kepaniteraan, paling lambat 3 hari setelah pemberitahuan keberatan.
1. **Pemeriksaan Keberatan.**
Paling lambat 1 haris setelah permohonan dinyatakan lengkap, Ketua Pengadilan menetapkan Majelis Hakim yang dipimpin oleh Hakim Senior yang ditunjuk oleh Ketua Pengadilan.
Setelah ditetapkan, pemeriksaan keberatan dilakukan, yang hanya didasarkan pada:

    1. Putusan dan berkas gugatan sederhana;
    1. Permohonan Keberatan dan Memori Keberatan; dan
    1. Kontra Memori Keberatan.
    
    Tidak terdapat pemeriksaan tambahan dalam pemeriksaan keberatan.

1. **Putusan Keberatan.**
Diucapkan paling lambat 7 hari setelah tanggal penetapan Majelis Hakim.
Pemberitahuan Putusan Keberatan disampaikan ke para pihak paling lambat 3 hari setelah diucapkan.
Putusan Keberatan berkekuatan hukum tetap terhitung sejak disampaikannya pemberitahuan.
Tidak tersedia upaya hukum banding, kasasi atau peninjauan kembali.

## Dokumen
1. **Blanko Gugatan.** Pasal 6 ayat (3). Berisi keterangan mengenai:

   1. Identitas Penggugat dan Tergugat;
   1. Penjelasan ringkas duduk perkara;
   1. Tuntutan penggugat.

   Pasal 6A menentukan bahwa Penggugat dan Tergugat dapat menggunakan administrasi perkara di pengadilan secara elektronik sesuai dengan ketentuan peraturan perundang-undangan.
   Pada Pasal 17A, Hakim dapat memerintahkan Sita Jaminan atas benda milik tergugat dan/atau milik penggugat yang ada dalam penguasaan tergugat.

1. **Bukti Surat yang sudah dilegalisasi.**
Menurut Pasal 6 ayat (4), wajib dilampirkan penggugat pada saat melakukan pendaftaran gugatan sederhana.
1. **Jawaban Tergugat.**
Diatur pada Pasal 16 dan Pasal 18*.
Pembacaan surat gugatan diikuti dengan jawaban tergugat.
Pada Pasal 18, hanya dalil yang dibantah saja yang dilakukan pembuktian berdasarkan hukum acara yang berlaku.
1. **Putusan Akta Perdamaian.** Lihat Pasal 15.
1. **Putusan.**
Berdasarkan Pasal 20, terdiri dari:

    1. Kepala putusan denan irah-irah yang berbunyi: "Demi Keadilan Berdasarkan Ketuhanan Yang Maha Esa";
    1. Identitas para pihak;
    1. Uraian singkat mengenai duduk perkara;
    1. Pertimbangan hukum;
    1. Amar Putusan.
1. **Akta Pernyataan Keberatan.**
Berdasarkan Pasal 21 ayat (2), diajukan ke Ketua Pengadilan dengan penandatanganan akta pernyataan keberatan di hadapan Panitera disertai alasan-alasannya.
1. **Permohonan Keberatan.**
Berdasarkan Pasal 22, paling lambat 7 hari setelah putusan diucapkan atau diberitahukan, permohonan keberatan diajukan ke Ketua Pengadilan dengan mengisi blanko permohonan keberatan yang disediakan di kepaniteraan.
1. **Memori Keberatan.**
Berdasarkan Pasal 23 ayat (1), berkas permohonan keberatan disertai dengan memori keberatan.
1. **Kontra Memori Keberatan.**
Berdasarkan Pasal 23 ayat (2), kontra memori keberatan dapat diajukan kepada Ketua Pengadilan dengan mengisi blanko yang disediakan di kepaniteraan.
Berdasarkan Pasal 24 ayat (2), diajukan dalam waktu 3 hari setelah pemberitahuan keberatan.
1. **Putusan Keberatan.**
Ketentuan Pasal 20 berlaku juga untuk Putusan Keberatan.
Berdasarkan Pasal 30, tidak ada upaya hukum atas Putusan Keberatan.
