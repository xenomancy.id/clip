Surat Edaran BI

Prosedur izin, ada 2:

- Izin melaksanakan prodak: Ternyata adanya Izin sebagai penerbit
- Izin melakukan ujicoba: Hanya berupa pelaporan, at least 30 hari ante testing, dan laporan pengakhiran, hingga 10 hari post testing.

Alur bagaimana, kalau ditolak dan diterima bagaimana? Jangka waktu?

Source: [Bank Indonesia](https://www.bi.go.id/id/sistem-pembayaran/informasi-perizinan/Contents/Default.aspx).

## Dasar Hukum
	
1. PBI No. 20/6/PBI/2018 tentang Uang Elektronik tanggal 3 Mei 2018.
2. SE BI No. 16/11/DKSP tanggal 22 Juli 2014 perihal Penyelenggaraan Uang Elektronik (*Electronic Money*).
3. SE BI No. 18/21/DKSP tanggal 27 September 2016 perihal Perubahan atas Surat Edaran Bank Indonesia Nomor 16/11/DKSP tanggal 22 Juli 2014 perihal Penyelenggaraan Uang Elektronik (*Electronic Money*).

### Izin Sebagai Penyelenggara

#### PBI 20/6/PBI/2018
Pasal 5: Ada 2 jenis, sebagai front-end atau back-end. Untuk penerbit, penyelenggara dompet elektronik, itu masuk front-end.
Pasal 6: Pihak yang mengajukan izin sebagai penyelenggara yang berupa Lembaga Bukan Bank (LBB) harus berbentuk PT.
Pasal 7: Mayoritas direktur harus berdomisili di Indonesia.

Pasal 8: LBB yang mengajukan permohonan izin harus  memenuhi persyaratan modal disetor minimum dan komposisi kepemilikan saham.
Pasal 9: Modal disetor minimal Rp. 3.000.000.000,00 (tiga milyar rupiah), untuk LBB yang telah berizin penerbit, wajib memelihara dan menyesuaikan pemenuhan modal disetor berdasarkan posisi Dana *Float* (lihat Pasal 50).
Pasal 10: Komposisi sahamnya minimal 51% WNI dan/atau Badan Hukum Indonesia.

#### SE BI No. 16/11/DKSP tanggal 22 Juli 2014 jo. SE BI No. 18/21/DKSP tanggal 27 September 2016

II.A Persyaratan sebagai penerbit, salah satunya adalah rekomendasi dari ***otoritas pengawas Lembaga Selain Bank*** bagi pemohon berupa LSB (jika ada).
LSB wajib ada izin dari BI manakala dana *float* yang dikelola adalah sebesar atau lebih dari Rp. 1.000.000.000,00 (satu milyar rupiah).
LSB yang mengajukan permohonan izin sebagai Penerbit wajib berbadan hukum Indonesia berbentuk PT yang telah menjalankan kegiatan usaha di bidang:
- keuangan;
- telekomunikasi;
- penyedia sistem dan jaringan;
- transportasi publik; dan/atau
- bidang usaha lainnya yang disetujui Bank Indonesia.

### Alur Pendaftaran

SE BI No. 16/11/DKSP tanggal 22 Juli 2014 jo. SE BI No. 18/21/DKSP tanggal 27 September 2016, bagian III.A. Pemrosesan Permohonan Izin sebagai Penerbit.

1. Permohonan yang diterima akan diproses sebagai berikut:
   - Pemeriksaan Administratif, meliputi
     - Pemeriksaan kelengkapan dokumen; dan
     - Pemeriksaan kesesuaian dokumen.

     Jika dokumen lengkap, dilakukan pemeriksaan kesesuaian dokumen.
     Jika dokumen belum lengkap, BI mengembalikan dokumen ke Pemohon.
   - Pemeriksaan lapangan (*on site visit*) untuk verifikasi kebenaran  dan kesesuaian dokumen, sekaligus untuk memastikan kesiapan operasional.

2. Jika dari pemeriksaan kesesuaian dokumen ditemukan dokumen yang tidak sesuai, pemohon harus memberikan dokumen yang telah disesuaikan dalam jangka waktu 90 hari kalender sejak tanggal surat pemberitahuan pertama kali dari BI mengenai ketidaksesuai persyaratan dokumen.
Jika lewat jangka waktu 90 hari kalender dan dokumen yang telah disesuaikan belum diterima oleh BI, BI menolak permohonan izin.

3. Jika permohonan izin ditolak sebagaimana dimaksud pada poin 2, maka permohonan izin berikutnya dapat diajukan kembali setelah 180 hari kalender sejak tanggal ditolaknya izin.

4. Jika dokumen permohonan dinyatakan lengkap dan sesuai persyaratan, BI melakukan pemeriksaan lapangan (*on site visit*).

5. Berdasarkan hasil pemeriksaan administratif dan pemeriksaan lapangan (*on site visit*), BI dapat menyetujui atau menolak permohonan izin secara tertulis.

6. Lembaga Selain Bank yang menyelenggarakan kegiatan uang elektronik dengan dana *float* di bawah Rp. 1.000.000.000,00 (satu milyar rupiah) mengajukan izin ke BI, lembaga tersebut tetap dapat menjalankan kegiatannya selama dalam proses pengajuan izin, selama tidak menambah dana *float*.

### Uji Coba

1. Calon Penerbit yang sedang dalam proses perizinan dapat melakukan uji coba dalam rangka menguji kesiapan penyelenggaraan uang elektronik.
Uji coba dilakukan terbatas dalam lingkungan internal calon penerbit.

1. Dalam pelaksanaan uji coba tersebut, calon penerbit harus menyampaikan laporan ke BI mengenai rencana pelaksanaan dan pengakhiran uji coba dengan ketentuan:
   - laporan rencana pelaksanaan uji coba disampaikan ke BI paling lambat 30 hari kalender sebelum pelaksanaan uji coba; dan
   - laporan pengakhiran uji coba disampaikan ke BI paling lambat 10 hari kalender setelah tanggal uji coba berakhir.

1. Penerbit atau calon penerbit yang akan menyelenggarakan kegiatan Layanan Keuangan Digital dapat melakukan uji coba dengan ketentuan sebagaimana diatur dalam ketentuan BI yang mengatur mengenai layanan keuangan digital.


## UKP

Berdasarkan Surat Penunjukan Direksi PT Usaha Kreatif Pembayaran Nomor: 0003/SK-UKP/X/2020 tentang Firma Hukum Dalam Melakukan *Legal Due Diligence.*

| Label | Value |
| --- | --- |
| Nama | PT. Usaha Kreatif Pembayaran |
| Domisili | Pakuwon Center, Superblok Tunjungan City, Lt. 16 OF 05, Jalan Embong Malang No. 1.3.5 Kel. Kedungdoro Kec. Tegalsari, Surabaya - 60261, Indonesia |
| Domisili elektronik | info@usahakreatif.id |
| Phone | +62 31 9925 3968 |
| Direktur Utama | Charlie Anthony |
