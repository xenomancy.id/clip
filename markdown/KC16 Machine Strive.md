# Machine Strife Universe

Affected by The Astral Plane via the manifestation of Kathryoton Particles, a subatomic particle whose behavior dictated not only by interactions in 4D space-time, but also dictated by its interactions in higher dimensions.
It allows its inhabitants to exploit metric phenomenon via the mediation of this particle.

## Places of Interests
| Domain | System | Planet/Hab | Location | Description |
| ------ | ------ | -------------- | -------- | ----------- |
| Free Region | Sol | Earth | White Tree City, Free States of Indovinesia, Contintent of Antarctica | A place of free market and high technology |

## Usage of Kathryoton Particle
The particle itself usually wouldn't manifest in 4D space-time, however it is possible to summon it by weakening spatial permeability via modulated resonating warp field.
Civilizations known to possess the ability to produce sufficiently modulated resonating warp field to withdraw the particles out of higher dimensions are: Aucafidian Grid, Denefasan Protectorates, Centurian Order, and Unified Gamman Administration.

Some low-tech cultures, such as the civilization on Earth (referred outside this Universe as MS-Earth), while unable to produce warp fields, can exploit this particle by Kathryoton Capture mechanism.
Kathryoton Capture mechanism captures free-roaming Kathryoton particles, usually as a result of warp travel on the vicinity of Earth, as nearby Aucafidian civilization often made a frequent visit to Earth and cis-lunar region.

The captured Kathryoton particles were caged inside fullerenes, that can be freed by applying fire to the fullerenes particles.
Once freed, the Kathryoton particles were guided with electromagnetic guidance, and be put in use.

The most common low-tech usage of Kathryoton particles is to initiate fusion reaction, as the particle in its freed form, tend to "run" away from this universe and "escape" 4D space-time.
In the process of "escaping" this universe, the particle tore open micro-wormholes to higher dimensions, and cause leakages of hypermatter from hyperspace.
The leaked hypermatter is around the same mass as the kathryoton particle that escaped.

Hypermatter that usually exists in higher dimension, has more degree of freedom, and therefore has higher lowest energy state compared to what is possilbe in 4D space-time.
When constrained to 4D space-time, it loses all of its extra freedom, and hence no longer in its lowest energy state.
To reach stability, it must decay into its lowest energy state, and free off the energy.

The decay results in the total conversion of mass, with about a third of the mass-energy decay into charged pions, another third into neutral pions, and another third into Gamma rays.
About a third of the total energy released is directly converted into electricity, and therefore the efficiency is more or less 25%.
The rest of the heat energy were then used to drive thermal engines.
The most sophisticated design made of normal matter can capture about 50% of the released energy into electricity.

## Earth
Compared to SM Universe, this universe's Earth has a nation set in Antarctica, called the Free States of Indovinesia.
It is a leading nation in term of material science and technological developments.
Also one of the first that allows commercial use of room-temperature superconductor.
They are also the first nation to discover Kathryoton particles, and managed to create Kathryoton Capture mechanism.
With the advent of Kathryoton Capture mechanism, they're able to discover that the kathryoton fields resulted by visitations of an unknown alien species, that made frequent visit on Earth and cislunar region.

With the advent of Kathryoton Catalyzed Fusion Generator (KCF Generator) developed by General Power, Ltd., a state owned company on Indovinesia, compact nuclear energy generation becomes economically feasible, and hence highly weaponized Frames enter the military.
One of the leading Frame manufacturing company is General Frames, Ltd., a privatized formerly Indovinesia state-owned company, that just as recent as 2043, develop the latest Organotron(TM) line Frames, GFOR-108 ARMOR.

## ARMOR
Armored Multifunctional Organotronic Robot, or shortened into ARMOR, is the latest Frame brand by General Frames, Ltd.
A distinct feature of this line of armor is its highly efficient Zeta Expert System, (ZER System) that allows smooth control over the movements compared to the old generation Frame technology.
With the release of GFR-108 ARMOR, a new expert system is introduced: Generalized Adaptive Zeta Expert System (GAZER System), that allows an even more flexibility and modularity in upgrading Frames.

## GFOR-108 ARMOR
ARMOR Model GFOR-108, the latest Organotron(TM) line by General Frames, Ltd., is a 7m tall Frame, with dry mass of 6 metric tons.
The first of its kind to utilize General Frames's Magnetobaric Battery instead of carrying a reactor with it.
Designed to be versatile in power usage with minimum excess heat inherent in most reactors, its heat signature is less pronounced.
The limited energy reserve is countered with its added flexibility and dexterity due to the use of General Frames's BCN-based Nanoactuators instead of hydraulics or ordinary servos.

To help with the added flexibility and dexterity of this Frame, the latest generation Generalized Adaptive Zeta Expert System (GAZER System) is incorporated to the control system.
A latest improvement to the earlier Zeta Expert System, GAZER System allows flexibility and modularity to be no longer any issue in hardware upgrade, as the system would adapt easily.
GAZER System is capable of learning and with minimal training, can quickly master and efficiently use any upgrade fitted into the Frame, removing the need to update the firmware after every upgrade and replacement parts.
