<style>body {text-align:justify}</style>

# <div style="text-align: center"> *NON DISCLOSURE AGREEMENT*<br>(Perjanjian Kerahasiaan) </div>


Pada hari ini, [____], kami yang bertandatangan di bawah ini:

1. (Nama pemilik bangunan), (Pekerjaan), (alamat).

    Dalam hal ini bertindak untuk dan atas nama PT. **[nama Perusahaan]** selaku **[Diisi sesuai dengan jabatan]** sebagaimana dalam Akta [\_\_\_\_] Nomor [\_\_\_\_] tertanggal [\_\_\_\_], yang dibuat oleh Notaris **[Nama Notaris + Gelarnya]** di **[Daerah Tempat Kedudukan Notaris]** yang telah disahkan oleh Menteri Hukum dan Hak Asasi Manusia sebagaimana dalam Surat Keputusan Nomor [____] tanggal **[Diisi sesuai dengan Surat Keputusan]**, yang selanjutnya disebut **“Pihak Pertama”**.

1. (pemilik bangunan), (Pekerjaan), (alamat).

    Dalam hal ini bertindak untuk dan atas nama PT. **[nama Perusahaan]** selaku **[Diisi sesuai dengan jabatan]** sebagaimana dalam Akta [\_\_\_\_] Nomor [\_\_\_\_] tertanggal [\_\_\_\_], yang dibuat oleh Notaris **[Nama Notaris + Gelarnya]** di **[Daerah Tempat Kedudukan Notaris]** yang telah disahkan oleh Menteri Hukum dan Hak Asasi Manusia sebagaimana dalam Surat Keputusan Nomor [____] tanggal **[Diisi sesuai dengan Surat Keputusan]**, yang selanjutnya disebut **“Pihak Kedua”**.

Pihak Pertama dan Pihak Kedua secara bersama-sama disebut sebagai Para Pihak.

Para Pihak terlebih dahulu menguraikan hal-hal berikut:

1. Pihak Pertama merupakan [____].

Berdasarkan uraian di atas, Para Pihak telah setuju dan sepakat untuk mengadakan dan melaksanakan suatu perjanjian kerahasiaan dengan syarat-syarat dan ketentuan-ketentuan sebagaimana diuraikan di bawah ini:

<div style="text-align: center"> Pasal 1<br>Definisi </div>

Pada perjanjian ini, yang dimaksud dengan:

1. **Informasi Rahasia** adalah Informasi yang meliputi data-data perusahaan, perijinan, etiket merek, sertifikat merek, penghargaan, iklan-iklan dan data-data lainnya secara lisan maupun tertulis yang disimpan, yang dapat dibaca oleh mesin, atau informasi lainnya atau data dalam bentuk apapun, yang diberikan oleh **Pihak Pertama** kepada **Pihak Kedua**.
2. **Hari Kerja** adalah hari operasional pelaksanaan pekerjaan yang terdiri dari hari senin sampai hari jumat mulai pukul ...:…– …:… WIB/WITA/WIT (………………) – (Diisi dan dipilih sesuai kebutuhan)
3. **Pekerjaan** adalah ……………………………………. (Diisi sesuai kebutuhan)
4. **Karyawan** adalah perwakilan Para Pihak yang akan melaksanakan pekerjaan sebagaimana dimaksud dalam perjanjian ini.
5. **Lampiran** adalah semua dokumen yang telah ditentukan dalam perjanjian dan merupakan suatu kesatuan yang tidak terpisahkan.
6. ***Force Majeure*** adalah keadaan-keadaan yang terjadi diluar kekuasaan Para Pihak yang dapat mengecualikan kewajiban pemenuhan prestasi-prestasi yang ditentukan dalam perjanjian.
7. **Addendum** adalah semua perubahan-perubahan dan penambahan-penambahan perjanjian yang akan ditentukan berdasarkan kesepakatan Para Pihak dan merupakan suatu kesatuan yang tidak terpisahkan dalam perjanjian ini.


<div style="text-align: center"> Pasal 1<br>Informasi Rahasia </div>
Menjabarkan apa itu informasi rahasia.

<div style="text-align: center"> Pasal 1<br>Perahasiaan </div>
Jadi apa saja kegiatan perahasiaan?

<div style="text-align: center"> Pasal 1<br>Syarat Pembukaan Rahasia </div>
Kapan boleh rahasia diungkap

<div style="text-align: center"> Pasal 1<br>Pengecualian Karena Hukum </div>
Pengecualian untuk ketentuan UU

<div style="text-align: center"> Pasal 1<br>Jangka Waktu </div>
Kapan kerahasiaan wajib dipertahankan.

<div style="text-align: center"> Pasal 1<br>Pengembalian atau Pemusnahan Informasi Rahasia </div>
Bagaimana pemrosesan info rahasia.

<div style="text-align: center"> Pasal 1<br>Non-Kompetisi </div>
Untuk keperluan agar para kontraktan tidak terlibat dalam kompetisi nantinya dengan menggunakan rahasia.

<div style="text-align: center"> Pasal 1<br>Akibat Kebocoran Informasi Rahasia </div>
Siapa yang bertanggungjawab, dan apa yang terjadi.

<div style="text-align: center"> Pasal 1<br>Korespondensi </div>
Kontak melalui siapa saja.

<div style="text-align: center"> Pasal 1<br>Wanprestasi</div>
1. Salah satu Pihak dikatakan telah melakukan wanprestasi atau cidera janji manakala kewajiban yang timbul berdasarkan perjanjian ini tidak dipenuhi olehnya.
1. Tidak dipenuhinya kewajiban yang timbul berdasarkan perjanjian ini sebagaimana dimaksud pada ayat (1) adalah:
   <ol type="a">
      <li>belum dibayarnya harga sewa oleh Pihak Kedua pada waktu yang ditentukan <strong>[Klausul Cara Pembayaran]</strong> ayat (1).</li>
   </ol>
1. Manakala salah satu Pihak melakukan wanprestasi sebagaimana dimaksud pada ayat (1), Pihak yang dirugikan dapat mengirimkan Surat Pemberitahuan Pelanggaran kepada Pihak yang melakukan wanprestasi tersebut maksimal sebanyak 3 (tiga) kali untuk setiap pelanggaran yang dilakukan.
1. Surat Pemberitahuan Pelanggaran sebagaimana dimaksud pada ayat (3) memuat sanksi yang wajib untuk dilaksanakan oleh Pihak yang melanggar.


<div style="text-align: center"> Pasal 1<br>Sanksi</div>
1. Sanksi yang dapat dikenakan kepada Pihak yang melanggar kewajibannya berdasarkan perjanjian ini adalah Sanksi Penggantian dan/atau Denda, sanksi-sanksi tambahan sesuai dengan kewajiban spesifik yang dilanggar Pihak tersebut, dan/atau Sanksi Pemutusan Perjanjian, dengan syarat-syarat yang ditentukan pada pasal ini.
1. Sanksi Penggantian adalah sanksi yang menimbulkan kewajiban bagi Pihak yang melakukan menimbulkan kerugian untuk menggantikan kepada pihak lainnya sebesar nilai kerugian yang dialami oleh pihak lainnya.
1. Sanksi dikenakan hanya atas pelanggaran:
   <ol type="a">
      <li><strong>[Klausul Wanprestasi]</strong> ayat (2) huruf a, sehingga Pihak Kedua dikenakan Denda sebesar [_________] per hari keterlambatan.</li>
   </ol>
1. Sanksi yang dikenakan sebagaimana dimaksud pada ayat (3) jatuh tempo pada tanggal diterimanya Surat Pemberitahuan Pelanggaran sebagaimana dimaksud pada Pasal 16 ayat (3) di domisili Pihak yang melanggar.
1. Sanksi Pemutusan Perjanjian dapat dilakukan oleh Pihak Pertama, hanya dengan alasan-alasan Pihak Kedua cidera janji dan tidak menjalankan sanksi sebagaimana diatur pada ayat (3) selama paling sedikit 3 (tiga) bulan setelah sanksi tersebut jatuh tempo sebagaimana dimaksud pada ayat (4).
1. Sanksi Pemutusan Perjanjian sebagaimana dimaksud pada ayat (5) tidak menghapus kewajiban-kewajiban yang masih belum diselesaikan oleh Para Pihak.

<div style="text-align: center"> Pasal 1<br>Penyelesaian Sengketa </div>
1. Sengketa lahir ketika:
   <ol type="a">
      <li>salah satu pihak tidak melaksanakan kewajibannya berdasarkan perjanjian ini, termasuk Sanksi yang wajib dilakukannya, dan pihak lainnya telah menerbitkan Surat Pemberitahuan Pelanggaran sebagaimana dimaksud pada <strong>[Klausul Sanksi]</strong> ayat (3) atas pelanggaran yang sama secara 3 (tiga) kali berturut-turut yang telah diterima oleh pihak yang melanggar.</li>
      <li>terdapat perbedaan pendapat antara Para Pihak sehubungan dengan makna dan/atau interpretasi dari isi perjanjian ini.
</li>
   </ol>
1. Dalam hal terjadi sengketa sebagaimana dimaksud pada ayat (1), upaya perdamaian dilakukan dengan cara musyawarah yang didahului dengan undangan tertulis oleh salah satu pihak atau kedua belah pihak.
1. Musyawarah sebagaimana diatur pada ayat (2) berlangsung selama paling lambat [ ] hari kalender sejak undangan tertulis yang dibuat oleh salah satu pihak dalam perjanjian ini sebagaimana dimaksud pada ayat (2) telah diterima di domisili pihak lainnya, yang mana pada selesainya musyawarah tersebut akan diterbitkan Nota Kesimpulan Musyawarah.
1. Nota Kesimpulan Musyawarah yang menyatakan dengan tegas tidak dicapainya kesepakatan oleh Para Pihak yang bersengketa, atau tidak diterbitkannya Nota Kesimpulan Musywarah setelah jangka waktu sebagaimana dimaksud pada ayat (3) telah lampau, dapat menjadi dasar gugatan ke pengadilan oleh pihak yang merasa dirugikan.
1. Gugatan sebagaimana dimaksud pada ayat (4) hanya dapat dilakukan di Kepaniteraan Pengadilan Negeri **[Domisili]**.

<div style="text-align: center"> Pasal 1<br><em>Force Majeure</em> </div>
1. Para Pihak sepakat untuk tidak saling menuntut hak dan kewajiban yang timbul atau jatuh tempo pada waktu adanya keadaan yang diluar kesalahan dan kemampuan Pihak Pertama dan/atau Pihak Kedua untuk menanganinya atau mencegahnya terjadi.
1. Pada saat terjadi keadaan sebagaimana dimaksud pada ayat (1), Para Pihak dapat saling sepakat maupun secara sepihak memutuskan sementara perjanjian ini tanpa ada kewajiban untuk melanjutkannya kembali.
1. Pemutusan sebagaimana dimaksud pada ayat (2) diikuti hilangnya hak menuntut pemenuhan prestasi yang belum dipenuhi oleh masing-masing Pihak.
1. Manakala setelah keadaan memaksa sebagaimana dimaksud pada ayat (1) telah lampau, dan Para Pihak bermaksud untuk melanjutkan kembali perjanjian ini, berlaku ketentuan **[Klausul Jangka Waktu]** ayat (3), dan dapat menyimpangi ketentuan **[Klausul Jangka Waktu]** ayat (4).

<div style="text-align: center"> Pasal 1<br>Penambahan atau Perubahan </div>
1. Jika dikemudian hari Para Pihak sepakat untuk melakukan penambahan dan/atau perubahan dari isi perjanjian ini, penambahan dan/atau perubahan tersebut dituangkan dalam perjanjian tersendiri yang merupakan bagian yang tidak terpisahkan dari perjanjian ini.
1. Segala bentuk coretan, penambahan, perbaikan, dan/atau renvoi yang ditambahkan langsung pada berkas fisik perjanjian ini, baik berkas asli maupun salinan dan/atau fotokopi, di rangkap pegangan Pihak Pertama maupun di rangkap pegangan Pihak Kedua, tidak memiliki kekuatan hukum yang mengikat.
1. Ketentuan ayat (2) tetap berlaku sekalipun Para Pihak telah sepakat untuk mengakui pemberlakuan coretan, penambahan, perbaikan, dan/atau renvoi tersebut, kecuali jika telah ada ketentuan lain yang disepakati sesuai dengan ketentuan ayat (1).


## <div style="text-align: center"> Penutup </div>
Para Pihak menyatakan dan menjamin telah membaca, mengerti dan menyetujui semua ketentuan-ketentuan yang terdapat dalam Perjanjian ini. Demikian Perjanjian Sewa Menyewa ini disetujui dan dibuat rangkap dua asli dan mempunyai kekuatan hukum yang sama. Ditandatangani oleh mereka yang disebut di bawah ini:

| PIHAK PERTAMA | PIHAK KEDUA |
| ------------- | ----------- |
| (tanda tangan) | (tanda tangan) |
| (nama pihak pertama) | (nama pihak kedua) |








