# KB25 Webinar HKHPM

...

Materi Pak Wahyu (missed)

Kalau dari konsultan ada mencurigai pendanaan terorisme, harus melapor ke PPATK.

Peraturan 8/2017 (?)

---

Materi Kedua

(audio lost temporarily)

POJK 7/POJK.04/2017

Laporan Pemeriksaan Segi Hukum, tapi di SP 2018 digunakan istilah Laporan Uji Tuntas. Kendala: konsistensi penggunaan istilah dari Aspek Hukum.

SP 2005, parameter pemeriksaan perkara menggunakan pernyataan dari direksi dan badan peradilan. SP 2012 cukup dengan surat pernyataan direksi. Tapi di SP 2018 tidak disebutkan kedua hal, tapi praktik diminta OJK untuk dilakukan pemeriksaan mandiri.

Kendala: batasan atau indikator apa yang dijadikan dasar pengkajian dampak perkara hukum terhadap tujuan uji tuntas.

Zaman sekarang bisa dicek dari internet info mengenai emiten, dan ada juga sistem informasi penelusuran perkara.

Pemeriksaan laporan keuangan

Lampiran VII Pasal 7 ayat 2 SP2018 bahwa Konsultan Hukum sebagai pertimbangan dalam memeriksa laporan keuangan... memperhatikan: arus kas dan/atau neraca rugi-laba; utang jangka pendek dan jangka panjang; dan kepemilikan terhadap aset.

Masalahnya adalah batasannya apa saja yang menjadi parameter pemeriksaan konsultan hukum. Biasanya yang diperiksa terkait utang pihak ketiga dan afiliasi, negative convenant.

Surat Edaran 01/DS-HKHPM/0119 tanggal 7 Januari 2019 perihal interpretasi standar profesi HKHPM 2018 dalam romawi VI Interpretasi Pemeriksaan terhadap Kewajiban Pajak.

Lampiran VII Pasal 18 ayat 2 SP 2018: konsultan hukum selain memenuhi ayat 1 dalam pasal ini wajib melakukan pemeriksaan meliputi kesanggupan pembeli siaga dalam melaksanakan HMETD, kesanggupan pemegang saham utama dalam melaksanakan HMETD, kesanggupan pengalihan.

Indikator apa soal kesanggupan? Soal komitmen atau apa?

Penggunaan Dana dalam Rangka Penawaran Umum Perdana. Dalam SP 2018 Lampiran VII Bab I bagian kesatu tentang penawaran umum perdana (pasal 1-13), tidak diatur mengenai pemeriksaan atas tujuan penggunaan dana, tapi OJK meminta konsultan huykum harus memeriksa dan memberikan opini hukum dalam kaitannya dengan transaksi material, transaksi afiliasi, dan benturan kepentingan atas penggunaan dana tersebut.

Tanggapan dari OJK menanyakan alasan yang melatarbelakangi sebagian besar penggunaan dana PU untuk peningkatan modal atau pembelian bidang tanah, persentase nilai transaksi dan ekuitas perseroan yang diungkapkan dalam prospektus, manfaat dan keuntungan transaksi kerja sama terhadap kinerja keuangan dan kelangsungan usaha perseroan,

Mengingat konsultan hukum tidak m...

Pemeriksaan terhadap sumber dana, dalam kaitan penerbitan efek bersifat sukuk.

Lampiran VII Pasal 15 angka 2 huruf h. Apa batasannya?

Lampiran VII Pasal 17 angka 5 huruf f, dalam hal kreditur juga bertindak sebagai penyedia jasa. Terkait prinsip kewajaran (?)

Penambahan modal tanpa memberikan HMETD. Lampiran VII Pasal 19 angka 3 huruf a, "kondisi lain" itu batasan atau indikatornya apa sebagai dasar kajian? Sehingga dapat mengakibatkan restrukturisasi bank oleh instansi pemerintah yang berwenang.

Dalam POJK No. 7 dan SP 201...

POJK 8/2017 diungkapkan di prospektus

SP2018, diminta komposisi pemegang saham saat pendirian, apa diungkapkan di prospektus atau di LDD dan POJK 8/POJK.04/2017 (?).

---

(HHP) Darmawan (?)

Transaksi Pasar Modal Non Penawaran Umum dalam SP HKHPM

- Penggabungan dan peleburan usaha
- Pengambilalihan saham
- Penawaran tender
- Pengambilalihan aset
- Penyertaan pada perusahaan lain
- d
- d

Apa yang disyaratkan OJK?

Ada di SP HKHPM namun belum tentu diminta OJK:

- OJK hanya menyaratkan LO untuk transaksi penggabungan dan peleburan
- dalam praktik go private juga minta adanya LO
- Pengambilalihan saham, penawaran tender, pengambilalihan aset, penyertaan pada perusahaan lain tidak menyaratkan adanya LO.

Catatan:
Tidak seluruh ketentuan yang ada di SP HKHPM non-PU, tidak semua disyaratkan LO. Hanya terkait dengan peraturan OJK terkait penggabungan dan merger, hanya itu yang secara eksplisit disyaratkan adanya LO dari KH. Untuk go private, tidak ada aturan khusus di OJK, tapi praktiknya diminta. Tapi bagaimana dengan sisanya? Tidak ada syarat khusus ditentukan. Dalam SP diatur pemeriksaan seperti apa dsb, tapi merger, go private dsb, OJK tidak menyaratkan adanya LO. Praktiknya, memang hampir semuanya tidak ada, kadang ada pihak yang perlu untuk perkara material, tapi peran KH tidak dalam pembuatan LO.

Beberapa kendala terkait SP Penggabungan dan Peleburan.

Apa yang dimaksud dengan pemeriksaan dalam SP, dan apakah atas setiap hal yang diperiksa KH harus memberikan opini hukum?

- Hal-hal yang dituliskan dalam SP adalah hal-hal yang harus dimuat dalam Rancangan Penggabungan (POJK mengenai merger)
- Dalam praktiknya KH diminta juga memberi opini hukum mengenai hal-hal yang diperiksa.

Pertanyaan, apasih yang perlu dilakukan?
Karena tidak semuanya aspek hukum.
Cakupan opini dalam hal ini sangat luas.
Sebenarnya apa sih yang dilakukan waktu pemeriksaan?
Apa hanya mencocokkan, mempelajari, melihat, atau lebih dari itu, harus dicari informasi yang lebih dalam lagi?

Beberapa kendala terkait SP pengambilalihan saham, penawaran tender, pengambilalihan aset, penyertaan pada perusahaan lain.

Applicability > SP vs Scope yang disepakati dengan klien.

Belum tentu klien sendiri perlu menunjuk konsultan hukum untuk melakukan ini.
SP vs Scope yang dimandatkan oleh klien belum tentu sama.
Misal kalau perusahaan asing, fokus utamanya adalah membantu dokumentasi dalam bahasa Indonesia.
Sesuai kesepakatan dengan klien, apa saja yang harus dilakukan oleh KH, dalam kapasitas sebagai KH.

Klien punya keinginan sendiri, misal dalam akuisisi, tidak wajib diserahkan OJK, jadi tergantung klien mau sejauh apa.

Saran terkait SP transaksi afiliasi dan benturan kepentingan > penyesuaian dengan POJK 42/2020.

OJK mensyaratkan adanya LO untuk merger disampaikan ke OJK.

---

Bono Daru Aji (AHP) (?)

Catatan tambahan dari perjalanan SP sejak 2018 yang sedikit banyak sudah dishare oleh saudara Agus dan Iqbal Darmawan. Jadi hanya akan disampaikan beberapa contoh masukan dari HKHPM yang diterima. Lebih banyak ingin diskusi atas beberapa hal yang jadi isu. Memperkenalkan tim dewan standar HKHPM, sehingga jika ada masalah bisa dihubungi. Dirinya sendiri, (missed), kukuh, Iqbal Darmawan.

Peran standar HKHPM 2018

1. Pedoman bagi konsultan hukum dalam menjalankan profesi
1. acuan bagi regulator seperti OJK dalam melakukan penilaian terhadap suatu transaksi;
1. Pengaturan sanksi KH terkait pelanggaran __ dan etik ...

Keanggotaan HKHPM:

- Standar HKHPM, terkait persyaratan KH HKHPM
- Catatan dan usulan:
  - pada praktiknya, yang tercatat sebagai KH HKHPM sebagian besar merupakan partner, meskipun tidak ada persyaratan yang mewajibkan hal tersebut.
  - Perlu ada juga mengenai associate agar dapat berpartisipasi (?)

Dalam standar profesi HKHPM, dalam menunjuk senior associate dan associate harus disertai surat penugasan dari kantor KH.

Catatan darn usulan: sebagai bagian dari kantor KH dan terikat kontrak kerja, surat penugasan tidak diperlukan.

Permasalahan yang masih sering ditanyakan oleh anggota HKHPML

Standar HKHPM: KH wajib memeriksa laporan keuangan.

Catatan: KH bingung memeriksa itu sejauh mana? Meski sudah banyak mengetahui, bahwa memeriksa tidak keluar dari kapasitas KH.

Usulan: Mohon ditegaskan apa kalimat memeriksa juga diberikan penegasan bahwa KH wajib menggunakan Laporan Keuangan sebagai salah satu acuan dalam memeriksa Laporan Uji Tuntas.

Permasalahan KH dalam praktik:

- Standar HKHPM: UT terhadap UMR didasarkan pada surat pernyataan dari direktur bidang SDM/keuangan. Ruang lingkup UT dalam transaksi modal dengan HMETD tidak termasuk kepada ketenagakerjaan, harta kekayaan dan asuransi.
- Tanggapan OJK: /...

Laporan Tahunan:

- Penyampaian laporan keuangan pada OJK, agar dapat pertimbangan dari HKHPM, agar beban administratif dari anggota berkurang. ...

Standar litigasi PM dan Keuangan

Standar HKHPM: bertujuan agar praktisi litigasi memiliki tambahan pengetahuan pasar modal sehingga dengan harapan dapat mengaplikasikan ketentuan yang tepat untuk kasus pasar modal dan pasar keuangan.

Catatan dan kusulan: perlu ditingkatkan pengetahuan kepada kh litigasi mengenai hal ini melalui training atau seminar free of charge, agar keteberadaan kh hkhpm tidak hanya terkait dengan transaksi pasar modal. pada praktiknya, kh yang menangani litigasi tidak memiliki pendidikan pmodal yang cukup dan tidak mengetahui pengetahuan pmodal.

Ada catatan sebagai pengingat: pada saat standar profesi dibuat, bagaimana tetap menjaga agar persyartan dan tata cara UT yang diatur ya tetap memberikan perlindungan yang baik terhadap investor dan penerapan standar yang baik. Ada beberapa masukan untuk memberikan ...

---

