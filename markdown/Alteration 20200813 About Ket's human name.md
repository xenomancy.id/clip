# Alteration 20200813 About Ket's human name.

Ket Khemachat Thunyakorn.
Nick: เข็ด (noun: skein, verb: be afraid, be scared, fear)
Full Name: เขมะชาติ ธันยกรณ์

When Ket was asked for his human name in Integra. Reason: Thai names because his favorite human is Michael, and Michael's favorite human is Chandra. Chandra is Thai, and Ket as Thai name is not weird. If it is from Michael's culture, what would he be called? Caitlyn? Catherine?

So Ket remained. Khemmacat because it is another Thai names he recognized. The same for Thunyakorn. So he stitched them together, even if Thunyakorn is not a proper Thai surname.

So, perhaps, more importantly, Ket's human form would be based on Ohm Pawat, that played as Highlight in Blacklist. His main specialty would be his speed, resistance, and Lion's Charisma (some sort of shield).

Barong Macan's human form, would be that of Ohm Thitiwat. His main specialty would be detection (long-ranged and short-ranged), and mobility.

For Barong Bangkal, we'd need someone that's bulky and muscular. He'd be pure muscles.