# Kedudukan Kreditur Pemegang Jaminan Kebendaan Dalam Hukum Kepailitan.

## Wanprestasi
- Tidak memenuhi prestasinya sama sekali.
- Memenuhi prestasi tetapi tidak tepat waktu.
- Memenuhi prestasi tetapi tidak dengan baik.
- Melakukan sesuatu yang menurut perjanjian tidak boleh dilakukan.

## Tujuan dari UU Kepailitan yang baik
- Untuk memberikan perlindungan kepada kreditur jika debitur wanprestasi, sampai kreditur tersebut memperoleh akses terhadap harta kekayaan debitur yang dinyatakan pailit.
- Namun perlindungan tersebut tidak boleh sampai merugikan kepentingan debitur.
- Kesimpulan: Berlaku asas perlindungan yang seimbang bagi kreditur maupun debitur: Asas *Equality before the law* [menghasilkan]>> *Equality of treatment*.

## Hak Untuk Didahulukan
Dalam hukum jaminan menyatakan kreditur punya hak eksekutorial terhadap benda jaminan ketika debitur wanprestasi, bahkan kreditur yang mendapatkan hak istimewa, yaitu pemegang jaminan kebendaan > dalam UUK dan PKPU dikenal dengan kreditur separatis, hak mana dapat dipertahankan dengan tidak termasuk dalam harta pailit > perwujudan kreditur separatis didahulukan daripada kreditur lainnya.

Hak Separatis berlaku sejak debitur dinyatakan pailit berdasarkan putusan pengadilan.

## Dasar Hukum Hak Separatis
- Pasal 55 ayat (1) UUK&PKPU
    > Dengan tetap memperhatikan ketentuan sebagaimana dimaksud dalam Pasal 56, Pasal 57, dan Pasal 58, setiap Kreditor pemegang gadai, jaminan fidusia, hak tanggungan, hipotek, atau hak agunan atas kebendaan lainnya, dapat mengeksekusi haknya seolah-olah tidak terjadi kepailitan.
- Pasal 21 UUHT
    > Apabila pemberi Hak Tanggungan dinyatakan pailit, pemegang Hak Tanggungan tetap berwenang melakukan segala hak yang diperolehnya menurut ketentuan Undang-undang ini.

- Pasal 27 ayat (3) UUJF
    > Hak yang didahulukan dari Penerima Fidusia tidak hapus karena adanya kepailitan dan atau likuidasi Pemberi Fidusia.

## Inkonsistensi
Namun bagaimana dengan ketentuan:
- Pasal 56 ayat (1) UUK&PKPU
    > Hak eksekusi Kreditor sebagaimana dimaksud dalam Pasal 55 ayat (1) dan hak pihak ketiga untuk menuntut hartanya yang berada dalam penguasaan Debitor Pailit atau Kurator, ditangguhkan untuk jangka waktu paling lama 90 (sembilan puluh) hari sejak tanggal putusan pernyataan pailit diucapkan.
- Pasal 228 ayat (6) UUK&PKPU
    > Apabila penundaan kewajiban pembayaran utang tetap sebagaimana dimaksud pada ayat (4) disetujui, penundaan tersebut berikut perpanjangannya tidak boleh melebihi 270 (dua ratus tujuh puluh) hari setelah putusan penundaan kewajiban pembayaran utang sementara diucapkan.

Terdapat penangguhan hak esekusi dari kreditur separatis.

## "Kreditur Separatis" > pemisahan
- Kedudukannya terpisah dari kreditur yang lain.
- Kreditur separatis dapat menjual sendiri dan mengambil sendiri dari hasil penjualan yang terpisah dengan - boedel pailit.
- Hak separatis adalah hak yang diberikan oleh hukum kepada kreditur pemegang hak jaminan, bahwa barang jaminan yang dibebani dengan hak jaminan tidak termasuk harta pailit.
- **Catatan:** Hak jaminan dimaksud dalam hal jaminan kebendaan.
- Pasal 1134 BW: Hak Istimewa / *privilege*, piutang dengan hak istimewa menimbulkan hak untuk didahulukan / *hak preferen* dari kreditur-kreditur lainnya (para kreditur konkuren).

## Unsur-Unsur Hak Kreditur Separatis
1. Kreditur diberi hak secara *ex lege*;
1. Hak timbul dari hak jaminan kebendaan;
1. Kreditur memiliki hak jaminan kebendaan;
1. Kreditur punya hak preferen;
1. Debitur telah dinyatakan pailit.
1. Kreditur tidak terkena akibat debitur pailit;
1. Hak jaminan tidak termasuk boedel pailit;
1. Kreditur dapat melaksanakan eksekusi terhadap benda jaminan;
1. Kreditur punya kewenangan untuk menjual sendiri & menerima hasil penjualannya secara terpisah dari boedel pailit;
1. Wilayah pengadilan yang memiliki kompetensi adalah Pengadilan Niaga.

## Penundaan Eksekusi
- Sifatnya kasuistis, sehingga merupakan tindakan eksepsional.
- Pasal 195 ayat (1) HIR dan Pasal 224 HIR: Putusan *Inkracht van gewijke* berlaku ketentuan eksekutorial, **pelaksanaannya tidak dapat ditunda**.
- Penundaan eksekusi dapat dilakukan karena perdamaian, atau diberhentikan eksekusinya karena terdapat kesepakatan.
- *Derden Verzet* berdasarkan Pasal 195 ayat (6) HIR (tidak mutlak). Eksekusi hanya akan ditunda manakala pelawan dapat membuktikan dalil perlawanan, jika tidak bisa dibuktikan eksekusi akan tetap dijalankan.

## Lima Asas Dalam Eksekusi
1. Putusan hakim yang hendak dieksekusi haruslah putusan yang telah *Inkracht van gewijde*;
1. Putusan hakim yang akan dieksekusi harus bersifat *condemnatoire* (menghukum);
1. Putusan tersebut tidak dilaksanakan secara sukarela;
1. Eksekusi harus atas perintah dan dipimpin oleh kepala PN.
1. Eksekusi harus sesuai dengan amar putusan.

## Penangguhan eksekusi 90 hari (*vide* Pasal 56 ayat (1) UUK&PKPU)
Selama penangguhan 90 hari sejak putusan pailit ditetapkan, kurator dapat menggunakan/menjual boedel pailit dibawah pengawasan kurator dalam rangka kelangsungan usaha debitur, sepanjang diberikan "perlindungan yang wajar" bagi kepentingan kreditur atau pihak ketiga yang menuntut yang berada dalam pengawasan debitur pailit/kurator.

## Maksud Perlindungan Yang wajar
- Ganti rugi atas terjadinya penurunan harta pailit;
- Hasil penjualan bersih;
- Hak kebendaan pengganti;
- Imbalan yang wajar dan adil serta pembayaran tunai lainnya.

## Slide 18
Kreditur pemegang hak jaminan adalah pihak yang terkena kewajiban penangguhan eksekusi.
Padahal kreditur tersebut memiliki hak untuk dipisahkan (*separatis*) dan hak untuk diutamakan (*preferen*) dari para kreditur lain.

## Slide 19
Hak Eksekusi Kreditur Separatis:

- Dilaksanakan
    - Pasal 55 ayat (1) UUK&PKPU
    - Pasal 21 UUHT
    - Pasal 27 ayat (3) UUJF
- Ditangguhkan
    - Pasal 56 ayat (1) UUK&PKPU

## Kesimpulan
Penangguhan eksekusi sebagaimana ketentuan Pasal 56 ayat (1) UUK&PKPU sekalipun dimaksudkan untuk menyelamatkan boedel pailit adalah tidak dapat dibenarkan, karena disamping hukum jaminan terdapat prinsip dan ketentuan bahwa kreditur separatis diberikan hak preferen dibandingkan dengan kreditur konkuren juga tentang penangguhan eksekusi tersebut tidak diketahui sumber haknya datang darimana.
Selain itu penundaan eksekusi sebagaimana ketentuan Pasal 56 ayat (1) UUK&PKPU adalah telah menyimpangi UU karena penangguhannya tidak berdasarkan pada alasan perdamaian (*vide* ketentuan Pasal 195 HIR jo. Pasal 226 HIR).

## Kesimpulan (lanjutan)
Sehingga hak istimewa yang diberikan oleh Hukum Jaminan terhadap kreditur pemegang jaminan kebendaan tidak dapat dikalahkan oleh kreditur-kreditur lain, terlebih dalam UUHT maupun UUJF tidak terdapat aturan yang menyebutkan bahwa kreditur separatis tunduk pada UU yang lain.
Misalnya di dalam UU ditambahkan tulisan: "Apabila ada ketentuan lain akan diatur dalam peraturan / UU yang lain."
Maka secara mutlak pemegang HT hanya tunduk pada UUHT (lihat Pasal 21 UUHT).
Demikian dengan ketentuan Pasal 27 ayat (3) UUJF, tidak menyebutkan bahwa pemegang hak jaminan fidusia tunduk pada UU lain, maka pemberlakuan ketentuan tersebut adalah mutlak tanpa terpengaruh UU atau peraturan yang lain.

