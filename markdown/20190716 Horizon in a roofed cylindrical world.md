# (Counter)Rotationward Horizon in a roofed cylindrical world

![Problem](https://img.xenomancy.id/main/0904-problem.jpeg)

## Problem
Imagine two concentric circles of a shared central point ```c```, where the larger one has a radius of ```R```, and the smaller one has a radius of ```r```.
A line that we would call ```x``` intersect the surface of the smaller circle, tangential to the surface.
Naturally, the line intersect the larger circles at two points, let's call them ```a``` and ```b```.
Line ```x``` divides the larger circle's circumference ```2*pi*R``` into two, that is ```Q``` and ```q```.
As long as ```r!=0```, they observe ```2*pi*R = Q + q``` where ```Q``` > ```q```.

Now, consider that another line is intersecting point ```a``` tangential to the larger circle.
With that line intersecting at ```a``` represents the normal horizon of an observer standing at point ```a```, said observer is observing point ```b```.
The observer discovers that point ```b``` has an altitude of ```B``` degrees.

With only ```R``` and ```r``` are known, draw problem diagrams and construct equations to calculate:

- ```q```
- the length of ```x``` in between point ```a``` and point ```b```
- the angle of ```B``` in degrees

Provide your reasoning and steps in the answer sheets, of how you come up with the solutions.

## Solution 20200904
To know ```q``` we must also know that a circle is considered a full turn.
So to know ```q```, we must know the total circumference multiplied by "turns" of ```q```.
To know the turns, we must first know the angle that point ```a``` and ```b``` made against ```c```.

The angle at ```c``` could be obtained by considering ```a, b,``` and ```c``` as an equilateral triangle, with the height of ```r```, and the base length of ```x```.
Because the triangle is an equilateral triangle with ```c-a``` and ```c-b``` lengths are equal, we can split them into two identical right-angle triangles.
Examining either one of them gave us a right-angle triangle with height of ```r```, hypotenuse of ```R```, and base of ```w = x/2```.
Knowing the height and the hypotenuse, the base ```w``` can be solved with Pythagorean theorem, then ```x``` can be acquired by ```x = 2w```.

The next problem would be to discover the angle at ```c```.
Actually, the previous step to find ```x``` is not necessary to get the angle at ```c``` of the ```abc``` triangle, which we would call ```T```.
As discussed before, the ```abc``` triangle can be divided into two right angle triangles, the angle at ```c``` of each right-angle triangles, denoted as ```A``` (they're identical) would be ```T=2A```.
To get ```A```, it is quite trivial: ```cos A = r/R```, therefore ```sec (r/R) = A```.
As seen, to obtain ```A``` we just need ```r``` and ```R```.

The result is then converted to radians, and then to turns:

```
A[radians] = A[degrees] * pi / 180 [degrees]
A[turns] = A[radians] / 2 * pi
```

Remember that ```A``` is half the angle of the original ```abc``` triangle at corner ```c```, therefore ```T[turns] = 2A[turns]```.

The expanded equation would be:

```
q = 2 * R * sec (r/R) * pi / 180[degrees]
```

For the second question, considering ```x = 2w```, the length of ```x``` can be acquired by:

```
x = 2 * ( R^2 - r^2 )^0.5
```


For the third question, we must remember that the contact between ```R``` and the line at ```a``` is tangent to the surface of circle ```R```.
Line ```a``` would then form an angle with the line that is tangent to the surface of circle ```r``` and intersect circle ```R``` at ```a``` and ```b```.
Let's call this angle at point ```a``` between line ```a``` and line ```b``` as ```B```, per the problem's requirements.

Assuming a flat euclidian geometry, the sum of all angles inside the```abc``` triangle is ```180[degrees]```.
Knowing, that the angle at point ```a``` of the ```abc``` triangle can be acquired by knowing either ```A``` or ```T```.
Since we already know that ```A[degrees] = sec (r/R)```, and ```T = 2A```, we can get ```T = 2 * sec (r/R)```.
Being an equilateral triangle, ```abc``` triangle has 2 corners with identical angles we call ```G```, excluding ```T```.
Therefore, we get the following relationship: ```180[degrees] = T + 2G```.

Knowing that ```R``` connects to ```x``` at an angle, but ```a``` is tangent to ```R```, the angle between ```R``` and line at ```a``` is ```90[degrees]```.
Therefore, ```G + B = 90[degrees]```, so ```B = 90[degrees] - G```.
As we know from the previous paragraph:

```
G = (180[degrees] - T) / 2
  = (180[degrees] - 2 * sec (r/R)) / 2```
```

Therefore:

```
B[degrees] = 90[degrees] - ((180[degrees] - 2 * sec (r/R) / 2)
```

#### Answer:
- ```q = 2 * R * sec (r/R) * pi / 180[degrees]```
- length of ```x = 2 * ( R^2 - r^2 )^0.5```
- ```B[degrees] = 90[degrees] - ((180[degrees] - 2 * sec (r/R) / 2)```
