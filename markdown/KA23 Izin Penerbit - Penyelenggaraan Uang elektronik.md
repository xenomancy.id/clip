# Alur Pemrosesan Permohonan Izin Sebagai Penerbit

SE BI No. 16/11/DKSP tanggal 22 Juli 2014 jo. SE BI No. 18/21/DKSP tanggal 27 September 2016, bagian III.A. Pemrosesan Permohonan Izin sebagai Penerbit.

1. Permohonan yang diterima akan diproses sebagai berikut:
   - Pemeriksaan Administratif, meliputi
     - Pemeriksaan kelengkapan dokumen; dan
     - Pemeriksaan kesesuaian dokumen.

     Jika dokumen lengkap, dilakukan pemeriksaan kesesuaian dokumen.
     Jika dokumen belum lengkap, BI mengembalikan dokumen ke Pemohon.
   - Pemeriksaan lapangan (*on site visit*) untuk verifikasi kebenaran  dan kesesuaian dokumen, sekaligus untuk memastikan kesiapan operasional.

2. Jika dari pemeriksaan kesesuaian dokumen ditemukan dokumen yang tidak sesuai, pemohon harus memberikan dokumen yang telah disesuaikan dalam jangka waktu 90 hari kalender sejak tanggal surat pemberitahuan pertama kali dari BI mengenai ketidaksesuai persyaratan dokumen.
Jika lewat jangka waktu 90 hari kalender dan dokumen yang telah disesuaikan belum diterima oleh BI, BI menolak permohonan izin.

3. Jika permohonan izin ditolak sebagaimana dimaksud pada poin 2, maka permohonan izin berikutnya dapat diajukan kembali setelah 180 hari kalender sejak tanggal ditolaknya izin.

4. Jika dokumen permohonan dinyatakan lengkap dan sesuai persyaratan, BI melakukan pemeriksaan lapangan (*on site visit*).

5. Berdasarkan hasil pemeriksaan administratif dan pemeriksaan lapangan (*on site visit*), BI dapat menyetujui atau menolak permohonan izin secara tertulis.

6. Lembaga Selain Bank yang menyelenggarakan kegiatan uang elektronik dengan dana *float* di bawah Rp. 1.000.000.000,00 (satu milyar rupiah) mengajukan izin ke BI, lembaga tersebut tetap dapat menjalankan kegiatannya selama dalam proses pengajuan izin, selama tidak menambah dana *float*.

# Uji Coba Penyelenggaraan Uang Elektronik

1. Calon Penerbit yang sedang dalam proses perizinan dapat melakukan uji coba dalam rangka menguji kesiapan penyelenggaraan uang elektronik.
Uji coba dilakukan terbatas dalam lingkungan internal calon penerbit.

1. Dalam pelaksanaan uji coba tersebut, calon penerbit harus menyampaikan laporan ke BI mengenai rencana pelaksanaan dan pengakhiran uji coba dengan ketentuan:
   - laporan rencana pelaksanaan uji coba disampaikan ke BI paling lambat 30 hari kalender sebelum pelaksanaan uji coba; dan
   - laporan pengakhiran uji coba disampaikan ke BI paling lambat 10 hari kalender setelah tanggal uji coba berakhir.

1. Penerbit atau calon penerbit yang akan menyelenggarakan kegiatan Layanan Keuangan Digital dapat melakukan uji coba dengan ketentuan sebagaimana diatur dalam ketentuan BI yang mengatur mengenai layanan keuangan digital.
