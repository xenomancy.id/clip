| Nama | NIM | Alamat | HP | Email | SKS Kumulatif | IPK | Jenis | Judul |
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| William Setiawan Marthianus | 031814253068 | Manyar Tirtomoyo 7 Nomor 16 | 081333002951 | willymarthianus@gmail.com | 147 | 3.42 | Ujian Tesis | Pemungutan Pajak Penghasilan Terhadap Pengemudi Jasa Angkutan Melalui Aplikasi Online |
| Bram Adi Kusuma | 031814253004 | Jalan Mojo 3 A Nomor 1, Gubeng, Surabaya, Jawa Timur | 082220267548 | bramabaru20@gmail.com | 41 | 3.09 | Ujian Tesis
Keabsahan Akta Perjanjian Kredit Terkait Penandatanganan Para Pihak Yang Tidak Bersamaan |
| Rico Andrian Hartono | 031814253076 | Permata Sukodono Raya E4-30, Sidoarjo | 081217881730 | ricoandrianh@gmail.com | 39 | 3.17 | Ujian Proposal Tesis
Prinsip Ganti Kerugian Dalam Pengadaan Tanah Untuk Kepentingan Umum Berdasarkan Undang-Undang Nomor 2 Tahun 2012 |
| Fadiyah Ramadhani Putri | 031814253053 | Jl. Suroto 1 Komplek Kenjeran | 082233610266 | fadiyahrama@gmail.com | 41 | 3.45 | Ujian Tesis | Kebijakan Pemerintah Dalam Pengenaan Pajak Penghasilan Pelaku Usaha Asing Game Online |
| Rico Andrian Hartono  | | | | rico.andrian.hartono-2018@fh.unair.ac.id | 41 | 3.18 | Ujian Tesis | Prinsip Keadilan Dalam Ganti Kerugian Pada Pengadaan Tanah Untuk Kepentingan Umum Berdasarkan Undang-Undang Nomor 2 Tahun 2012 |
