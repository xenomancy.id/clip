# Peraturan Relevan Mengenai Jangkauan *Legal Due Diligence* berdasarkan PBI No. 20/6/PBI/2018 tentang Uang Elektronik

## Pasal 4
1. Setiap pihak yang bertindak sebagai Penyelenggara wajib terlebih dahulu memperoleh izin dari Bank Indonesia.
1. Kewajiban sebagaimana dimaksud pada ayat (1) dikecualikan bagi pihak yang bertindak sebagai Penyelenggara berupa Penerbit Uang Elektronik *closed loop* dengan jumlah Dana *Float* kurang dari Rp1.000.000.000,00 (satu milyar rupiah).

   > **Penjelasan:** Dalam hal terdapat pihak yang menyelenggarakan lebih dari 1 (satu) jenis atau produk Uang Elektronik *closed loop* maka jumlah Dana *Float* diperhitungkan dari seluruh Uang Elektronik closed loop yang diselenggarakan oleh pihak tersebut.

   > Contoh 1:

   > PT A menyelenggarakan 2 (dua) produk Uang Elektronik *closed
loop* yaitu:

   > - Uang Elektronik *closed loop* X yang hanya digunakan di
lokasi B; dan
   > - Uang Elektronik *closed loop* Y yang hanya digunakan di
lokasi C.

   > Dengan demikian, jumlah Dana *Float* diperhitungkan dari
penyelenggaraan Uang Elektronik *closed loop* X dan Uang
Elektronik *closed loop* Y.

   > Contoh 2:

   > PT W menyelenggarakan 2 (dua) produk Uang Elektronik *closed
loop* yaitu:

   > - Uang Elektronik *closed loop* A yang menggunakan media
penyimpan *chip based*; dan
   > - Uang Elektronik *closed loop* B yang menggunakan media
penyimpan *server based*,

   > Dengan demikian, jumlah Dana *Float* diperhitungkan dari
penyelenggaraan Uang Elektronik *closed loop* A dan Uang
Elektronik *closed loop* B.

1. Pihak yang mengajukan permohonan izin untuk menjadi Penyelenggara harus memenuhi persyaratan:
   - umum; dan
   - aspek kelayakan.

## Pasal 13
1. Persyaratan aspek kelayakan sebagaimana dimaksud
dalam Pasal 4 ayat (3) huruf b meliputi aspek:
   - kelembagaan dan hukum;
   - kelayakan bisnis dan kesiapan operasional; dan
   - tata kelola, risiko, dan pengendalian.
1. Persyaratan aspek kelembagaan dan hukum sebagaimana dimaksud pada ayat (1) huruf a paling sedikit berupa:
   - legalitas dan profil perusahaan; dan

       > **Penjelasan:** Legalitas dan profil perusahaan antara lain dibuktikan dengan dokumen profil perusahaan, anggaran dasar perusahaan berikut seluruh perubahannya, izin kegiatan usaha yang telah dimiliki, tanda daftar perusahaan, dan izin atau persetujuan dari otoritas terkait, apabila ada, termasuk informasi mengenai profil masing-masing anggota direksi dan anggota dewan komisaris berupa nama, alamat, riwayat hidup, pengalaman, dan kualifikasi beserta buktinya.
   - kesiapan perangkat hukum untuk penyelenggaraan Uang Elektronik.

     > **Penjelasan:** Kesiapan perangkat hukum untuk penyelenggaraan Uang
Elektronik antara lain dibuktikan dengan konsep perjanjian
tertulis atau pokok perjanjian tertulis antara pihak yang
akan mengajukan permohonan sebagai Penyelenggara
dengan pihak lain.

1. Persyaratan aspek kelayakan bisnis dan kesiapan operasional sebagaimana dimaksud pada ayat (1) huruf b paling sedikit berupa:
   - analisis kelayakan bisnis;

     > **Penjelasan:** Analisis kelayakan bisnis antara lain berupa hasil analisis bisnis yang paling sedikit memuat informasi mengenai model dan rencana bisnis, target pasar, jenis dan layanan Uang Elektronik yang akan diselenggarakan, dan struktur harga dan biaya yang akan diterapkan serta rencana pengembangan usaha ke depan.
   - kesiapan operasional, sistem, dan teknologi informasi yang akan digunakan;

     > **Penjelasan:** Kesiapan operasional, sistem, dan teknologi informasi yang akan digunakan antara lain berupa dokumen rencana peralatan dan sarana usaha serta lokasi atau ruangan yang akan digunakan untuk kegiatan operasional, peralatan teknis terkait sistem baik *hardware* maupun *software* serta jaringan yang akan digunakan, dan hasil uji coba (*user acceptance test*) atas Uang Elektronik yang akan diselenggarakan. 
   - kinerja keuangan; dan

     > **Penjelasan:** Kinerja keuangan antara lain dibuktikan dengan laporan keuangan tahunan atau neraca keuangan.
   - kesiapan struktur organisasi dan sumber daya manusia.

     > **Penjelasan:** Kesiapan struktur organisasi dan sumber daya manusia antara lain berupa rencana struktur organisasi dan kesiapan sumber daya manusia.
1. Persyaratan aspek tata kelola, risiko, dan pengendalian sebagaimana dimaksud pada ayat (1) huruf c paling sedikit berupa:
   - bagi Penerbit:
     1. kebijakan dan prosedur penerapan manajemen risiko;

         > **Penjelasan:** Konsep penerapan manajemen risiko antara lain berupa bukti kesiapan penerapan manajemen risiko yang paling sedikit berupa risiko operasional, risiko hukum, risiko setelmen, risiko likuiditas, *risiko market conduct*, dan risiko reputasi.
     2. kebijakan dan prosedur penerapan anti pencucian uang dan pencegahan pendanaan terorisme;
     3. kebijakan dan prosedur penerapan perlindungan konsumen; dan
     4. kebijakan dan prosedur penerapan keamanan sistem informasi; dan
   - bagi *Acquirer*, Prinsipal, Penyelenggara *Switching*, Penyelenggara Kliring, dan Penyelenggara Penyelesaian Akhir:
     1. kebijakan dan prosedur penerapan manajemen risiko; dan

         > **Penjelasan:** Konsep penerapan manajemen risiko antara lain berupa bukti kesiapan penerapan manajemen risiko yang paling sedikit berupa risiko operasional, risiko hukum, risiko setelmen, risiko likuiditas, *risiko market conduct*, dan risiko reputasi. 
     2. kebijakan dan prosedur penerapan keamanan sistem informasi.

## Pasal 14
1. Selain pemenuhan persyaratan aspek kelayakan sebagaimana dimaksud dalam Pasal 13, Bank atau Lembaga Selain Bank yang mengajukan permohonan izin sebagai Penyelenggara harus menyampaikan pernyataan dan jaminan (*representation and warranties*) secara tertulis kepada Bank Indonesia.
1. Pernyataan dan jaminan (*representation and warranties*) sebagaimana dimaksud pada ayat (1) paling sedikit memuat:
   - Bank atau Lembaga Selain Bank telah didirikan secara patut dan sah berdasarkan hukum Negara Kesatuan Republik Indonesia;
   - Bank atau Lembaga Selain Bank tidak dalam kondisi wanprestasi (*default*), tidak dalam pengenaan sanksi oleh otoritas terkait, dan/atau tidak terlibat dalam perkara pidana atau perdata, yang dapat berpengaruh secara material terhadap kelangsungan usaha Bank atau Lembaga Selain Bank;
   - tidak terdapat permohonan kepailitan atau penundaan kewajiban pembayaran utang terhadap Bank atau Lembaga Selain Bank di pengadilan niaga yang berwenang di Indonesia; dan
   - Bank atau Lembaga Selain Bank menjamin untuk:
     1. memenuhi ketentuan peraturan perundang-undangan, baik atas kegiatan yang dilakukan sendiri atau bersama-sama dengan pihak terafiliasi;

          > **Penjelasan:** Termasuk pihak terafiliasi antara lain pihak lain yang memiliki hubungan keuangan atau hubungan kepemilikan dengan Penyelenggara.

     2. menjaga kesehatan kondisi keuangan yang diindikasikan dengan kondisi likuiditas, profitabilitas, dan solvabilitas yang baik;
     3. menyelenggarakan kegiatan Uang Elektronik dengan model bisnis yang memberikan manfaat bagi perekonomian Indonesia;
     4. tidak memindahkan lokasi kantor pusat di Indonesia ke negara lain serta memastikan kantor pusat tersebut memiliki kewenangan penuh untuk mengambil keputusan atas penyelenggaraan kegiatan Uang Elektronik di Indonesia; dan
     5. memastikan terpeliharanya pemenuhan isi surat pernyataan dan jaminan sepanjang penyelenggaraan Uang Elektronik.
1. Surat pernyataan dan jaminan sebagaimana dimaksud pada ayat (1) diajukan dan ditandatangani oleh direksi yang berwenang mewakili Bank atau Lembaga Selain Bank serta harus disertai dengan pernyataan dari konsultan hukum yang independen dan profesional berdasarkan hasil uji tuntas dari segi hukum (*legal due diligence*).
> **Penjelasan:** Yang dimaksud dengan “konsultan hukum yang independen dan profesional” adalah pihak yang secara khusus menyediakan jasa konsultasi hukum dan merupakan entitas yang terpisah dari Penyelenggara.

## Pasal 16
Penyelenggara yang telah memperoleh izin dan akan melakukan:

- pengembangan produk dan/atau aktivitas Uang Elektronik; dan/atau
- kerja sama dengan pihak lain,

wajib terlebih dahulu memperoleh persetujuan dari Bank Indonesia.

## Pasal 17
1. Persetujuan untuk pengembangan produk dan aktivitas Uang Elektronik sebagaimana dimaksud dalam Pasal 16 huruf a meliputi pengembangan fitur, jenis, layanan, dan/atau fasilitas dari Uang Elektronik yang telah berjalan.

   > **Penjelasan:** Termasuk dalam pengembangan produk dan aktivitas Uang
Elektronik antara lain:

   > - pengembangan jenis produk Uang Elektronik;
   > - pengembangan mekanisme autentikasi Uang Elektronik dan
otorisasi transaksi Uang Elektronik;
   > - penambahan fitur pada Uang Elektronik;
   > - pengembangan infrastruktur dan standar keamanan;
   > - penyediaan layanan Pengisian Ulang (*Top Up*) kepada
Pengguna melalui Penyedia Barang dan/atau Jasa; dan/atau
   > - pengembangan fitur, jenis, layanan, dan/atau fasilitas produk Uang Elektronik lainnya yang berkaitan dengan inovasi layanan dan teknologi yang berpengaruh terhadap eksposur risiko secara signifikan.
1. Persetujuan untuk melakukan kerja sama sebagaimana dimaksud dalam Pasal 16 huruf b meliputi:
   - kerja sama dengan Penyelenggara dan/atau Penyelenggara Jasa Sistem Pembayaran lain;
   - kerja sama dengan Penyelenggara Penunjang; dan/atau
   - kerja sama dengan pihak lainnya.

     > **Penjelasan:** Kerja sama dengan pihak lainnya antara lain kerja sama dengan pihak yang menerbitkan Uang Elektronik di luar wilayah Negara Kesatuan Republik Indonesia.

1. Kerja sama sebagaimana dimaksud pada ayat (2) huruf a dan huruf c hanya dapat dilakukan dengan Penyelenggara dan/atau Penyelenggara Jasa Sistem Pembayaran yang telah memperoleh izin.

   > **Penjelasan:** Yang dimaksud dengan “izin” adalah:

   > - izin dari Bank Indonesia untuk Penyelenggara dan/atau Penyelenggara Jasa Sistem Pembayaran yang menyelenggarakan kegiatan dan berkedudukan hukum di Indonesia; atau
   > - izin dari otoritas negara setempat untuk penyelenggara dan/atau penyelenggara jasa sistem pembayaran asing.


## Pasal 18
1. Pemberian persetujuan kepada Penyelenggara untuk pengembangan produk dan/atau aktivitas Uang Elektronik sebagaimana dimaksud dalam Pasal 17 ayat (1) mempertimbangkan pemenuhan persyaratan yang meliputi aspek:
   - kesiapan operasional;

      > **Penjelasan:** Aspek kesiapan operasional antara lain dibuktikan dengan:

      > 1. rekomendasi atau persetujuan dari otoritas terkait atas
rencana pengembangan produk dan/atau aktivitas
Uang Elektronik yang akan dilakukan; dan
      > 2. informasi umum mengenai pengembangan produk
dan/atau aktivitas Uang Elektronik antara lain berisi
penjelasan mengenai pengembangan yang akan
diselenggarakan, potensi pasar, rencana kerja sama,
rencana wilayah penyelenggaraan, struktur biaya
layanan, dan target pendapatan yang akan dicapai.

      > Rekomendasi atau persetujuan dari otoritas terkait
diberlakukan dalam hal terdapat otoritas terkait yang 
berwenang untuk mengawasi dan memberikan rekomendasi
atau persetujuan.
   - keamanan dan keandalan sistem;

     > **Penjelasan:** Aspek keamanan dan keandalan sistem antara lain
dibuktikan dengan laporan hasil audit sistem informasi dari
auditor independen internal atau eksternal, prosedur
pengendalian pengamanan (*security control*), dan hasil
asesmen atas pengembangan produk dan/atau aktivitas
Uang Elektronik yang akan diselenggarakan.
   - penerapan manajemen risiko; dan

     > **Penjelasan:** Aspek penerapan manajemen risiko antara lain dibuktikan
dengan hasil asesmen terhadap manajemen risiko yang telah
diselenggarakan serta rencana penyesuaian kebijakan dan
prosedur manajemen risiko atas pengembangan produk
dan/atau aktivitas Uang Elektronik yang akan
diselenggarakan.
   - perlindungan konsumen.
1. Selain pemenuhan aspek sebagaimana dimaksud pada ayat (1), Bank Indonesia juga mempertimbangkan hasil
pengawasan terhadap kinerja Penyelenggara.

   > **Penjelasan:** Kinerja Penyelenggara antara lain dibuktikan dengan:

   > - kepatuhan terhadap ketentuan peraturan perundangundangan
dan/atau kebijakan Bank Indonesia di bidang
sistem pembayaran atau yang berkaitan dengan sistem
pembayaran. Khusus untuk Bank antara lain berkaitan juga
dengan kepesertaan dalam Bank Indonesia-Real Time Gross
Settlement, Sistem Kliring Nasional Bank Indonesia,
dan/atau Bank Indonesia-Scripless Security Settlement
System;
   > - penerapan manajemen risiko antara lain risiko operasional
dan risiko setelmen;
   > - penerapan perlindungan konsumen antara lain penanganan
dan penyelesaian pengaduan Pengguna;
   > - kinerja finansial; dan/atau
   > - tata kelola yang baik dalam penyelenggaraan kegiatan Uang
Elektronik.

## Pasal 19
1. Pemberian persetujuan kepada Penyelenggara untuk
kerja sama sebagaimana dimaksud dalam Pasal 17 ayat
(2) mempertimbangkan pemenuhan persyaratan yang
meliputi aspek:
   - legalitas dan profil pihak yang akan diajak bekerja
sama;

     > **Penjelasan:** Aspek legalitas dan profil pihak yang akan diajak bekerja
sama antara lain dibuktikan dengan dokumen profil
perusahaan, anggaran dasar perusahaan berikut seluruh
perubahannya, izin kegiatan usaha yang telah dimiliki,
tanda daftar perusahaan, dan izin atau persetujuan dari
otoritas terkait apabila ada.
   - kompetensi pihak yang akan diajak bekerja sama;

     > **Penjelasan:** Aspek kompetensi pihak yang akan diajak bekerja sama
antara lain dibuktikan dengan kecukupan sumber daya
manusia, rekam jejak pengurus dan pengalaman dalam
menyelenggarakan kegiatan Uang Elektronik, kegiatan jasa
sistem pembayaran, dan/atau kegiatan jasa penunjang.
   - kinerja pihak yang akan diajak bekerja sama;

     > **Penjelasan:** Aspek kinerja meliputi kinerja finansial dan kinerja
operasional yang antara lain dibuktikan dengan laporan
keuangan pihak yang akan diajak bekerja sama, rekam
jejak Penyelenggara, Penyelenggara Jasa Sistem
Pembayaran, dan/atau Penyelenggara Penunjang, dan/atau
hasil uji coba sistem.
   - keamanan dan keandalan sistem serta infrastruktur; dan

     > **Penjelasan:** Aspek keamanan dan keandalan sistem serta infrastruktur
antara lain dibuktikan dengan pemenuhan standar terkait
keamanan sistem dan infrastruktur yang digunakan sesuai
dengan standar nasional, internasional, atau yang berlaku
umum di industri serta keamanan dan kerahasiaan data
   - hukum.

     > **Penjelasan:** Aspek hukum dibuktikan antara lain dengan kejelasan
ruang lingkup kerja sama dan hak serta kewajiban masingmasing
pihak, rencana pelaksanaan, dan jangka waktu
kerja sama.

1. Selain pemenuhan aspek sebagaimana dimaksud pada
ayat (1), Bank Indonesia juga mempertimbangkan hasil
pengawasan terhadap kinerja Penyelenggara.

   > **Penjelasan:** Kinerja Penyelenggara antara lain dibuktikan dengan:

   > - kepatuhan terhadap ketentuan peraturan perundangundangan
dan/atau kebijakan Bank Indonesia di bidang 
sistem pembayaran atau yang berkaitan dengan sistem
pembayaran. Khusus untuk Bank antara lain berkaitan juga
dengan kepesertaan dalam Bank Indonesia-Real Time Gross
Settlement, Sistem Kliring Nasional Bank Indonesia,
dan/atau Bank Indonesia-Scripless Security Settlement
System;
   > - penerapan manajemen risiko antara lain risiko operasional
dan risiko setelmen;
   > - penerapan perlindungan konsumen antara lain penanganan
dan penyelesaian pengaduan Pengguna;
   > - kinerja finansial; dan/atau
   > - tata kelola yang baik dalam penyelenggaraan kegiatan Uang
Elektronik.


## Pasal 21
1. Bank atau Lembaga Selain Bank yang mengajukan permohonan:
   - izin sebagai Penyelenggara sebagaimana dimaksud dalam Pasal 4 ayat (1); atau
   - persetujuan untuk pengembangan produk dan aktivitas Uang Elektronik dan kerja sama sebagaimana dimaksud dalam Pasal 16,

   harus menyampaikan permohonan secara tertulis dalam bahasa Indonesia kepada Bank Indonesia.

1. Permohonan sebagaimana dimaksud pada ayat (1) disertai dengan dokumen pendukung pemenuhan aspek sebagaimana dimaksud dalam Pasal 13, Pasal 18, dan Pasal 19.
> **Penjelasan:** Dokumen pendukung mencakup dokumen pemenuhan aspek kelayakan sebagai Penyelenggara.
1. Selain dokumen pendukung sebagaimana dimaksud pada ayat (2), permohonan izin sebagaimana dimaksud pada ayat (1) huruf a juga disertai dengan surat pernyataan dan jaminan (*representations and warranties*) sebagaimana dimaksud dalam Pasal 14.
> **Penjelasan:** Surat pernyataan dan jaminan (*representations and warranties*) disertai dengan pernyataan dari konsultan hukum yang independen dan profesional berdasarkan hasil uji tuntas dari segi hukum (*legal due diligence*).
