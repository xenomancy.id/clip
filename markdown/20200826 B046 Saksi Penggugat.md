No. 104/MST/XI/2019

Tarakan, 4 November 2019

Kepada Yth,

**Ketua Pengadilan Negeri Tarakan**

*cq.* Majelis Hakim dalam Perkara

Nomor: 25/Pdt.G/2019/PN.Tar.

Pengadilan Negeri Tarakan

Jl. Diponegoro No. 99

TARAKAN

**Perihal: Kesimpulan Atas Gugatan Perbuatan Melawan Hukum No. 25/Pdt.G/2019/PN.Tar.

---

->**Antara:**<-

1. **H binti ABP**, selanjutnya disebut sebagai **Penggugat I Konvensi / Tergugat I Rekonvensi**;
1. **AI binti ABP**, selanjutnya disebut sebagai **Penggugat II Konvensi / Tergugat II Rekonvensi**;
1. **AS binti ABP**, selanjutnya disebut sebagai **Penggugat III Konvensi / Tergugat III Rekonvensi**;
1. **N binti ABP**, selanjutnya disebut sebagai **Penggugat IV Konvensi / Tergugat IV Rekonvensi**;
1. **DR binti ABP**, selanjutnya disebut sebagai **Penggugat V Konvensi / Tergugat V Rekonvensi**;

Atau secara bersama-sama disebut sebagai **Para Penggugat Konvensi / Para Tergugat Rekonvensi.**

->***Melawan:***<-

**GK** selanjutnya disebut sebagai **Tergugat Konvensi / Penggugat Rekonvensi.**

===

Dengan Hormat,

Untuk dan atas nama TKPR, dengan ini menyampaikan Kesimpulan sebagai berikut:

1. Bahwa, pada tanggal 6 Agustus 2019 PPK telah mengajukan Gugatan Perbuatan Melawan Hukum, yang didaftarkan pada Pengadilan Negeri Tarakan, pada tanggal 12 Agustus 2019, dengan nomor registrasi perkara: B043;

1. Bahwa, atas gugatan tersebut telah dilakukan proses mediasi oleh mediator yang ditunjuk oleh PNT, dan Mediasi tersebut dinyatakan gagal pada tanggal 2 September 2019;

1. Bahwa, Jawaban dengan disertai Gugatan Rekonvensi dari TKPR telah diserahkan di hadapan persidangan pada tanggal 23 September 2019;

1. Bahwa, berdasarkan pada gugatan PPK, TKPR telah mengajukan Eksepsi yang dimuat dalam Jawaban tertanggal 23 September 2019 dan *Duplik* tertanggal 7 Oktober 2019, yang terhadapnya juga telah mematahkan tangkisan terhadap 

---

Abbreviation:

- PNT: Pengadilan Negeri Tarakan
- B043: 25/Pdt.G/2019/PN.Tar.
- TKPR: **Tergugat Konvensi / Penggugat Rekonvensi.**
- PK-I: **Penggugat I Konvensi / Tergugat I Rekonvensi**
- PK-II: **Penggugat II Konvensi / Tergugat II Rekonvensi**
- PK-III: **Penggugat III Konvensi / Tergugat III Rekonvensi**
- PK-IV: **Penggugat IV Konvensi / Tergugat IV Rekonvensi**
- PK-V: **Penggugat V Konvensi / Tergugat V Rekonvensi**
- PPK: **Para Penggugat Konvensi / Para Tergugat Rekonvensi.**